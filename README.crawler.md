Installation instructions for Piperka crawler
=============================================

I'm using Debian for all my systems.  YMMV if you use some other
environment.  There's a number of dependencies required, I've listed
them along with the parts that require them instead of placing them
all on one line.

```
sudo apt install ghc cabal-install zlib1g-dev libpq-dev libicu-dev
```

```
cabal sandbox init
cabal configure --enable-tests
cabal install --only-dependencies --enable-tests
cabal build
```

Perl dependencies
-----------------

The parser is written in perl and it needs its own dependencies
installed.

```
sudo apt install liblinux-prctl-perl libjson-perl libxml-feed-perl
```

Database
--------

The crawler uses a PostgreSQL database.  To set it up for the test
suite, create a local database named `crawler-test` and make sure that
the user you're running the tests as has access to it.

```
sudo apt install postgresql
sudo -u postgres createuser $(whoami)
sudo -u postgres createddb crawler-test -O $(whoami)
```

Using and testing the crawler
-----------------------------

The crawler still needs a couple of more dependencies:

```
sudo apt install curl memcached
```

Use `export PERLLIB=lib/crawler` and run with `cabal run
crawler-test`.  Using the repl is often useful when running the test
suite.  Start it with `cabal repl crawler-test`.

Some tests need locale set so consider `export LC_CTYPE=en_US.UTF-8`
or similar.
