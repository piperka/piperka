{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Piperka.Listing.Header.Splices
  ( listingHeaderSplices
  , listingHeaderAttrSplices
  ) where

import Control.Monad
import Control.Monad.Trans
import Data.Maybe
import Data.Map.Syntax
import Data.Text (Text)
import Data.Text.Encoding (decodeLatin1, encodeUtf8)
import Data.Vector (Vector)
import Heist
import qualified HTMLEntities.Text as HTML
import Snap
import qualified Text.XmlHtml as X

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Listing.Header.Query
import Piperka.Listing.Types
import Piperka.Profile.Types
import Piperka.Update.Types (hiddenCount)
import Piperka.Util

commonHeaderSplices
  :: ListingMode
  -> Splices (RuntimeSplice AppHandler ListingParam -> Splice AppHandler)
commonHeaderSplices mode = do
  "hilightButton" ## const $ callTemplate "_hilightButton"
  "sortOptions" ## const $ do
    let linkSplice = do
          xs <- X.childNodes <$> getParamNode
          typ <- fromJust . X.getAttribute "type" <$> getParamNode
          let nod = X.Element "a" [("href", mkLink typ)] xs
          return $ yieldPure $ htmlNodeSplice id [nod]
    let splices = do
          "isProfile" ## return $ mempty
          "lnk" ## linkSplice
    withLocalSplices splices mempty $ callTemplate "_sortOptions"
  "qSearch" ## const $ do
    let sortSplice = case mode of
          Top -> return $ yieldPureText "top"
          _ -> return $ yieldRuntimeText $
               lift $ maybe "name" (HTML.text . decodeLatin1) <$>
               getQueryParam "sort"
    withLocalSplices (do
                         "sort" ## sortSplice
                         "id" ## return (yieldPureText "quicksearch"))
      mempty $ callTemplate "_qSearch"
  "alphabetIndex" ## const $ eitherDefer stdSqlErrorSplice
    (withSplices (callTemplate "_alphabetIndex") alphabetIndexSplices) $
    lift alphabetIndex
  where
    mkLink nam = uncurry encodePathToText $
                 (\(p, q) -> (p, ("sort", Just $ encodeUtf8 nam):q)) $
                 getListingPathQuery mode undefined

-- Profile splices
listingHeaderSplices
  :: ListingMode
  -> Splices (RuntimeSplice AppHandler ListingParam -> Splice AppHandler)
listingHeaderSplices Profile = commonHeaderSplices Profile <> profileSplices
listingHeaderSplices Update = commonHeaderSplices Update <> updateSplices
listingHeaderSplices mode = commonHeaderSplices mode

profileSplices :: Splices (RuntimeSplice AppHandler ListingParam -> Splice AppHandler)
profileSplices = mapV (. fmap getProfile) $ do
  "profileName" ## pureSplice . textSplice $ HTML.text . name . profile
  "profileEsc" ## pureSplice $ urlEncodeBuilder . encodeUtf8 . name . profile
  "isPrivate" ## isPrivate
  "isMine" ## isMine
  "mayAllowFollow" ## mayAllowFollow
  "requesting" ## requesting
  "mayInterest" ## mayInterest
  "publicFollow" ## publicFollow
  "havePermission" ## checkedSplice . fmap (perm . profile)
  "yourOrProfileName" ## yourOrProfileName
  "youOrThisUser" ## youOrThisUser
  "grandTotal" ## pureSplice . textSplice $ intToText . total . profile
  "nComics" ## pureSplice . textSplice $ intToText . inComics . profile
  "writeUp" ## pureSplice . textSplice $ maybe "" id . writeup . profile
  "profileSortOptions" ## sortOptions
  where
    isPrivate = checkedSplice . fmap
      (\p -> case p of
               Own _ -> (privacy $ profile p) == Private
               _ -> False)
    isMine = deferMany (withSplices runChildren isMineSplices) . fmap
      (\case Own x -> Just x
             _ -> Nothing)
    isMineSplices = mapV (pureSplice . textSplice) $ do
      "numFollowers" ## (\num -> (if num == 0 then "no" else intToText num) <>
                                 " " <> plural "follower" "followers" num) . followers
      "numFollowees" ## (\num -> if num == 0 then "nobody"
                                 else (intToText num) <>
                                      " " <> plural "user" "users" num) . followees
    mayAllowFollow = deferMany
      (\n -> withLocalSplices mempty (checkedAttrSplice $ myPerm <$> n) runChildren) . fmap
      (\case
          Other x -> case myPrivacy x of
                       Friends -> Just x
                       _ -> Nothing
          _ -> Nothing)
    requesting =
      deferMany (withSplices runChildren requestingSplices) . fmap
      (\p -> case p of Other x ->
                         guard (not $ perm $ profile p) >> Just x
                       _ -> Nothing)
    requestingSplices = do
      "viewerIsPrivate" ## checkedSplice . fmap titfortat
      "interest" ## checkedSplice . fmap (\p -> (not $ titfortat p) && interest p)
    mayInterest =
      checkedSplice . fmap (\case Common x -> not $ perm x
                                  Other x -> not $ perm $ profile x
                                  _ -> False)
    publicFollow = deferMany
      (\n -> withLocalSplices mempty (checkedAttrSplice $ interest <$> n) runChildren) . fmap
      (\case
          Other x -> guard (titfortat x || (perm $ profile x)) >> Just x
          _ -> Nothing)
    yourOrProfileName n = return $ yieldRuntimeText $ do
      p <- n
      return $ case p of Own _ -> "your comics"
                         _ -> name $ profile p
    youOrThisUser n = return $ yieldRuntimeText $ do
      p <- n
      return $ case p of Own _ -> "you"
                         _ -> "this user"
    sortOptions n = do
      let linkSplice = do
            typ <- fromJust . X.getAttribute "type" <$> getParamNode
            xs <- X.childNodes <$> getParamNode
            return $ yieldRuntime $ do
              nam <- name . profile <$> n
              let link = encodePathToText ["profile.html"]
                         [ ("name", Just $ encodeUtf8 nam)
                         , ("sort", Just $ encodeUtf8 typ)]
              let nod = X.Element "a" [("href", link)] xs
              return $ htmlNodeSplice id [nod]
      let splices = do
            "isProfile" ## runChildren
            "lnk" ## linkSplice
      withLocalSplices splices mempty $ callTemplate "_sortOptions"

alphabetIndexSplices
  :: Splices (RuntimeAppHandler (Vector (Text, Int)))
alphabetIndexSplices = "jumpTo" ## manyWith runChildren
  ("letter" ## pureSplice . textSplice $ fst)
  ("href" ## \n _ -> n >>= \n' -> return
    [("href", "browse.html?offset=" <> (intToText $ snd n'))])

listingHeaderAttrSplices
  :: Splices (AttrSplice AppHandler)
listingHeaderAttrSplices = "guardName" ## const $ do
  mode <- lift $ getParam "sort"
  return $ if maybe True (== "name") mode then [] else [("class", "script")]

updateSplices :: Splices (RuntimeSplice AppHandler ListingParam -> Splice AppHandler)
updateSplices = mapV (. fmap getUpdateParam) $ do
  "haveHiddenComics" ## deferManyElse (callTemplate "_explainHidden")
    (withSplices runChildren splices) . fmap
    (((>>) <$> guard . (>0) <*> Just) . hiddenCount)
  where
    splices = mapV (pureSplice . textSplice) $ do
      "count" ## intToText
      "plural" ## \n -> if n > 1 then "s" else ""
