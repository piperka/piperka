{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Listing.Query (
  getListing
  ) where

import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Int
import Data.Maybe
import Data.Text (Text, toCaseFold)
import Data.Text.Encoding (decodeUtf8)
import Data.Time.Clock
import qualified Data.Vector as V
import Hasql.Session hiding (run)
import Snap
import Snap.Snaplet.CustomAuth (currentUser)
import Snap.Snaplet.Hasql

import Prelude hiding (Ordering)

import Application hiding (uid, uname)
import Backend ()
import Piperka.Account (getHideInactive)
import Piperka.Listing
import Piperka.Listing.Types
import qualified Piperka.Listing.Types.Ordering as L (Ordering(..))
import qualified Piperka.Profile.Types as PT
import Piperka.Update.Statements
import Piperka.Update.Types (hold)
import qualified Piperka.Update.Types as U
import Piperka.Profile.Statements
import Piperka.Listing.Statements
import Piperka.Recommend (recommends)
import Piperka.Recommend.Thread (readModel, getRecommends, getRelatedCount)

getListing
  :: ListingMode
  -> L.Ordering
  -> Int32
  -> Int32
  -> Maybe (UserID, Text)
  -> AppHandler (Either ListingError ListingParam)
getListing Browse ord offset limit ux =
  getListing' False ord offset limit $ fmap fst ux
getListing Top ord offset limit ux =
  getListing' True ord offset limit $ fmap fst ux

getListing Profile ord offset limit ux = do
  let uid = fmap fst ux
  let uname = fromJust $ fmap snd ux
  recomm <- case (ord, uid) of
    (L.RecommendedDesc, Just uid') ->
      view recommendHandle >>= maybe (pure mempty)
      (fmap (V.fromList . getRecommends False Nothing uid') . liftIO . readModel)
    _ -> pure mempty
  name <- (fmap . fmap) decodeUtf8 $ getParam "name"
  fmap (either (Left . SqlError) id) $ run $ runExceptT $ do
    prof <- maybe (throwE Missing) return =<<
      (lift $ do
          case (name, uid) of
            (Nothing, Nothing) -> return Nothing
            (Nothing, Just uid') -> ownData uid'
            (Just name', Nothing) -> fmap PT.Common . checkSecretProfile <$>
                                     statement name' profileDataFetch
            (Just name', Just uid') ->
              if ((\f a b -> f a == f b) toCaseFold name' uname)
              then ownData uid'
              else fmap PT.Other . checkSecretProfile <$>
                   statement (name', uid') profileOtherDataFetch
      )
    let pUid = PT.uid $ PT.profile prof
    if PT.perm $ PT.profile prof
      then lift $ maybe
           (ProfileParam (commonToPlain prof) <$> statement
            (pUid, limit, offset) (profileFetch ord))
           (\uid' -> UserProfileParam prof <$> statement
                     (pUid, uid', limit, offset, recomm)
                     (profileFetchSubscribed ord))
           uid
      else return $ maybe
           (ProfileParam (commonToPlain prof) V.empty)
           (const $ UserProfileParam prof V.empty) uid
  where
    ownData uid = Just . PT.Own <$> statement uid profileOwnDataFetch
    commonToPlain (PT.Common x) = x
    commonToPlain _ = undefined
    checkSecretProfile p = p >>= \p' ->
      guard (PT.privacy (PT.profile p') /= PT.Private) >> p

-- Update mode gets its ordering always from user settings.
getListing Update _ offset limit ux =
  getHideInactive >>= \hide ->
  withTop auth currentUser >>= \usr ->
  flip (maybe $ return $ Left Missing) ux $ \(uid', _) ->
  fmap (either (Left . SqlError) nullIsMissing) $ run $ do
  updateOptions <-
    (\o -> o { U.total = (if hide then subtract $ U.hiddenCount o else id) $ U.total o })
    <$> maybe (statement uid' updateOptionsFetch) (pure . fromUserWithStats) usr
  fmap (updateOptions,) $ statement (uid', hide, limit, offset) $
    if hold updateOptions then updatesDirectLinkFetch else updatesFetch
  where
    nullIsMissing = runExceptT $ do
      lift (V.null . snd) >>= flip when (throwE Missing)
      lift $ UpdateParam <$> fst <*> snd

getListing Recommend _ _ _ ux =
  flip (maybe $ return $ Left Missing) ux $ \(uid', _) ->
  fmap (either (Left . SqlError) Right) $ runExceptT $ do
  rc <- ExceptT $ recommends True uid'
  ExceptT $ run $ UserParam True 100 <$>
    statement (uid', rc) recommendFetch

getListing Graveyard _ offset limit _ =
  fmap (either (Left . SqlError) Right) $ run $ GraveyardParam
  <$> (fromIntegral <$> statement () graveyardTotalFetch)
  <*> statement (limit, offset) graveyardFetch

getListing Revert _ offset limit ux =
  flip (maybe $ return $ Left Missing) ux $ \(uid', _) -> do
  now <- liftIO getCurrentTime
  fmap (either (Left . SqlError) Right) $ run $ RevertParam
    <$> (fromIntegral <$> statement uid' revertTotalFetch)
    <*> statement (uid', limit, offset) (revertFetch now)

getListing Support _ offset limit ux =
  flip (maybe $ return $ Left Missing) ux $ \(uid', _) -> do
  fmap (either (Left . SqlError) Right) $ run $ SupportParam
    <$> (fromIntegral <$> statement uid' supportTotalFetch)
    <*> statement (uid', limit, offset) supportFetch

-- Helper function for common case of Top/Browse
getListing'
  :: Bool
  -> L.Ordering
  -> Int32
  -> Int32
  -> Maybe UserID
  -> AppHandler (Either ListingError ListingParam)
getListing' isTop ord offset limit uid = do
  (recomm, rTotal) <- case (ord, uid) of
    (L.RecommendedDesc, Just uid') -> do
      view recommendHandle >>=
        maybe (pure (mempty, Nothing))
        (fmap ((,)
               <$> ((snd . V.splitAt (fromIntegral offset) .
                     V.fromList . getRecommends False Nothing uid'))
               <*> Just . fromIntegral . getRelatedCount) .
          liftIO . readModel)
    _ -> pure (mempty, Nothing)
  fmap (either (Left . SqlError) Right) $ run $ do
    total <- maybe
      (fromIntegral <$> statement () comicsTotalFetch) pure rTotal
    maybe
      (ListingParam isTop total <$> statement (limit, offset) (comicsFetch ord))
      (\uid' -> UserParam isTop total <$> statement
                (uid', limit, if ord == L.RecommendedDesc then 0 else offset, recomm)
                (comicsFetchSubscribed ord))
      uid
