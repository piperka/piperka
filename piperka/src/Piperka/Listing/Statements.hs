{-# LANGUAGE OverloadedStrings #-}

module Piperka.Listing.Statements where

import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Control.Applicative
import Control.Monad
import Contravariant.Extras.Contrazip
import Data.ByteString (ByteString)
import Data.Int
import Data.Maybe
import Data.String (IsString)
import Data.Text (Text)
import Data.Time.Clock
import Data.Time.LocalTime
import Data.Vector hiding ((++), map, null, replicateM)
import Hasql.Statement
import Prelude hiding (Ordering)

import Application
import Piperka.ComicInfo.Statements (decodeSchedule)
import qualified Piperka.Encoders as EN
import Piperka.Listing.Types
import Piperka.Listing.Types.Ordering

parseOrdering :: (IsString a, Eq a) => a -> Ordering
parseOrdering "new" = NewDesc
parseOrdering "update" = UpdateDesc
parseOrdering "top" = TopDesc
parseOrdering "hot" = HotDesc
parseOrdering "recommend" = RecommendedDesc
parseOrdering _ = TitleAsc

orderingSqlPart :: Ordering -> ByteString
orderingSqlPart NewDesc = "cid DESC"
orderingSqlPart UpdateAsc = "(SELECT last_updated FROM comics AS c LEFT JOIN crawler_config USING (cid) WHERE c.cid=x.cid) ASC NULLS LAST, ordering_form(title)"
orderingSqlPart UpdateDesc = "(SELECT last_updated FROM comics AS c LEFT JOIN crawler_config USING (cid) WHERE c.cid=x.cid) DESC NULLS LAST, ordering_form(title)"
orderingSqlPart TopDesc = "readers DESC, ordering_form(title)"
orderingSqlPart TitleAsc = "ordering_form(title)"
orderingSqlPart HotDesc = "recent_change DESC NULLS LAST, ordering_form(title)"
orderingSqlPart RecommendedDesc = " r_ord"

updateListingRow :: DE.Row (Maybe Text) -> DE.Row UpdateListingItem
updateListingRow x =
  UpdateListingItem
  <$> DE.column DE.int4
  <*> DE.column DE.bool
  <*> x
  <*> listingRow

userListingRow :: DE.Row UserListingItem
userListingRow =
  UserListingItem
  <$> DE.column DE.bool
  <*> DE.column DE.bool
  <*> DE.column DE.bool
  <*> DE.column DE.bool
  <*> listingRow

revertListingRow :: UTCTime -> DE.Row RevertListingItem
revertListingRow now =
  RevertListingItem
  <$> DE.column (diffUTCTime now . localTimeToUTC utc <$> DE.timestamp)
  <*> listingRow

supportListingRow :: DE.Row SupportListingItem
supportListingRow =
  SupportListingItem
  <$> DE.nullableColumn DE.text
  <*> DE.nullableColumn DE.text
  <*> listingRow

listingRow :: DE.Row ListingItem
listingRow =
  ListingItem
  <$> DE.column DE.int4
  <*> DE.column DE.text
  <*> (fmap (\x -> if (x > 7) then UpdateHigh
                   else if (x < 0.000001) then UpdateLow
                        else UpdateMid) <$> DE.nullableColumn DE.float4)
  <*> decodeSchedule

decodeUserListing :: DE.Result (Vector UserListingItem)
decodeUserListing = DE.rowVector userListingRow

decodeListing :: DE.Result (Vector ListingItem)
decodeListing = DE.rowVector listingRow

encode2 :: EN.Params (Int32, Int32)
encode2 = contrazip2 (EN.param EN.int4) (EN.param EN.int4)

encode3 :: EN.Params (UserID, Int32, Int32)
encode3 = contrazip3 (EN.param EN.uid) (EN.param EN.int4) (EN.param EN.int4)

encode4 :: EN.Params (UserID, Bool, Int32, Int32)
encode4 = contrazip4 (EN.param EN.uid) (EN.param EN.bool) (EN.param EN.int4) (EN.param EN.int4)

comicsFetchSubscribed :: Ordering -> Statement (UserID, Int32, Int32, Vector Int32) (Vector UserListingItem)
comicsFetchSubscribed order = fromJust $ lookup order table
  where
    table = map (\o -> (o, Statement (sql o)
                           encoder decodeUserListing True)) allOrderings
    encoder = contrazip4 (EN.param EN.uid) (EN.param EN.int4) (EN.param EN.int4) $
              EN.param $ EN.array $ EN.dimension foldl' $ EN.element EN.int4
    inject = case order of
      RecommendedDesc ->
        "JOIN unnest($4) WITH ORDINALITY recommended(cid, r_ord) USING (cid) "
      _ -> ""
    sql o = "SELECT COALESCE(subscribed, false), \
            \disinterest.cid IS NOT NULL, \
            \COALESCE(added_on > seen_comics_before, false) AS is_new, \
            \COALESCE(perm_intr, false), cid, title, update_value, \
            \hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates \
            \FROM comics AS x LEFT JOIN crawler_config USING (cid) " <> inject <>
            "LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
            \LEFT JOIN inferred_schedule AS inf USING (cid) \
            \CROSS JOIN user_site_activity \
            \LEFT JOIN (SELECT uid, cid, true AS subscribed \
            \ FROM subscriptions) AS subscribed USING (uid, cid) \
            \LEFT JOIN disinterest USING (uid, cid) \
            \LEFT JOIN (SELECT DISTINCT uid, cid, true AS perm_intr \
            \ FROM permitted_interest) AS perm_intr USING (uid, cid) \
            \WHERE title IS NOT NULL AND uid=$1 ORDER BY " <>
            (orderingSqlPart o) <> " LIMIT $2 OFFSET $3"

updatesFetch :: Statement (UserID, Bool, Int32, Int32) (Vector UpdateListingItem)
updatesFetch =
  Statement sql encode4 (DE.rowVector $ updateListingRow $ pure Nothing) True
  where
    sql = "SELECT * from user_updates($1, $2) LIMIT $3 OFFSET $4"

updatesDirectLinkFetch :: Statement (UserID, Bool, Int32, Int32) (Vector UpdateListingItem)
updatesDirectLinkFetch =
  Statement sql encode4 (DE.rowVector $ updateListingRow $ DE.nullableColumn DE.text) True
  where
    sql = "SELECT * FROM user_updates_link($1, $2) LIMIT $3 OFFSET $4"

profileFetchSubscribed :: Ordering -> Statement (UserID, UserID, Int32, Int32, Vector Int32) (Vector UserListingItem)
profileFetchSubscribed order = fromJust $ lookup order table
  where
    table = map (\o -> (o, Statement (sql o)
                           encoder decodeUserListing True)) allOrderings
    encoder = contrazip5 (EN.param EN.uid) (EN.param EN.uid)
              (EN.param EN.int4) (EN.param EN.int4) $
              EN.param $ EN.array $ EN.dimension foldl' $ EN.element EN.int4
    inject = case order of
      RecommendedDesc ->
        "JOIN unnest($5) WITH ORDINALITY recommended(cid, r_ord) USING (cid) "
      _ -> ""
    sql o = "SELECT subs, reject, is_new, pi, cid, title, update_value, \
            \code, schedule, weekly_updates, monthly_updates \
            \FROM (SELECT DISTINCT COALESCE(subscribed, false) AS subs, \
            \readers, recent_change, d.cid IS NOT NULL AS reject, \
            \COALESCE(added_on > usa.seen_comics_before, false) AS is_new, \
            \COALESCE(perm_intr, false) AS pi, comics.cid, title, update_value, \
            \hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates \
            \FROM comics LEFT JOIN crawler_config USING (cid) \
            \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
            \LEFT JOIN inferred_schedule AS inf USING (cid) \
            \JOIN subscriptions USING (cid) \
            \CROSS JOIN user_site_activity AS usa \
            \LEFT JOIN (SELECT uid, cid, true as subscribed FROM subscriptions) \
            \AS subscribed ON subscribed.uid=usa.uid \
            \AND subscribed.cid=comics.cid \
            \LEFT JOIN (SELECT uid, cid, interest, \
            \ true AS perm_intr FROM permitted_interest) AS perm_intr \
            \ON perm_intr.uid=usa.uid AND perm_intr.cid=comics.cid \
            \AND interest <> subscriptions.uid \
            \LEFT JOIN disinterest AS d ON d.cid=comics.cid AND d.uid=usa.uid \
            \WHERE subscriptions.uid=$1 \
            \AND usa.uid=$2) AS x " <> inject <>
            "ORDER BY " <> (orderingSqlPart o) <> " LIMIT $3 OFFSET $4"

comicsFetch :: Ordering -> Statement (Int32, Int32) (Vector ListingItem)
comicsFetch = fromJust . flip lookup table
  where
    table = map (\o -> (o, Statement (sql o)
                           encode2 decodeListing True)) allOrderings
    sql o = "SELECT cid, title, update_value, \
            \hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates \
            \FROM comics AS x LEFT JOIN crawler_config USING (cid) \
            \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
            \LEFT JOIN inferred_schedule AS inf USING (cid) \
            \ORDER BY " <>
            (orderingSqlPart o) <> " LIMIT $1 OFFSET $2"

profileFetch :: Ordering -> Statement (UserID, Int32, Int32) (Vector ListingItem)
profileFetch = fromJust . flip lookup table
  where
    table = map (\o -> (o, Statement (sql o)
                           encode3 decodeListing True)) allOrderings
    sql o = "SELECT cid, title, update_value, \
            \hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates \
            \FROM comics AS x \
            \LEFT JOIN crawler_config USING (cid) \
            \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
            \LEFT JOIN inferred_schedule AS inf USING (cid) \
            \JOIN subscriptions USING (cid) \
            \WHERE subscriptions.uid=$1 ORDER BY " <>
            (orderingSqlPart o) <> " LIMIT $2 OFFSET $3"

recommendFetch :: Statement (UserID, Vector Int32) (Vector UserListingItem)
recommendFetch = Statement sql encode decodeUserListing True
  where
    encode = contrazip2 (EN.param EN.uid)
             (EN.param $ EN.array (EN.dimension Data.Vector.foldl' $ EN.element EN.int4))
    sql = "SELECT COALESCE(subscribed, false), false, \
          \COALESCE(added_on > seen_comics_before, false) AS is_new, \
          \COALESCE(perm_intr, false), cid, title, update_value, \
          \hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates \
          \FROM comics AS x LEFT JOIN crawler_config USING (cid) \
          \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
          \LEFT JOIN inferred_schedule AS inf USING (cid) \
          \JOIN (SELECT cid, row_number() OVER () \
          \ FROM unnest($2 :: int[]) AS cid) AS rc USING (cid) \
          \CROSS JOIN user_site_activity \
          \LEFT JOIN (SELECT uid, cid, true AS subscribed \
          \ FROM subscriptions) AS subscribed USING (uid, cid) \
          \LEFT JOIN (SELECT DISTINCT uid, cid, true AS perm_intr \
          \ FROM permitted_interest) AS perm_intr USING (uid, cid) \
          \LEFT JOIN disinterest USING (uid, cid) \
          \WHERE title IS NOT NULL AND uid=$1 \
          \AND disinterest.cid IS NULL \
          \ORDER BY rc.row_number"

graveyardFetch :: Statement (Int32, Int32) (Vector ListingItem)
graveyardFetch = Statement sql encode2 decodeListing True
  where
    sql = "SELECT cid, title, null, null, null, null, null \
          \FROM graveyard ORDER BY ordering_form(title) \
          \LIMIT $1 OFFSET $2"

revertFetch :: UTCTime -> Statement (UserID, Int32, Int32) (Vector RevertListingItem)
revertFetch now = Statement sql encode3 (DE.rowVector (revertListingRow now)) True
  where
    sql = "SELECT used_on, cid, title, update_value, null, null, null, null \
          \FROM recent JOIN comics AS x USING (cid) \
          \LEFT JOIN crawler_config USING (cid) \
          \WHERE uid=$1 ORDER BY used_on DESC LIMIT $2 OFFSET $3"

supportFetch :: Statement (UserID, Int32, Int32) (Vector SupportListingItem)
supportFetch = Statement sql encode3 (DE.rowVector supportListingRow) True
  where
    sql = "SELECT patreon.entry, kofi.entry, c.cid, title, update_value, \
          \hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates \
          \FROM comics AS c JOIN crawler_config USING (cid) \
          \JOIN subscriptions USING (cid) \
          \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
          \LEFT JOIN inferred_schedule AS inf USING (cid) \
          \LEFT JOIN external_entry AS patreon ON (patreon.cid=c.cid AND patreon.epid=6) \
          \LEFT JOIN external_entry AS kofi ON (kofi.cid=c.cid AND kofi.epid=7) \
          \WHERE subscriptions.uid=$1 \
          \AND (patreon.entry IS NOT NULL OR kofi.entry IS NOT NULL) \
          \ORDER BY ordering_form(title) LIMIT $2 OFFSET $3"

comicsTotalFetch :: Statement () Int32
comicsTotalFetch = Statement sql EN.unit (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "SELECT count(*) FROM comics"

graveyardTotalFetch :: Statement () Int32
graveyardTotalFetch = Statement sql EN.unit (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "SELECT count(*) FROM graveyard"

revertTotalFetch :: Statement UserID Int32
revertTotalFetch = Statement sql (EN.param EN.uid) (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "SELECT count(*) FROM recent JOIN comics USING (cid) WHERE uid=$1"

supportTotalFetch :: Statement UserID Int32
supportTotalFetch = Statement sql (EN.param EN.uid) (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "SELECT count(distinct c.cid) FROM comics AS c \
          \JOIN subscriptions USING (cid) \
          \JOIN external_entry AS e ON (c.cid=e.cid AND epid IN (6,7)) \
          \WHERE uid=$1"
