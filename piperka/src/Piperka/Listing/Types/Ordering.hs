{-# LANGUAGE OverloadedStrings #-}

module Piperka.Listing.Types.Ordering where

import Prelude hiding (Ordering)
import Data.Text (Text)

data Ordering = NewDesc | UpdateAsc | UpdateDesc | TopDesc | TitleAsc
              | HotDesc
              | RecommendedDesc
              deriving (Show, Eq, Ord)

allOrderings :: [Ordering]
allOrderings = [ NewDesc, UpdateAsc, UpdateDesc, TopDesc, TitleAsc
               , HotDesc
               , RecommendedDesc
               ]

orderingToText :: Ordering -> Text
orderingToText NewDesc = "new"
orderingToText UpdateAsc = "updateasc"
orderingToText UpdateDesc = "update"
orderingToText TopDesc = "top"
orderingToText TitleAsc = "name"
orderingToText HotDesc = "hot"
orderingToText RecommendedDesc = "recommend"
