{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Piperka.Listing.Summary.Splices (renderSummary) where

import Control.Monad
import Data.List (find)
import Data.Map.Syntax
import Data.Maybe (isJust, isNothing)
import Data.Text (Text)
import qualified Data.Text as T
import Heist
import Heist.Splices

import Application
import Piperka.ComicInfo.Schedule
import Piperka.ComicInfo.Types (Schedule(..))
import Piperka.Heist
import Piperka.Listing.Types
import Piperka.Util (intToText)

renderSummary
  :: AppInit
  -> RuntimeAppHandler ListingItem
renderSummary ini = deferMany
  (withSplices (callTemplate "_listingSummary") (summarySplices ini)) . fmap updateStatus

summarySplices
  :: AppInit
  -> Splices (RuntimeAppHandler (Either Int Schedule))
summarySplices ini = do
  let left = either Just (const Nothing)
      right = either (const Nothing) Just
      times f = manyWithSplices runChildren timesSplices . fmap (right >=> f)
  "schedule" ## manyWithSplices runChildren scheduleSplices .
    fmap (right >=> \case {Schedule x -> Just x; _ -> Nothing})
  "weekly" ## times $ \case {Weekly x -> Just x; _ -> Nothing}
  "monthly" ## times $ \case {Monthly x -> Just x; _ -> Nothing}
  "reason" ## manyWithSplices runChildren reasonSplices .
    fmap (left >=> flip lookup (hiatusNames ini))

scheduleSplices
  :: Splices (RuntimeAppHandler [Int])
scheduleSplices =
  "letters" ## manyWith runChildren splices attrSplices . fmap
  (\xs -> let ys = map (flip quotRem 24) xs in
      map (\i -> (i, snd <$> find ((==i) . fst) ys)) [0..6])
  where
    letters :: [(Int, Text)]
    letters = zip [0..6] $ map (T.pack . (:[])) "MTWTFSS"
    splices :: Splices (RuntimeAppHandler (Int, Maybe Int))
    splices = do
      "letter" ## return . yieldRuntimeText .
        fmap (maybe "" id . flip lookup letters . fst)
      "present" ## ifCSplice (isJust . snd)
      "missing" ## ifCSplice (isNothing . snd)
    attrSplices = "class" ## \n _ ->
      maybe [] (\i -> [("class", "s" <> intToText i)]) . snd <$> n

reasonSplices
  :: Splices (RuntimeAppHandler Text)
reasonSplices = "msg" ## pureSplice . textSplice $ id
