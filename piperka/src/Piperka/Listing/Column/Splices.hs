{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Listing.Column.Splices (listingColumnSplices) where

import Control.Error.Util (bool)
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text, unpack, intercalate)
import qualified Data.Text as T
import qualified Data.Vector as V
import qualified HTMLEntities.Text as HTML
import Heist
import Heist.Compiled.LowLevel
import Heist.Splices (ifCSplice)
import Network.URI.Encode (encodeText)
import Text.XmlHtml

import Application hiding (prefs)
import Piperka.Heist
import Piperka.Listing.Navigate.Splices
import Piperka.Listing.Summary.Splices
import Piperka.Listing.Types hiding (listing', listing'')
import Piperka.Update.Types
import Piperka.Util (intToText)

listingColumnSplices
  :: AppInit
  -> ListingMode
  -> Splice AppHandler
  -> RuntimeSplice AppHandler (UserPrefs, Int, ListingParam, Maybe NavigateData)
  -> Splices (Splice AppHandler)
listingColumnSplices ini mode after n = do
  "subscribeControl" ##
    if listingModeSubscribes mode then runChildren else return mempty
  "navigateControl" ## case mode of
    Recommend -> runChildren
    _ -> do
      ns <- runNodeList . elementChildren =<< getParamNode
      deferManyElse
        (return ns)
        (\n' -> withLocalSplices
          (("apply-content" ## return ns) <> mapV ($ n') navigateSplices)
          mempty (callTemplate "_navigate")) $ (\(_,_,_,a) -> a) <$> n
  "columnMode" ## renderMode ini mode $ (\(a,b,c,_) -> (a,b,c)) <$> n
  "afterListing" ## after
  "totalCount" ## pure . yieldRuntimeText $
    (intToText :: Int -> Text) . extractTotal . (\(_,_,c,_) -> c) <$> n

renderMode
  :: AppInit
  -> ListingMode
  -> (RuntimeSplice AppHandler
      (UserPrefs, Int, ListingParam))
  -> Splice AppHandler
renderMode ini mode runtime = do
  col <- read . unpack . fromJust . getAttribute "col" <$> getParamNode
  tplL <- mkColumnSplices paramsL
  tplU <- mkColumnSplices paramsU
  tplP <- mkColumnSplices paramsP
  tplR <- mkColumnSplices paramsR
  tplS <- mkColumnSplices paramsS
  col `seq` return $ yieldRuntime $ do
    (prefs, offset, param) <- runtime
    -- Recommend mode overrides nrows to always fit all results.
    let userCols = columns prefs
        nrows = fromIntegral $
          (case (mode, userCols) of
              (Recommend, TwoColumn) -> max 50
              (Recommend, ThreeColumn) -> max 34
              _ -> id) $ rows prefs
    if (col /= userCols)
      then return mempty
      else do
      let splitFunc =
            zipWith (\c (a,b) -> (a,(b,c)))
            (zip <$> id <*> tail $ [(1+offset),(1+offset+nrows)..]) .
            takeWhile (not . V.null . snd) .
            case userCols of
             OneColumn -> (:[]) . (1,)
             TwoColumn -> \v -> let (v1, v2) = V.splitAt nrows v
                                in [(1,v1), (2,v2)]
             ThreeColumn -> \v -> let (v1, v1') = V.splitAt nrows v
                                      (v2, v3) = V.splitAt nrows v1'
                                  in [(1,v1), (2,v2), (3,v3)]
             FourColumn -> \v -> let (v1, v1') = V.splitAt nrows v
                                     (v2, v2') = V.splitAt nrows v1'
                                     (v3, v4) = V.splitAt nrows v2'
                                  in [(1,v1), (2,v2), (3,v3), (4,v4)]
      tpl <- mconcat [ tplL param splitFunc
                     , tplU param splitFunc
                     , tplP param splitFunc
                     , tplR param splitFunc
                     , tplS param splitFunc
                     ]
      codeGen tpl
  where
    stdSplices :: SubListing a => Splices (RuntimeAppHandler a)
    stdSplices = mapV (. fmap listing) (listingItemSplices ini)
    paramsL = (ListingMode, listingItemAttrSplices
              , stdSplices, extractListing)
    paramsU = (UserMode, userListingItemAttrSplices
              , stdSplices <> userListingItemSplices, extractUserListing)
    paramsP = (UpdateMode, updateListingItemAttrSplices
              , stdSplices <> updateListingItemSplices, extractUpdateListing)
    paramsR = (RevertMode, revertListingItemAttrSplices
              , stdSplices <> revertListingItemSplices, extractRevertListing)
    paramsS = (SupportMode, mempty
              , stdSplices <> supportListingItemSplices, extractSupportListing)
    mkColumnSplices (itemMode, itemAttrSplices, itemSplices, extract) = do
      promise <- newEmptyPromise
      tpl <- (let withUpdateSplices spl =
                    withSplices spl updateListingSplices
                    (snd <$> getPromise promise) in
              (case itemMode of UpdateMode -> withUpdateSplices
                                _ -> id) .
               (withSplices runChildren
                 (columnSplices mode itemMode $
                  manyWith runChildren itemSplices itemAttrSplices)))
             (fst <$> getPromise promise)
      return $ \param f -> maybe (return mempty)
                           (\lst -> putPromise promise (f lst, param) >> return tpl) $
                           extract param

columnSplices
  :: ListingMode
  -> ListingItemMode
  -> RuntimeAppHandler (V.Vector a)
  -> Splices (RuntimeSplice AppHandler [(Int, (V.Vector a, (Int, Int)))] -> Splice AppHandler)
columnSplices mode itemMode itemsAction = "column" ## \runtime -> do
  colNum <- read . unpack . fromJust . getAttribute "column" <$>
            getParamNode
  colNum `seq` maybeSplice
    (\runtime' ->
      (if mode `elem` [Top, Recommend]
        then withLocalSplices mempty ("startNum" ## startNum runtime')
        else id) $
      withSplices (callTemplate "_column")
      (singleColumnSplices mode itemMode itemsAction) $
      fmap fst runtime') $ fmap (lookup colNum) runtime
  where
    startNum n _ = do
      (i,j) <- snd <$> n
      return $ (if j > 1000 then (("class", "wide"):) else id) $
        [("start", intToText i)]

updateListingSplices
  :: Splices (RuntimeSplice AppHandler ListingParam -> Splice AppHandler)
updateListingSplices = do
  "holdbookmark" ## checkedSplice . fmap (hold . getUpdateParam)
  "offsetBackParam" ## \runtime -> return $ yieldRuntimeText $ do
    offset <- offsetMode . getUpdateParam <$> runtime
    return $ if offset then "&offset_back=1" else ""

singleColumnSplices
  :: ListingMode
  -> ListingItemMode
  -> RuntimeAppHandler (V.Vector a)
  -> Splices (RuntimeSplice AppHandler (V.Vector a) -> Splice AppHandler)
singleColumnSplices mode itemMode itemsAction = do
  "listingMode" ## renderSingleColumn mode
  "item" ## \n -> renderItem itemMode (itemsAction n)
  "listingStdItem" ## const $ callTemplate "_listingStdItem"

renderSingleColumn
  :: ListingMode
  -> RuntimeSplice AppHandler (V.Vector a)
  -> Splice AppHandler
renderSingleColumn mode _ = do
  modes :: [ListingMode] <- read . unpack . fromJust . getAttribute "type"
                            <$> getParamNode
  if mode `elem` modes
    then runChildren
    else return $ yieldPure mempty

renderItem
  :: ListingItemMode
  -> Splice AppHandler
  -> Splice AppHandler
renderItem itemMode itemsSplice = do
  allowedMode :: ListingItemMode <- read . unpack . fromJust .
                                    getAttribute "type" <$> getParamNode
  if (itemMode /= allowedMode)
    then return $ yieldPure mempty
    else itemsSplice

listingItemSplices
  :: AppInit
  -> Splices (RuntimeSplice AppHandler ListingItem -> Splice AppHandler)
listingItemSplices ini = ("summaryRow" ## renderSummary ini) <>
  (mapV (pureSplice . textSplice) $ do
      "title" ## HTML.text . title
      "cid" ## intToText . cid
  )

listingItemAttrSplices
  :: Splices (RuntimeSplice  AppHandler ListingItem -> AttrSplice AppHandler)
listingItemAttrSplices = do
  "freqClass" ## \n _ -> freqClass <$> n

userListingItemSplices
  :: Splices (RuntimeSplice AppHandler UserListingItem -> Splice AppHandler)
userListingItemSplices = do
  "followee" ## checkedSplice . fmap friends
  "subscribed" ## ifCSplice $ (&&) <$> not . reject <*> subs
  "unsubscribed" ## ifCSplice $ (&&) <$> not . reject <*> not . subs
  "rejected" ## ifCSplice reject
  "isNew" ## checkedSplice . fmap newComic

userListingItemAttrSplices
  :: Splices (RuntimeSplice AppHandler UserListingItem -> AttrSplice AppHandler)
userListingItemAttrSplices = do
  "freqClass" ## \n _ -> do
    x <- n
    let fc = freqClass x
    return $ if reject x
      then pure . ("class",) $ maybe "rejected" (<> " rejected") .
           fmap snd $ listToMaybe fc
      else fc

updateListingItemSplices
  :: Splices (RuntimeSplice AppHandler UpdateListingItem -> Splice AppHandler)
updateListingItemSplices = mapV (pureSplice . textSplice) $ do
  "new" ## intToText . new
  "directLink" ## HTML.text . fromMaybe "" . directLink

updateListingItemAttrSplices
  :: Splices (RuntimeSplice AppHandler UpdateListingItem -> AttrSplice AppHandler)
updateListingItemAttrSplices = do
  "class" ## \n t ->
    ((\xs -> if null xs then [] else [("class", intercalate " " xs)])
     . foldr ($) [] . flip map
     [ const $ if T.null t then id else (t:)
     , bool id ("nsfw":) . nsfw
     , maybe id ((:) . snd) . listToMaybe . freqClass
     ] . flip id) <$> n

revertListingItemSplices
  :: Splices (RuntimeSplice AppHandler RevertListingItem -> Splice AppHandler)
revertListingItemSplices = do
  "time" ## \n -> do
    node <- getParamNode
    let dv :: Int
        dv = read . unpack . fromJust $ getAttribute "div" node
        mx = maybe maxBound (read . unpack) $ getAttribute "max" node
        timeSplices = mapV (pureSplice . textSplice) $ do
          "tick" ## intToText
          "plural" ## bool "s" "" . (== 1)
    dv `seq` mx `seq`
      maybeSplice (withSplices runChildren timeSplices) $ fmap
      (\l -> let a = floor (age l) `div` dv
             in if a == 0 || a >= mx then Nothing else Just a) n

revertListingItemAttrSplices
  :: Splices (RuntimeSplice AppHandler RevertListingItem -> AttrSplice AppHandler)
revertListingItemAttrSplices = do
  "id" ## \n t -> do
    c <- intToText . cid . listing <$> n
    let idVal = "recent" <> c
    return $ case t of
      "id" -> [("id", idVal), ("value", c)]
      "for" -> [("for", idVal)]
      _ -> undefined

supportListingItemSplices
  :: Splices (RuntimeSplice AppHandler SupportListingItem -> Splice AppHandler)
supportListingItemSplices = do
  let maybePage f = manyWithSplices runChildren
        ("page" ## pureSplice . textSplice $ encodeText) . fmap f
  "patreon" ## maybePage patreon
  "kofi" ## maybePage kofi

freqClass
  :: SubListing a
  => a
  -> [(Text, Text)]
freqClass = let cl (Just UpdateHigh) = [("class", "update-high")]
                cl (Just UpdateMid) = [("class", "update-mid")]
                cl (Just UpdateLow) = [("class", "update-low")]
                cl _ = []
            in cl . updateFreq . listing
