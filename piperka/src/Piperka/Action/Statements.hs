{-# LANGUAGE OverloadedStrings #-}

module Piperka.Action.Statements where

import Contravariant.Extras.Contrazip
import qualified Data.Foldable as F
import Data.Int
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (sql)
import Hasql.Statement
import Network.IP.Addr
import Network.URI.Encode (decodeText)

import Application (UserID)
import qualified Piperka.Encoders as EN

decodeBookmark
  :: DE.Row (Int, Text, Maybe (Int, Int, Bool))
decodeBookmark =
  (,,)
  <$> DE.column (fromIntegral <$> DE.int4)
  <*> DE.column DE.text
  <*> ((\(a,b,c) -> (,,) <$> a <*> b <*> c) <$>
       ((,,)
        <$> DE.nullableColumn (fromIntegral <$> DE.int4)
        <*> DE.nullableColumn (fromIntegral <$> DE.int4)
        <*> DE.nullableColumn DE.bool))

decodeUnreadStats
  :: DE.Row (Int32, Int32)
decodeUnreadStats =
  (,)
  <$> DE.column DE.int4
  <*> DE.column DE.int4

getUnreadStats
  :: Statement UserID (Int32, Int32)
getUnreadStats =
  Statement sql (EN.param EN.uid) (DE.singleRow decodeUnreadStats) True
  where
    sql = "SELECT COALESCE(SUM(num), 0), COUNT(*) FROM comic_remain_frag_cache \
          \JOIN comics USING (cid) WHERE uid=$1 AND num > 0"

bookmarkWithDecode
  :: Foldable t
  => Text
  -> (Text -> Session (t (Int, Text, Maybe a)))
  -> (Text -> Session (t (Int, Text, Maybe a)))
  -> Session (t (Int, Text, Maybe a))
bookmarkWithDecode url stmt stmt' = do
  let url' = decodeText url
  if url == url'
    then stmt url
    else do
    bm <- stmt url'
    case (F.find (const True) bm, F.length bm) of
      (Just (_, _, Nothing), 1) -> stmt' url
      _ -> return bm

bookmarkFetch
  :: Statement (Text, Bool) [(Int, Text, Maybe (Int, Int, Bool))]
bookmarkFetch =
  Statement sql (contrazip2 (EN.param EN.text) (EN.param EN.bool))
  (DE.rowList decodeBookmark) True
  where
    sql = "SELECT cid, title, ord, subord, at_max \
          \FROM bookmark($1, $2) AS b JOIN comics USING (cid)"

-- url, want_here, host, uid
bookmarkAndLogFetch
  :: Statement (Text, Bool, NetAddr IP, Maybe UserID) [(Int, Text, Maybe (Int, Int, Bool))]
bookmarkAndLogFetch =
  Statement sql (contrazip4 (EN.param EN.text) (EN.param EN.bool)
                 (EN.param EN.inet) (EN.nullableParam EN.uid))
  (DE.rowList decodeBookmark) True
  where
    sql = "SELECT b.v_cid, title, b.v_ord, b.v_subord, b.v_at_max \
          \FROM bookmark_and_log($1, $2, $3, $4) AS b \
          \JOIN comics ON (b.v_cid = cid)"

-- No explicit cache refresh needed after the set procedures, they do
-- it themselves.
bookmarkSet
  :: Statement (UserID, Int32, Int32, Int32) (Int32, Int32)
bookmarkSet =
  Statement sql (contrazip4 (EN.param EN.uid) (EN.param EN.int4)
                 (EN.param EN.int4) (EN.param EN.int4))
  (DE.singleRow decodeUnreadStats) True
-- uid, cid, ord, subord
  where
    sql = "SELECT * FROM set_bookmark($1, $2, $3, $4)"

subscribeSet
  :: Statement (UserID, Int32, Bool) (Int32, Int32)
subscribeSet =
  Statement sql (contrazip3 (EN.param EN.uid) (EN.param EN.int4)
                 (EN.param EN.bool))
  (DE.singleRow decodeUnreadStats) True
  where
    sql = "SELECT * FROM set_bookmark($1, $2, $3)"

unsubscribeSet
  :: Statement (UserID, Int32) (Int32, Int32)
unsubscribeSet =
  Statement sql (contrazip2 (EN.param EN.uid) (EN.param EN.int4))
  (DE.singleRow decodeUnreadStats) True
  where
    sql = "SELECT * FROM unset_bookmark($1, $2)"

rejectSet
  :: UserID
  -> Int32
  -> Session (Int32, Int32)
rejectSet uid cid = do
  _ <- statement (uid, cid) deleteSubscription
  statement (uid, cid) rejectComic
  statement uid $ Statement sql3 (EN.param EN.uid) (DE.singleRow decodeUnreadStats) True
  where
    encoder = contrazip2 (EN.param EN.uid) (EN.param EN.int4)
    deleteSubscription = Statement sql1 encoder DE.unit True
    rejectComic = Statement sql2 encoder DE.unit True
    sql1 = "DELETE FROM subscriptions WHERE uid=$1 AND cid=$2"
    sql2 = "INSERT INTO disinterest (uid, cid) VALUES ($1, $2) \
           \ON CONFLICT (uid, cid) DO NOTHING"
    sql3 = "SELECT coalesce(sum(num), 0), count(*) from comic_remain_frag_cache \
           \JOIN comics USING (cid) WHERE uid=$1 AND num > 0"

unrejectSet
  :: Statement (UserID, Int32) (Int32, Int32)
unrejectSet =
  Statement sql (contrazip2 (EN.param EN.uid) (EN.param EN.int4))
  (DE.singleRow decodeUnreadStats) True
  where
    sql = "WITH d as (\
          \DELETE FROM disinterest WHERE uid=$1 AND cid=$2) \
          \SELECT coalesce(sum(num), 0), count(*) from comic_remain_frag_cache \
          \JOIN comics USING (cid) WHERE uid=$1 AND num > 0"

revertUpdatesSet
  :: UserID
  -> Vector Int32
  -> Session (Int32, Int32)
revertUpdatesSet uid cids = do
  _ <- statement (uid, cids) applyReverts
  _ <- statement (uid, cids) deleteReverts
  refreshCache uid
  statement uid getUnreadStats
  where
    encoder = (contrazip2 (EN.param EN.uid)
                (EN.param $ EN.array (EN.dimension V.foldl' $
                                      EN.element EN.int4)))
    applyReverts = Statement sql1 encoder DE.unit True
    deleteReverts = Statement sql2 encoder DE.unit True
    sql1 = "UPDATE subscriptions SET ord=\
           \COALESCE((SELECT ord FROM recent WHERE \
           \uid=subscriptions.uid and cid=subscriptions.cid ), 0), \
           \subord=COALESCE((SELECT subord FROM recent WHERE \
           \uid=subscriptions.uid and cid=subscriptions.cid), 0) \
           \WHERE subscriptions.uid=$1 AND \
           \subscriptions.cid = ANY ($2 :: int[])"
    sql2 = "DELETE FROM recent WHERE uid = $1 and cid = ANY ($2 :: int[])"

refreshCache
  :: UserID
  -> Session ()
refreshCache = flip statement $ Statement sql (EN.param EN.uid) DE.unit True
  where
    sql = "INSERT INTO comic_remain_frag_cache \
          \SELECT $1, cid, num FROM comic_remain_frag($1) \
          \WHERE cid NOT IN (SELECT cid FROM comic_remain_frag_cache WHERE uid=$1)"
