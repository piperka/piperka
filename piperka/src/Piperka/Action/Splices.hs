{-# LANGUAGE OverloadedStrings #-}

module Piperka.Action.Splices (renderAction) where

import Data.Map.Syntax
import Data.Text (Text)
import Heist

import Application
import Piperka.Action
import Piperka.Action.Types
import Piperka.Bookmark.Splices
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (intToText)

renderAction
  :: Splice AppHandler
  -> RuntimeAppHandler (Maybe (Maybe ActionError, Maybe Action))
renderAction contentSplice =
  deferManyElse contentSplice
  (withSplices runChildren (actionSplices contentSplice))

actionSplices
  :: Splice AppHandler
  -> Splices (RuntimeAppHandler (Maybe ActionError, Maybe Action))
actionSplices contentSplice = do
  "bookmark" ## deferMany renderBookmarkResult . fmap maybeBookmark
  "logout" ## renderLogout
  "csrfFail" ## renderCsrfFail
  "sqlErr" ## renderSqlErr contentSplice
  "unknownAction" ## renderUnknown
  where
    renderLogout n = do
      sub <- runChildren
      flip bindLater n $ \(_, act) -> do
        case act of Just Logout -> codeGen sub
                    _ -> return mempty
    renderUnknown n = do
      sub <- runChildren
      flip bindLater n $ \(err, _) -> do
        case err of Just UnknownAction -> codeGen sub
                    _ -> return mempty
    maybeBookmark (Nothing, Just (Bookmark xs)) = Just xs
    maybeBookmark _ = Nothing

renderCsrfFail
  :: RuntimeAppHandler (Maybe ActionError, Maybe Action)
renderCsrfFail = deferMany
  (withSplices runChildren $ do
      "describe" ## stdConditionalSplice failCond
      "actionInputs" ## \n' -> manyWithSplices runChildren
                               (mapV (pureSplice . textSplice) $ do
                                   "name" ## fst
                                   "value" ## snd) `defer` (encodeAction . fst <$> n')
  ) . fmap maybeCsrfFail
  where
    withTitleSplice s = withSplices s
                        (mapV (pureSplice . textSplice) $ do
                            "title" ## fst
                            "cid" ## snd)
    failCond Logout =
      ( "logout", Simple)
    failCond (Bookmark _) =
      ( "bookmark"
      , WithParam (\ ~(Bookmark b) _ ->
                     return $ case b of [b'@(_, _, Just _)] -> Just b'
                                        _ -> Nothing)
        (\s -> deferMany (withSplices s itemPageSplices)))
    failCond (Subscribe _ _) =
      ( "subscribe"
      , WithParam (\ ~(Subscribe cid _) ~(Just t) ->
                     return (t, intToText cid))
        withTitleSplice)
    failCond (Unsubscribe _) =
      ( "unsubscribe"
      , WithParam (\ ~(Unsubscribe cid) ~(Just t) ->
                     return (t, intToText cid))
        withTitleSplice)
    failCond (Revert _) = ( "revert", Simple)
    failCond (Reject _) =
      ( "reject"
      , WithParam (\ ~(Reject cid) ~(Just t) ->
                     return (t, intToText cid))
        withTitleSplice)
    failCond (Unreject _) =
      ( "unreject"
      , WithParam (\ ~(Unreject cid) ~(Just t) ->
                     return (t, intToText cid))
        withTitleSplice)

maybeCsrfFail
  :: (Maybe ActionError, Maybe Action)
  -> Maybe (Action, Maybe Text)
maybeCsrfFail (Just CsrfFail, Just act@(Bookmark [(_, _, Just _)])) =
  Just (act, Nothing)
maybeCsrfFail (_, Just (Bookmark _)) = Nothing
maybeCsrfFail (Just CsrfFail, Just act) =
  Just (act, Nothing)
maybeCsrfFail (Just (CsrfFailWithComic title), Just act) =
  Just (act, Just title)
maybeCsrfFail _ = Nothing

renderSqlErr
  :: Splice AppHandler
  -> RuntimeAppHandler (Maybe ActionError, Maybe Action)
renderSqlErr contentSplice =
  eitherDefer stdSqlErrorSplice
  (const contentSplice) . fmap mapAction
  where
    mapAction (Just (SqlError e), _) = Left e
    mapAction _ = Right ()
