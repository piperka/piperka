module Piperka.Action.Types where

import Data.Int
import Data.Text hiding (empty)
import Data.Vector (Vector, empty)
import Hasql.Session (QueryError)

import Piperka.Bookmark.Types

data ActionError = SqlError QueryError
                 | CsrfFail
                 | CsrfFailWithComic Text
                 | UnknownAction
                 | NeedsLogin
                 deriving (Show, Eq)

data Action = Logout
            | Bookmark [BookmarkResult]
            | Subscribe Int Bool
            | Unsubscribe Int
            | Revert (Vector Int32)
            | Reject Int
            | Unreject Int
            deriving (Show, Eq)

instance Enum Action where
  fromEnum Logout = 0
  fromEnum (Bookmark _) = 1
  fromEnum (Subscribe _ _) = 2
  fromEnum (Unsubscribe _) = 3
  fromEnum (Revert _) = 4
  fromEnum (Reject _) = 5
  fromEnum (Unreject _) = 6
  toEnum 0 = Logout
  toEnum 1 = Bookmark []
  toEnum 2 = Subscribe 0 False
  toEnum 3 = Unsubscribe 0
  toEnum 4 = Revert empty
  toEnum 5 = Reject 0
  toEnum 6 = Unreject 0
  toEnum _ = error "Action"

instance Bounded Action where
  minBound = Logout
  maxBound = Unreject 0
