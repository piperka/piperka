module Piperka.Action.Query where

import Control.Monad.Trans
import Data.Int
import Data.Text (Text)
import Data.Vector (Vector)
import Hasql.Session hiding (run)
import Network.IP.Addr
import Snap.Snaplet.Hasql (run, HasHasql)

import Application (UserID)
import Piperka.Action.Statements
import Piperka.Action.Types

getBookmark
  :: (HasHasql m, MonadIO m)
  => Text
  -> Bool
  -> NetAddr IP
  -> Maybe UserID
  -> m (Either QueryError Action)
getBookmark url wantHere ip uid = (fmap Bookmark) <$>
  (run $ bookmarkWithDecode url
    (\u -> statement (u, wantHere, ip, uid) bookmarkAndLogFetch)
    (\u -> statement (u, wantHere) bookmarkFetch))

setBookmark
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Int32
  -> Int32
  -> Int32
  -> m (Either QueryError (Int32, Int32))
setBookmark uid cid ord subord =
  run $ statement (uid, cid, ord, subord) bookmarkSet

subscribe
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Int32
  -> Bool
  -> m (Either QueryError (Int32, Int32))
subscribe uid cid startAtFirst =
  run $ statement (uid, cid, startAtFirst) subscribeSet

unsubscribe
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Int32
  -> m (Either QueryError (Int32, Int32))
unsubscribe uid cid =
  run $ statement (uid, cid) unsubscribeSet

reject
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Int32
  -> m (Either QueryError (Int32, Int32))
reject uid cid = run $ rejectSet uid cid

unreject
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Int32
  -> m (Either QueryError (Int32, Int32))
unreject uid cid = run $ statement (uid, cid) unrejectSet

revertUpdates
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Vector Int32
  -> m (Either QueryError (Int32, Int32))
revertUpdates uid cids =
  run $ revertUpdatesSet uid cids
