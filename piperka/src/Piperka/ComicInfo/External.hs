{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

-- Read and parse external entries from XML at splice compile time

module Piperka.ComicInfo.External (generateExternal) where

import Control.Error.Util (hush)
import Control.Monad
import qualified Data.ByteString as B
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.List
import Data.Maybe
import Data.Text (Text)
import Data.Text.ICU (Regex, ParseError)
import qualified Data.Text.ICU as ICU
import Data.Text.Read
import qualified Text.XmlHtml as X
import Text.XmlHtml (Node)

import Application
import Piperka.ComicInfo.Types
import Piperka.File

generateExternal
  :: SiteIdentity
  -> IO (Either String ((Int -> Text -> Maybe ExternalEntry), IntMap Regex))
generateExternal site = do
  file <- searchSiteFile site $ "x/epedias.xml"
  doc <- X.parseXML "x/epedias.xml" <$> B.readFile file
  return $ process =<< doc

process
  :: X.Document
  -> Either String (Int -> Text -> Maybe ExternalEntry, IntMap Regex)
process doc = do
  (epedias, ext) <- maybe (Left "failure parsing epedias data") return $ do
    epedias <- fmap X.childElements .
      find (\x -> X.isElement x && X.elementTag x == "epedias") $
      X.docContent doc
    (epedias,) <$> sequence (map mkEntry epedias)
  validate <- either (Left . show) Right $
    IntMap.fromList . catMaybes <$> sequence (map mkValidate epedias)
  return (mkLookup ext, validate)

mkEntry :: Node -> Maybe (Int, Text -> ExternalEntry)
mkEntry x = do
  guard $ X.isElement x && X.elementTag x == "entry"
  i <- fst <$> (join $ hush . decimal <$> (lookup "id" $ X.elementAttrs x))
  t <- lookup "tag" $ X.elementAttrs x
  n <- lookup "name" =<< X.elementAttrs <$>
       (find ((== "link") . X.elementTag) $ X.childElements x)
  d <- X.nodeText <$> (find (\y -> X.isElement y && X.elementTag y == "description") $
                       X.childNodes x)
  urlbase <- find ((== "urlbase") . X.elementTag) $ X.childElements x
  b <- lookup "href" $ X.elementAttrs urlbase
  let l = maybe "" id $ lookup "tail" $ X.elementAttrs urlbase
  return $ (i, ExternalEntry d t n i b l)

mkLookup :: (Eq a, MonadPlus m) => [(a, b -> c)] -> a -> b -> m c
mkLookup el = maybe (const mzero) (return .) . flip lookup el

mkValidate
  :: Node
  -> Either ParseError (Maybe (Int, Regex))
mkValidate x = maybe
  (return Nothing)
  (\(i,r) -> Just . (i,) <$> ICU.regex' [] r) $ do
  guard $ X.isElement x && X.elementTag x == "entry"
  let attrs = X.elementAttrs x
  (,)
    <$> (fst <$> (join $ hush . decimal <$> lookup "id" attrs))
    <*> lookup "regex" attrs
