{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Piperka.ComicInfo.Schedule (renderInferredSchedule, timesSplices) where

import Control.Monad
import Data.Map.Syntax
import Heist
import Heist.Splices (ifCSplice)

import Application
import Piperka.ComicInfo.Types
import Piperka.Heist
import Piperka.Util (intToText)

renderInferredSchedule
  :: RuntimeAppHandler ComicInfo
renderInferredSchedule = deferMany render .
  fmap (inferredSchedule >=> either (const Nothing) Just)

render
  :: RuntimeAppHandler Schedule
render = withSplices runChildren $ do
  let times f = manyWithSplices runChildren timesSplices . fmap f
  "indeterminate" ## ifCSplice $ \case {Schedule x -> null x; _ -> False}
  "weekly" ## times $ \case {Weekly x -> Just x; _ -> Nothing}
  "monthly" ## times $ \case {Monthly x -> Just x; _ -> Nothing}
  "updates" ## manyWithSplices runChildren scheduleSplices .
    fmap (\case Schedule xs ->
                  zip ((replicate (length xs - 1) True) <> [False])
                  (map (flip quotRem 24) xs)
                _ -> [])
  where
    day a = (== a) . fst . snd
    scheduleSplices = do
      zipWithM (\k v -> k ## ifCSplice $ day v)
        ["mon", "tue", "wed", "thu", "fri", "sat", "sun"] [0..6]
      "hour" ## pureSplice . textSplice $ intToText . snd . snd
      "notLast" ## ifCSplice fst

timesSplices
  :: Splices (RuntimeAppHandler Int)
timesSplices = do
  "once" ## ifCSplice (== 1)
  "multi" ## manyWithSplices runChildren
    ("num" ## pureSplice . textSplice $ intToText) .
    fmap (\x -> guard (x > 1) >> Just x)
