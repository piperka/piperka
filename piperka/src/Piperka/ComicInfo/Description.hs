{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Piperka.ComicInfo.Description where

import Data.Text (Text, strip)
import Text.HTML.TagSoup

-- Get the first paragraph of the description, strip HTML
simpleDescription :: Text -> Text
simpleDescription txt =
  let tags = parseTags txt
      untilNextParagraph =
        strip . foldr (\case { (TagText t) -> (t <>) ; _ -> id }) "" .
        filter (\case { (TagText _) -> True; _ -> False}) .
        takeWhile (\case { (TagOpen "p" _) -> False; _ -> True })
  in case tags of
       (TagOpen "p" _:rs) -> untilNextParagraph rs
       rs -> untilNextParagraph rs
