{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.ComicInfo.Splices
  (
    getComicInfoData
  , prefetchSplices
  , renderComicInfo
  , renderWithCid
  , renderWithCidTitle
  )
  where

import Control.Error.Util
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.State (modify)
import Control.Lens (view, set)
import Data.Maybe
import Data.Map.Syntax
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Vector as V
import Heist
import qualified HTMLEntities.Text as HTML
import Snap
import qualified Text.XmlHtml as X

import Application
import Piperka.Auth
import Piperka.ComicInfo.Description
import Piperka.ComicInfo.Query
import Piperka.ComicInfo.Schedule
import Piperka.ComicInfo.Types
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Splices.Related
import Piperka.Util (formatTime', formatTime'', getCid, intToText)

renderFailure
  :: Bool
  -> RuntimeAppHandler ComicInfoError
renderFailure deadPage n = do
  let mergedSplices = mapV (pureSplice . textSplice) $ do
        "otherComic" ## \(c,_,d) ->
          (if d then "deadinfo.html?cid=" else "info.html?cid=") <> intToText c
        "title" ## HTML.text . \(_,x,_) -> x
  sqlErr <- stdSqlErrorSplice $ (\(~(SqlError e)) -> e) <$> n
  removed <- callTemplate "/cinfo/_removed"
  missing <- callTemplate $
    if deadPage then "/cinfo/_dead_missing" else "/cinfo/_missing"
  merged <- withSplices (callTemplate "/cinfo/_merged") mergedSplices $
            (\(~(Merged x)) -> x) <$> n
  let errs Missing = lift (modifyResponse $ setResponseCode 404) >> codeGen missing
      errs (SqlError _) = codeGen sqlErr
      errs FoundDead = codeGen removed
      errs (Merged _) = codeGen merged
  bindLater errs n

-- When used as a top level splice
renderComicInfo
  :: AppInit
  -> RuntimeAppHandler (Maybe MyData)
renderComicInfo ini n = do
  nodes <- X.elementChildren <$> getParamNode
  let success n' = withLocalSplices
        (mapV ($ n') $ comicInfoSplices ini) ("deadRel" ## const $ return []) $
        if null nodes then callTemplate "/include/cinfo" else runNodeList nodes
      fetch = yieldRuntimeEffect $ do
        u <- n
        lift $ modify . set prefetchComicInfo . Just =<< getComicInfoData Info u
  rd <- render success (renderFailure False)
  return $ fetch <> rd

renderWithCid
  :: AppInit
  -> Splice AppHandler
renderWithCid ini = do
  deadPage <- maybe False (read . T.unpack) . X.getAttribute "dead" <$>
              getParamNode
  let success n' = withLocalSplices
                   (do
                       "exists" ## renderExists ini n'
                       "related" ## renderRelated)
                   ("deadRel" ## deadRel) runChildren
      failure =
        withLocalSplices ("exists" ## return mempty) mempty . renderFailure deadPage
      deadRel = const $ return $ if deadPage then [("rel", "nofollow")] else []
  deadPage `seq` render success failure

getComicInfoData
  :: Prefetch
  -> Maybe MyData
  -> AppHandler (Either ComicInfoError ComicInfo)
getComicInfoData mode u = do
  tlookup <- view taglookup
  elookup <- view extlookup
  let getInfo = getComicInfo tlookup elookup
      getDeadInfo = getDeadComicInfo tlookup

  runExceptT $ do
    c <- snd <$> (hoistEither . note Missing =<< lift getCid)
    info <- ExceptT $ (case mode of
                         Info -> getInfo
                         Dead -> getDeadInfo) c u
    return info

render
  :: RuntimeAppHandler ComicInfo
  -> RuntimeAppHandler ComicInfoError
  -> Splice AppHandler
render success failure =
  withLocalSplices
  ("cid" ## return $ yieldRuntimeText $
    (maybe "" (T.decodeLatin1 . fst) <$> lift getCid))
  mempty $
  eitherDefer failure success $
  (maybe (Left Missing) id <$> (lift $ view prefetchComicInfo))

renderExists
  :: AppInit
  -> RuntimeAppHandler ComicInfo
renderExists ini = withSplices runChildren $ do
  "title" ## pureSplice . textSplice $ HTML.text . title
  "comicInfo" ## \n -> withSplices (callTemplate "/include/cinfo")
                       (comicInfoSplices ini) n
  "ifSubscribed" ## \n -> do
    nodes <- X.elementChildren <$> getParamNode
    let nodeFilter name = runNodeList $ concatMap X.childNodes $
                          filter (maybe False (== name) . X.tagName) nodes
    t <- nodeFilter "true"
    f <- nodeFilter "false"
    r <- nodeFilter "rejected"
    flip bindLater n $ \info -> do
      codeGen $ case (fromMaybe False $ subscribed info, rejected info) of
        (_, True) -> r
        (True, _) -> t
        _ -> f
  "crawlErrors" ## renderCrawlErrors
  "ifMapped" ## \n -> do
    x <- runChildren
    flip bindLater n $ \n' -> do
      if mapped n' then codeGen x else return mempty
  "notCidAuthor" ## \n -> do
    x <- runChildren
    flip bindLater n $ \n' -> do
      usr <- lift currentUserPlain
      let gg = codeGen x
      flip (maybe gg) usr $ \u ->
        if author u `elem` map Just (authors n') then return mempty else gg

renderCrawlErrors
  :: RuntimeAppHandler ComicInfo
renderCrawlErrors n = do
  let rowSplices = mapV (pureSplice . textSplice) $ do
        "ord" ## intToText . ord
        "time" ## T.pack . formatTime' . time
        "url" ## archiveUrl
        "code" ## maybe "" intToText . code
        "msg" ## msg
  let renderRows = manyWithSplices runChildren rowSplices $ crawlErrors <$> n
  inner <- withLocalSplices ("rows" ## renderRows) mempty runChildren
  flip bindLater n $ \info ->
    if V.null $ crawlErrors info then return mempty else codeGen inner

comicInfoSplices
  :: AppInit
  -> Splices (RuntimeAppHandler ComicInfo)
comicInfoSplices ini = do
  "comicInfo" ## const $ runChildren
  "banner" ## renderBanner
  "title" ## pureSplice . textSplice $ HTML.text . title
  "readersLink" ## renderReadersLink
  "subscriptions" ## pureSplice . textSplice $ intToText . readers
  "hasFragments" ## renderHasFragments
  "fragmentCount" ## pureSplice . textSplice $ intToText . fragmentCount
  "pageCount" ## deferManyElse runChildren (pureSplice . textSplice $ intToText) .
    fmap pageCount
  "ifKnowPages" ## renderIfKnowPages
  "homepage" ## pureSplice . textSplice $ homepage  -- TODO: Escape?
  "homepageText" ## pureSplice . textSplice $ HTML.text . homepage
  "ifExternLinks" ## renderIfExternLinks
  "ifHasDates" ## checkedSplice .
    fmap ((||) <$> isJust . addedOn <*> isJust . lastUpdated)
  "ifAddDate" ## renderIfAddDate
  "ifUpdateDate" ## renderIfUpdateDate
  "ifBookmarkUpdated" ## renderIfBookmarkUpdated
  "categories" ## renderCategories
  "description" ## renderDescription
  "inferredSchedule" ## renderInferredSchedule
  "hiatusStatus" ## manyWithSplices runChildren hiatusSplices .
    fmap (inferredSchedule >=> either Just (const Nothing))
  "ifAuthors" ## renderIfAuthors
  "dead" ## deferMany (withSplices runChildren deadSplices) . fmap dead
  where
    renderBanner n = manyWithSplices runChildren
      ("bannerUrl" ## pureSplice . textSplice $ id) $
      ((("/banners/" <>) <$>) . banner) <$> n
    renderReadersLink n = do
      node <- getParamNode
      noLink <- runChildren
      withLink <- runNode $ X.Element "a"
                  [("href", "readers.html?cid=${h:cid}")] $
                  X.elementChildren node
      flip bindLater n $ \info ->
        codeGen $ if publicReaders info then withLink else noLink
    renderHasFragments n = do
      x <- runChildren
      flip bindLater n $ \info ->
        if fragmentCount info > 0 then codeGen x else return mempty
    renderIfKnowPages n = do
      nodes <- X.elementChildren <$> getParamNode
      let archivePagesSplices = do
            "firstPageUrl" ## pureSplice . textSplice $ \(x, _, _) -> x
            "lastPageUrl" ## pureSplice . textSplice $ \(_, x, _) -> x
            "ifFixedHead" ## \n' -> do
              c <- runChildren
              flip bindLater n' $ \(_, _, x) ->
                if x then codeGen c else return mempty
      let nodeFilter name = runNodeList $
                            concatMap X.childNodes $
                            filter (maybe False (== name) . X.tagName) nodes
      t <- withSplices (nodeFilter "true") archivePagesSplices $
           fromJust . archivePages <$> n
      f <- nodeFilter "false"
      flip bindLater n $ \info ->
        codeGen $ if (isJust $ archivePages info) then t else f
    renderIfExternLinks n = do
      c <- withSplices runChildren ("externLink" ## renderExternLink) $
           extern <$> n
      flip bindLater n $ \info ->
        if null $ extern info then return mempty else codeGen c
    renderExternLink :: RuntimeSplice AppHandler [ExternalEntry] -> Splice AppHandler
    renderExternLink = manyWithSplices runChildren $
                       mapV (pureSplice . textSplice) $ do
                         "url" ## \n -> (base n <> urlPart n <> tailPart n)
                         "description" ## eDescription
                         "siteName" ## epediaTagName
    renderIfAddDate n =
      manyWithSplices runChildren
      ("addDate" ## pureSplice . textSplice $ T.pack . formatTime') $
      addedOn <$> n
    renderIfUpdateDate =
      manyWithSplices runChildren
      ("lastUpdateDate" ## pureSplice . textSplice $ T.pack . formatTime'') .
      fmap lastUpdated
    renderIfBookmarkUpdated =
      manyWithSplices runChildren
      ("bookmarkUpdated" ## pureSplice . textSplice $ T.pack . formatTime') .
      fmap subscribeSet
    renderCategories n =
      let splices = do
            "name" ## pureSplice . textSplice $ tagName . snd
          attrSplices = mapV (\f x _ -> f <$> x) $ do
            "description" ##
              maybe [] (\d -> [(T.pack "title", d)]) . tagDescription . snd
            "class" ## bool [(T.pack "class", "odd")] [] . fst
      in manyWith runChildren splices attrSplices $
         (zip (cycle [False, True]) . tags) <$> n
    renderDescription = pureSplice . textSplice $ description
    hiatusSplices = mapV (pureSplice . textSplice) $ do
      "stamp" ## T.pack . formatTime'' . fst
      "status" ## maybe "unknown" id . flip lookup (hiatusNames ini) . snd
    renderIfAuthors = manyWithSplices runChildren
      ("common" ## manyWithSplices runChildren
       (mapV (pureSplice . textSplice) $ do
           "cid" ## intToText . fst
           "title" ## HTML.text . snd)) . fmap authorComics
    deadSplices = do
      "removeDate" ## pureSplice . textSplice $ T.pack . formatTime' . snd
      "reason" ## pureSplice . textSplice $ fst

renderWithCidTitle
  :: Splice AppHandler
renderWithCidTitle = let splices = mapV (pureSplice . textSplice) $ do
                           "cid" ## intToText . fst
                           "title" ## HTML.text . snd
  in deferManyElse (callTemplate "/include/edit_status/_notFound")
     (withSplices runChildren splices) $
     lift $ runMaybeT $ do
  c <- snd <$> MaybeT getCid
  (c,) <$> (MaybeT $ join . hush <$> getComicTitle c)

prefetchSplices
   :: Splices (RuntimeAppHandler ComicInfo)
prefetchSplices = mapV (pureSplice . textSplice) $ do
  "title" ## HTML.text . title
  "description" ## HTML.text . simpleDescription . description
