{-# LANGUAGE OverloadedStrings #-}

-- A bit of a misnomer, may be any explanation as for why a comic
-- doesn't update anymore.  Named for its distinct usage.
module Piperka.ComicInfo.Hiatus where

import Control.Error.Util (hush)
import Control.Monad
import qualified Data.ByteString as B
import Data.Functor
import Data.List
import Data.Text (Text)
import Data.Text.Read
import Text.XmlHtml (Document)
import qualified Text.XmlHtml as X
import Text.XmlHtml (Node)

import Application

generateHiatus
  :: SiteIdentity
  -> IO (Either String [(Int, Text)])
generateHiatus site = B.readFile ("site/" <> sitePath site <> "x/hiatus.xml") <&>
  (\x -> X.parseXML "x/hiatus.xml" x >>= process)

process
  :: Document
  -> Either String [(Int, Text)]
process doc = maybe (Left "failure parsing hiatus data") return $ do
  hiatus <- find ((== Just "hiatus") . X.tagName) $ X.docContent doc
  sequence $ map mkReason $ X.childElements hiatus

mkReason :: Node -> Maybe (Int, Text)
mkReason x = (guard $ X.tagName x == Just "reason") >>
  (,)
  <$> (fst <$> (join $ hush . decimal <$> (lookup "id" $ X.elementAttrs x)))
  <*> (X.nodeText <$> (find ((== Just "name") . X.tagName) $ X.childNodes x))
