module Piperka.ComicInfo.Xslt where

import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8')
import System.Exit
import System.Process hiding (readCreateProcessWithExitCode)
import System.Process.ByteString

import Application
import Piperka.File

generatePart
  :: SiteIdentity
  -> String
  -> IO (Either String Text)
generatePart site x = generatePart' site x x

generatePart'
  :: SiteIdentity
  -> String
  -> String
  -> IO (Either String Text)
generatePart' site x y = do
  file <- searchSiteFile site $ "x/" <> y <> ".xml"
  let cfg = System.Process.shell $
        "xsltproc --param as-part 1 xslt/" <> x <> ".xslt " <> file <>
        " | xpath -e '//body/*/' /dev/stdin 2>/dev/null"
  status <- readCreateProcessWithExitCode cfg mempty
  case status of
    (ExitSuccess, out, _) -> return $! either (Left . show) Right $ decodeUtf8' out
    (ExitFailure i, _, err) -> return $! Left $ show i ++ " " ++ show err
                               ++ " (make sure both xsltproc and xpath are installed)"
