{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.ComicInfo.Statements where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad
import Data.Bifunctor
import Data.Time.LocalTime
import Data.Int
import Data.List (foldl')
import Data.Maybe (fromJust)
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Statement
import Network.IP.Addr

import Application (UserID)
import Piperka.ComicInfo.Types
import qualified Piperka.Encoders as EN

type ComicInfoSeed = Vector CrawlError
                     -> [ExternalEntry]
                     -> ComicInfo

decodeSchedule :: DE.Row (Maybe (Either Int Schedule))
decodeSchedule = do
  reason <- DE.nullableColumn $ fromIntegral <$> DE.int2
  schedule <- DE.nullableColumn $ DE.array $
    DE.dimension replicateM $ DE.element $ fromIntegral <$> DE.int2
  weekly <- DE.nullableColumn $ fromIntegral <$> DE.int2
  monthly <- DE.nullableColumn $ fromIntegral <$> DE.int2
  return $
    (Left <$> reason) <|>
    (schedule >>= (>>) <$> guard . not . null <*> return . Right . Schedule) <|>
    (Right . Weekly <$> weekly) <|>
    (Right . Monthly <$> monthly)

decodeComicInfo :: ([Int] -> [ComicTag]) -> DE.Row ComicInfoSeed
decodeComicInfo taglookup =
  ComicInfo
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.text
  <*> DE.column DE.text
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.bool
  <*> (liftA (fmap fromIntegral) $ DE.nullableColumn DE.int4)
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> liftA (\(a,b,c) -> do
                a' <- a; b' <- b; c' <- c
                return (a',b',c')) ((,,)
                                    <$> DE.nullableColumn DE.text
                                    <*> DE.nullableColumn DE.text
                                    <*> DE.nullableColumn DE.bool)
  <*> (liftA (fmap $ localTimeToUTC utc) $ DE.nullableColumn DE.timestamp)
  <*> DE.nullableColumn (localTimeToUTC utc <$> DE.timestamp)
  <*> DE.nullableColumn DE.bool
  <*> DE.nullableColumn (localTimeToUTC utc <$> DE.timestamp)
  <*> DE.column DE.bool
  <*> DE.column DE.bool
  <*> DE.nullableColumn (DE.composite ((,)
                                      <$> DE.field DE.text
                                      <*> (liftA (localTimeToUTC utc) $
                                           DE.field DE.timestamp)))
  <*> DE.nullableColumn DE.text
  <*> (liftA (maybe "" id) $ DE.nullableColumn DE.text)
  <*> (do
          schedule <- decodeSchedule
          stamp <- DE.nullableColumn (localTimeToUTC utc <$> DE.timestamp)
          -- Invariant: hiatus code is defined iff hiatus stamp is
          return $ first (fromJust stamp,) <$> schedule)
  <*> (DE.nullableColumn $ DE.array $ DE.dimension V.replicateM $
       DE.element $ DE.composite $ (,)
       <$> DE.field (fromIntegral <$> DE.int4)
       <*> DE.field DE.text)
  <*> (DE.column (DE.array $ DE.dimension replicateM $ DE.element DE.int4))
  <*> (liftA (taglookup . map fromIntegral) $
       DE.column (DE.array $ DE.dimension replicateM $ DE.element DE.int4))

decodeDeadComicInfo
  :: ([Int] -> [ComicTag])
  -> DE.Row ComicInfo
decodeDeadComicInfo taglookup =
  decodeComicInfo taglookup
  <*> pure mempty
  <*> pure mempty

decodeCrawlError :: DE.Row CrawlError
decodeCrawlError =
  CrawlError
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> (liftA (localTimeToUTC utc) $ DE.column DE.timestamp)
  <*> DE.column DE.text
  <*> (liftA (fmap fromIntegral) $ DE.nullableColumn DE.int4)
  <*> DE.column DE.text

decodeExternalEntry
  :: (Int -> Text -> Maybe ExternalEntry)
  -> DE.Row (Maybe ExternalEntry)
decodeExternalEntry extlookup =
  extlookup
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> DE.column DE.text

comicInfoFetch
  :: ([Int] -> [ComicTag])
  -> Statement (Maybe UserID, Int32) (Maybe ComicInfoSeed)
comicInfoFetch taglookup =
  Statement sql (contrazip2 (EN.nullableParam EN.uid) (EN.param EN.int4))
  (DE.rowMaybe $ decodeComicInfo taglookup) True
  where
    sql = "SELECT c.cid, title, homepage, readers, \
          \c.cid IN (SELECT cid FROM subscriptions AS subs \
          \ LEFT JOIN (SELECT cid, interest AS uid, interest \
          \ FROM permitted_interest WHERE uid=u.uid) AS interest USING (cid, uid) \
          \ WHERE uid IN (SELECT uid FROM users WHERE countme AND privacy=3) \
          \  OR interest IS NOT NULL) as public_readers, \
          \m.ord+1, (SELECT COUNT(*) FROM page_fragments WHERE cid=c.cid), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND ord=0 LIMIT 1), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid ORDER BY ord DESC LIMIT 1), \
          \fixed_head.cid IS NOT NULL, \
          \added_on, NULLIF(last_updated, '-infinity'), \
          \s.cid IS NOT NULL AS subscribed, s.last_set, \
          \disinterest.cid IS NOT NULL, \
          \mapped, null AS dead, file AS banner, description, \
          \hiatus.code, CASE WHEN hiatus.code IS NULL THEN schedule ELSE NULL END, \
          \weekly_updates, monthly_updates, hiatus.stamp, \
          \(SELECT array_agg((cid, title)) FROM (SELECT DISTINCT b.cid, title, ordering_form(title) \
          \ FROM author_comics AS a JOIN author_comics AS b USING (auid) \
          \ JOIN comics ON (b.cid=comics.cid) WHERE a.cid=$2 AND b.cid<>a.cid \
          \ ORDER BY ordering_form(title)) AS x), \
          \COALESCE((SELECT array_agg(auid) FROM author JOIN author_comics USING (auid) \
          \  WHERE cid=$2), '{}'), \
          \COALESCE((SELECT array_agg(tagid) FROM comic_tag WHERE cid=c.cid), '{}') AS tags \
          \FROM comics AS c LEFT JOIN banners USING (cid) \
          \LEFT JOIN inferred_schedule USING (cid) \
          \LEFT JOIN (SELECT cid, code, stamp FROM hiatus_status WHERE active) AS hiatus USING (cid) \
          \LEFT JOIN users AS u ON (u.uid=$1) \
          \LEFT JOIN disinterest USING (uid, cid) \
          \LEFT JOIN crawler_config USING (cid) \
          \LEFT JOIN subscriptions AS s ON s.cid=c.cid and s.uid=u.uid \
          \LEFT JOIN fixed_head ON fixed_head.cid=c.cid \
          \LEFT JOIN max_update_ord AS m ON c.cid=m.cid \
          \WHERE c.cid=$2"

deadInfoFetch
  :: ([Int] -> [ComicTag])
  -> Statement (Maybe UserID, Int32) (Maybe ComicInfo)
deadInfoFetch taglookup =
  Statement sql (contrazip2 (EN.nullableParam EN.uid) (EN.param EN.int4))
  (DE.rowMaybe $ decodeDeadComicInfo taglookup) True
  where
    sql = "SELECT c.cid, title, homepage, 0 AS readers, false AS public_readers, \
          \m.ord+1, (SELECT count(*) FROM page_fragments WHERE cid=c.cid), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND ord=0 LIMIT 1), \
          \(SELECT url_base||name||url_tail FROM updates WHERE cid=c.cid AND name IS NOT NULL ORDER BY ord DESC LIMIT 1), \
          \fixed_head.cid IS NOT NULL, \
          \added_on, NULL, \
          \s.cid IS NOT NULL AS subscribed, s.last_set, \
          \false, false, (reason, removed_on), file AS banner, description, NULL, \
          \NULL, NULL, NULL, NULL, \
          \(SELECT array_agg((cid, title)) FROM (SELECT DISTINCT b.cid, title, ordering_form(title) \
          \ FROM author_comics AS a JOIN author_comics AS b USING (auid) \
          \ JOIN all_comics ON (b.cid=all_comics.cid) WHERE a.cid=$2 AND b.cid<>a.cid \
          \ ORDER BY ordering_form(title)) AS x), \
          \'{}'::int[], \
          \COALESCE((SELECT array_agg(tagid) FROM comic_tag WHERE cid=c.cid), '{}') AS tags \
          \FROM graveyard AS c LEFT JOIN banners USING (cid) \
          \LEFT JOIN users AS u ON (u.uid=$1) \
          \LEFT JOIN subscriptions AS s ON s.cid=c.cid AND s.uid=u.uid \
          \LEFT JOIN fixed_head ON fixed_head.cid=c.cid \
          \LEFT JOIN max_update_ord AS m ON c.cid=m.cid \
          \WHERE c.cid=$2"

crawlErrorFetch
  :: Statement Int32 (Vector CrawlError)
crawlErrorFetch =
  Statement sql (EN.param EN.int4) (DE.rowVector decodeCrawlError) True
  where
    sql = "SELECT ord, stamp, url, http_code, coalesce(http_message, '') \
           \FROM crawl_error WHERE cid=$1 AND stamp > date 'now' - integer '30' \
           \ORDER BY errid DESC LIMIT 5"

externalEntryFetch
  :: (Int -> Text -> Maybe ExternalEntry)
  -> Statement Int32 [Maybe ExternalEntry]
externalEntryFetch extlookup =
  Statement sql (EN.param EN.int4)
  (DE.rowList $ decodeExternalEntry extlookup) True
  where
    sql = "SELECT epid, entry FROM external_entry WHERE cid=$1 ORDER BY epid"

isComicDeadFetch
  :: Statement Int32 (Bool, Maybe (Int, Text, Bool))
isComicDeadFetch =
  Statement sql (EN.param EN.int4) (DE.singleRow $ decode) True
  where
    decode = (,)
      <$> (DE.column DE.bool)
      <*> (DE.nullableColumn $ DE.composite $ (,,)
            <$> (fromIntegral <$> DE.field DE.int4)
            <*> DE.field DE.text
            <*> DE.field DE.bool)
    sql = "SELECT $1 IN (SELECT cid FROM graveyard), (\
          \SELECT (cid, title, cid IN (SELECT cid FROM graveyard)) \
          \FROM merged JOIN all_comics USING (cid) WHERE merged_from=$1)"

relatedFetch
  :: Statement (Maybe UserID, [Int32]) (Vector (Text, Bool))
relatedFetch =
  Statement sql encoder (DE.rowVector decoder) True
  where
    decoder = (,)
      <$> DE.column DE.text
      <*> DE.column DE.bool
    encoder = contrazip2 (EN.nullableParam EN.uid)
      (EN.param . EN.array . EN.dimension foldl' . EN.element $ EN.int4)
    sql = "SELECT title, \
          \COALESCE(($1, cid) IN (SELECT uid, cid FROM disinterest), false) \
          \FROM unnest($2) WITH ORDINALITY related(cid, ord) JOIN comics USING (cid) \
          \ORDER BY ord"

comicTitleFetch
  :: Statement Int32 (Maybe Text)
comicTitleFetch =
  Statement sql (EN.param EN.int4) (DE.rowMaybe $ DE.column DE.text) True
  where
    sql = "SELECT title FROM comics WHERE cid=$1"

saveTitleHistory
  :: Statement (Int32, NetAddr IP, UserID) ()
saveTitleHistory =
  Statement sql
  (contrazip3 (EN.param EN.int4) (EN.param EN.inet) (EN.param EN.uid))
  DE.unit True
  where
    sql = "INSERT INTO comic_title_history (uid, cid, from_ip, old_title) VALUES \
          \($3, $1, $2, (SELECT title FROM comics WHERE cid=$1))"

updateTitle
  :: Statement (Int32, Text) Bool
updateTitle = (> 0) <$>
  Statement sql (contrazip2 (EN.param EN.int4) (EN.param EN.text)) DE.rowsAffected True
  where
    sql = "UPDATE comics SET title=$2 WHERE cid=$1"

updateScheduleCrawl
  :: Statement Int32 Bool
updateScheduleCrawl = (> 0) <$>
  Statement sql (EN.param EN.int4) DE.rowsAffected True
  where
    sql = "UPDATE crawler_config SET update_score=60 WHERE cid=$1"
