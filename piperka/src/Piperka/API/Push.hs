{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.Push (updatePush) where

import Contravariant.Extras.Contrazip
import Control.Error.Util (hush)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8', encodeUtf8)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql
import Web.WebPush

import Application hiding (auth)
import Piperka.API.Common
import qualified Piperka.Encoders as EN
import Piperka.Util

updatePush
  :: VAPIDKeys
  -> AppHandler ()
updatePush key = route
  [ ("key", getPublicKey key)
  , ("subscribe", subscribe)
  , ("unsubscribe", unsubscribe)
  ]

getPublicKey
  :: VAPIDKeys
  -> AppHandler ()
getPublicKey key = do
  let publ = vapidPublicKeyBytes key
  modifyResponse (setHeader "Content-Type" "application/octet-stream")
  writeBS $ B.pack publ

getRequiredParam
  :: ByteString
  -> AppHandler Text
getRequiredParam t = do
  x <- maybe (simpleFail 400 $ encodeUtf8 $ T.pack $
              "Required parameter "<> show t <>" missing") return =<<
       getParamText t
  when (T.length x > 500) $
    simpleFail 400 $ encodeUtf8 $ T.pack $ show t <> " length too long"
  return x

subscribe
  :: AppHandler ()
subscribe = do
  (endpoint, auth, p256dh) <-
    (,,)
    <$> getRequiredParam "endpoint"
    <*> getRequiredParam "auth"
    <*> getRequiredParam "p256dh"
  rq <- getRequest
  let agent = (hush . decodeUtf8' =<<) $ getHeader "User-Agent" rq
  runUserQueries $ \u -> do
    void $ lift validateCsrf
    ExceptT $ run $ statement (uid u, endpoint, auth, p256dh, agent) $ Statement
      "INSERT INTO user_push_notification (uid, endpoint, auth, p256dh, agent) \
      \VALUES ($1, $2, $3, $4, $5) ON CONFLICT (uid, endpoint) DO NOTHING"
      (contrazip5
       (EN.param EN.uid)
       (EN.param EN.text)
       (EN.param EN.text)
       (EN.param EN.text)
       (EN.nullableParam EN.text)) DE.unit True
  writeBS "{\"ok\": true}"

unsubscribe
  :: AppHandler ()
unsubscribe = do
  endpoint <- getRequiredParam "endpoint"
  runUserQueries $ \u -> do
    void $ lift validateCsrf
    ExceptT $ run $ statement (uid u, endpoint) $ Statement
      "DELETE FROM user_push_notification WHERE uid=$1 AND endpoint=$2"
      (contrazip2 (EN.param EN.uid) (EN.param EN.text)) DE.unit True
  writeBS "{\"ok\": true}"
