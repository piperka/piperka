{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.SortedList where

import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common

data SortedCid = Single Int | Grouped (Vector Int)

instance ToJSON SortedCid where
  toJSON (Single cid) = toJSON cid
  toJSON (Grouped xs) = toJSON xs

data SortType = RecentUpdate | TopOrder | DateAdded

parseSort
  :: ByteString
  -> Maybe SortType
parseSort "1" = Just TopOrder
parseSort "3" = Just RecentUpdate
parseSort "4" = Just DateAdded
parseSort _ = Nothing

sortedList
  :: ByteString
  -> AppHandler ()
sortedList hostname = do
  modifyResponse $
    addHeader "Access-Control-Allow-Origin" ("http://" <> hostname)
  sortType <-
    getParam "sort" >>=
    (maybe
     (simpleFail 400 "Missing or invalid required parameter sort") return .
      (parseSort =<<))
  runQueries $
    (ExceptT $ run $ statement () $ Statement (sql sortType)
      (EN.unit) (DE.rowVector decoder) True) >>=
    lift . writeLBS . encode
  where
    decoder = do
      v <- DE.column (DE.array $ DE.dimension V.replicateM
                      (DE.element $ fromIntegral <$> DE.int4))
      return $ (if V.length v == 1 then Single . V.unsafeHead else Grouped) v
    sql TopOrder =
      "SELECT array_agg(cid) FROM comics \
      \GROUP BY readers ORDER BY readers DESC"
    sql RecentUpdate =
      "SELECT array_agg(cid) FROM comics \
      \LEFT JOIN crawler_config USING (cid) \
      \GROUP BY last_updated ORDER BY last_updated DESC NULLS LAST"
    sql DateAdded =
      "SELECT array_agg(cid) FROM comics \
      \GROUP BY added_on ORDER BY added_on DESC NULLS LAST"
