{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.Author where

import Contravariant.Extras.Contrazip
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Foldable (foldl')
import Data.Functor.Contravariant
import Data.ByteString.Char8 (readInt)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql (run)

import Application
import Piperka.API.Common
import Piperka.Util

editAuthor
  :: AppHandler ()
editAuthor = do
  name <- maybe (simpleFail 400 "Author name empty") return =<<
    (runMaybeT $ do
        name <- T.strip <$> (MaybeT $ getParamText "name")
        guard $ not $ T.null name
        return name)
  rq <- getRequest
  cids <- maybe (simpleFail 400 "Cid parsing failed") return $
    ((return $ maybe [] id $ rqParam "cid" rq) >>=
     mapM (fmap fst . readInt))
  runModQueries Moderator $ const $ do
    lift validateCsrf
    (($ cids) . ($ name) . maybe createAuthor updateAuthor) $
      fromIntegral . fst <$> (readInt =<< (join $ listToMaybe <$> rqParam "auid" rq))

encodeAuthor
  :: EN.Params (Text, [Int])
encodeAuthor = contrazip2
  (EN.param EN.text)
  (EN.param $ EN.array $ EN.dimension foldl' $ EN.element $ fromIntegral >$< EN.int4)

createAuthor
  :: Text
  -> [Int]
  -> ExceptT QueryError AppHandler ()
createAuthor name cids = do
  auid <- ExceptT $ run $ statement (name, cids) $ Statement sql
          encodeAuthor
          (DE.singleRow $ DE.column DE.int4) True
  lift $ writeLBS $ encode $ object [ "auid" .= auid ]
  where
    sql = "WITH new_author AS (\
          \INSERT INTO author (name) VALUES ($1) RETURNING auid \
          \), del AS (\
          \DELETE FROM author_comics USING new_author \
          \WHERE author_comics.auid = new_author.auid \
          \), ins AS (\
          \INSERT INTO author_comics (auid, cid) \
          \SELECT auid, unnest($2) FROM new_author \
          \) SELECT auid FROM new_author"

updateAuthor
  :: Int
  -> Text
  -> [Int]
  -> ExceptT QueryError AppHandler ()
updateAuthor auid name cids = do
  ExceptT $ run $ statement (fromIntegral auid, (name, cids)) $ Statement sql
    (contrazip2 (EN.param EN.int4) encodeAuthor) DE.unit True
  lift $ writeLBS $ encode $ object [ "auid" .= auid ]
  where
    sql = "WITH del AS (\
          \DELETE FROM author_comics WHERE auid = $1 \
          \), upd AS (\
          \UPDATE author SET name=$2 WHERE auid = $1 \
          \) INSERT INTO author_comics (auid, cid) SELECT $1, unnest($3)"
