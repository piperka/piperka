{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

{-# OPTIONS -Wno-unused-top-binds #-}

module Piperka.API.UserPrefs (userPrefs) where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Error.Util (bool, hoistMaybe)
import Control.Monad (void, when)
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Functor.Contravariant
import Data.Maybe (isJust)
import Data.Text (Text)
import Data.UUID
import Data.Vector (Vector)
import GHC.Generics
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql

import Application hiding (rows)
import qualified Piperka.Decoders as DE
import qualified Piperka.Encoders as EN
import Piperka.API.Common
import Piperka.Action.Statements (refreshCache)
import Piperka.Subscription.Types

data UserInfo = UserInfo {
    name :: Text
  , new_windows :: Bool
  , rows :: Int
  , cols :: Int
  , subscriptions :: Vector Subscription
  } deriving (Generic)

data CountsMode = GetTotal | GetTotalHidden

decodeUserPrefs
  :: DE.Row (UserID, Vector Subscription -> UserInfo)
decodeUserPrefs =
  (,)
  <$> DE.column DE.uid
  <*> (UserInfo
       <$> DE.column DE.text
       <*> DE.column DE.bool
       <*> (liftA fromIntegral $ DE.column DE.int4)
       <*> (liftA fromIntegral $ DE.column DE.int4)
      )

instance ToJSON UserInfo where
  toEncoding = genericToEncoding defaultOptions

userPrefs
  :: ByteString
  -> AppHandler ()
userPrefs hostname = do
  modifyResponse $
    addHeader "Access-Control-Allow-Origin" ("http://" <> hostname) .
    addHeader "Access-Control-Allow-Credentials" "true"
  bookmark <- (decodeBookmarkSet =<<) <$> getParam "bookmark[]"
  maybe readPrefs (runUserQueries . setBookmark) bookmark
  where
    readPrefs :: AppHandler ()
    readPrefs = do
      ses <- (fromASCIIBytes . cookieValue =<<) <$>
             getCookie sessionCookieName
      void $ runMaybeT $
        (hoistMaybe ses >>= lift . getFullPrefs)
        <|> (lift $ simpleFail 403 "User authentication failed")
    getFullPrefs :: UUID -> AppHandler ()
    getFullPrefs ses = runQueries $ do
      (u, f) <-
        maybe (lift $ simpleFail 403 "User authentication failed") return =<<
        (ExceptT $ run $ statement ses $ Statement sql1
          (EN.param EN.uuid) (DE.rowMaybe decodeUserPrefs) True)
      lift . writeLBS . encode . f =<<
        (ExceptT $ run $ statement u $ Statement sql2
         (EN.param EN.uid) (DE.rowVector decodeSubscription) True)
    sql1 = "SELECT uid, name, new_windows, display_rows, display_columns \
           \FROM recover_session($1) JOIN users USING (uid)"
    sql2 = "SELECT cid, subscriptions.ord+\
           \CASE WHEN subscriptions.subord > COALESCE((SELECT MAX(subord) FROM page_fragments \
           \WHERE cid=subscriptions.cid AND ord=subscriptions.ord), 0) \
           \THEN 1 ELSE 0 END, \
           \max_update_ord.ord, max_update_ord.subord, num, till_scheduled \
           \FROM comics LEFT JOIN nearest_infer USING (cid) JOIN subscriptions USING (cid) \
           \JOIN max_update_ord USING (cid) \
           \JOIN comic_remain_frag_cache USING (uid, cid) WHERE uid=$1 \
           \ORDER BY Num DESC, ordering_form(title)"

setBookmark
  :: (Int, BookmarkSet)
  -> MyData
  -> UserQueryHandler ()
setBookmark (c, bookmark) p = do
  lift validateCsrf
  getUnread <- lift $ (parseUnread =<<) <$> getParam "getunread"
  getStatRow <- lift $ maybe False (== "1") <$> getParam "getstatrow"
  let u = uid p
  let cid = fromIntegral c
  ord <- case bookmark of
    Page o' ->
      let ord = fromIntegral $ bool 0 o' (o' > 0)
      in (ExceptT $ run $ statement (u, cid, ord) $
           Statement sql1
           (contrazip3
             (EN.param EN.uid)
             (EN.param EN.int4)
             (EN.param EN.int4))
           DE.unit True) >> (return $ Just ord)
    Max ->
      ExceptT $ run $ statement (u, cid) $
      Statement sql2
      (contrazip2 (EN.param EN.uid) (EN.param EN.int4))
      (DE.singleRow $ liftA Just $ DE.column DE.int4) True
    Del ->
      ((ExceptT $ run $ statement (u, cid) $
        Statement sql3
        (contrazip2 (EN.param EN.uid) (EN.param EN.int4))
        DE.unit True) >> return Nothing)
  let summary totalMode =
        (\(totalNew, newIn) ->
            (("total_new" .= totalNew :) . ("new_in" .= newIn :))) <$>
        (ExceptT $ run $ statement u $
         Statement (case totalMode of
                      GetTotal -> sql4
                      GetTotalHidden -> sql4') (EN.param EN.uid)
         (DE.singleRow $ (,) <$> DE.column DE.int4 <*> DE.column DE.int4) True)
      statRow =
        (\s -> ("stat_row" .= s :)) <$>
        (ExceptT $ run $ statement (u, c) $
         Statement sql5 (contrazip2 (EN.param EN.uid)
                         (fromIntegral >$< EN.param EN.int4))
         (DE.singleRow decodeSubscription) True)
  -- Manipulating the subscriptions table directly necessiates a cache
  -- refresh.
  when (isJust getUnread || getStatRow) $ ExceptT $ run $ refreshCache u
  addStats <- maybe (return id) summary getUnread
  addStatRow <- bool (return id) statRow getStatRow
  lift $ writeLBS $ encode $ object $ addStatRow . addStats . maybe
    id (\o -> ("ord" .= o :)) ord $ ["ok" .= True]
  where
    sql1 = "INSERT INTO subscriptions (uid, cid, ord, subord) \
           \SELECT $1, cid, ord, CASE WHEN $3 = 0 THEN 0 ELSE \
           \COALESCE((SELECT MAX(subord)+1 \
           \FROM page_fragments WHERE cid=updates.cid \
           \AND ord=updates.ord), 1) END FROM updates WHERE cid=$2 AND \
           \ord = CASE WHEN $3 = 0 THEN 0 ELSE $3-1 END \
           \ON CONFLICT (uid, cid) \
           \DO UPDATE SET ord = EXCLUDED.ord, subord = EXCLUDED.subord"
    sql2 = "INSERT INTO subscriptions (uid, cid, ord, subord) \
           \SELECT $1, cid, ord, subord+1 \
           \FROM max_update_ord WHERE cid=$2 \
           \ON CONFLICT (uid, cid) \
           \DO UPDATE SET ord = EXCLUDED.ord, subord = EXCLUDED.subord \
           \RETURNING ord+1"
    sql3 = "DELETE FROM subscriptions WHERE uid=$1 AND cid=$2"
    sql4 = "SELECT total_new, new_in FROM user_unread_stats($1)"
    sql4' = "SELECT total_new_hide, new_in_hide FROM user_unread_stats($1)"
    sql5 = "SELECT cid, subscriptions.ord+\
           \CASE WHEN subscriptions.subord > COALESCE((SELECT MAX(subord) FROM page_fragments \
           \WHERE cid=subscriptions.cid AND ord=subscriptions.ord), 0) \
           \THEN 1 ELSE 0 END, \
           \max_update_ord.ord, max_update_ord.subord, num, till_scheduled \
           \FROM subscriptions JOIN comic_remain_frag_cache USING (uid,cid) \
           \JOIN max_update_ord USING (cid) \
           \LEFT JOIN nearest_infer USING (cid) WHERE uid=$1 AND cid=$2"
    parseUnread "1" = Just GetTotal
    parseUnread "2" = Just GetTotalHidden
    parseUnread _ = Nothing
