{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ViewPatterns #-}

module Piperka.API.Export (exportUserData) where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Textual
import Data.Vector (Vector)
import qualified Data.Vector as V
import GHC.Generics
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (sql, run)
import Hasql.Statement
import Network.IP.Addr
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.Auth.Types (ProviderID)
import Piperka.API.Common
import qualified Piperka.Decoders as DE
import qualified Piperka.Encoders as EN
import Piperka.OAuth2.Providers

data Export = Export
  { name :: Text
  , email :: Maybe Text
  , created :: Maybe Int
  , lastLogin :: Maybe Int
  , writeup :: Maybe Text
  , subscriptions :: Vector (Int, Int, Int, Maybe Int)
  , oauthLogins :: [(Text, Text)]
  , bookmarkLog :: Vector (Int, Maybe Int, Maybe Int, Maybe Int, Maybe Text, Maybe Text)
  , followers :: Vector Text
  , followees :: Vector Text
  , authorName :: Maybe Text
  , author :: Vector Int
  } deriving (Show, Generic)

instance ToJSON Export where
  toEncoding = genericToEncoding defaultOptions

exportUserData
  :: AppHandler ()
exportUserData = writeLBS . encode =<< runUserQueries . export =<< getProviders

export
  :: [(ProviderID, (Text, Text))]
  -> MyData
  -> ExceptT QueryError AppHandler Export
export providers (uid -> uid) = do
  ExceptT $ run $ statement uid $ Statement sql (EN.param EN.uid) (DE.singleRow decoder) True
    where
      sql = "SELECT users.name, email, extract(epoch FROM created_on), \
            \extract(epoch FROM last_login), writeup, \
            \subs, oauths, bookmarks, followers, followees, author.name, authored \
            \FROM users \
            \LEFT JOIN author USING (auid) \
            \CROSS JOIN (SELECT array_agg(row(cid, s.ord, s.subord, \
            \  extract(epoch FROM used_on))) AS subs \
            \ FROM subscriptions AS s LEFT JOIN recent USING (uid, cid) \
            \ WHERE uid=$1) AS subs \
            \CROSS JOIN (SELECT array_agg(row(opid, identification)) AS oauths \
            \ FROM login_method_oauth2 WHERE uid=$1) AS oauths \
            \CROSS JOIN (SELECT array_agg(row(extract(epoch FROM at_date), \
            \  cid, ord, subord, url, host) ORDER BY at_date DESC) AS bookmarks \
            \ FROM bookmarking_log WHERE uid=$1) AS bookmarks \
            \CROSS JOIN (SELECT array_agg(name) AS followers \
            \ FROM follower JOIN users ON follower.interest=users.uid \
            \ WHERE follower.uid=$1 AND privacy >= 2) AS followers \
            \CROSS JOIN (SELECT array_agg(name) AS followees \
            \ FROM follower JOIN users USING (uid) \
            \ WHERE interest=$1 AND privacy >= 2) AS followees \
            \CROSS JOIN (SELECT array_agg(cid) AS authored \
            \ FROM author_comics JOIN users USING (auid) WHERE uid=$1) AS authored \
            \WHERE uid=$1"
      decoder = Export
        <$> DE.column DE.text
        <*> DE.nullableColumn DE.text
        <*> DE.nullableColumn dateInt
        <*> DE.nullableColumn dateInt
        <*> DE.nullableColumn DE.text
        <*> (maybe mempty id <$>
             (DE.nullableColumn $ DE.array $ DE.dimension V.replicateM $ DE.element $
              DE.composite $ (,,,)
              <$> DE.field (fromIntegral <$> DE.int4)
              <*> DE.field (fromIntegral <$> DE.int4)
              <*> DE.field (fromIntegral <$> DE.int4)
              <*> DE.nullableField dateInt
             ))
        <*> (maybe mempty catMaybes <$>
             (DE.nullableColumn $ DE.array $ DE.dimension replicateM $ DE.element $
              runMaybeT ((,) <$> MaybeT fst <*> lift snd) <$>
              (DE.composite $ (,)
               <$> (fmap fst . flip lookup providers <$> DE.field DE.opid)
               <*> DE.field DE.text
              )))
        <*> (maybe mempty id <$>
             (DE.nullableColumn $ DE.array $ DE.dimension V.replicateM $ DE.element $
              DE.composite $ (,,,,,)
              <$> DE.field dateInt
              <*> DE.nullableField (fromIntegral <$> DE.int4)
              <*> DE.nullableField (fromIntegral <$> DE.int4)
              <*> DE.nullableField (fromIntegral <$> DE.int4)
              <*> DE.nullableField DE.text
              <*> DE.nullableField (Data.Textual.toText . netHost <$> DE.inet)
             ))
        <*> (maybe mempty id <$>
             (DE.nullableColumn $ DE.array $ DE.dimension V.replicateM $ DE.element DE.text))
        <*> (maybe mempty id <$>
             (DE.nullableColumn $ DE.array $ DE.dimension V.replicateM $ DE.element DE.text))
        <*> DE.nullableColumn DE.text
        <*> (maybe mempty id <$>
             (DE.nullableColumn $ DE.array $ DE.dimension V.replicateM $
              DE.element (fromIntegral <$> DE.int4)))
      dateInt = (`div` 1000) . fromIntegral <$> DE.int8
