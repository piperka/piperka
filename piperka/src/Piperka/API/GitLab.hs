{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}

module Piperka.API.GitLab (gitlabWebhook) where

import Control.Concurrent
import Control.Monad
import Control.Monad.Trans
import Data.ByteString (ByteString)
import Data.ByteString.UTF8 (fromString)
import Snap
import Web.Hook.GitLab

import Application
import Piperka.API.Common

gitlabWebhook
  :: Chan AnnounceEvent
  -> ByteString
  -> AppHandler ()
gitlabWebhook ann secret = getHeader "X-Gitlab-Token" <$> getRequest >>= \secret2 -> do
  when (Just secret /= secret2) $
    simpleFail 403 "Gitlab Token mismatch"
  Web.Hook.GitLab.parse <$> readRequestBody 100000 >>=
    either
    (\e -> logError (fromString e) >> simpleFail 400 "Webhook response parse failed")
    (\case
        EventPush (Push{..}) -> do
          liftIO $ writeChan ann $ RepoUpdated (repoName pushRepository) $
            map (\Commit{..} -> (commitMessage, commitUrl)) pushCommits
          modifyResponse $ setResponseStatus 204 "Accepted"
        _ -> simpleFail 400 "Not a push event"
    )
