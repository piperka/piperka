{-# LANGUAGE OverloadedStrings #-}
module Piperka.API.Random where

import Contravariant.Extras.Contrazip (contrazip2)
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Functor.Contravariant
import Data.Text.Encoding (encodeUtf8)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql
import System.Random (randomIO)

import Application
import Piperka.API.Common
import Piperka.Util (getCid)

randomPage
  :: AppHandler ()
randomPage = maybe anyRandom cidRandom =<< fmap snd <$> getCid

anyRandom
  :: AppHandler ()
anyRandom =
  runQueries (ExceptT $ run $ statement () $
              Statement sql
              EN.unit (DE.singleRow $ DE.column $ fromIntegral <$> DE.int4) True) >>=
  cidRandom
  where
    sql = "SELECT cid FROM comics ORDER BY random() LIMIT 1"

cidRandom
  :: Int
  -> AppHandler ()
cidRandom cid = do
  random <- liftIO randomIO
  url <- runQueries (ExceptT $ run $ statement (cid, random) $
                     Statement sql
                     (contrazip2 (EN.param $ fromIntegral >$< EN.int4) (EN.param EN.int8))
                     (DE.singleRow $ DE.column DE.text) True)
  redirect' (encodeUtf8 url) 303
  where
    sql = "SELECT CASE WHEN name IS NULL THEN coalesce(fixed_head, homepage) \
          \ ELSE url_base||name||coalesce(url_tail,'') END \
          \FROM updates JOIN comics USING (cid) \
          \WHERE ord=(SELECT abs($2 % (MAX(ord)+1)) FROM updates WHERE cid=$1) \
          \AND cid=$1 LIMIT 1"
