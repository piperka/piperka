{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.Recommend where

import Control.Lens
import Control.Monad
import Control.Monad.Trans (liftIO)
import Data.Aeson
import Data.ByteString (ByteString)
import Snap
import Snap.Snaplet.CustomAuth

import Application
import Piperka.API.Common
import Piperka.Recommend
import Piperka.Recommend.Thread (getRelated, readModel)
import Piperka.Util (getCid)

recommendList
  :: ByteString
  -> AppHandler ()
recommendList hostname = do
  modifyResponse $
    addHeader "Access-Control-Allow-Origin" ("http://" <> hostname) .
    addHeader "Access-Control-Allow-Credentials" "true"
  onlyUnsub <- maybe True (/= "1") <$> getParam "getSubbed"
  withTop apiAuth (recoverSession >> currentUser) >>=
    maybe (simpleFail 403 "User authentication failed")
    (writeLBS . encode <=<
     fmap (either (const mempty) id) . recommends onlyUnsub . uid)

recommendDims
  :: AppHandler ()
recommendDims = dimensions >>= writeLBS . encode

related
  :: AppHandler ()
related = do
  model <- maybe (simpleFail 500 "server started without recommendations") (liftIO . readModel) =<< view recommendHandle
  cid <- getCid >>= maybe (simpleFail 400 "cid required") (return . fromIntegral . snd)
  writeLBS . encode . take 100 $ getRelated 100 cid model
