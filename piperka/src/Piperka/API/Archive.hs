{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}

{-# OPTIONS -Wno-unused-top-binds #-}

module Piperka.API.Archive (dumpArchive, rssFeed) where

import Control.Lens
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Int
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Time
import Data.Time.RFC822
import Data.Maybe (fromMaybe)
import Data.Vector (Vector)
import GHC.Generics
import qualified Hasql.Encoders as EN
import qualified Hasql.Decoders as DE
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql
import Text.Feed.Export (textFeed)
import Text.Feed.Types (Feed(RSSFeed))
import Text.RSS.Syntax

import Application
import Piperka.API.Common
import Piperka.Util (getCid, intToText)

type Page = (Maybe Text, Int32, [Text], Bool, Maybe Text)

data Archive = Archive {
    url_base :: Text
  , url_tail :: Text
  , fixed_head :: Maybe Text
  , homepage :: Text
  , pages :: Vector Page
  } deriving (Generic)

instance ToJSON Archive where
  toEncoding = genericToEncoding defaultOptions

decodeArchiveInfo :: DE.Row (Vector Page -> Archive)
decodeArchiveInfo =
  Archive
  <$> DE.column DE.text
  <*> DE.column DE.text
  <*> DE.nullableColumn DE.text
  <*> DE.column DE.text

decodePageInfo :: DE.Row Page
decodePageInfo = do
  pageName <- DE.nullableColumn DE.text
  fragments <- DE.column $ DE.array $ DE.dimension replicateM $ DE.element DE.text
  thumb <- DE.column DE.bool
  title <- DE.nullableColumn DE.text
  return (pageName, fromIntegral $ length fragments, fragments, thumb, title)

dumpArchive
  :: ByteString
  -> AppHandler ()
dumpArchive hostname = do
  modifyResponse $
    addHeader "Access-Control-Allow-Origin" ("http://" <> hostname)
  c <- maybe (simpleFail 404 "Required parameter cid missing") return =<<
       fmap (fromIntegral . snd) <$> getCid
  runQueries $ do
    aMain' <- ExceptT $ run $ statement c $ Statement sql
              (EN.param EN.int4) (DE.rowMaybe decodeArchiveInfo) True
    maybe (lift $ simpleFail 404 "No such comic")
      (\aMain ->
         (ExceptT $ run $ statement c $ Statement sql'
          (EN.param EN.int4) (DE.rowVector decodePageInfo) True) >>=
         lift . writeLBS . encode . aMain
      ) aMain'
  where
    sql = "SELECT url_base, url_tail, fixed_head, homepage \
          \FROM comics WHERE cid=$1"
    sql' = "SELECT name, COALESCE(ARRAY_AGG(fragment) FILTER \
           \ (WHERE page_fragments.fragment IS NOT NULL), '{}'), \
           \COALESCE(thumb, false), title \
           \FROM updates AS u LEFT JOIN page_fragments using (cid, ord) \
           \LEFT JOIN screenshot_raw AS s ON (s.cid=u.cid AND s.ord=u.ord AND thumb) \
           \WHERE u.cid=$1 GROUP BY name, u.ord, thumb, title ORDER BY u.ord"

rssFeed
  :: ByteString
  -> AppHandler ()
rssFeed hostname = do
  let host = decodeUtf8 hostname
  c <- maybe (simpleFail 404 "Required parameter cid missing") return =<<
       fmap (fromIntegral . snd) <$> getCid
  runQueries $ do
    feedMain <- ExceptT $ run $ statement c $ Statement sql
                (EN.param EN.int4) (DE.rowMaybe decodeDescription) True
    flip (maybe (lift $ simpleFail 404 "No such comic")) feedMain $ \(title, b, t, upd) -> do
      lift $ modifyResponse (setHeader "Content-Type" "application/rss+xml")
      xs <- ExceptT $ run $ statement c $ Statement sql'
            (EN.param EN.int4) (DE.rowList decodePage) True
      let pubDate = formatTimeRFC822 <$> upd
          chan = (nullChannel
                  (host <> " — " <> title)
                  ("https://" <> host <> "/info.html?cid=" <> intToText c))
                 { rssGenerator = Just host
                 , rssPubDate = pubDate
                 }
          feed = map (\(ord, subord, era, name, fragment) ->
                        (nullItem (title <> " — Page " <> intToText (succ ord) <>
                                   maybe "" ((" subpage " <>) . intToText) subord))
                        { rssItemLink = Just $ b <> name <> t <> maybe "" ("#" <>) fragment
                        , rssItemGuid = Just $ nullGuid $ host <> "://" <> intToText c <>
                                        "." <> intToText ord <>
                                        maybe "" (("." <>) . intToText) subord <>
                                        maybe "" (("+" <>) . intToText) era
                        }) xs
          rss = RSS
            { rssVersion = "2.0"
            , rssAttrs = []
            , rssOther = []
            , rssChannel =
                chan
                { rssItems = over _head (\x -> x { rssItemPubDate = pubDate }) feed
                }
            }
      lift . writeLazyText . fromMaybe "" $ textFeed $ RSSFeed rss
  where
    decodeDescription =
      (,,,)
      <$> DE.column DE.text
      <*> DE.column DE.text
      <*> DE.column DE.text
      <*> DE.nullableColumn ((\x -> ZonedTime x utc) <$> DE.timestamp)
    decodePage =
      (,,,,)
      <$> DE.column DE.int4
      <*> DE.nullableColumn DE.int4
      <*> DE.nullableColumn DE.int4
      <*> DE.column DE.text
      <*> DE.nullableColumn DE.text
    sql = "SELECT title, url_base, url_tail, last_updated \
          \FROM comics LEFT JOIN crawler_config USING (cid) \
          \WHERE cid=$1"
    sql' = "SELECT updates.ord, page_fragments.subord, era, name, fragment \
           \FROM updates LEFT JOIN page_fragments USING (cid, ord) \
           \LEFT JOIN page_era ON (page_era.cid=updates.cid \
           \ AND page_era.ord=updates.ord \
           \ AND page_era.subord=COALESCE(page_fragments.subord, 0)) \
           \WHERE updates.cid=$1 ORDER BY ord DESC, subord DESC"
