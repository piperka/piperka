{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Piperka.API.Hiatus (controlHiatus) where

import Contravariant.Extras.Contrazip
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Int
import Data.Maybe
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import qualified Piperka.Encoders as EN
import Piperka.Util (getParamInt, getCid)

controlHiatus
  :: AppInit
  -> AppHandler ()
controlHiatus ini = runModQueries Moderator $ \MyData{..} -> do
  let isValidId = flip elem (map fst $ hiatusNames ini)
      processUserEdit Nothing _ = return ()
      processUserEdit (Just u) hsid = markDoneUserEdit (fromIntegral u) uid hsid
  void $ lift validateCsrf
  (cid, clear, dfr, stat, uhid) <- lift $ (,,,,)
    <$> (getCid >>= maybe (simpleFail 400 "cid required") (return . fromIntegral . snd))
    <*> (isJust <$> getParam "clear")
    <*> (isJust <$> getParam "defer")
    <*> (fmap snd <$> getParamInt "status")
    <*> (fmap snd <$> getParamInt "uhid")
  case (clear, dfr, stat, isJust uhid) of
    (True, _, _, _) -> clearHiatus uid cid
    (_, True, _, _) -> deferHiatus cid
    (_, _, Just s, _) -> do
      when (not $ isValidId s) $ lift $ simpleFail 400 "invalid hiatus id"
      setHiatus uid cid (fromIntegral s) >>= processUserEdit uhid . Just
    (False, False, Nothing, True) -> processUserEdit uhid Nothing
    _ -> lift $ simpleFail 500 "hiatus parse fail"

clearHiatus
  :: UserID
  -> Int32
  -> ExceptT QueryError (Handler App App) ()
clearHiatus u c = ExceptT $ run $ statement (u, c) $ Statement sql
  (contrazip2 (EN.param EN.uid) (EN.param EN.int4)) DE.unit True
  where
    sql = "INSERT INTO hiatus_status (uid, cid, active) VALUES ($1, $2, false)"

deferHiatus
  :: Int32
  -> ExceptT QueryError (Handler App App) ()
deferHiatus cid = ExceptT $ run $ statement cid $ Statement sql
  (EN.param EN.int4) DE.unit True
  where
    sql = "INSERT INTO hiatus_defer VALUES ($1) \
          \ON CONFLICT (cid) DO UPDATE SET stamp=now()"

setHiatus
  :: UserID
  -> Int32
  -> Int16
  -> ExceptT QueryError (Handler App App) Int32
setHiatus u c s = ExceptT $ run $ statement (u, c, s) $ Statement sql
  (contrazip3 (EN.param EN.uid) (EN.param EN.int4) (EN.param EN.int2))
  (DE.singleRow $ DE.column DE.int4) True
  where
    sql = "INSERT INTO hiatus_status (uid, cid, code) \
          \VALUES ($1, $2, $3) RETURNING hsid"

markDoneUserEdit
  :: Int32
  -> UserID
  -> Maybe Int32
  -> ExceptT QueryError (Handler App App) ()
markDoneUserEdit uhid uid hsid = ExceptT $ run $ statement (uhid, uid, hsid) $
  Statement sql
  (contrazip3 (EN.param EN.int4) (EN.param EN.uid) (EN.nullableParam EN.int4))
  DE.unit True
  where
    sql = "UPDATE hiatus_user_edit SET processed_by=$2, hsid=$3 WHERE uhid=$1"
