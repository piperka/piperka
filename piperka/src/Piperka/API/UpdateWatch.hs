{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.API.UpdateWatch where

import Control.Concurrent
import Control.Lens
import Control.Monad
import Control.Monad.State
import qualified Data.ByteString.Char8 as B8
import Data.ByteString.UTF8 (toString)
import qualified Data.IntSet as IntSet
import Data.IORef
import Data.Text (Text)
import qualified Data.UUID as UUID
import Network.WebSockets as WS
import Network.WebSockets.Snap
import Snap
import Snap.Snaplet.Hasql
import System.Random (randomRIO)
import Text.Read (readMaybe)

import Application hiding (uid)
import Piperka.API.Common
import Piperka.Push.Statements
import Piperka.UserID
import Piperka.Util (maybeParseInt)

updateWatch :: AppHandler ()
updateWatch = route $
  [ ("init/:csrf_ham", runUserQueries (lift . initUpdateWatch))
  ]

initUpdateWatch
  :: MyData
  -> AppHandler ()
initUpdateWatch usr = getParam "csrf_ham" >>= \hm ->
  if hm /= (Just $ UUID.toASCIIBytes $ ucsrfToken usr)
  then modifyResponse $ setResponseStatus 400 "csrf_ham missing or invalid"
  else do
    chan <- gets (^. updateWatcherChan)
    liftIO (dupChan chan) >>= runWebSocketsSnap . watchApp

watchApp
  :: Chan ()
  -> ServerApp
watchApp chan pending = do
  conn <- acceptRequest pending
  forever $ do
    void $ readChan chan
    randomRIO (0, 10*1000000) >>= threadDelay
    sendTextData conn ("true" :: Text)

-- Invoked by the crawler after the hourly run.
crawlDone
  :: (IORef IntSet.IntSet, Chan ())
  -> Handler MasterApp App ()
crawlDone (updSet, updateChan) = do
  modifyResponse $ setResponseStatus 204 "No content"
  liftIO $ writeChan updateChan ()
  bracketDbOpen $ run updateUsers >>=
    either
    (logError . B8.pack . ("error fetching updated users " <>) . show)
    (\upd -> liftIO $ atomicModifyIORef updSet ((,()) . IntSet.union upd))

-- Called after an index render to refresh which comics were up to
-- date at the time.
refreshUserUpdates
  :: Handler MasterApp MasterApp ()
refreshUserUpdates = do
  modifyResponse $ setResponseStatus 204 "No content"
  site <- (readMaybe . toString =<<) <$> getParam "site"
  u <- fmap (UserID . fromIntegral) . (maybeParseInt =<<) <$> getParam "uid"
  let refresh :: UserID -> Handler MasterApp App ()
      refresh uid =
        (bracketDbOpen . run $ refreshNewUserUpdate uid) >>=
        either
        -- OK to show as this is only used from localhost
        (\err -> simpleFail 500 $
          "DB error on refreshUserUpdates uid " <>
          B8.pack ((\(UserID x) -> show x) uid) <> " error " <> B8.pack (show err)
        )
        (const $ return ())
  case (site, u) of
    (Just Piperka, Just uid) -> withTop sitePiperka $ refresh uid
    (Just Teksti, Just uid) -> withTop siteTeksti $ refresh uid
    _ -> simpleFail 404 $ "Bad site or uid parameter"
