{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DeriveGeneric #-}

module Piperka.API.Account (apiLogin, apiCreateAccount, deleteAccount) where

import Contravariant.Extras.Contrazip
import Control.Monad
import Control.Monad.Trans
import Data.Aeson
import Data.ByteString.UTF8 (fromString)
import Data.Text (Text)
import Data.Time.Clock
import Data.UUID (UUID, toASCIIBytes, toText)
import qualified Data.Vector as V
import GHC.Generics
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap
import Snap.Snaplet.CustomAuth (createAccount)
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import Piperka.Auth (setCSRFCookie)
import Piperka.Account.Delete
import Piperka.Account.Types
import qualified Piperka.Decoders as DE
import Piperka.Subscription
import Piperka.Util (getParamText)

login
  :: (HasHasql m, MonadIO m)
  => Text
  -> Text
  -> m (Either QueryError (Maybe (Text, UserID, UUID, UUID)))
login u p = run $ statement (u, p) $ Statement sql
            (contrazip2 (EN.param EN.text) (EN.param EN.text))
            (DE.rowMaybe $ (,,,)
             <$> (DE.column DE.text)
             <*> (DE.column DE.uid)
             <*> (DE.column DE.uuid)
             <*> (DE.column DE.uuid)) True
  where
    sql = "SELECT name, uid, p_session, csrf_ham FROM auth_login($1, $2) \
          \JOIN users USING (uid)"

writeSuccess
  :: MonadSnap m
  => Text
  -> UUID
  -> m ()
writeSuccess name token = writeLBS $ encode $ object
  [ "name" .= name
  , "csrf_ham" .= toText token
  ]

importBookmarks
  :: UserID
  -> AppHandler ()
importBookmarks u = do
  bm <- getRequestSubscriptions
  when (not $ V.null bm) $ run (setBookmarks bm u) >>=
    either (\x -> do
               logError $ fromString $ show x
               writeLBS $ encode $
                 object ["errmsg" .= ("Bookmark import failed" :: Text)]
               finishWith =<< setResponseCode 500 <$> getResponse
               return ()
           ) (const $ return ())

apiLogin
  :: AppHandler ()
apiLogin = do
  usr <- requiredParam "user" getParamText
  passwd <- requiredParam "passwd" getParamText
  lg <- either (simpleFail 403 . fromString . show) return =<<
    login usr passwd
  case lg of
    Nothing ->
      writeLBS $ encode $ object ["errmsg" .= ("Login failed" :: Text)]
    Just (name, u, sesTok, csrfTok) -> do
      importBookmarks u
      now <- liftIO getCurrentTime
      modifyResponse $ addResponseCookie $
        Cookie sessionCookieName (toASCIIBytes sesTok)
        (Just $ addUTCTime (5*365*24*60*60) now) Nothing (Just "/") True True
      setCSRFCookie csrfTok
      writeSuccess name csrfTok

apiCreateAccount
  :: AppHandler ()
apiCreateAccount = do
  let success u = do
        importBookmarks (uid u)
        writeSuccess (uname u) (ucsrfToken u)
      storeQueryError e = logError (fromString $ show e) >> return "QueryError"
      failure e = do
        modifyResponse $ setResponseCode 500
        err <- either storeQueryError (return . show) e
        writeLBS $ encode $ object [ "err" .= True
                                   , "code" .= err
                                   ]
  withTop apiAuth createAccount >>= either failure success

data SSOKey = SSOKey
  { key :: Text
  } deriving (Generic)

instance ToJSON SSOKey where
  toEncoding = genericToEncoding defaultOptions

deleteAccount
  :: AppHandler ()
deleteAccount = runUserQueries $ \usr -> do
  req <- lift $ getDeleteRequest usr
  dState <- actDeleteRequest usr req
  lift $ writeLBS $ encode $ object $ case dState of
    Just (DeleteWaiting t) -> [ "waiting" .= (ceiling :: Float -> Int) (realToFrac t) ]
    Just DeleteAvailable | csrfOk req && certain req && final req ->
                           [ "deleted" .= True ]
    Just DeleteAvailable -> [ "available" .= True ]
    _ -> []
