{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.API.Crawler where

import Control.Lens hiding ((.=))
import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import qualified Data.ByteString as B
import Data.Default.Class
import qualified Data.IntMap as IntMap
import Data.IORef
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.UUID (UUID)
import qualified Data.UUID as UUID
import Hasql.Session hiding (run)
import qualified Network.WebSockets as WS
import Network.WebSockets.Snap
import Snap
import Snap.Snaplet.Hasql

import Application
import Crawler.Command
import Crawler.Command.Types
import Crawler.DB.Fetch
import Crawler.Types
import Piperka.API.Common
import Piperka.Util (getCid)

crawler
  :: DBConfig
  -> AppHandler ()
crawler cfg = route $
  (map (_2 %~ (runModQueries Admin $))
   [ ("init/:csrf_ham", initCrawl cfg)
   ])

data ExistingInfo = ExistingInfo Value

instance ToJSON ExistingInfo where
  toJSON (ExistingInfo bookmarks) =
    object ["bookmarks" .= bookmarks]

initCrawl
  :: DBConfig
  -> MyData
  -> ExceptT QueryError (Handler App App) ()
initCrawl cfg usr = (lift $ getParam "csrf_ham") >>= \hm ->
  if (hm /= (Just $ UUID.toASCIIBytes $ ucsrfToken usr))
  then lift $ modifyResponse $ setResponseStatus 400 "csrf_ham missing or invalid"
  else do
    when (maybe True (< Admin) $ moderator usr) $ lift $ do
      modifyResponse $ setResponseStatus 400 "admin only"
      finishWith =<< getResponse
    ch <- lift $ gets (^. crawlHandle)
    archive <- runMaybeT $
      MaybeT (fmap snd <$> lift getCid) >>=
      fmap (snd . head . IntMap.toList) .
      MaybeT . ExceptT . run . fetchSingleArchive False . fromIntegral
    token <- liftIO $ do
      token <- round . (* 1000) <$> getPOSIXTime
      start <- getCurrentTime
      let cr = Crawl start False [] (maybe def id archive) Nothing 0 mempty
      atomicModifyIORef ch ((,()) . (IntMap.insert token cr))
      return token
    lift $ runWebSocketsSnap $ app cfg (ucsrfToken usr) ((IntMap.! token) <$> readIORef ch)
      $ atomicModifyIORef ch . (\f  -> (,()) . IntMap.adjust f token)

instance WS.WebSocketsData CrawlControl where
  fromDataMessage (WS.Text raw _) = maybe CmdUnknown id $ decode raw
  fromDataMessage (WS.Binary _) = CmdUnknown
  fromLazyByteString = maybe CmdUnknown id . decode
  toLazyByteString = encode

instance WS.WebSocketsData CrawlResult where
  fromDataMessage _ = error "no CrawlMessage fromDataMessage"
  fromLazyByteString = error "no CrawlMessage fromLazyByteString"
  toLazyByteString = encode

app
  :: DBConfig
  -> UUID
  -> IO Crawl
  -> ((Crawl -> Crawl) -> IO ())
  -> WS.ServerApp
app dbCfg hm getState modifyState pending = do
  conn <- WS.acceptRequest pending
  token <- WS.receiveData conn
  if UUID.fromASCIIBytes token /= Just hm then WS.sendCloseCode conn 1002 B.empty else
    let crawlerCfg = (dbConfig .~ dbCfg) $ def
        comm = CrawlComm
          { sendData = WS.sendTextData conn
          , receiveData = WS.receiveData conn
          , getCrawl = getState
          , adjust = modifyState
          }
    in crawlerApp crawlerCfg comm
