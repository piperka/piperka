{-# LANGUAGE OverloadedStrings #-}

module Piperka.API.AutoFix where

import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.API.Common
import Piperka.Maint.Health.Query
import Piperka.Util

autoFixer :: AppHandler ()
autoFixer = runModQueries Admin $ const $ do
  cid <- lift $ validateCsrf >>
         getCid >>=
         maybe (simpleFail 404 ("required parameter cid missing")) (pure . snd)
  res <- ExceptT $ run $ applyAutoFix cid
  lift $ writeLBS $ encode $ object [ "success" .= res ]
