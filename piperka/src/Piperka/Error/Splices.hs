{-# LANGUAGE OverloadedStrings #-}

module Piperka.Error.Splices where

import Control.Monad.Trans
import Data.ByteString.UTF8 (fromString)
import Data.Map.Syntax
import Data.Text (pack)
import Data.Time.Clock
import Hasql.Session
import Heist
import Snap

import Application(RuntimeAppHandler)
import Piperka.Heist

sqlErrorSplices
  :: Splices (RuntimeAppHandler QueryError)
sqlErrorSplices = do
  "sqlError" ## \e -> return $ yieldRuntimeText $ do
    err <- show <$> e
    stamp <- show <$> liftIO getCurrentTime
    lift $ logError $ fromString $ stamp <> " " <> err
    lift $ modifyResponse $ setResponseStatus 500 "Database error"
    return $ pack stamp

stdSqlErrorSplice
  :: RuntimeAppHandler QueryError
stdSqlErrorSplice = withSplices (callTemplate "_sqlErr") sqlErrorSplices
