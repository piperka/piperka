{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}

module Piperka.Splices
  (
    piperkaSplices
  , piperkaAttrSplices
  )
  where

import qualified Codec.MIME.Base64 as MIME
import Control.Applicative ((<|>))
import Control.Error.Util (bool, hush)
import Control.Exception (try, SomeException)
import Control.Lens
import Control.Monad.State
import qualified Data.ByteString as B
import Data.DList (DList)
import Data.List (partition)
import Data.Map.Syntax
import Data.Maybe
import qualified Data.Text as T
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import qualified Data.Text.IO
import Data.UUID
import Heist
import Heist.Splices (ifCSplice)
import Heist.Internal.Types
import qualified HTMLEntities.Text as HTML
import qualified Network.HTTP.Types.URI as URI
import Snap
import Snap.Snaplet.CustomAuth
import qualified Text.XmlHtml as X
import Web.WebPush (vapidPublicKeyBytes)

import Application
import Backend()
import Piperka.Account
import Piperka.Account.Delete
import Piperka.Action.Splices
import Piperka.Action.Types
import Piperka.API.Submit.Splices
import Piperka.Auth (currentUserPlain)
import Piperka.Auth.Splices
import Piperka.Bookmark.Bookmarklet
import Piperka.Bookmark.Splices
import Piperka.ComicInfo.Types (Prefetch(..))
import qualified Piperka.ComicInfo.Types as ComicInfo
import Piperka.Heist
import Piperka.OAuth2.Splices
import Piperka.Maint.Splices
import Piperka.Maint.Crawler.Splices
import Piperka.Maint.Health
import Piperka.Maint.Hiatus.Splices
import Piperka.Maint.Ticket.Splices
import Piperka.Mobile
import Piperka.Profile.Follower
import Piperka.Recommend
import Piperka.Splices.Account
import Piperka.Splices.Ads
import Piperka.Splices.Authors
import Piperka.Splices.Flavor
import Piperka.Splices.Hiatus
import Piperka.Splices.LastUpdate
import Piperka.Splices.Othersite
import Piperka.Splices.Providers
import Piperka.Splices.Random
import Piperka.Splices.Schedule
import Piperka.Splices.Stats
import Piperka.Splices.UserDeleted
import Piperka.Submission
import Piperka.Submit.Splices
import Piperka.Listing.Render
import Piperka.ComicInfo.Splices
import Piperka.Messages
import Piperka.Readers
import Piperka.Recover
import Piperka.Ticket
import Piperka.Ticket.User.Splices
import Piperka.Util

piperkaSplices
  :: AppInit
  -> Splices (Splice AppHandler)
piperkaSplices ini = do
  "ifProd" ## do
    dev <- maybe False (read . T.unpack) . X.getAttribute "dev" <$> getParamNode
    if dev == ("dev" == T.take 3 (appHostname ini)) then runChildren else return mempty
  "devPrefix" ## pure $ yieldPureText $
    if "dev." == T.take 4 (appHostname ini) then "dev." else ""
  "otherSiteSSO" ## callTemplate "/_otherSiteSSO"
  "site" ## do
    site <- read . T.unpack . fromJust . X.getAttribute "site" <$> getParamNode
    if site == siteIdentity ini then runChildren else return mempty
  "siteName" ## do
    node <- getParamNode
    let domain = maybe False (read . T.unpack) $ X.getAttribute "domain" node
    callTemplate $ if domain then "/include/_siteName_domain" else "/include/_siteName"
  "siteId" ## return $ yieldPureText $ case siteIdentity ini of
    Piperka -> "site-p"
    Teksti -> "site-t"
  "comicWord" ## do
    node <- getParamNode
    let web = maybe False (read . T.unpack) $ X.getAttribute "web" node
        capital = maybe False (read . T.unpack) $ X.getAttribute "capital" node
    callTemplate $ case (web, capital) of
      (False, False) -> "/include/_comicWord"
      (True, False) -> "/include/_comicWord_web"
      (False, True) -> "/include/_comicWord_capital"
      (True, True) -> "/include/_comicWord_web_capital"
  "comicWordWeb" ## callTemplate "/include/_comicWord_web"
  "newest" ## return $ yieldRuntimeText $
    maybe "" ("&newest=" <>) . fmap (intToText . snd) <$> (lift $ getParamInt "newest")
  "vapidPublic" ## return . yieldPureText . T.pack . MIME.encodeRaw False .
    vapidPublicKeyBytes $ vapidKeys ini
  "hostname" ## return $ yieldPureText $ appHostname ini
  "ifHost" ## do
    host <- fromJust . X.getAttribute "host" <$> getParamNode
    if host == appHostname ini then runChildren else return mempty
  "ifPage" ## do
    node <- getParamNode
    let nms = fromJust $
          ((:[]) . encodeUtf8 <$> X.getAttribute "name" node) <|>
          (read . T.unpack <$> X.getAttribute "names" node)
        oper = bool id not $ maybe False (read . T.unpack) $ X.getAttribute "not" node
    ctx <- head . _curContext <$> getHS
    if oper $ elem ctx nms then runChildren else return mempty
  "ifMinimal" ## do
    node <- getParamNode
    let oper = bool (==) (/=) $ maybe False (read . T.unpack) $ X.getAttribute "not" node
    chld <- runChildren
    oper `seq` return $ yieldRuntime $ do
      useMinimal <- lift $ view minimal
      codeGen $ if useMinimal `oper` True then chld else mempty
  "isRandom" ## do
    chld <- runChildren
    return $ yieldRuntime $ (lift $ getPostParam "rnd") >>= \case
      { Just _ -> codeGen chld ; _ -> return mempty }
  "randomComicButton" ## renderRandomButton
  "mobile" ## mobileSplice
  "submitAPI" ## submitAPISplices
  "script" ## hashTaggedNode "script" "src"
  "stylesheet" ## hashTaggedNode "link" "href"
  "subscribeForm" ## subscribeForm
  "submit" ## renderSubmit ini
  "paramAttrs" ## withLocalSplices mempty ("value" ## paramValue) runChildren
  "updateStatus" ## updateStatus
  "kaolSubs" ## renderKaolSubs
  "fortune" ## renderFortune
  "providers" ## renderProviders
  "authors" ## renderAuthors
  "editAuthor" ## renderEditAuthor
-- Overloaded, action uses its own version
  "bookmark" ## deferManyElse (callTemplate "_readerBasic") renderBookmark $ lift (getParamText "url")
-- TODO: These are statically known at load time, using it as
-- RuntimeSplice is wrong (if convenient).
  "hiatusClassifications" ## manyWithSplices runChildren
    (mapV (pureSplice . textSplice) $ do
        "id" ## intToText . fst
        "name" ## snd) $ return $ hiatusNames ini
  "hiatusExplanation" ## return . yieldPureText $ hiatusPart ini
  "piperka" ## prepareAds ini (renderPiperka ini)
  "withCid" ## renderWithCid ini
  "withCidTitle" ## renderWithCidTitle
  "externA" ## renderExternA

-- Splice definition overridden if used via withCid.
  "comicInfo" ## (renderMinimal ini) (renderComicInfo ini)
  <> messagesSplices
  where
    updateStatusFile = (\site -> "run/shared/" <> site <> "/update_status") $
      case siteIdentity ini of
        Piperka -> "piperka"
        Teksti -> "teksti"
    updateStatus = return $ yieldRuntimeText $
      either (const @Text @SomeException "") id
      <$> (liftIO $ try $ Data.Text.IO.readFile updateStatusFile)
    subscribeForm = do
      action <- X.getAttribute "action" <$> getParamNode
      withLocalSplices mempty
        ("action" ## const $ return $ maybe [] (\x -> [("action", x)]) action)
        $ callTemplate "_subscribe"
    hashTaggedNode tagName srcAttr = do
      node <- getParamNode
      let src = fromJust $ X.getAttribute srcAttr node
      token <- maybe (liftIO $ randomString 6) return $
        lookup src $ scriptHash ini
      runNode $ X.setAttribute srcAttr (src <> "?v=" <> token) $
        node {X.elementTag = tagName}

piperkaAttrSplices
  :: Splices (AttrSplice AppHandler)
piperkaAttrSplices = do
  "mobileClass" ## mobileClassAttrSplice
  "maybeOg" ## \_ -> do
    hasCI <- lift $ isJust <$> view prefetchComicInfo
    pure $ if hasCI then [("prefix", "og: https://ogp.me/ns#")] else []
  "stickyShowHidden" ## const $ do
    sticky <- lift $ stickyShowHidden <$> view activePrefs
    pure $ if sticky then [("data-sticky-show-hidden", "1")] else []

renderMinimal
  :: AppInit
  -> RuntimeAppHandler (Maybe MyData)
  -> Splice AppHandler
renderMinimal ini action =
  (\n -> withSplices (action n) (contentSplices' ini) n) `defer`
  (lift currentUserPlain)

renderContent
  :: AppInit
  -> [X.Node]
  -> RuntimeAppHandler (Maybe MyData)
renderContent ini ns n = do
  authContent <- deferMany (withSplices (callTemplate "_authFailure") authErrorSplices) $ do
    suppress <- lift $ withTop' id $ view suppressError
    if suppress then return Nothing else do
      err1 <- lift $ withTop auth $ getAuthFailData
      err2 <- lift $ withTop apiAuth $ getAuthFailData
      return $ err1 <|> err2
  content <- withSplices (runNodeList ns) (contentSplices' ini) n
  return $ authContent <> content

renderPiperka
  :: AppInit
  -> Splice AppHandler
renderPiperka ini = do
  node <- getParamNode
  let xs = X.childNodes node
      isExtra (X.Element "extra" _ _) = True
      isExtra _ = False
      (extraNodes, contentNodes) = partition isExtra xs
      fetch = read . T.unpack <$> X.getAttribute "prefetch" node
      minifiable = maybe False (read . T.unpack) $ X.getAttribute "minifiable" node
  title <- X.getAttribute "title" node & maybe
    (pure $ pure Nothing)
    ((fmap . fmap) (fmap snd . listToMaybe) . runAttributesRaw . pure . ("title",))
  let getInner n = do
        content <- renderContent ini contentNodes $ snd <$> n
        extra <- withSplices (runNodeList extraNodes) (contentSplices' ini) $ snd <$> n
        withSplices (callTemplate "_base")
          (contentSplices ini title fetch content extra) n
      renderInner n =
        let inner = getInner $ (\(a, u) -> (a, user <$> u)) <$> n
        in deferManyElse
           (withLocalSplices nullStatsSplices
            ("newComicsClass" ## const $ return []) inner)
           (\n' -> withLocalSplices (mapV ($ n') statsSplices)
                   (statsAttrSplices n') inner) $ fmap snd n
  normal <- defer renderInner $ do
    u <- lift $ withTop auth currentUser
    a <- lift $ withTop' id $ view actionResult
    let fillPrefetch mode = do
          when (mode == ComicInfo.Info) prefetchRecommends
          modify . set prefetchComicInfo . Just =<< getComicInfoData mode (user <$> u)
    maybe (return ()) (lift . fillPrefetch) fetch
    return (a, u)
  bare <- if minifiable then
    renderMinimal ini $ nullCreateSplice .
    (withSplices (runNodeList xs)
     ((contentSplices' ini) <> ("onlyWithStats" ## const $ return mempty)))
    else return $ yieldRuntimeEffect $ lift $ do
    rq <- getRequest
    let rd = URI.renderQuery True .
          filter ((/= "min") . fst) .
          URI.parseQuery $ rqQueryString rq
        pth = case (rqContextPath rq, rqPathInfo rq) of
          (s, "") -> if B.length s > 1 then B.init s else s
          (s, p) -> s <> p
    redirect $ pth <> rd
  fetch `seq` minifiable `seq` return $ yieldRuntime $ do
    useMinimal <- lift $ view minimal
    codeGen $ if useMinimal then bare else normal

statsSplices
  :: Splices (RuntimeAppHandler UserWithStats)
statsSplices = do
  "unreadStats" ## unreadCountSplice .
    (>>= \stats -> do
        hide <- lift getHideInactive
        return $ (if hide then unreadCountHidden else unreadCount) stats)
  "newComics" ## newComicsSplice . fmap newComics
  "modStats" ## \n -> do
    let modSplices =
          (mapV (pureSplice . textSplice . (fmap intToText)) $ do
              "modCount" ## (\(a,_,_) -> a)
              "modDays" ## (\(_,a,_) -> a)
              "unresolvedTickets" ## (\(_,_,a) -> a)
          )
          <> ("haveModCount" ## ifCSplice $ (>0) . (\(a,_,_) -> a))
          <> ("nag" ## ifCSplice $ (>6) . (\(_,a,_) -> a))
    spl <- deferMany (withSplices runChildren modSplices) $ modStats <$> n
    return $ yieldRuntime $ do
      m <- isJust . moderator . user <$> n
      if m then codeGen spl else return mempty
  "onlyWithStats" ## const $ runChildren

statsAttrSplices
  :: RuntimeSplice AppHandler UserWithStats
  -> Splices (AttrSplice AppHandler)
statsAttrSplices n = do
  "newComicsClass" ## \t -> do
    nc <- newComics <$> n
    return $ if nc > 0 then [("class", t)] else []

nullStatsSplices
  :: Splices (Splice AppHandler)
nullStatsSplices = mconcat $ map (## (return mempty))
  ["unreadStats", "newComics", "modStats", "onlyWithStats"]

contentSplices
  :: AppInit
  -> (RuntimeSplice AppHandler (Maybe Text))
  -> Maybe Prefetch
  -> DList (Chunk AppHandler)
  -> DList (Chunk AppHandler)
  -> Splices (RuntimeAppHandler (Maybe (Maybe ActionError, Maybe Action), Maybe MyData))
contentSplices ini title fetch content extra =
  ("action" ## (\n -> renderAction (return content) $ fst <$> n)) <>
  ("extra" ## const $ return extra) <>
  ("withPrefetchInfo" ## const $ manyWithSplices runChildren prefetchSplices $
    lift $ join . fmap hush <$> view prefetchComicInfo) <>
  ("title" ## \n -> do
      suffix <- withSplices runChildren (mapV (. fmap snd) (contentSplices' ini)) n
      let prefix = yieldRuntimeText $ do
            titlePart <- maybe "" (<> " — ") <$> title
            let titleWithComic = do
                  comicTitle <- maybe ("Error")
                    (either (const "Error") (HTML.text . ComicInfo.title)) <$>
                    (lift $ view prefetchComicInfo)
                  return $ titlePart <> comicTitle <> " — "
            maybe (return titlePart) (const titleWithComic) fetch
      return $ prefix <> suffix
  ) <>
  (mapV (. fmap snd)) (contentSplices' ini)

contentSplices'
  :: AppInit
  -> Splices (RuntimeAppHandler (Maybe MyData))
contentSplices' ini = do
  "ifLoggedIn" ## deferMany (withSplices runChildren (loggedInSplices ini))
  "ifLoggedOut" ## ifCSplice isNothing
  "notMod" ## \(n :: RuntimeSplice AppHandler (Maybe MyData)) -> do
    level <- maybe Moderator (read . T.unpack) . X.getAttribute "level" <$> getParamNode
    forbid <- maybe True (read . T.unpack) . X.getAttribute "forbid" <$> getParamNode
    spl <- runChildren
    level `seq` forbid `seq` return $ yieldRuntime $ do
      m <- maybe False (>=level) . (moderator =<<) <$> n
      if m
        then return mempty
        else when forbid (lift (modifyResponse $ setResponseCode 403)) >> codeGen spl
  "listing" ## renderListing ini
  "csrfForm" ## csrfForm
  "passwordRecovery" ## renderPasswordRecovery (siteIdentity ini) (appHostname ini)
  "usePasswordHash" ## renderUsePasswordHash
  "ifMod" ## \n -> do
    level <- maybe Moderator (read . T.unpack) . X.getAttribute "level" <$> getParamNode
    level `seq` deferMany
      (withSplices runChildren (moderatorSplices ini)) $ n >>= \n' ->
      return $ n' >>= moderator >>= guard . (>= level) >> n'
  "email" ## defer $ \n -> return $ yieldRuntimeText $
    maybe (return "") (lift . getUserEmail . uid) =<< n
  "oauth2Create" ## renderOAuth2
  "readers" ## renderReaders
  "ticket" ## renderTicket
  "statusEdited" ## statusEdited ini

loggedInSplices
  :: AppInit
  -> Splices (RuntimeAppHandler MyData)
loggedInSplices ini = do
  "loggedInUser" ## pureSplice . textSplice $ HTML.text . uname
  "withBookmarklet" ## renderWithBookmarklet
  "csrf" ## pureSplice . textSplice $ toText . ucsrfToken
  "csrfProtected" ## \u -> withSplices runChildren ("if" ## checkedSplice) `defer` do
    uCSRF <- ucsrfToken <$> u
    csrf <- (fromASCIIBytes =<<) <$> lift (getParam "csrf_ham")
    pure $ Just uCSRF == csrf
  "profileLink" ## profileLink
  "followers" ## renderFollowers
  "accountForm" ## renderAccountForm
  "recommend" ## renderRecommend
  "userDeletedComics" ## renderUserDeletedComics
  "userTickets" ## renderUserTickets
  "explicitStats" ## explicitStats
  "othersiteUpdates" ## othersiteUpdates ini
  "upcomingSchedule" ## upcomingSchedule
  "isAuthor" ## deferMany renderIsAuthor . fmap ((>>) <$> author <*> Just)
  "isNotAuthor" ## ifCSplice (isNothing . author)
  "isCidAuthor" ## renderIsCidAuthor $ siteIdentity ini
  "authorClaim" ## renderAuthorClaim
  "lastUpdateDisclaimer" ## renderLastUpdateDisclaimer
  "SSOSeed" ## getSSOSeed ini
  "accountDelete" ## renderAccountDelete

moderatorSplices
  :: AppInit
  -> Splices (RuntimeAppHandler MyData)
moderatorSplices ini = do
  "listOfEdits" ## renderListOfEdits
  "listOfStatusEdits" ## renderListOfStatusEdits ini
  "hiatus" ## renderHiatus
  "submissions" ## const renderSubmissions
  "genentry" ## renderGenentry $ siteIdentity ini
  "ticketList" ## renderTicketList
  "ticketDetail" ## renderTicketDetail
  "graveyard" ## renderMorgue $ siteIdentity ini
  "crawlerControl" ## renderCrawlerControl
  "crawlerArchive" ## renderCrawlerArchive
  "healthBaseRedirects" ## const healthBaseRedirects
  "healthCrawlerFailures" ## const healthCrawler

profileLink
  :: RuntimeAppHandler MyData
profileLink =
  let mkLink = \prof -> encodePathToText ["profile.html"]
                        [("name", Just $ encodeUtf8 prof)]
  in pureSplice . textSplice $ mkLink . uname

csrfForm
  :: RuntimeAppHandler a
csrfForm _ = do
  rootNode <- getParamNode
  let sub = X.childNodes rootNode
  inner <- runNodeList sub
  let formSplices = do
        "apply-form-content" ## return inner
        "form" ## do
          node <- getParamNode
          let node' = node { X.elementTag = "form"
                           , X.elementAttrs = X.elementAttrs rootNode
                           , X.elementChildren = X.childElements node
                           }
          runNode node'
  withLocalSplices formSplices mempty (callTemplate "_csrfForm")

renderExternA
  :: Splice AppHandler
renderExternA = do
  node <- getParamNode
  let node' = node {X.elementTag = "a"}
  externTpl <- runNode
               . X.setAttribute "target" "_blank"
               . X.setAttribute "rel" "noopener noreferrer" $ node'
  localTpl <- runNode node'
  return $ yieldRuntime $ do
    p <- lift $ view activePrefs
    codeGen $ if newExternWindows p then externTpl else localTpl

paramValue
  :: AttrSplice AppHandler
paramValue n = do
  t <- lift $ getParamText $ encodeUtf8 n
  return $ maybe [] ((:[]) . ("value",) . HTML.text) t
