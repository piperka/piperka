{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Piperka.Discord where

import Control.Concurrent
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Text (Text)
import qualified Data.Text as T
import Discord
import Discord.Types
import qualified Discord.Requests as R

import Application
import Piperka.Util (intToText)

sendDiscordEvents
  :: SiteIdentity
  -> Chan AnnounceEvent
  -> Text
  -> IO ThreadId
sendDiscordEvents site origChan tok = do
  chan <- dupChan origChan
  let rc dis = ExceptT . restCall dis
      (comicWord, host) = case site of
        Piperka -> ("comic", "https://piperka.net/")
        Teksti -> ("web fiction", "https://teksti.eu/")
      -- TODO: Get a full list of escapes
      escape = T.concatMap $ \c ->
        if c `elem` ("|*" :: String)
        then T.pack ['\\', c]
        else T.singleton c
      targetChan (RepoUpdated _ _) (ChannelText {..}) = channelName == "devel"
      targetChan _ (ChannelText {..}) = channelName == "news"
      targetChan _ _ = False
      messageText (ComicAdded cid title) =
        "New " <> comicWord <> " added: " <> escape title <> " " <>
        host <> "info.html?cid=" <> intToText cid
      messageText (ComicRetired cid reason title) =
        "A " <> comicWord <> " was removed: " <> escape title <> " " <>
        host <> "deadinfo.html?cid=" <> intToText cid <>
        "\nReason given: *" <> escape reason <> "*"
      messageText (RepoUpdated repo commits) =
        T.concat $ (("**" <> repo <> " updated**") :) $
        map (\(msg, url) -> "\n" <>
              (if T.null msg then "*empty commit message*"
               else escape $ if T.last msg == '\n' then T.init msg else msg) <>
              "\n<" <> url <> ">") commits
      send ev dis = runExceptT $ do
        guilds <- rc dis R.GetCurrentUserGuilds
        forM_ guilds $ \pg -> do
          guild <- rc dis . R.GetGuild $ partialGuildId pg
          chans <- fmap (map channelId . filter (targetChan ev)) . rc dis .
                   R.GetGuildChannels $ guildId guild
          forM_ chans $ \ch ->
            rc dis . R.CreateMessage ch $ messageText ev
        lift $ stopDiscord dis
  forkIO . forever $ do
    ev <- readChan chan
    runDiscord $ def { discordToken = tok, discordOnStart = void . send ev }
