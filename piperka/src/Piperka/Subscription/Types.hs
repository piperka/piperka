{-# LANGUAGE OverloadedStrings #-}

module Piperka.Subscription.Types where

import Control.Applicative
import Data.Aeson
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (split)
import qualified Hasql.Decoders as DE

import Piperka.Util (maybeParseInt)

data Subscription = Subscription Int Int Int Int Int (Maybe Int)

instance ToJSON Subscription where
  toJSON (Subscription a b c d e f) = toJSON $ [a, b, c, d, e] <> foldMap pure f

decodeSubscription :: DE.Row Subscription
decodeSubscription =
  Subscription
  <$> (liftA fromIntegral $ DE.column DE.int4)
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> (liftA fromIntegral $ DE.column DE.int4)
  <*> (DE.nullableColumn $ fromIntegral <$> DE.int2)

data BookmarkSet = Max | Del | Page Int
  deriving (Show)

decodeBookmarkSet
  :: ByteString
  -> Maybe (Int, BookmarkSet)
decodeBookmarkSet b = do
  [c, d] <- return $ split ' ' b
  c' <- maybeParseInt c
  d' <- case d of
          "max" -> return Max
          "del" -> return Del
          _ -> Page <$> maybeParseInt d
  return (c', d')
