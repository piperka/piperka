{-# LANGUAGE OverloadedStrings #-}

module Piperka.OAuth2.Providers where

import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import qualified Data.Configurator as C
import qualified Data.Configurator.Types as C
import Data.Maybe
import Data.Text (Text)
import Snap

import Application
import Piperka.Auth.Types

getProviders
  :: AppHandler [(ProviderID, (Text, Text))]
getProviders = getSnapletUserConfig >>= liftIO . getProviders'

getProviders'
  :: C.Config
  -> IO [(ProviderID, (Text, Text))]
getProviders' cfg = C.lookupDefault [] cfg "oauth2.providers" >>=
  (fmap catMaybes .
   mapM (\n -> runMaybeT $
               (\a b c -> (a, (b, c)))
               <$> (ProviderID <$> (MaybeT $ C.lookup cfg ("oauth2." <> n <> ".opid")))
               <*> pure n
               <*> (MaybeT $ C.lookup cfg ("oauth2." <> n <> ".label"))))
