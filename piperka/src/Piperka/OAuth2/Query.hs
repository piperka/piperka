{-# LANGUAGE OverloadedStrings #-}

module Piperka.OAuth2.Query where

import Contravariant.Extras.Contrazip
import Control.Monad.Trans
import Data.Text (Text)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql (run, HasHasql)

import Piperka.Auth.Types (AuthID, ProviderID)
import qualified Piperka.Decoders as DE
import qualified Piperka.Encoders as EN

reserveOAuth2Identity
  :: (HasHasql m, MonadIO m)
  => ProviderID
  -> Text
  -> m (Either QueryError AuthID)
reserveOAuth2Identity provider token = run $ statement (provider, token) stmt
  where
    stmt = Statement sql encode (DE.singleRow decode) True
    encode = contrazip2
      (EN.param EN.opid)
      (EN.param EN.text)
    decode = DE.column DE.authid
    sql = "INSERT INTO login_method_oauth2 (opid, identification) \
          \VALUES ($1, $2) RETURNING lmid"
