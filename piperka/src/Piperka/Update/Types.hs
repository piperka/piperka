module Piperka.Update.Types where

import Data.Int

data UpdateOptions = UpdateOptions
  { total :: Int32
  , hiddenCount :: Int32
  , offsetMode :: Bool
  , hold :: Bool
  }
  deriving (Show, Eq)
