{-# LANGUAGE OverloadedStrings #-}

module Piperka.Update.Statements where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad
import Data.ByteString (ByteString)
import Data.List
import Data.Int
import Data.Text (Text)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement
import Network.IP.Addr
import Prelude hiding (Ordering)

import Application hiding (uid)
import Piperka.Update.Types
import qualified Piperka.Encoders as EN

updateOptionsRow :: DE.Row UpdateOptions
updateOptionsRow =
  UpdateOptions
  <$> DE.column DE.int4
  <*> DE.column DE.int4
  <*> DE.column DE.bool
  <*> DE.column DE.bool

updateOptionsFetch
  :: Statement UserID UpdateOptions
updateOptionsFetch =
  Statement sql (EN.param EN.uid) (DE.singleRow updateOptionsRow) True
  where
    sql = "SELECT new_in, new_in_hide, offset_bookmark_by_one, hold_bookmark \
          \FROM users, user_unread_stats($1) WHERE uid=$1"

updateAndRedirect
  :: UserID
  -> Int32
  -> Bool
  -> NetAddr IP
  -> Session (Maybe ByteString)
updateAndRedirect uid cid back ip = do
  S.sql "COMMIT"
  S.sql "BEGIN"
  statement uid $ Statement "SELECT pg_advisory_xact_lock(0,$1)"
    (EN.param EN.uid) DE.unit True
  statement (uid, cid, back, ip) $
    Statement sql encode (DE.rowMaybe $ DE.column DE.bytea) True
  where
    encode = contrazip4
      (EN.param EN.uid)
      (EN.param EN.int4)
      (EN.param EN.bool)
      (EN.param EN.inet)
    sql = "WITH page AS (\
          \SELECT COALESCE(url, fixed_head, homepage) AS url, \
          \last_ord AS ord, last_subord AS subord FROM \
          \redir_url_and_last($1, $2, CASE WHEN $3 THEN -1 ELSE 0 END) \
          \CROSS JOIN comics WHERE cid=$2), \
          \history AS (\
          \INSERT INTO redirect_log \
          \(uid, host, cid, ord, subord, max_ord, offset_back, url) SELECT \
          \$1, $4, $2, page.ord, page.subord, \
          \(SELECT max(ord) FROM updates WHERE cid=$2), $3, page.url FROM page), \
          \deleted_recent AS (\
          \DELETE FROM recent WHERE uid=$1 AND cid=$2), r AS (\
          \INSERT INTO recent (uid, cid, ord, subord, used_on) SELECT \
          \$1, $2, page.ord, page.subord, now() FROM page), upd AS (\
          \UPDATE subscriptions SET ord=max_update_ord.ord, subord=max_update_ord.subord+1 \
          \FROM max_update_ord \
          \WHERE uid=$1 AND subscriptions.cid=$2 and max_update_ord.cid=subscriptions.cid) \
          \SELECT url FROM page"

fetchUpcomingSchedule
  :: UserID
  -> Session [(Int, [(Int32, Text)])]
fetchUpcomingSchedule = flip statement $ Statement sql (EN.param EN.uid)
  (fmap (map (\xs -> (fromIntegral $ fst $ head xs, map snd xs)) .
         groupBy (\a b -> fst a == fst b)) . DE.rowList $
   (,)
   <$> DE.column DE.int2
   <*> ((,)
        <$> DE.column DE.int4
        <*> DE.column DE.text)
  ) True
  where
    sql = "SELECT till_scheduled, cid, title FROM comics \
          \JOIN nearest_infer USING (cid) \
          \JOIN comic_remain_frag_cache USING (cid) \
          \WHERE till_scheduled < 24 AND uid=$1 AND num = 0 \
          \ORDER BY till_scheduled, ordering_form(title)"

fromUserWithStats :: UserWithStats -> UpdateOptions
fromUserWithStats = UpdateOptions
  <$> snd . unreadCount
  <*> snd . unreadCountHidden
  <*> offsetBookmarkMode
  <*> holdBookmark
