{-# LANGUAGE OverloadedStrings #-}

module Piperka.Update.Handlers (mayRedir) where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.ByteString.Char8 as B8
import Data.Maybe
import Data.UUID (fromASCIIBytes)
import Snap.Core
import Snap.Snaplet.Hasql

import Application
import Piperka.Auth (currentUserPlain)
import Piperka.Update.Statements (updateAndRedirect)
import Piperka.Util (getParamInt, rqRemote)

mayRedir
  :: AppHandler Bool
mayRedir = currentUserPlain >>= flip maybe redir
  (return False)

-- TODO: Show an error to user.
hushAndReport
  :: Show e
  => AppHandler (Either e (Maybe a))
  -> MaybeT AppHandler a
hushAndReport = (either
                 ((\e -> (lift $ logError $ B8.pack $ show e) >> mzero))
                 (MaybeT . return) =<<) . lift

redir
  :: MyData
  -> AppHandler Bool
redir usr = fmap isJust . runMaybeT $ do
  csrf <- MaybeT $ getParam "csrf_ham"
  guard (fromASCIIBytes csrf == (Just $ ucsrfToken usr))
  cid <- MaybeT $ (fmap (fromIntegral . snd)) <$> getParamInt "redir"
  offset <- maybe False (== "1") <$> lift (getParam "offset_back")
  ip <- lift $ rqRemote <$> getRequest
  url <- hushAndReport $ run $ updateAndRedirect (uid usr) cid offset ip
  lift $ do
    modifyResponse $ setHeader "Referrer-policy" "origin"
    redirect url
