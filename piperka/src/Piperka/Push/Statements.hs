{-# LANGUAGE OverloadedStrings #-}

module Piperka.Push.Statements where

import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Hasql.Session hiding (sql)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Statement

import Application
import qualified Piperka.Encoders as EN

-- Set new_user_update to contain all bookmarked comics with no
-- current unread pages.  updated is set to true in
-- updates_new_user_update trigger function on inserts to updates.
refreshNewUserUpdate
  :: UserID
  -> Session ()
refreshNewUserUpdate usr =
  let stmt x = statement usr $ Statement x (EN.param EN.uid) DE.unit True
  in stmt del >> stmt ins
  where
    del = "DELETE FROM new_user_update WHERE uid=$1"
    ins = "INSERT INTO new_user_update (uid, cid, ord, subord) \
          \SELECT $1, cid, max(ord), coalesce(max(subord), 1) \
          \FROM (SELECT cid, num FROM comic_remain_frag_cache WHERE uid=$1 AND num=0) AS a \
          \JOIN comics USING (cid) JOIN updates USING (cid) \
          \LEFT JOIN page_fragments USING (cid, ord) \
          \GROUP BY cid, num \
          \ON CONFLICT (uid, cid) DO UPDATE SET \
          \ord=EXCLUDED.ord, subord=EXCLUDED.subord, updated=false, notified=false"

updateUsers
  :: Session IntSet
updateUsers = IntSet.fromAscList <$>
  (statement () $ Statement sql EN.unit (DE.rowList decoder) True)
  where
    decoder = DE.column (fromIntegral <$> DE.int4)
    sql = "SELECT DISTINCT uid FROM new_user_update WHERE updated ORDER BY uid"
