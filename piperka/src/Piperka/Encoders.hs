module Piperka.Encoders (uid, authid, opid) where

import Data.Functor.Contravariant
import Hasql.Encoders

import Piperka.Auth.Types
import Piperka.UserID

uid :: Value UserID
uid = (\(UserID u) -> u) >$< int4

authid :: Value AuthID
authid = unAuthid >$< int4

opid :: Value ProviderID
opid = unProviderid >$< int4
