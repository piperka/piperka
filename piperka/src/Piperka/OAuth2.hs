{-# LANGUAGE OverloadedStrings #-}

module Piperka.OAuth2 (piperkaOAuth2) where

import Control.Lens (set)
import Control.Monad.State
import Data.List (find)
import Data.Maybe (fromJust)
import Data.Text (Text)
import Hasql.Session (QueryError)
import Network.HTTP.Client (Manager)
import Snap
import Snap.Snaplet.CustomAuth.OAuth2
import Snap.Snaplet.Hasql (bracketDbOpen)
import Snap.Snaplet.Heist
import Snap.Snaplet.Session
import Snap.Snaplet.CustomAuth

import Application hiding (httpManager)
import qualified Backend
import Piperka.Auth
import Piperka.Auth.Types (AuthID, ProviderID)
import Piperka.Account.Action (actionCallback)
import Piperka.OAuth2.Query

piperkaOAuth2
  :: SnapletLens (Snaplet App) SessionManager
  -> Manager
  -> [(ProviderID, (Text, Text))]
  -> OAuth2Settings MyData AuthID QueryError App
piperkaOAuth2 s m prov = OAuth2Settings
  { oauth2Check = Backend.oauth2Check . parseProvider
  , oauth2Login = Backend.oauth2Login . parseProvider
  , oauth2Failure = handleFailure
  , prepareOAuth2Create = prepareCreate . parseProvider
  , oauth2AccountCreated = accountCreated
  , oauth2LoginDone = loginDone
  , resumeAction = \_ b c -> withTop' id $ actionCallback b c
  , stateStore = s
  , httpManager = m
  , bracket = bracketDbOpen
  }
  where
    -- fromJust because snaplet-customauth makes sure that it's only
    -- called with known values.
    parseProvider = fst . fromJust . flip find prov . (\x -> ((== x) . fst . snd))

handleFailure
  :: OAuth2Stage
  -> Handler App ApiAuth ()
handleFailure SCreate = do
  failData <- withTop apiAuth getAuthFailData
  let retry = do
        withTop' id $ modify $ set suppressError True
        cRender "newUserSelectName_"
  case failData of
    Just (Create DuplicateName) -> retry
    Just (Create InvalidName) -> retry
    _ -> withTop' id $ authHandler False $ cRender "oauth2Failure_"

handleFailure _ = withTop' id $ authHandler False $
                  cRender "oauth2Failure_"

prepareCreate
  :: ProviderID
  -> Text
  -> Handler App ApiAuth (Either QueryError AuthID)
prepareCreate provider text = reserveOAuth2Identity provider text

accountCreated
  :: MyData
  -> Handler App ApiAuth ()
accountCreated usr = do
  withTop auth $ setUser $ defaultUserStats usr
  cRender "welcome_"

loginDone
  :: Handler App ApiAuth ()
loginDone = do
  usr <- withTop apiAuth $ currentUser
  maybe (cRender "newUserSelectName_") (const $ redirect' "/updates.html" 303) usr
