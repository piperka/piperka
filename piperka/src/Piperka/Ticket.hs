{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

-- This module handles ticket form display and its submit handling.
-- The moderator side for handling and displaying them is at
-- Piperka.Maint.Ticket.  For the user side, see Piperka.Ticket.User.

module Piperka.Ticket (renderTicket, renderReason) where

import Control.Applicative (empty)
import Control.Error.Util hiding (err)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import qualified Data.ByteString.Char8 as B
import Data.List
import qualified Data.Map.Strict as M
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8')
import Hasql.Session (QueryError)
import Heist
import Heist.Compiled.LowLevel
import qualified HTMLEntities.Text as HTML
import Network.URI.Encode as URI
import Snap
import Text.XmlHtml as X

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Ticket.Query
import Piperka.Ticket.Types
import Piperka.Util (getParamInt, intToText)

renderTicket
  :: RuntimeAppHandler (Maybe MyData)
renderTicket n = do
  nodes <- X.childElements <$> getParamNode
  let lk x = fromJust $ find ((== x) . X.elementTag) nodes
  p1 <- newEmptyPromise
  p2 <- newEmptyPromise
  [subm, miss] <- mapM (runNodeList . childNodes . lk) ["submitted", "missing"]
  exts <- deferMany
    (withSplices (runNodeList $ childNodes $ lk "exists") extsSplices) $
    getPromise p1
  err <- stdSqlErrorSplice $ getPromise p2
  return $ yieldRuntime $
    either (\e -> putPromise p2 e >> codeGen err) return =<<
    (runExceptT $ do
        u <- lift n
        submRes <- ExceptT $ readSubmit u
        if submRes then lift $ codeGen subm else do
          cid <- fmap snd <$> (lift $ lift $ getParamInt "cid")
          maybe (lift $ codeGen miss)
            (\c -> ExceptT (lift $ getTicketInfo c) >>=
                   lift . putPromise p1 . fmap (c,) >> (lift $ codeGen exts)) cid
    )

readSubmit
  :: Maybe MyData
  -> RuntimeSplice AppHandler (Either QueryError Bool)
readSubmit u = do
  params <- lift $ rqPostParams <$> getRequest
  let lk = join . fmap listToMaybe . flip M.lookup params
      lkInt x = fst <$> (B.readInt =<< lk x)
      lkText x = hush . decodeUtf8' =<< lk x
      lkText' a = (\x -> let x' = T.strip x in
                      guard (not $ T.null x') >> return x') =<< lkText a
      submitData = do
        typ <- lk "type"
        -- Spambots generally won't execute javascript
        lk "jsok" >>= guard . (== "1")
        let comment = lkText' "comment"
        op <- case typ of
          "stale" -> return $ Stale
            (lkText' "first_page")
            (lkText' "homepage")
            (lkText' "title")
          "stuck" -> Stuck <$> lkInt "stuck"
          "gone" -> Gone <$> lkInt "gone"
          "restore" -> return Restore
          _ -> empty
        cid <- lkInt "cid"
        return (cid, comment, op)
  maybe (return $ return False)
    (\e -> runExceptT $ do
        ExceptT $ lift $ saveTicket (uid <$> u) e
        return True) submitData

extsSplices
  :: Splices (RuntimeAppHandler (Int, (Text, TicketInfo)))
extsSplices = do
  "cid" ## pureSplice . textSplice $ intToText . fst
  "title" ## pureSplice . textSplice $ HTML.text . fst . snd
  "live" ## maybeSplice (withSplices runChildren liveSplices) .
    fmap (getLive . snd . snd)
  "dead" ## maybeSplice
    (withSplices runChildren ("removalReason" ## pureSplice . textSplice $ id)) .
    fmap (getDead . snd . snd)
  where
    getDead (DeadTicketInfo r) = Just r
    getDead _ = Nothing
    getLive t@(TicketInfo _ _ _) = Just t
    getLive _ = Nothing
    liveSplices = mapV (pureSplice . textSplice) $ do
      "firstPage" ## maybe "" HTML.text . firstPageI
      "lastPage" ## maybe "" HTML.text . lastPageI
      "homePage" ## HTML.text . homePageI

renderReason
  :: RuntimeAppHandler Submit
renderReason =
  withSplices (callTemplate "/_ticketSubmit") $ do
  "when" ## \n -> do
    cas <- fromJust . X.getAttribute "case" <$> getParamNode
    nil <- callTemplate "_null"
    let maybeField = deferManyElse (return nil) $ pureSplice . textSplice $ HTML.text
        staleSplices = do
          "title" ## maybeField . fmap (\(_,_,t) -> t)
          "firstPage" ## maybeField . fmap (\(f,_,_) -> f)
          "homePage" ## maybeField . fmap (\(_,h,_) -> h)
          "firstPageEnc" ## pureSplice . textSplice $
            maybe "" URI.encodeText . \(f,_,_) -> f
        maybeRun splices f = deferMany (withSplices runChildren splices) $ f <$> n
    case cas of
      "stale" -> maybeRun staleSplices
        (\n' -> case n' of (Stale f h t) -> Just (f, h, t)
                           _ -> Nothing)
      "stuck" -> maybeRun reasonSplices
        (\n' -> case n' of (Stuck r) -> Just r
                           _ -> Nothing)
      "gone" -> maybeRun reasonSplices
        (\n' -> case n' of (Gone r) -> Just r
                           _ -> Nothing)
      "restore" -> maybeRun mempty
        (\n' -> case n' of Restore -> Just ()
                           _ -> Nothing)
      _ -> return mempty
  where
    reasonSplices = "reason" ## \n -> do
      cas <- fromJust . X.getAttribute "case" <$> getParamNode
      deferMany (const runChildren) $
        bool Nothing (Just ()) . (== cas) . intToText <$> n
