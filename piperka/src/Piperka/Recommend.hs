{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Recommend where

import Control.Concurrent
import Control.DeepSeq
import Control.Error.Util (bool)
import Control.Lens
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Int
import qualified Data.IntMap as IntMap
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Map.Syntax
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import Hasql.Session (QueryError(..), CommandError(..))
import Snap.Snaplet.Hasql

import Application
import Piperka.Heist
import Piperka.Recommend.Statements
import Piperka.Recommend.Thread
import Piperka.Util (getCid, getParamInt, intToText)

renderRecommend
  :: RuntimeAppHandler MyData
renderRecommend n =
  let oneEmpty a b = withLocalSplices ((a ## runChildren) >> (b ## pure mempty))
                     mempty runChildren
  in deferManyElse
  (callTemplate "/_develRecommends")
  (deferManyElse
   (oneEmpty "tooFew" "canRecommend")
   (const $ oneEmpty "canRecommend" "tooFew")) $ do
    u <- uid <$> n
    lift $ view recommendHandle >>=
      maybe (pure Nothing)
      (fmap (Just . bool Nothing (Just ()) . hasRecommends u) . liftIO . readModel)

-- Fetch recommendations and process parameters filtering the results.
recommends
  :: Bool
  -> UserID
  -> AppHandler (Either QueryError (Vector Int32))
recommends onlyUnsub usr = do
  i <- (>>= ((>>)
             <$> guard . ((&&) <$> (<= 2000) <*> (>= 200))
             <*> return)) . fmap snd <$> getParamInt "newest"
  top <- maybe (Just 100) (((>>) <$> guard . (>0) <*> Just) . snd) <$> getParamInt "top"
  runExceptT $ do
    lim <- maybe (pure Nothing) (fmap Just . ExceptT . run . fetchNewestLim . fromIntegral) i
    let (c, f) = maybe (top, id)
          (\l -> (Nothing, maybe id V.take top .
                           V.filter (>= fromIntegral l))) lim
    lift (view recommendHandle) >>=
      maybe (throwE $ QueryError "" [] $ ClientError $ Just "development mode")
      (fmap (f . V.fromList . getRecommends onlyUnsub c usr) . liftIO . readModel)

dimensions
  :: AppHandler (HashMap Text (Vector Int))
dimensions =
  view recommendHandle >>=
  maybe (pure mempty)
  (fmap (HashMap.fromList . map (bimap intToText V.fromList) . IntMap.toList . getDimensions) .
   liftIO . readModel)

prefetchRecommends
  :: AppHandler ()
prefetchRecommends = void $ runMaybeT $ do
  c <- MaybeT $ (fmap snd) <$> getCid
  h <- MaybeT $ view recommendHandle
  lift $ do
    var <- liftIO newEmptyMVar
    modify $ set recommendComics var
    liftIO $ forkIO $ do
      rel <- getRelated 10 c <$> readModel h
      deepseq rel $ putMVar var rel
