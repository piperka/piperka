{-# LANGUAGE OverloadedStrings #-}

module Piperka.Subscription where

import Contravariant.Extras.Contrazip
import Control.Monad
import Data.ByteString.Char8 (split)
import Data.Functor.Contravariant
import Data.Maybe
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run)
import Hasql.Statement
import Snap.Core

import Application
import qualified Piperka.Encoders as EN
import Piperka.Subscription.Types
import Piperka.Util (maybeParseInt)

getRequestSubscriptions
  :: AppHandler (Vector (Int, Maybe Int))
getRequestSubscriptions =
  (V.fromList . mapMaybe (toBookmark . map maybeParseInt . take 2 . split ' ') .
   join . maybeToList . rqParam "bm") <$> getRequest
  where
    toBookmark [Just c, Just o] = Just (c, Just o)
    toBookmark [Just c, _] = Just (c, Nothing)
    toBookmark [Just c] = Just (c, Nothing)
    toBookmark _ = Nothing

encodeBookmarkSets
  :: EN.Params (Vector (Int, Maybe Int))
encodeBookmarkSets = let vector = EN.param . EN.array . EN.dimension V.foldl'
  in contramap V.unzip $ contrazip2
     (vector $ EN.element $ fromIntegral >$< EN.int4)
     (vector $ EN.nullableElement $ fromIntegral >$< EN.int4)

anonymousBookmarks
  :: Vector (Int, Maybe Int)
  -> Session (Vector Subscription)
anonymousBookmarks bs = statement bs $ Statement sql1
  encodeBookmarkSets
  (DE.rowVector decodeSubscription)
  True
  where
    sql1 =
      "SELECT cid, ord, max_ord, max_subord, max_ord-ord+1, till_scheduled \
      \FROM (SELECT cid, GREATEST(0, LEAST(d.ord, 1+max_update_ord.ord)) AS ord, \
      \ max_update_ord.ord AS max_ord, max_update_ord.subord AS max_subord \
      \ FROM comics \
      \ JOIN UNNEST($1 :: int[], $2 :: int[]) AS d (cid, ord) USING (cid)\
      \ JOIN max_update_ord USING (cid)) AS d \
      \LEFT JOIN nearest_infer USING (cid)"

setBookmarks
  :: Vector (Int, Maybe Int)
  -> UserID
  -> Session ()
setBookmarks ss u = statement (u, ss) $ Statement sql1
  (contrazip2
   (EN.param EN.uid)
   encodeBookmarkSets
  ) DE.unit True
  where
    sql1 =
      "INSERT INTO subscriptions (uid, cid, ord, subord) \
      \SELECT $1, cid, ord, 0 \
      \FROM (SELECT cid, GREATEST(0, LEAST(d.ord, 1+max_update_ord.ord)) AS ord \
      \ FROM UNNEST($2 :: int[], $3 :: int[]) AS d (cid, ord) \
      \ JOIN max_update_ord USING (cid) \
      \ JOIN comics USING (cid)) AS a \
      \ON CONFLICT (uid, cid) DO UPDATE SET ord=EXCLUDED.ord"
