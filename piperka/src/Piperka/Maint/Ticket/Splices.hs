{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Maint.Ticket.Splices (renderTicketDetail, renderTicketList) where

import Control.Error.Util hiding (err)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import qualified Data.ByteString.Char8 as B
import qualified Data.Map.Strict as M
import Data.Map.Syntax
import Data.Maybe
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8')
import Hasql.Session (QueryError)
import Heist
import qualified HTMLEntities.Text as HTML
import Snap

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Maint.Ticket.Query
import Piperka.Maint.Ticket.Types
import Piperka.Ticket (renderReason)
import Piperka.Util (formatTime', intToText)

renderTicketList
  :: RuntimeAppHandler MyData
renderTicketList = const $ eitherDefer stdSqlErrorSplice
  (deferMany runTwice) $ do
  notCid <- isNothing <$> (lift $ getQueryParam "cid")
  isPost <- (== POST) . rqMethod <$> lift getRequest
  case (notCid, isPost) of
    (_, True) -> lift $ runExceptT $ do
      ExceptT saveTicketReply
      Just <$> ExceptT getTickets
    (True, _) -> fmap Just <$> getTickets
    _ -> return $ Right Nothing
  where
    resolvedSplice t = withLocalSplices (resolvedSplices t) mempty runChildren
    resolvedSplices t = do
      "resolved" ## if t then runChildren else return mempty
      "notResolved" ## if t then return mempty else runChildren
    ticketSplices = mapV (pureSplice . textSplice) $ do
      "cid" ## intToText . ticketCid . fst
      "title" ## title . fst
      "stamp" ## T.pack . formatTime' . stamp . fst
      "count" ## intToText . snd
    ticketAttrSplices = do
      "class" ## \n _ -> do
        rm <- removalRequest . fst <$> n
        return $ if rm then [("class", "youCare")] else []
    renderTickets t = withSplices (resolvedSplice t)
      ("comics" ## manyWith runChildren ticketSplices ticketAttrSplices)
    runTwice n = do
      a <- renderTickets False $ fst <$> n
      b <- renderTickets True $ snd <$> n
      return $ a <> b

saveTicketReply
  :: AppHandler (Either QueryError ())
saveTicketReply = do
  params <- getParams
  let getInt n = fmap fst . B.readInt =<< join (listToMaybe <$> M.lookup n params)
      rpy = (,,,)
        <$> getInt "cid"
        <*> ((== ["all"]) <$> M.lookup "to_close" params)
        <*> pure (getInt "to_close")
        <*> (hush . decodeUtf8' =<<
             (join $ listToMaybe <$> M.lookup "resolve_msg" params))
      rpy' = case rpy of
        (Just (c, True, _, m)) -> Just $ Right (c, m)
        (Just (_, False, Just t, m)) -> Just $ Left (t, m)
        _ -> Nothing
  maybe (return $ Right ()) (either saveResolution saveResolutionAll) rpy'

renderTicketDetail
  :: RuntimeAppHandler a
renderTicketDetail = const $
  eitherDefer stdSqlErrorSplice
  (deferMany
   (withSplices runChildren splices)) $ do
  c <- (fmap fst . B.readInt =<<) <$> (lift $ getQueryParam "cid")
  resolved <- (== (Just "1")) <$> (lift $ getParam "resolved")
  runExceptT $ do
    detail <- maybe (return Nothing) (ExceptT . getTicketComicDetail) c
    return $ (,)
      <$> fmap (resolved,) c
      <*> detail

  where
    splices = do
      "tickets" ## eitherDefer stdSqlErrorSplice
        (manyWithSplices runChildren detailSplices) . (getComicTickets . fst =<<)
      "notResolved" ## checkedSplice . fmap (not . fst . fst)
      (mapV (pureSplice . textSplice) $ do
          "cid" ## intToText . snd . fst
        )
      (mapV (\(f, s) n -> do
                nul <- callTemplate "_null"
                return $ yieldRuntime $
                  f . snd <$> n >>=
                  maybe (codeGen nul) (return . textSplice s)) $ do
          "bookmarkRegexp" ## (bookmarkRegexp, HTML.text)
          "parserType" ## (fmap (intToText . parserType) . crawlParams, id)
          "updateValue" ## (fmap (T.pack . show . updateValue) . crawlParams, id)
          "extraURL" ## (extraURL <=< crawlParams, HTML.text)
          "extraData" ## (extraData <=< crawlParams, HTML.text)
          "updateScore" ## (fmap (T.pack . show) . (updateScore <=< crawlParams), id)
        )

detailSplices
  :: Splices (RuntimeAppHandler TicketDetail)
detailSplices =
  (mapV (pureSplice . textSplice) $ do
      "cid" ## intToText . ticketCid . listTicket
      "tid" ## intToText . ticketTid
      "title" ## title . listTicket
      "stamp" ## T.pack . formatTime' . stamp . listTicket
      "comment" ## HTML.text . maybe "" id . fst . reason
  ) <>
  ("resolved" ## maybeSplice
   (withSplices runChildren
    (mapV (pureSplice . textSplice) $ do
        "msg" ## HTML.text . fst
        "stamp" ## T.pack . formatTime' . snd
    )) . fmap resolution
  ) <>
  ("reporter" ##
    (\n -> do
        nul <- callTemplate "_null"
        return $ yieldRuntime $
          n >>= maybe (codeGen nul) (return . textSplice HTML.text)) . fmap reporter
  ) <>
  ("ticketSubmit" ## renderReason . fmap (snd . reason))
