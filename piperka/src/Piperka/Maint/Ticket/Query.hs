{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Maint.Ticket.Query where

import Contravariant.Extras.Contrazip
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Aeson (fromJSON, Result(Success))
import Data.Bifunctor
import Data.Functor.Contravariant
import Data.List (partition)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Time.LocalTime
import Data.Vector (Vector, fromList)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql

import Piperka.Maint.Ticket.Types
import Piperka.Ticket.Types

decodeTicketListTicket
  :: DE.Row TicketListTicket
decodeTicketListTicket = TicketListTicket
  <$> (fromIntegral <$> DE.column DE.int4)
  <*> (HTML.text <$> DE.column DE.text)
  <*> DE.column DE.bool
  <*> (localTimeToUTC utc <$> DE.column DE.timestamp)

decodeReason
  :: MaybeT DE.Row (Maybe Text, Submit)
decodeReason = do
  val :: Result (Maybe Text, Submit) <-
    lift $ fromJSON <$> DE.column DE.jsonb
  case val of Success a -> return a
              _ -> mzero

getTickets
  :: (HasHasql m, MonadIO m)
  => m (Either QueryError (Vector (TicketListTicket, Int), Vector (TicketListTicket, Int)))
getTickets =
  (fmap (join bimap (fromList . map snd) . partition fst)) <$>
  (run $ statement () $ Statement sql EN.unit (DE.rowList decode) True)
  where
    decode = (,)
      <$> DE.column DE.bool
      <*> ((,)
           <$> decodeTicketListTicket
           <*> (fromIntegral <$> DE.column DE.int4))
    sql = "SELECT NOT resolved, cid, title, \
          \'gone' = ANY(ARRAY_AGG(ticket_type)), \
          \MAX(stamp), COUNT(*) FROM \
          \(SELECT cid, title, stamp, message->1->>'type' AS ticket_type, \
          \tid IN (SELECT tid FROM ticket_resolve) AS resolved \
          \FROM ticket JOIN all_comics USING (cid)) AS x \
          \GROUP BY cid, title, resolved \
          \ORDER BY ordering_form(title)"

getTicketComicDetail
  :: (HasHasql m, MonadIO m)
  => Int
  -> m (Either QueryError (Maybe TicketComicDetail))
getTicketComicDetail c = run $ runMaybeT $ TicketComicDetail
  <$> (MaybeT $ statement c $ Statement sql1 encode
       (DE.rowMaybe $ DE.nullableColumn DE.text) True)
  <*> (lift $ statement c $ Statement sql2 encode (DE.rowMaybe decode) True)
  where
    encode = fromIntegral >$< EN.param EN.int4
    decode = TicketCrawlParams
      <$> (fromIntegral <$> DE.column DE.int4)
      <*> DE.nullableColumn DE.float4
      <*> DE.column DE.float4
      <*> DE.nullableColumn DE.text
      <*> DE.nullableColumn DE.text
    sql1 = "SELECT bookmark_regexp FROM all_comics WHERE cid=$1"
    sql2 = "SELECT parser_type, update_score, update_value, \
           \extra_url, extra_data FROM crawler_config WHERE cid=$1"

getComicTickets
  :: (HasHasql m, MonadIO m)
  => (Bool, Int)
  -> m (Either QueryError (Vector TicketDetail))
getComicTickets c = fmap (fromList . catMaybes) <$>
  (run $ statement c $ Statement sql
   (contrazip2 (EN.param EN.bool) (fromIntegral >$< EN.param EN.int4))
   (DE.rowList decode) True)
  where
    decode = runMaybeT $ TicketDetail
      <$> (lift (fromIntegral <$> DE.column DE.int4))
      <*> lift (DE.nullableColumn DE.text)
      <*> (lift decodeTicketListTicket)
      <*> decodeReason
      <*> (lift $ do
              a <- DE.nullableColumn DE.text
              b <- (fmap $ localTimeToUTC utc) <$> DE.nullableColumn DE.timestamp
              return $ (,) <$> a <*> b
          )
    sql = "SELECT tid, name, cid, title, 'gone' = message->1->>'type', \
          \ticket.stamp, message, \
          \ticket_resolve.msg, ticket_resolve.stamp \
          \FROM ticket JOIN \
          \(SELECT cid, title FROM comics UNION \
          \SELECT cid, title FROM graveyard) AS x USING (cid) \
          \LEFT JOIN ticket_resolve USING (tid) \
          \LEFT JOIN users USING (uid) \
          \WHERE ticket_resolve.msg IS NOT NULL = $1 \
          \AND cid=$2 ORDER BY ticket.stamp DESC"

saveResolutionAll
  :: (HasHasql m, MonadIO m)
  => (Int, Text)
  -> m (Either QueryError ())
saveResolutionAll x = run $ statement x $ Statement sql
  (contrazip2 (fromIntegral >$< EN.param EN.int4) (EN.param EN.text)) DE.unit True
  where
    sql = "INSERT INTO ticket_resolve (tid, msg) \
          \SELECT tid, $2 FROM ticket \
          \WHERE tid NOT IN (SELECT tid FROM ticket_resolve) AND cid=$1"

saveResolution
  :: (HasHasql m, MonadIO m)
  => (Int, Text)
  -> m (Either QueryError ())
saveResolution x = run $ statement x $ Statement sql
  (contrazip2 (fromIntegral >$< EN.param EN.int4) (EN.param EN.text)) DE.unit True
  where
    sql = "INSERT INTO ticket_resolve (tid, msg) VALUES ($1, $2)"
