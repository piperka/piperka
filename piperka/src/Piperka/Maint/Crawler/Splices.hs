{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Crawler.Splices (renderCrawlerControl, renderCrawlerArchive) where

import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Int (Int32)
import Data.Map.Syntax
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Vector as V
import Heist
import Heist.Splices
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql
import qualified Text.XmlHtml as X

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (getCid, getParamInt, intToText)
import Piperka.Maint.Crawler.Query

import Crawler.DB.Fetch (fetchNewCid)
import Crawler.Types hiding (parserType)

getCidAndSourceCid :: RuntimeSplice AppHandler (Maybe (Int32, Maybe Int32))
getCidAndSourceCid = lift $ runMaybeT $
  (,)
  <$> (fromIntegral . snd <$> MaybeT getCid)
  <*> (lift $ fmap (fromIntegral . snd) <$> getParamInt "source")

renderCrawlerControl
  :: RuntimeAppHandler MyData
renderCrawlerControl _ =
  deferManyElse
  runWithEmptySplices
  (withLocalSplices
   <$> (("sourceMode" ##) . sourceModeSplice . fmap snd)
   <*> (("sourceMode" ##) . sourceModeAttrSplice)
   <*> (eitherDefer stdSqlErrorSplice
        (deferManyElse runWithEmptySplices
         (withSplices runChildren (splices)
         )) . (lift . run . fetchComicParams . fst =<<))
  ) getCidAndSourceCid
  where
    runWithEmptySplices =
      eitherDefer stdSqlErrorSplice
      (withSplices
       (withLocalSplices (emptySplices configSpliceNames)
        ("sourceMode" ## \x -> return $
          if not . read $ T.unpack x then [] else [("disabled", "disabled")]) runChildren)
       ("cid" ## pureSplice . textSplice $ intToText)) $
      run fetchNewCid
    configSpliceNames = [ "parserType"
                        , "urlBase"
                        , "urlTail"
                        , "homepage"
                        , "fixedHead"
                        , "extraURL"
                        , "extraData"
                        , "sourceMode"
                        ]
    splices = mapV (pureSplice . textSplice) $ do
      "cid" ## intToText . cid . fst
      "homepage" ## HTML.text . homepage . fst
      "fixedHead" ## maybe mempty HTML.text . fixedHead . fst
      "parserType" ## intToText . parserType . fst
      "urlBase" ## HTML.text . urlBase . snd
      "urlTail" ## HTML.text . urlTail . snd
      "extraURL" ## maybe mempty HTML.text . extraURL . snd
      "extraData" ## maybe mempty HTML.text . extraData . snd
    sourceModeSplice =
      manyWithSplices runChildren ("source" ## pureSplice . textSplice $ intToText)
    sourceModeAttrSplice n x = do
      let doDisable = read $ T.unpack x
      hasSource <- isJust . snd <$> n
      return $ if hasSource /= doDisable then [("disabled", "disabled")] else []

renderCrawlerArchive
  :: RuntimeAppHandler MyData
renderCrawlerArchive _ =
  deferManyElse
  runWithEmptySplices
  (eitherDefer stdSqlErrorSplice
   (\n -> withLocalSplices
     ("archiveRows" ## archiveRows n)
     ("currentId" ## currentIdSplice n) runChildren) . (fetchArchives =<<)
  ) getCidAndSourceCid
  where
    fetchArchives (c, sourceCid) = run $
      (,)
      <$> fetchArchive c c
      <*> (maybe (return Nothing) (fmap Just . fetchArchive c) sourceCid)
    runWithEmptySplices =
      withLocalSplices (emptySplices ["archiveRows"])
      ("currentId" ## const $ return [("id", "tgt-0")]) runChildren
    archiveRows n = do
      useSourceResults <- maybe False (read . T.unpack) . X.getAttribute "source" <$> getParamNode
      manyWith runChildren splices attrSplices $
        (if useSourceResults then fromMaybe <$> fst <*> snd else fst) <$> n
    currentIdSplice n = const $ do
      xs <- (fromMaybe <$> fst <*> snd) <$> n
      return [("id", "tgt-" <> (intToText $ V.length xs))]
    splices = do
      "ifBookmarks" ## ifCSplice ((0<) . bookmarks)
      "archiveLink" ##
        (deferManyElse (return $ yieldPureText "current") $ withSplices runChildren $
         mapV (pureSplice . textSplice) $ do
            "name" ## HTML.text . fst
            "href" ## HTML.text . snd) . fmap archivePage
      mapV (pureSplice . textSplice) $ do
        "ord" ## intToText . archiveOrd
        "bookmarks" ## intToText . bookmarks
    attrSplices = do
      "id" ## \n t -> do
        ord <- archiveOrd <$> n
        return [("id", t <> (intToText ord))]
      "archiveClass" ## \n -> const $ do
        hasBookmarks <- (0<) . bookmarks <$> n
        isCurrent <- isNothing . archivePage <$> n
        return $ if (hasBookmarks || isCurrent)
          then [("class", foldr1 (\a b -> a <> " " <> b) $
                  (if hasBookmarks then ("has_bookmarks":) else id) .
                  (if isCurrent then ("current":) else id) $ [])]
          else []
