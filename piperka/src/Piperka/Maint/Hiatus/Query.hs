{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Hiatus.Query where

import Contravariant.Extras.Contrazip
import Data.Functor.Contravariant
import Data.Int
import Data.Text (Text)
import Data.Time.Clock
import Data.Time.LocalTime
import Data.Vector (Vector)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement

import Application hiding (uid)
import qualified Piperka.Encoders as EN

fetchPendingHiatus
  :: Session (Vector (Int32, Maybe UTCTime, Text))
fetchPendingHiatus = statement () $ Statement sql EN.unit (DE.rowVector decoder) True
  where
    decoder = (,,)
      <$> DE.column DE.int4
      <*> DE.nullableColumn (localTimeToUTC utc <$> DE.timestamp)
      <*> DE.column DE.text
    sql = "SELECT cid, stamp, title FROM hiatus_comics JOIN comics USING (cid) \
          \JOIN crawler_config USING (cid) \
          \WHERE cid NOT IN (SELECT cid FROM hiatus_defer \
          \ WHERE stamp > now() - '2 weeks'::interval) \
          \AND cid NOT IN (SELECT cid FROM hiatus_status WHERE active=true) \
          \AND crawler_config.last_updated < now() - '5 mons'::interval \
          \ORDER BY readers DESC, ordering_form(title) LIMIT 100"

deferHiatus
  :: Int
  -> Session ()
deferHiatus cid = statement cid $ Statement sql
  (fromIntegral >$< EN.param EN.int4) DE.unit True
  where
    sql = "INSERT INTO hiatus_defer VALUES ($1) \
          \ON CONFLICT (cid) DO UPDATES SET stamp=now()"

setHiatusStatus
  :: UserID
  -> Int
  -> Int
  -> Session ()
setHiatusStatus uid cid code = statement (uid, cid, code) $ Statement sql
  (contrazip3 (EN.param EN.uid)
   (fromIntegral >$< EN.param EN.int4)
   (fromIntegral >$< EN.param EN.int2)) DE.unit True
  where
    sql = "INSERT INTO hiatus_status (uid, cid, code) VALUES ($1, $2, $3)"
