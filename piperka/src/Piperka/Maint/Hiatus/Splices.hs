{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Hiatus.Splices where

import Control.Monad.Trans
import Data.Map.Syntax
import Data.Maybe
import qualified Data.Text as T
import Heist.Splices
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Maint.Hiatus.Query
import Piperka.Util (intToText, getCid, formatTime')

renderHiatus
  :: RuntimeAppHandler MyData
renderHiatus = const $ flip (withSplices runChildren) (lift getCid) $ do
  "single" ## ifCSplice isJust
  "all" ## deferMany (const (withLocalSplices
                             ("pendingHiatus" ## renderPendingClassification)
                             mempty runChildren)) .
    fmap (maybe (Just ()) (const Nothing))

renderPendingClassification
  :: Splice AppHandler
renderPendingClassification =
  eitherDefer stdSqlErrorSplice render $ run fetchPendingHiatus
  where
    render = manyWithSplices runChildren $ mapV (pureSplice . textSplice) $ do
      "cid" ## intToText . \(c,_,_) -> c
      "stamp" ## maybe "" (T.pack . formatTime') . \(_,s,_) -> s
      "title" ## HTML.text . \(_,_,t) -> t
