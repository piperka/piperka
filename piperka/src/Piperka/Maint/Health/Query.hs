{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Health.Query where

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad
import Data.Aeson
import Data.Functor
import Data.Functor.Contravariant
import qualified Data.HashMap.Strict as Map
import qualified Data.Text as T
import Data.Time.LocalTime
import Data.Vector (Vector)
import qualified Data.Vector as V
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Network.URI

import Piperka.Maint.Health.Internal
import Piperka.Maint.Health.Types hiding (cid, target)

-- Recent failures for crawl attempts with HTTP or curl errors when
-- accessing the archive page stored on Piperka with no recent
-- successes.
fetchCrawlFailures
  :: Session (Vector CrawlFailure)
fetchCrawlFailures = statement () $ Statement sql (EN.unit) (DE.rowVector decoder) True
  where
    decoder = CrawlFailure
      <$> (fromIntegral <$> DE.column DE.int4)
      <*> DE.column DE.text
      <*> DE.nullableColumn DE.text
      <*> (do
              let lk k (Object o) = Map.lookup k o
                  lk _ _ = Nothing
                  res (Error err) = Left err
                  res (Success x) = Right x
              val <- DE.column DE.jsonb
              return $ (,)
                <$> (maybe (Left "end not found in jsonb")
                     (res . fromJSON) . lk "end") val
                <*> (maybe (pure Nothing)
                     (fmap Just . res . fromJSON) . lk "parse") val
          )
      <*> (liftA (fmap $ localTimeToUTC utc) $ DE.nullableColumn DE.timestamp)
    sql = "SELECT cid, title, origin, event, last_success \
          \FROM crawler_health JOIN comics USING (cid)"

fetchBaseRedirects
  :: Session (Vector BaseRedir)
fetchBaseRedirects = statement () $ Statement sql EN.unit (DE.rowVector decoder) True
  where
    decoder = do
      basic <- BaseRedir
        <$> (DE.column $ fromIntegral <$> DE.int4)
        <*> DE.column DE.text
        <*> DE.column DE.text
        <*> DE.column DE.text
      target <- DE.column DE.text
      rf <- do
        -- Try to make an absolute URI from it
        redir <- DE.nullableColumn
          ((\x' -> let x = T.unpack x' in
               maybe (Left x') Right $
               parseURI x <|>
               (relativeTo
                <$> parseRelativeReference x
                <*> parseURI (T.unpack target))) <$> DE.text)
        code <- DE.nullableColumn (fromIntegral <$> DE.int4)
        pure $ maybe (Left 0) id $ (Right <$> redir) <|> (Left <$> code)
      pure $ basic target rf
    sql = "SELECT cid, title, url_base, homepage, origin_url, fail_url, fail_code \
          \FROM crawl_head_redirects JOIN comics USING (cid) ORDER BY cid"

applyAutoFix
  :: Int
  -> Session Bool
applyAutoFix cid =
  -- Wasteful implementation but good enough for maint stuff
  (fetchBaseRedirects <&> (V.find ((== cid) . cid'') >=> autoFix)) >>=
  maybe (pure False) (autoFix' >=> (const $ pure True))
  where
    autoFix' (WebToonsGenre g) =
      statement (cid, g) $ Statement
      "UPDATE comics SET url_base=regexp_replace(url_base, \
      \'^(https://www.webtoons.com/[^/]+/)[^/]+(/.+)', '\\1' || $2 || '\\2'), \
      \homepage=regexp_replace(homepage, \
      \'^(https://www.webtoons.com/[^/]+/)[^/]+(/.+)', '\\1' || $2 || '\\2') \
      \WHERE cid=$1"
      (contrazip2 (EN.param (fromIntegral >$< EN.int4)) (EN.param EN.text))
      DE.unit True
    autoFix' (ProtocolWWW prot www) = do
      flip (maybe (return ())) prot $ \p ->
        statement (cid, p) $ Statement
        "UPDATE comics SET url_base=regexp_replace(url_base, \
        \CASE WHEN $2 THEN '^http(://.+)' ELSE '^https(://.+)' END, \
        \CASE WHEN $2 THEN 'https\\1' ELSE 'http\\1' END) \
        \, homepage=regexp_replace(homepage, \
        \CASE WHEN $2 THEN '^http(://.+)' ELSE '^https(://.+)' END, \
        \CASE WHEN $2 THEN 'https\\1' ELSE 'http\\1' END) \
        \WHERE cid=$1"
        (contrazip2 (EN.param (fromIntegral >$< EN.int4)) (EN.param EN.bool))
        DE.unit True
      flip (maybe (return ())) www $ \w ->
        statement (cid, w) $ Statement
        "UPDATE comics SET url_base=\
        \CASE WHEN $2 = url_base SIMILAR TO 'https?://www.%' \
        \THEN url_base ELSE regexp_replace(url_base, \
        \CASE WHEN $2 THEN '^(https?://)(.+)' ELSE '^(https?://)www\\.(.+)' END, \
        \'\\1' || CASE WHEN $2 THEN 'www.' ELSE '' END || '\\2') END \
        \, homepage=\
        \CASE WHEN $2 = homepage SIMILAR TO 'https?://www.%' \
        \THEN homepage ELSE regexp_replace(homepage, \
        \CASE WHEN $2 THEN '^(https?://)(.+)' ELSE '^(https?://)www\\.(.+)' END, \
        \'\\1' || CASE WHEN $2 THEN 'www.' ELSE '' END || '\\2') END \
        \WHERE cid=$1"
        (contrazip2 (EN.param (fromIntegral >$< EN.int4)) (EN.param EN.bool))
        DE.unit True
    autoFix' (TrailingSlash trailing) =
      statement (cid, trailing) $ Statement
      "UPDATE comics SET url_tail=CASE WHEN $2 THEN '/' ELSE '' END \
      \WHERE cid=$1"
      (contrazip2 (EN.param (fromIntegral >$< EN.int4)) (EN.param EN.bool))
      DE.unit True
