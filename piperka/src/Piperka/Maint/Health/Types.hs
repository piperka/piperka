module Piperka.Maint.Health.Types where

import Data.Text (Text)
import Data.Time.Clock
import Network.URI (URI)

import Crawler.Types

class HealthBasics a where
  cid :: a -> Int
  title :: a -> Text

data CrawlFailure = CrawlFailure
  { cid' :: Int
  , title' :: Text
  , origin :: Maybe Text
  , end :: Either String (CrawlEndCondition, Maybe ParseResult)
  , lastSuccess :: Maybe UTCTime
  }

instance HealthBasics CrawlFailure where
  cid = cid'
  title = title'

data BaseRedir = BaseRedir
  { cid'' :: Int
  , title'' :: Text
  , base :: Text
  , homepage :: Text
  , target :: Text
  , redirectOrFailcode :: Either Int (Either Text URI)
  } deriving (Show)

instance HealthBasics BaseRedir where
  cid = cid''
  title = title''

data AutoCorrection = WebToonsGenre Text
                    | ProtocolWWW (Maybe Bool) (Maybe Bool)
                    | TrailingSlash Bool
  deriving (Show, Eq)
