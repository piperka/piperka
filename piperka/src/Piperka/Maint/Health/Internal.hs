{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Piperka.Maint.Health.Internal where

import Control.Applicative
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.ICU as ICU

import Piperka.Maint.Health.Types
import Piperka.Util (uriToText)

redirEnc
  :: a
  -> (T.Text -> a)
  -> BaseRedir
  -> a
redirEnc x f = either (const x) (either (const x) (f . uriToText)) . redirectOrFailcode

autoFix
  :: BaseRedir
  -> Maybe AutoCorrection
autoFix (x@BaseRedir{..}) = redirEnc Nothing Just x >>= \redirect ->
  (do
      m1 <- ICU.find wtRegex base
      m2 <- ICU.find wtRegex redirect
      -- Same language
      ((==) <$> ICU.group 1 m1 <*> ICU.group 1 m2) >>= guard
      -- Same name
      ((==) <$> ICU.group 3 m1 <*> ICU.group 3 m2) >>= guard
      -- Genre change
      g <- ICU.group 2 m2
      ((/= g) <$> ICU.group 2 m1) >>= guard
      pure $ WebToonsGenre g
  ) <|>
  (do
      m1 <- ICU.find protRegex base
      m2 <- ICU.find protRegex redirect
      m3 <- ICU.find protRegex homepage
      b <- ICU.find protRegex base >>= ICU.group 3
      -- Same base sans protocol and www prefix
      guard . (== b) . T.take (T.length b) =<< ICU.group 3 m2
      guard . (== (T.takeWhile (/= '/') b)) . T.takeWhile (/= '/') =<< ICU.group 3 m3
      p1 <- ICU.group 1 m1
      p2 <- ICU.group 1 m2
      w1 <- ICU.group 2 m1
      w2 <- ICU.group 2 m2
      let p = ProtocolWWW
              (case (p1, p2) of
                 ("http", "https") -> Just True
                 ("https", "http") -> Just False
                 _ -> Nothing)
              (case (w1, w2) of
                 ("", "www.") -> Just True
                 ("www.", "") -> Just False
                 _ -> Nothing)
      guard $ p /= ProtocolWWW Nothing Nothing
      pure p
  ) <|>
  (do
      guard . not $ T.null target || T.null redirect
      -- True if slash needs to be added
      trailing <- let tTrail = '/' == T.last target
                      rTrail = '/' == T.last redirect
                  in case (tTrail, rTrail) of
                       (True, False) -> Just False
                       (False, True) -> Just True
                       _ -> Nothing
      guard $ if trailing then target == T.init redirect else T.init target == redirect
      pure $ TrailingSlash trailing
  )
  where
    wtRegex = ICU.regex [] "^https://www.webtoons.com/([^/]+)/([^/]+)/([^/]+)/.*"
    protRegex = ICU.regex [] "^(https?)://(www\\.)?(.*)"
