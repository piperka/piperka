{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Piperka.Maint.Query where

import Contravariant.Extras.Contrazip
import Control.Concurrent.Async
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.ByteString.Lazy (writeFile)
import Data.Int
import Data.Functor.Contravariant
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import qualified Hasql.Session as S
import Hasql.Statement
import Prelude hiding (writeFile)
import Snap.Snaplet.Hasql

import Application
import Piperka.Maint.Types
import Piperka.Util (intToText)

precrawlArchive
  :: (HasHasql m, MonadIO m)
  => m (Either QueryError (Vector (Maybe Text)))
precrawlArchive =
  run $ statement () $ Statement sql EN.unit
  (DE.rowVector $ DE.nullableColumn DE.text) True
  where
    sql = "SELECT name FROM updates \
          \WHERE cid=COALESCE((SELECT max(cid)+1 FROM comics), 1) ORDER BY ord"

insertComic
  :: (HasHasql m, MonadIO m)
  => Genentry
  -> ExceptT QueryError m ()
insertComic e =
  ExceptT $ run $ statement e $ Statement sql encoder DE.unit True
  where
    encodeList = EN.param . EN.array . EN.dimension foldl . EN.element
    encoder = mconcat
      [ fromIntegral . cid >$< EN.param EN.int4
      , title >$< EN.param EN.text
      , homepage >$< EN.param EN.text
      , fixedHead >$< EN.nullableParam EN.text
      , urlBase >$< EN.param EN.text
      , urlTail >$< EN.param EN.text
      , description >$< EN.param EN.text
      , bookmarkRegexp >$< EN.nullableParam EN.text
      , parserType >$< EN.param EN.int4
      , extraData >$< EN.nullableParam EN.text
      , extraUrl >$< EN.nullableParam EN.text
      , tags >$< encodeList EN.int2
      , unzip . epedias >$< contrazip2 (encodeList EN.int2) (encodeList EN.text)
      ]
    sql = "WITH new_comic AS (\
          \INSERT INTO comics (cid, title, homepage, fixed_head, \
          \url_base, url_tail, description, bookmark_regexp) \
          \VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING cid), \
          \new_crawler AS (\
          \INSERT INTO crawler_config (cid, parser_type, extra_data, extra_url) \
          \SELECT new_comic.cid, $9, $10, $11 FROM new_comic), \
          \new_tags AS (\
          \INSERT INTO comic_tag SELECT new_comic.cid, unnest($12) FROM new_comic), \
          \new_epedia AS (\
          \INSERT INTO external_entry \
          \SELECT new_comic.cid, epid, entry \
          \FROM new_comic, unnest ($13, $14) AS u (epid, entry)) \
          \SELECT new_comic.cid FROM new_comic"

deleteSubmit
  :: (HasHasql m, MonadIO m)
  => Int
  -> ExceptT QueryError m ()
deleteSubmit s =
  ExceptT $ run $ statement s $ Statement sql
  (fromIntegral >$< EN.param EN.int4) DE.unit True
  where
    sql = "DELETE FROM submit WHERE sid=$1"

refreshAlphabet
  :: Session ()
refreshAlphabet = S.sql
  "REFRESH MATERIALIZED VIEW alphabet_index"

commit
  :: (HasHasql m, MonadIO m)
  => ExceptT QueryError m ()
commit = ExceptT $ run $ S.sql "commit"

data OrderedComic = OrderedComic
  { oCid :: Value
  , oTitle :: Value
  , oAliases :: Vector Text
  , oNSFW :: Bool
  }

comicTitles
  :: (HasHasql m, MonadIO m)
  => SiteIdentity
  -> ExceptT QueryError m ()
comicTitles site = do
  allComics <- ExceptT $ run $ statement () $ Statement sql1 EN.unit
    (DE.rowList $
     (,,)
     <$> (intToText <$> DE.column DE.int4)
     <*> DE.column DE.text
     <*> DE.column DE.bool) True
  let writeTitles fileName = writeFile fileName . encode . object .
        map (\(c,t,_) -> c .= t)
      path = case site of
        Piperka -> "data/piperka/d/"
        Teksti -> "data/teksti/d/"
  comicsOrdered <- ExceptT $ run $ statement () $ Statement sql2 EN.unit
    (DE.rowVector $
     OrderedComic
     <$> (toJSON <$> DE.column DE.int4)
     <*> (toJSON <$> DE.column DE.text)
     <*> DE.column (DE.array $ DE.dimension V.replicateM $ DE.element DE.text)
     <*> DE.column DE.bool
    ) True
  liftIO $ runConcurrently $ mconcat $ map Concurrently $
    [ writeFile (path <> "comics_ordered.json") $
      encode $ Array $ V.map
      (\OrderedComic{..} -> Array $ V.fromList
                            [oCid, oTitle]) comicsOrdered
    , writeFile (path <> "comics_ordered_rated.json") $
      encode $ Array $ V.map
      (\OrderedComic{..} -> Array $ V.fromList $
                            (if oNSFW then (++ [toJSON (1::Int)]) else id)
                            [oCid, oTitle]) comicsOrdered
    , writeFile (path <> "comics_ordered2.json") $
      encode $ Array $ V.map
      (\OrderedComic{..} -> Array $ V.fromList $
                            [oCid, oTitle] ++ if null oAliases
                                              then [] else [toJSON oAliases]) comicsOrdered
    , writeTitles (path <> "comictitles.json") $
      filter (\(_,_,x) -> x) allComics
    , writeTitles (path <> "comictitles_all.json") allComics
    ]
  where
    sql1 = "SELECT cid, title, true FROM comics UNION \
           \SELECT cid, title, false FROM graveyard"
    sql2 = "SELECT cid, title, \
           \CASE WHEN max(alias) IS NULL THEN '{}' ELSE array_agg(alias) END, \
           \cid IN (SELECT cid FROM comic_tag WHERE tagid = 13) \
           \FROM comics LEFT JOIN comic_alias USING (cid) \
           \GROUP BY cid, title ORDER BY ordering_form(title)"

getParserType
  :: Int32 -> Session (Maybe Int16)
getParserType = flip statement $ Statement
  "SELECT parser_type FROM crawler_config WHERE cid=$1"
  (EN.param EN.int4) (DE.rowMaybe $ DE.column DE.int2) True

getMorgueComic
  :: Int32
  -> Session (Maybe (Text, MorgueComic))
getMorgueComic = flip statement $ Statement
  "SELECT title, false, '' FROM comics WHERE cid=$1 \
  \UNION \
  \SELECT title, true, reason FROM graveyard WHERE cid=$1"
  (EN.param EN.int4) (DE.rowMaybe decoder) True
  where
    decoder = ((,) <$> DE.column DE.text) <*> do
      dead <- DE.column DE.bool
      if dead then DeadComic <$> DE.column DE.text
        else return AliveComic

buryComic
  :: Int32
  -> Text
  -> Session (Maybe Text)
buryComic c reason = do
  affected <- statement (c, reason) $
    Statement sql (contrazip2 (EN.param EN.int4) (EN.param EN.text))
    DE.rowsAffected True
  case affected of
    0 -> pure Nothing
    _ -> do
      statement c $
        Statement "DELETE FROM comics WHERE cid=$1" (EN.param EN.int4) DE.unit True
      refreshAlphabet
      statement c $
        Statement "SELECT title FROM graveyard WHERE cid=$1"
        (EN.param EN.int4) (fmap Just . DE.singleRow $ DE.column DE.text) True
  where
    sql = "INSERT INTO graveyard \
          \(cid, title, url_base, url_tail, fixed_head, homepage, \
          \added_on, description, bookmark_regexp, tags, reason) \
          \SELECT cid, title, url_base, url_tail, fixed_head, homepage, \
          \added_on, description, bookmark_regexp, tags, $2 \
          \FROM comics WHERE cid=$1"

reviveComic
  :: Int32
  -> Session Bool
reviveComic c = do
  affected <- statement c $
    Statement sql (EN.param EN.int4) DE.rowsAffected True
  case affected of
    0 -> return False
    _ -> do
      statement c $
        Statement "DELETE FROM graveyard WHERE cid=$1" (EN.param EN.int4) DE.unit True
      refreshAlphabet
      return True
  where
    sql = "INSERT INTO comics \
          \(cid, title, url_base, url_tail, fixed_head, homepage, \
          \added_on, description, bookmark_regexp, tags, restored_on, readers) \
          \SELECT cid, title, url_base, url_tail, fixed_head, homepage, \
          \added_on, description, bookmark_regexp, tags, now(), \
          \(SELECT COUNT(*) FROM subscriptions JOIN users USING (uid) \
          \ WHERE countme AND subscriptions.cid=graveyard.cid) \
          \FROM graveyard WHERE cid=$1"

mergeComic
  :: Int32
  -> Int32
  -> Session Bool
mergeComic src tgt = maybe False (const True) <$>
  (runMaybeT $ do
      guard =<< (lift $ statement tgt $
                 Statement "SELECT $1 IN (SELECT cid FROM comics)"
                 (EN.param EN.int4) (DE.singleRow $ DE.column DE.bool) True)
      guard . (>0) =<< (lift $ statement src $
                        Statement "DELETE FROM comics WHERE cid=$1"
                        (EN.param EN.int4) DE.rowsAffected True)
      lift $ do
        mapM_ (\t -> statement src $
                Statement ("DELETE FROM " <> t <> " WHERE cid=$1")
                (EN.param EN.int4) DE.unit True)
          [ "updates", "banners", "crawler_config"
          , "page_fragments", "external_entry" ]
        mapM_ (\sql -> statement (src, tgt) $ Statement sql
                       (contrazip2 (EN.param EN.int4) (EN.param EN.int4)) DE.unit True)
          [ "DELETE FROM subscriptions WHERE cid=$1 \
            \AND uid IN (SELECT uid FROM subscriptions WHERE cid=$2)"
          , "UPDATE subscriptions SET cid=$2 WHERE cid=$1"
          , "INSERT INTO ticket_resolve (tid, msg) \
            \SELECT tid, 'Merged into '||$2||' (was '||$1||')' \
            \FROM ticket WHERE cid=$1 \
            \AND tid NOT IN (SELECT tid FROM ticket_resolve)"
          , "UPDATE ticket SET cid=$2 WHERE cid=$1"
          , "UPDATE merged SET cid=$2 where cid=$1"
          , "INSERT INTO merged (cid, merged_from) VALUES ($2, $1)"
          ]
        refreshAlphabet
        return ())
