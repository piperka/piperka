{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Maint.Splices (renderGenentry, renderMorgue) where

import Control.Concurrent (writeChan)
import Control.Error.Util
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import qualified Data.ByteString.Char8 as B8
import Data.Int
import Data.List (partition)
import qualified Data.Map.Lazy as M
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8')
import qualified Data.Text.Lazy as L
import Data.UUID
import qualified Data.Vector as V
import Hasql.Session (QueryError)
import Heist
import Heist.Splices
import qualified HTMLEntities.Text as HTML
import Network.Mail.SMTP (Address(..), sendMail, simpleMail, plainTextPart)
import Snap
import Snap.Snaplet.Hasql (run)
import qualified Text.XmlHtml as X

import Application
import Piperka.API.Submit.Banner (saveFromSubmit, finishBannerSave)
import Piperka.ComicInfo.Epedia (eEntries)
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Maint.Types
import Piperka.Maint.Query
import Piperka.Util (getCid, getParamInt, intToText)

import Crawler.DB.Fetch (fetchNewCid)

renderGenentry
  :: SiteIdentity
  -> RuntimeAppHandler MyData
renderGenentry site n = do
  node <- getParamNode
  let (special, content) =
        partition (maybe False (`elem` ["success", "failure"]) . X.tagName) $
        X.childNodes node
      part x = X.childNodes $ head $ filter ((== (Just x)) . X.tagName) special
      success = withSplices (runNodeList $ part "success") $ do
        "href" ## pureSplice . textSplice $
          ("info.html?cid=" <>) . intToText
      failure = withSplices (runNodeList $ part "failure") $ do
        "message" ## pureSplice . textSplice $ id
  tpl1 <- eitherDefer
          failure success $ processSubmitGenentry site =<< n
  tpl2 <- withLocalSplices precrawl mempty $ runNodeList content
  return $ yieldRuntime $ do
    isSubmit <- (== (Just "genentry")) <$> lift (getPostParam "formtype")
    codeGen $ if isSubmit then tpl1 else tpl2

precrawl
  :: Splices (Splice AppHandler)
precrawl = do
  "newCid" ## return $ yieldRuntimeText $
    intToText . either (const (-1)) id <$> (lift $ run fetchNewCid)
  "precrawl" ## manyWithSplices runChildren
    ("page" ## pureSplice . textSplice $ maybe "<i>NULL</i>" HTML.text)
    (either (const V.empty) id <$> (lift precrawlArchive))

processSubmitGenentry
  :: SiteIdentity
  -> MyData
  -> RuntimeSplice AppHandler (Either Text Int)
processSubmitGenentry site u = do
  params <- lift getParams
  csrf <- (fromASCIIBytes =<<) <$> lift (getParam "csrf_ham")
  ann <- lift $ view siteAnnouncement
  let lookupSingle x = listToMaybe =<< M.lookup x params
      lookupText x = hush . decodeUtf8' =<< lookupSingle x
      lookupBool = pure . maybe False (== "1") . lookupSingle
      lookupInt x = (fromIntegral . fst) <$> (B8.readInt =<< lookupSingle x)
      nullToNothing n = if T.null n then Nothing else Just n
      tgs = maybe [] (map (fromIntegral . fst) . catMaybes . map B8.readInt) $
            M.lookup "category" params
      eps = eEntries params
      genentry = Genentry
        <$> pure (lookupInt "sid")
        <*> note "cid" (lookupInt "cid")
        <*> note "title" (nullToNothing =<< T.strip <$> lookupText "title")
        <*> (note "homepage"
             (nullToNothing =<< T.stripStart <$> lookupText "homepage"))
        <*> pure (nullToNothing =<< T.stripStart <$> lookupText "fixed_head")
        <*> (note "url_base"
             (nullToNothing =<< T.stripStart <$> lookupText "url_base"))
        <*> note "url_tail" (lookupText "url_tail")
        <*> lookupBool "acceptbanner"
        <*> note "description" (T.strip <$> lookupText "description")
        <*> note "email" (T.strip <$> lookupText "email")
        <*> lookupBool "want_email"
        <*> note "email_subject" (lookupText "email_subject")
        <*> note "email_message" (lookupText "email_message")
        <*> pure (nullToNothing =<< T.strip <$> lookupText "bookmark_regexp")
        <*> note "parser_id" (lookupInt "parser_id")
        <*> pure (nullToNothing =<< lookupText "extra_data")
        <*> pure (nullToNothing =<< lookupText "extra_url")
        <*> pure tgs
        <*> pure eps
  lift $ runExceptT $ do
    when (csrf /= (Just $ ucsrfToken u)) $ throwE "csrf fail"
    entry <- hoistEither genentry
    let newCid = cid entry
    withExceptT (T.pack . show) $ insertComic entry
    maybe (return ())
      (\s -> when (acceptbanner entry) $ withExceptT (T.pack . show) $ do
          (_, banner) <- ExceptT $
            saveFromSubmit (fromIntegral s) Nothing (fromIntegral newCid) Nothing
          finishBannerSave site (Nothing, banner)
      ) $ sid entry
    withExceptT (T.pack . show) $ do
      maybe (return ()) deleteSubmit $ sid entry
      ExceptT $ run refreshAlphabet
      commit
      comicTitles site
    liftIO $ writeChan ann $ ComicAdded newCid (title entry)
    when (wantsEmail entry) $ do
      let msg = T.replace "__NEWCID__" (intToText newCid)
                (emailMessage entry)
          mail = simpleMail (Address Nothing "piperka@piperka.net")
                 [Address Nothing (email entry)] [] []
                 (emailSubject entry)
                 [plainTextPart $ L.fromStrict msg]
      liftIO $ sendMail "localhost" mail
    return newCid

data MorgueAction = Dig | Bury | Merge deriving (Show, Eq)

renderMorgue
  :: SiteIdentity
  -> RuntimeAppHandler MyData
renderMorgue site u = do
  node <- getParamNode
  let subNodes = map (\n -> (X.elementTag n, X.childNodes n)) $
                 X.childElements node
      fetchComic = run . getMorgueComic . fromIntegral . fromJust
      isComicSplices = do
        "title" ## pureSplice . textSplice $ fst
        "alive" ## ifCSplice ((== AliveComic) . snd)
        "dead" ## deferMany
          (withSplices runChildren
           ("reason" ## pureSplice . textSplice $ HTML.text)) . fmap
          (\n' -> case snd n' of
                    DeadComic reason -> Just reason
                    _ -> Nothing)
      process
        :: (Maybe Int32, UUID)
        -> RuntimeSplice AppHandler (Either QueryError (Maybe MorgueAction))
      process n' = lift $ do
        ann <- view siteAnnouncement
        csrf <- (fromASCIIBytes =<<) <$> getParam "csrf_ham"
        let csrfOk = (== csrf) $ Just $ snd n'
            c = fromJust $ fst n'
        action <- getPostParam "morgue"
        reason <- maybe T.empty id . (hush . decodeUtf8' =<<) <$>
          getParam "reason"
        mergeTo <- fmap (fromIntegral . snd) <$> getParamInt "merge_into"
        runExceptT $ do
          buried <- case (csrfOk, action) of
            (False, _) -> return Nothing
            (_, Just "bury") -> bool Nothing (Just Bury) <$>
              ((ExceptT $ run $ buryComic c reason) >>=
                maybe (pure False)
                (\t -> (liftIO . writeChan ann $
                        ComicRetired (fromIntegral c) reason t) >> pure True)
              )
            (_, Just "dig") -> bool Nothing (Just Dig) <$>
              (ExceptT $ run $ reviveComic c)
            (_, Just "merge") -> runMaybeT $ do
              tgt <- MaybeT . return $ mergeTo
              guard =<< (lift $ ExceptT $ run $ mergeComic c tgt)
              return Merge
            _ -> error "impossible"
          when (isJust buried) (comicTitles site)
          return buried
      processedSplices = do
        "alive" ## ifCSplice (== (Just Dig))
        "dead" ## ifCSplice (== (Just Bury))
        "merged" ## ifCSplice (== (Just Merge))
        "undead" ## ifCSplice isNothing
      morgue n' = do
        tpl2 <- withSplices
          (eitherDefer stdSqlErrorSplice
           (withSplices (runNodeList $ fromJust $ lookup "processed" subNodes)
            processedSplices) $ process =<< n')
          ("cid" ## pureSplice . textSplice $ maybe "" intToText . fst) n'
        tpl3 <- runNodeList $ fromJust $ lookup "noComic" subNodes
        tpl1 <- eitherDefer stdSqlErrorSplice
          (deferManyElse (return tpl3)
           (withSplices (runNodeList $ fromJust $ lookup "isComic" subNodes)
            isComicSplices)) $ fetchComic . fst =<< n'
        return $ yieldRuntime $ do
          hasCid <- maybe False (const True) . fst <$> n'
          hasMorgueAction <- maybe False (`elem` ["bury", "dig", "merge"]) <$>
            (runMaybeT $ guard hasCid >> (MaybeT $ lift $ getPostParam "morgue"))
          codeGen $ case (hasCid, hasMorgueAction) of
            (True, False) -> tpl1
            (True, True) -> tpl2
            _ -> tpl3
  morgue $ u >>= \u' -> do
    (, ucsrfToken u') . fmap (fromIntegral . snd) <$> lift getCid
