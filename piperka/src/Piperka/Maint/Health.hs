{-# LANGUAGE OverloadedStrings #-}

module Piperka.Maint.Health where

import Control.Lens
import Data.Map.Syntax
import Data.Maybe
import qualified Data.Text as T
import Heist (Splices)
import qualified HTMLEntities.Text as HTML
import Network.URI.Encode as URI
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Maint.Health.Internal
import Piperka.Maint.Health.Query
import Piperka.Maint.Health.Types
import Piperka.Util (abbrevText, formatTime', intToText, uriToText)

import Crawler.Types

healthBaseRedirects
  :: Splice AppHandler
healthBaseRedirects = eitherDefer stdSqlErrorSplice
  (manyWith runChildren (basicHealthSplices <> baseRedirectSplices) redirectHref) failures
  where
    redirectHref = "redirectHref" ## \n _ ->
      maybe [] (\x -> [("href", x)]) .
      redirEnc Nothing (Just . URI.encodeTextWith (/= '"')) <$> n
    failures = run fetchBaseRedirects
    baseRedirectSplices =
      ("ifAutoFix" ## checkedSplice . fmap (isJust . autoFix)) <>
      (mapV (pureSplice . textSplice) $ do
          "base" ## HTML.text . abbrevText 100 . base
          "target" ## HTML.text . abbrevText 100 . target
          "redirect" ## HTML.text . abbrevText 100 .
            either intToText (either id uriToText) . redirectOrFailcode
          "redirectEnc" ## redirEnc "" URI.encodeText
      )

healthCrawler
  :: Splice AppHandler
healthCrawler = eitherDefer stdSqlErrorSplice
  (manyWithSplices runChildren (basicHealthSplices <> crawlFailureSplices)) failures
  where
    failures = run fetchCrawlFailures
    crawlFailureSplices = do
      "error" ## pureSplice . textSplice $ errMsg . end
      "errorAbbrev" ## pureSplice . textSplice $ abbrevText 100 . errMsg . end
      "hasLastSuccess" ## maybeSplice lastSuccessSplice . fmap lastSuccess
      "hasOrigin" ## maybeSplice originSplice . fmap origin
    errMsg (Left msg) = "Other error: " <> (HTML.text $ T.pack msg)
    errMsg (Right (EndError code txt, _)) =
      "HTTP " <> (intToText code) <> " " <> (HTML.text txt)
    errMsg (Right (EndCrawl (CurlFailure code _), _)) =
      "curl " <> (intToText code)
    errMsg (Right (EndCrawl FileForbidden, Just (Fetch _ tgt))) =
      "file " <> (HTML.text $ tgt ^. nextURL)
    errMsg (Right err) = HTML.text $ T.pack $ show err
    lastSuccessSplice = withSplices runChildren $
      "lastSuccess" ## pureSplice . textSplice $ T.pack . formatTime'
    originSplice = withSplices runChildren $ mapV (pureSplice . textSplice) $ do
      "origin" ## id
      "originText" ## HTML.text . abbrevText 100

basicHealthSplices
  :: HealthBasics t
  => Splices (RuntimeAppHandler t)
basicHealthSplices = do
  "cid" ## pureSplice . textSplice $ intToText . cid
  "title" ## pureSplice . textSplice $ HTML.text . title
