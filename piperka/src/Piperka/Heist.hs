{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExistentialQuantification #-}

-- Re-exports Heist.Compiled with a few changes
module Piperka.Heist
  ( Splice
  , renderTemplate
  , codeGen
  , runChildren
  , textSplice
  , xmlNodeSplice
  , htmlNodeSplice
  , pureSplice
  , deferMany
  , defer
  , bindLater
  , withSplices
  , manyWithSplices
  , manyWith
  , withLocalSplices
  , yieldPure
  , yieldRuntime
  , yieldRuntimeEffect
  , yieldPureText
  , yieldRuntimeText
  , runNodeList
  , runNode
  , runAttributes
  , runAttributesRaw
  , callTemplate
  -- Custom Piperka
  , deferManyElse
  , maybeSplice
  , eitherDefer
  , checkedSplice
  , checkedAttrSplice
  , emptySplices
  , stdConditionalSplice
  , IndexedAction(..)
  ) where

import Control.Monad
import qualified Data.Foldable as Foldable
import qualified Data.IntMap as I
import Data.List
import qualified Data.Map.Strict as M
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Heist.Compiled
import Heist.Compiled.LowLevel
import Heist
import qualified Text.XmlHtml as X

{-# INLINE foldMapM #-}
foldMapM :: (Monad f, Monoid m, Foldable list)
         => (a -> f m)
         -> list a
         -> f m
foldMapM f =
  Foldable.foldlM (\xs x -> xs `seq` liftM (xs <>) (f x)) mempty

-- Like deferMany for Maybe but without defer
maybeSplice
  :: Monad n
  => (RuntimeSplice n a -> Splice n)
  -> RuntimeSplice n (Maybe a)
  -> Splice n
maybeSplice f action = do
  some <- f $ fromJust <$> action
  flip bindLater action $ codeGen . maybe mempty (const some)

deferManyElse
  :: (Foldable f, Monad n)
  => Splice n
  -> (RuntimeSplice n a -> Splice n)
  -> RuntimeSplice n (f a)
  -> Splice n
deferManyElse els f getItems = do
  els' <- els
  promise <- newEmptyPromise
  chunks <- f $ getPromise promise
  return $ yieldRuntime $ do
    items <- getItems
    if null items
      then codeGen els'
      else foldMapM (\item -> putPromise promise item >> codeGen chunks) items

eitherDefer
  :: Monad n
  => (RuntimeSplice n a -> Splice n)
  -> (RuntimeSplice n b -> Splice n)
  -> RuntimeSplice n (Either a b)
  -> Splice n
eitherDefer pff pfs n = do
  pf <- newEmptyPromise
  ps <- newEmptyPromise
  actionSuccess <- pfs $ getPromise ps
  actionFailure <- pff $ getPromise pf
  return $ yieldRuntime $
    n >>= either
    (\x -> putPromise pf x >> codeGen actionFailure)
    (\x -> putPromise ps x >> codeGen actionSuccess)

checkedSplice
  :: Monad n
  => RuntimeSplice n Bool
  -> Splice n
checkedSplice runtime = do
  tpl <- runChildren
  checkVal <- maybe True (read . T.unpack) . X.getAttribute "check"
              <$> getParamNode
  checkVal `seq` return $ yieldRuntime $ do
    check <- runtime
    if check == checkVal then codeGen tpl else mempty

checkedAttrSplice
  :: Monad n
  => RuntimeSplice n Bool
  -> Splices (AttrSplice n)
checkedAttrSplice runtime = do
  "checked" ## const $ do
    val <- runtime
    return $ if val then [("checked", "")] else []

emptySplices :: [T.Text] -> Splices (Splice a)
emptySplices = mapM_ (\x -> x ## return mempty)

data IndexedAction a b n = Simple
                         | forall c.
                           WithParam (a -> b -> RuntimeSplice n c)
                           (Splice n -> RuntimeSplice n c -> Splice n)

-- TODO: Error handling
stdConditionalSplice
  :: forall a b n. (Eq a, Bounded a, Enum a, Monad n)
  => (a -> (Text, IndexedAction a b n))
  -> RuntimeSplice n (a,b)
  -> Splice n
stdConditionalSplice act n = do
  node <- getParamNode
  let cs = M.fromList $ map (\x -> (X.elementTag x, runNodeList $ X.elementChildren x)) $
           X.childElements node
      makeSplice (t, Simple, i) = do
        x <- cs M.! t
        return (i, x)
      makeSplice (t, WithParam f m, i) = do
        let s' = cs M.! t
        x <- deferMap (uncurry f) (m s') n
        return (i, x)
      unfoldrCond f = f minBound :
        unfoldr (\a -> if a == maxBound
                       then Nothing
                       else let a' = succ a in Just (f a', a')) minBound
      nodeNames = unfoldrCond $ fst . act
      actions = unfoldrCond $ snd . act
  m <- I.fromList <$> (mapM makeSplice $ zip3 nodeNames actions $ map fromEnum
                       (enumFromTo minBound maxBound :: [a]))
  flip bindLater n $ \x -> codeGen $ m I.! (fromEnum $ fst x)
