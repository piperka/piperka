{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Ticket.User.Splices (renderUserTickets) where

import Data.Map.Syntax
import qualified Data.Text as T
import qualified Data.Vector as V
import qualified HTMLEntities.Text as HTML

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Ticket (renderReason)
import Piperka.Ticket.User.Query
import Piperka.Ticket.User.Types
import Piperka.Util (formatTime', intToText)

renderUserTickets
  :: RuntimeAppHandler MyData
renderUserTickets n =
  eitherDefer stdSqlErrorSplice
  (deferManyElse (callTemplate "/include/my_tickets/_noTickets") $
   withSplices runChildren splices) $
  getUserTickets =<< uid <$> n
  where
    splices =
      (mapV (pureSplice . textSplice . fmap intToText) $ do
          "nOpen" ## V.length . fst
          "nClosed" ## V.length . snd) <>
      ("common" ## \n' -> do
          let resolvedId x = "resolvedId" ## const $ return [("id", x)]
          tpl1 <- withLocalSplices
                  ("list" ## manyWithSplices runChildren
                   (ticketSplices <> ("resolved" ## const $ return mempty)) $
                   fst <$> n')
                  (resolvedId "tabs-1") runChildren
          tpl2 <- withLocalSplices
                  ("list" ## manyWithSplices runChildren
                   ((mapV (. fmap ticket) ticketSplices) <> resolvedSplices) $
                   snd <$> n')
                  (resolvedId "tabs-2") runChildren
          return $ tpl1 <> tpl2
      )
    ticketSplices =
      (mapV (pureSplice . textSplice) $ do
          "cid" ## intToText . cid
          "name" ## HTML.text . name
          "stamp" ## T.pack . formatTime' . stamp
          "message" ## maybe "" HTML.text . fst . reason)
      <> ("reason" ## renderReason . fmap (snd . reason))
    resolvedSplices = do
      "resolved" ## const $ runChildren
      "resolvedDate" ## pureSplice . textSplice $
        T.pack . formatTime' . resolvedStamp
      "hasMessage" ## checkedSplice . fmap (not . T.null . resolvedMessage)
      "resolvedMessage" ## pureSplice . textSplice $
        HTML.text . resolvedMessage
