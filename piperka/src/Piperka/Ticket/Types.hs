{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Ticket.Types where

import Data.Aeson
import Data.Text (Text)

data Submit = Stale
  { firstPage :: Maybe Text
  , homePage :: Maybe Text
  , titleChange :: Maybe Text
  } | Stuck
  { stuck :: Int
  } | Gone
  { gone :: Int
  } | Restore

instance FromJSON Submit where
  parseJSON = withObject "Submit" $ \v -> do
    typ :: Text <- v .: "type"
    case typ of
      "stale" -> Stale
        <$> v .: "firstPage"
        <*> v .: "homePage"
        <*> v .: "title"
      "stuck" -> Stuck <$> v .: "stuck"
      "gone" -> Gone <$> v .: "gone"
      "restore" -> return Restore
      _ -> return Restore

instance ToJSON Submit where
  toJSON (Stale f h t) = object [ "type" .= ("stale" :: Text)
                                , "firstPage" .= f
                                , "homePage" .= h
                                , "title" .= t
                                ]
  toJSON (Stuck r) = object [ "type" .= ("stuck" :: Text)
                            , "stuck" .= r
                            ]
  toJSON (Gone r) = object [ "type" .= ("gone" :: Text)
                           , "gone" .= r
                           ]
  toJSON Restore = object [ "type" .= ("restore" :: Text) ]

data TicketInfo = TicketInfo
  { firstPageI :: Maybe Text
  , lastPageI :: Maybe Text
  , homePageI :: Text
  } | DeadTicketInfo Text
