{-# LANGUAGE OverloadedStrings #-}

module Piperka.Mobile where

import Control.Lens
import Control.Monad.State
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Data.Char (toLower)
import Data.List
import qualified Data.Text as T
import Heist
import Snap
import qualified Text.XmlHtml as X

import Application
import Piperka.Heist

mobileMode
  :: AppHandler ()
  -> AppHandler ()
mobileMode action = do
  -- mobi cookie is set on client side
  rq <- getRequest
  let val "0" = pure False
      val "1" = pure True
      val _ = Nothing
      mobiCookie = val =<< cookieValue <$>
        find ((== "mobi") . cookieName) (rqCookies rq)
      mobiUA = maybe False id $ not . B.null . snd . B.breakSubstring "mobi" .
               C.map toLower <$> getHeader "User-Agent" rq
  modify $ set mobile (mobiCookie, mobiUA)
  action

getMobi
  :: AppHandler Bool
getMobi = isMobile <$> view mobile

isMobile
  :: (Maybe Bool, Bool) -> Bool
isMobile (selection, detected) =
  selection == Just True || (selection /= Just False && detected)

data MobileMode = Mobile | Desktop | ReallyDesktop | LatentMobile
  deriving (Show, Eq, Read)

mobileSplice
  :: Splice AppHandler
mobileSplice = do
  node <- getParamNode
  let mobi = maybe Mobile (read . T.unpack) $ X.getAttribute "mode" node
      p = case mobi of
        Mobile -> isMobile
        Desktop -> not . isMobile
        ReallyDesktop -> (&&) <$> (/= Just True) . fst <*> not . snd
        LatentMobile -> (&&) <$> (== Just False) . fst <*> snd
  tpl <- runChildren
  return $ yieldRuntime $ do
    x <- lift $ view mobile
    if p x then codeGen tpl else return mempty

mobileClassAttrSplice
  :: AttrSplice AppHandler
mobileClassAttrSplice t = do
  mobi <- lift getMobi
  return $ if mobi then [("class", t)] else [("class", "not-"<>t)]
