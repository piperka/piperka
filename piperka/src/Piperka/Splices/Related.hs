{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.Related where

import Control.Concurrent
import Control.Lens
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Map.Syntax
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Vector as V
import Heist
import qualified HTMLEntities.Text as HTML
import Numeric

import Application
import Piperka.Auth
import Piperka.ComicInfo.Query
import Piperka.Error.Splices
import Piperka.Heist

renderRelated
  :: Splice AppHandler
renderRelated =
  deferManyElse (callTemplate "/_develRecommends")
  (eitherDefer stdSqlErrorSplice
   (maybeSplice $ withSplices (callTemplate "/_related") relatedSplices)) $ runMaybeT $ do
  _ <- MaybeT $ lift $ view recommendHandle
  lift $ do
    -- Injected by prefetchRecommends in Piperka.Recommend
    rel <- liftIO . readMVar =<< lift (view recommendComics)
    u <- fmap uid <$> (lift currentUserPlain)
    fmap (\v -> if null v then Nothing else Just (zip rel $ V.toList v)) <$>
      getRelatedTitles u (map (fromIntegral . fst) rel)

relatedSplices
  :: Splices (RuntimeAppHandler [((Int, Double), (Text, Bool))])
relatedSplices = "related" ## manyWith runChildren splices attrSplices
  where
    splices :: Splices (RuntimeAppHandler ((Int, Double), (Text, Bool)))
    splices = mapV (pureSplice . textSplice) $ do
      "score" ## T.pack . flip (showFFloat (Just 2)) "" . snd . fst
      "cid" ## T.pack . flip showInt "" . fst . fst
      "title" ## HTML.text . fst . snd
    attrSplices = do
      "rejectedClass" ## \n t -> do
        x <- snd . snd <$> n
        return $ if x then [("class", t)] else []
