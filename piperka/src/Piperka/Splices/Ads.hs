{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Splices.Ads (prepareAds) where

import Control.Applicative
import Control.Lens
import Control.Monad
import Control.Monad.State
import Data.List
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Map.Syntax
import Data.Maybe (isJust, fromJust)
import Heist
import Snap
import Text.XmlHtml as X

import Application
import Piperka.Heist

prepareAds
  :: AppInit
  -> Splice AppHandler
  -> Splice AppHandler
prepareAds ini proceed = do
  adsEnabledForPage <- maybe True (read . T.unpack) . X.getAttribute "ads" <$>
                       getParamNode
  x <- adsEnabledForPage `seq`
    withLocalSplices (adSplices ini adsEnabledForPage) mempty proceed
  return $ yieldRuntimeEffect setConsent <> x

setConsent :: RuntimeSplice AppHandler ()
setConsent = lift $ do
  cookies <- rqCookies <$> getRequest
  let oldCookie = find ((== "autoAdConsent") . cookieName) cookies
      newCookie = find ((== "ads") . cookieName) cookies
      consent = (== "1") . cookieValue <$> (newCookie <|> oldCookie)
  when (isJust oldCookie && isJust newCookie) $
    expireCookie $ Cookie "autoAdConsent" "" Nothing Nothing (Just "/") True True
  when (isJust consent) $
    modify $ set autoAds consent

adSplices
  :: AppInit
  -> Bool
  -> Splices (Splice AppHandler)
adSplices ini adsEnabledForPage = do
  "adAskOptOut" ## emptyIfNoAdsElse $
    if adAskOptOut ini then runChildren else return mempty
  "autoAd" ## emptyIfNoAdsElse $ do
    p <- maybe (`elem` [Nothing, Just True]) ((==) . read . T.unpack) .
         X.getAttribute "enabled" <$> getParamNode
    x <- runChildren
    p `seq` return $ yieldRuntime $ do
      ads <- lift $ view autoAds
      if p ads then codeGen x else return mempty
  "adsEnabledForSite" ## do
    check <- maybe True (read . T.unpack) . X.getAttribute "check" <$> getParamNode
    if check /= adsDisabled ini then runChildren else return mempty
  "noAutoAd" ## do
    x <- runChildren
    return $ yieldRuntime $ do
      ads <- lift $ view autoAds
      if adsDisabled ini || not adsEnabledForPage || ads == Just False
        then codeGen x
        else return mempty
  "adCode" ## emptyIfNoAdsElse $
    encodeUtf8 . fromJust . X.getAttribute "id" <$> getParamNode >>=
    callTemplate . ("/include/adcode/_" <>)
  "autoAdChoice" ##
    withLocalSplices mempty
    ("checked" ## \t -> do
        let p = (== (Just $ read $ T.unpack t))
        consent <- lift $ view $ autoAds
        p `seq` return $ if p consent then [("checked", "")] else []) runChildren
  where
    emptyIfNoAdsElse x =
      if adsEnabledForPage && (not $ adsDisabled ini)
      then x else return mempty
