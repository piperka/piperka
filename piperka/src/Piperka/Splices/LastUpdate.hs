{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.LastUpdate where

import Control.Monad.Trans
import Data.Map.Syntax
import qualified Data.Text as T
import Data.UUID
import Hasql.Session hiding (run)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Statement
import Heist
import Snap
import qualified Text.XmlHtml as X
import Snap.Snaplet.Hasql

import Application
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (getCid, intToText)

renderLastUpdateDisclaimer
  :: RuntimeAppHandler MyData
renderLastUpdateDisclaimer n = do
  maySet <- maybe False (read . T.unpack) . X.getAttribute "maySet" <$> getParamNode
  eitherDefer stdSqlErrorSplice (withSplices runChildren lastUpdateSplices) $ do
    usr <- n
    rq <- lift $ getRequest
    let seen = lastUpdateDisclaimer usr
    if not seen && maySet && rqMethod rq == POST
      then do
      csrf <- (fromASCIIBytes =<<) <$> lift (getParam "csrf_ham")
      setSeen <- (== Just "1") <$> lift (getParam "seenDisclaimer")
      if csrf /= (Just $ ucsrfToken usr) || not setSeen
        then pure $ Right (False, True)
        else fmap (const (True, False)) <$>
             (run $
              statement (uid usr) $ Statement
              "UPDATE users SET last_update_disclaimer=true WHERE uid=$1"
              (EN.param EN.uid) DE.unit True)
      else pure $ Right (seen, False)

lastUpdateSplices
  :: Splices (RuntimeAppHandler (Bool, Bool))
lastUpdateSplices = do
  "setFailed" ## checkedSplice . fmap snd
  "seenDisclaimer" ## \n -> do
    accepted <- maybe True (read . T.unpack) . X.getAttribute "accepted" <$> getParamNode
    checkedSplice $ (== accepted) . fst <$> n
  "hasCid" ## const $ manyWithSplices runChildren
    ("cid" ## pureSplice . textSplice $ intToText) $ fmap snd <$> lift getCid
