{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.Hiatus
  ( statusEdited
  , renderListOfStatusEdits
  ) where

import Contravariant.Extras.Contrazip
import Control.Error.Util
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Int
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8')
import Data.Time (UTCTime)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import qualified HTMLEntities.Text as HTML
import Snap
import Snap.Snaplet.Hasql (run)

import Application
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util

type HiatusEdit = (Int32, Maybe UserID, Int16, Text)

-- User submissions

-- disregard csrf as this doesn't change users' settings
statusEdited
  :: AppInit
  -> RuntimeAppHandler (Maybe MyData)
statusEdited ini n = do
  let succFailSplice a b = withLocalSplices
        ((a ## runChildren) <> (b ## return mempty)) mempty runChildren
      isValidId = flip elem (map fst $ hiatusNames ini) . fromIntegral
  sc <- succFailSplice "success" "failure"
  fl <- succFailSplice "failure" "success"
  let renderResult True = codeGen sc
      renderResult False = codeGen fl
      readParams u = runMaybeT $ do
        s <- fromIntegral . snd <$> MaybeT (getParamInt "status")
        guard $ isValidId s
        (,,,)
          <$> (fromIntegral . snd <$> MaybeT getCid)
          <*> lift (return $ uid <$> u)
          <*> pure s
          <*> (maybe "" (T.take 100) . (hush . decodeUtf8' =<<) <$>
               lift (getParam "reason"))
  tpl <- eitherDefer stdSqlErrorSplice (bindLater renderResult) $ n >>=
    (lift . readParams >=> maybe (return $ Right False)
      (\e -> runExceptT $ do
          ExceptT $ run $ submitHiatusEdit e
          return True)
    )
  return $ yieldRuntime $ do
    isPost <- lift $ isJust <$> getPostParam "hiatus_user_edit"
    if isPost then codeGen tpl else return mempty

submitHiatusEdit
  :: HiatusEdit
  -> Session ()
submitHiatusEdit = flip statement $ Statement sql encoder (DE.unit) True
  where
    encoder = contrazip4
      (EN.param EN.int4)
      (EN.nullableParam EN.uid)
      (EN.param EN.int2)
      (EN.param EN.text)
    sql = "INSERT INTO hiatus_user_edit (cid, uid, code, reason) \
          \VALUES ($1, $2, $3, $4)"

-- Moderator renders
data EditStatusEntry = EditStatusEntry
  { uhid :: Int32
  , cid :: Int32
  , title :: Text
  , name :: Text
  , currentStatus :: Maybe Int16
  , proposedStatus :: Int16
  , reason :: Text
  , addedOn :: UTCTime
  }

renderListOfStatusEdits
  :: AppInit
  -> RuntimeAppHandler MyData
renderListOfStatusEdits ini =
  eitherDefer stdSqlErrorSplice (manyWithSplices runChildren listSplices) .
  (lift . getUserStatusEdits . uid =<<)
  where
    hn = hiatusNames ini
    listSplices = mapV (pureSplice . textSplice) $ do
      "title" ## HTML.text . title
      "name" ## HTML.text . name
      "cid" ## intToText . cid
      "uhid" ## intToText . uhid
      "code" ## intToText . proposedStatus
      "currentHiatus" ## maybe "" (maybe "unknown" id . flip lookup hn . fromIntegral) .
        currentStatus
      "proposedHiatus" ## maybe "unknown" id . flip lookup hn . fromIntegral .
        proposedStatus
      "reason" ## HTML.text . reason
      "addedOn" ## T.pack . formatTime' . addedOn
    getUserStatusEdits = run . flip statement stmt
    stmt = Statement sql (EN.param EN.uid) (DE.rowVector decoder) False
    sql = "SELECT uhid, cid, title, coalesce(name, ''), \
          \cs.code, prop.code, reason, prop.stamp AT TIME ZONE 'utc' \
          \FROM comics JOIN hiatus_user_edit AS prop USING (cid) \
          \LEFT JOIN users USING (uid) \
          \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS cs USING (cid) \
          \WHERE processed_by IS NULL ORDER BY ordering_form(title), prop.stamp DESC"
    decoder =
      EditStatusEntry
      <$> DE.column DE.int4
      <*> DE.column DE.int4
      <*> DE.column DE.text
      <*> DE.column DE.text
      <*> DE.nullableColumn DE.int2
      <*> DE.column DE.int2
      <*> DE.column DE.text
      <*> DE.column DE.timestamptz
