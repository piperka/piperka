{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.Stats
  ( unreadCountSplice
  , newComicsSplice
  , explicitStats
  ) where

import Control.Monad
import Data.Int
import Data.Map.Syntax
import qualified Data.Text as T
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist
import Snap.Snaplet.Hasql
import qualified Text.XmlHtml as X

import Application
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (intToText)

unreadCountSplice
  :: RuntimeAppHandler (Int32, Int32)
unreadCountSplice n = do
  useParens <- maybe True (read . T.unpack) . X.getAttribute "parens" <$> getParamNode
  let parenSplices = if useParens
        then do
        "openParen" ## return $ yieldPureText "("
        "closeParen" ## return $ yieldPureText ")"
        else do
        "openParen" ## return mempty
        "closeParen" ## return mempty
  withLocalSplices parenSplices mempty $ maybeSplice
    (withSplices (callTemplate "/_stats/_unread")
     (mapV (pureSplice . textSplice) $
      ("pages" ## intToText . fst) <> ("comics" ## intToText . snd))) $
    fmap (\a@(n', _) -> guard (n' > 0) >> return a) n

newComicsSplice
  :: RuntimeAppHandler Int32
newComicsSplice = maybeSplice
  (\n -> withLocalSplices
    ("new" ## (pureSplice . textSplice $ intToText) n)
-- TODO: bring newest cid to this link for cache invalidation
    ("href" ## const $ return [("href", "browse.html?sort=new")])
    (callTemplate "/_stats/_new")) .
  fmap (\n -> guard (n > 0) >> return n)

explicitStats
  :: RuntimeAppHandler MyData
explicitStats = eitherDefer
  stdSqlErrorSplice
  (withSplices runChildren
   (("unread" ## unreadCountSplice . fmap snd) <>
    ("newComics" ## newComicsSplice . fmap fst))) .
  (run . flip statement fetchStats . uid =<<)

-- As opposed to the fetch done by UserWithStats auth this doesn't
-- update the seen_comics_before stamp in users table.
fetchStats
  :: Statement UserID (Int32, (Int32, Int32))
fetchStats = Statement sql
  (EN.param EN.uid)
  (DE.singleRow $ (,)
   <$> DE.column DE.int4
   <*> ((,)
        <$> DE.column DE.int4
        <*> DE.column DE.int4)
  ) True
  where
    sql =
      "SELECT (SELECT COUNT(*) FROM comics, user_site_activity \
      \ WHERE uid=$1 AND comics.added_on > user_site_activity.seen_comics_before), \
      \total_new, new_in FROM user_unread_stats($1)"
