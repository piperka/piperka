{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Splices.Authors
  ( renderAuthors
  , renderEditAuthor
  , renderIsAuthor
  , renderIsCidAuthor
  , renderAuthorClaim
  ) where

import Contravariant.Extras.Contrazip
import Control.Lens
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Map.Syntax
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.ICU as ICU
import qualified Data.UUID
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist
import qualified HTMLEntities.Text as HTML
import Heist.Splices
import Snap
import Snap.Snaplet.Hasql (run)

import Application
import Piperka.ComicInfo.Query
import qualified Piperka.ComicInfo.Types as CI
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Maint.Query
import Piperka.Util

data Comic = Comic
  { cid :: Int
  , title :: Text
  , active :: Bool
  }

data Author = Author
  { name :: Text
  , auid :: Int
  , comics :: Vector Comic
  }

renderAuthors
  :: Splice AppHandler
renderAuthors =
  eitherDefer stdSqlErrorSplice
  (manyWithSplices runChildren authorSplices) $
  run $ statement () $ Statement sql EN.unit (DE.rowVector authorDecoder) True
  where
    sql = "SELECT name, auid, array_agg((cid, title, true)) \
          \FROM author JOIN author_comics USING (auid) JOIN comics USING (cid) \
          \WHERE name <> '' \
          \GROUP BY name, auid ORDER BY name"

renderEditAuthor
  :: Splice AppHandler
renderEditAuthor = withLocalSplices
  ("qSearch" ##
   withLocalSplices (mapV (return . yieldPureText) $ do
                        "sort" ## "name"
                        "id" ## "editAuthorSearch"
                    )
   mempty (callTemplate "_qSearch")) mempty $
  deferManyElse
  (withLocalSplices nullAuthorSplices mempty runChildren)
  (eitherDefer
   stdSqlErrorSplice
   (deferManyElse (callTemplate "_authorMissing")
    (withSplices runChildren authorSplices)) .
   (run . flip statement
    (Statement sql (EN.param EN.int4) (DE.rowMaybe authorDecoder) True) =<<)
  ) $
  fmap (fromIntegral . snd) <$> (lift $ getParamInt "auid")
  where
    sql = "SELECT name, auid, \
          \array_agg((cid, COALESCE(comics.title, graveyard.title), comics.cid IS NULL)) \
          \FROM author JOIN author_comics USING (auid) \
          \LEFT JOIN graveyard USING (cid) \
          \LEFT JOIN comics USING (cid) WHERE auid=$1 \
          \GROUP BY auid, name"
    nullAuthorSplices = emptySplices ["name", "hasId", "comics"] <>
      ("noId" ## runChildren)

authorDecoder
  :: DE.Row Author
authorDecoder =
  Author
  <$> DE.column DE.text
  <*> DE.column (fromIntegral <$> DE.int4)
  <*> (DE.column $ DE.array $ DE.dimension V.replicateM $
       DE.element $ DE.composite $
       Comic
       <$> DE.field (fromIntegral <$> DE.int4)
       <*> DE.field DE.text
       <*> DE.field DE.bool
      )

authorSplices
  :: Splices (RuntimeAppHandler Author)
authorSplices = do
  "name" ## pureSplice . textSplice $ HTML.text . name
  "auid" ## pureSplice . textSplice $ intToText . auid
  "hasId" ## const runChildren
  "noId" ## const $ return mempty
  "comics" ## manyWith runChildren
    (mapV (pureSplice . textSplice) $ do
        "cid" ## intToText . cid
        "title" ## HTML.text . title)
    ("class" ## \n t -> do
        x <- n
        return $ if active x then [] else [("class", t)]
    ) . fmap comics

renderIsAuthor
  :: RuntimeAppHandler MyData
renderIsAuthor usr = withLocalSplices
  ("author" ## authorSplice) mempty runChildren
  where
    authorSplice = (\x -> withLocalSplices x mempty runChildren) $ do
      "authorAction" ## eitherDefer stdSqlErrorSplice
        (withSplices runChildren ("nameUpdated" ## ifCSplice id)) $
        maybe (Right False) (fmap (>0)) <$> runMaybeT
        (do
            u <- lift usr
            let i = fromJust $ author u
            csrf <- MaybeT . lift $ (Data.UUID.fromText =<<) <$> getParamText "csrf_ham"
            guard $ ucsrfToken u == csrf
            newName <- MaybeT . lift $ getParamText "authorName"
            lift $ run $ statement (i, T.take 100 . T.unwords $ T.words newName) $ Statement
              "UPDATE author SET name=$2 WHERE auid=$1"
              (contrazip2 (EN.param EN.int4) (EN.param EN.text))
              DE.rowsAffected True
        )
      "withAuthored" ## eitherDefer stdSqlErrorSplice renderWithName $ do
        u <- usr
        fmap ((,u) . maybe "#INVALID#" id) <$>
          (run $ statement (fromJust $ author u) $ Statement
           "SELECT name FROM author WHERE auid=$1" (EN.param EN.int4)
           (DE.rowMaybe $ DE.column DE.text) True)
    renderWithName = withSplices runChildren $ do
      "authorName" ## pureSplice . textSplice $ HTML.text . fst
      "authoredComics" ## renderAuthoredComics . fmap snd

renderAuthoredComics
  :: RuntimeAppHandler MyData
renderAuthoredComics usr =
  eitherDefer stdSqlErrorSplice
  (deferManyElse (callTemplate "/include/my_comics/_noAuthored") $
   withSplices runChildren splices) $ do
  a <- author <$> usr
  flip (maybe (return $ Right V.empty)) a $ \a' ->
    run $ statement a' $ Statement sql (EN.param EN.int4) (DE.rowVector decoder) True
  where
    sql = "SELECT cid, title FROM comics JOIN author_comics USING (cid) \
          \WHERE auid=$1 ORDER BY ordering_form(title)"
    decoder =
      (,)
      <$> DE.column DE.int4
      <*> DE.column DE.text
    splices = mapV (pureSplice . textSplice) $ do
      "cid" ## intToText . fst
      "title" ## HTML.text . snd

-- Guard that author matches the comic or user is admin
renderIsCidAuthor
  :: SiteIdentity
  -> RuntimeAppHandler MyData
renderIsCidAuthor site u = manyWithSplices runChildren splices $ do
  usr <- u
  info <- lift $ view prefetchComicInfo
  return $ do
    when (moderator usr /= Just Admin) $
      guard =<< (either (const False) ((author usr `elem`) . map Just . CI.authors) <$> info)
    return usr
  where
    titleR = ICU.regex [] "^\\S.*\\S$"
    authorSplice = withSplices runChildren $ do
      "titleUpdate" ## ifCSplice fst
      "crawlScheduled" ## ifCSplice snd
    authorAction u' = do
      maybe (Right (False, False)) id <$> runMaybeT
        (do
            csrf <- MaybeT . lift $ (Data.UUID.fromText =<<) <$> getParamText "csrf_ham"
            guard $ ucsrfToken u' == csrf
            c <- MaybeT . lift $ fmap snd <$> getCid
            nt <- lift . lift $ getParamText "newTitle"
            guard $ isNothing nt || isJust (nt >>= ICU.find titleR)
            crawl <- lift . lift $ (== Just "1") <$> getParam "crawlNow"
            lift $ case (nt, crawl) of
              (Just t, _) -> do
                let t' = T.take 100 . T.unwords $ T.words t
                remote <- lift $ rqRemote <$> getRequest
                res <- runExceptT $ do
                  res <- ExceptT $ setTitle c remote (uid u') t'
                  comicTitles site
                  return res
                -- Update for the title splice below to show the
                -- changed name on the same render.
                lift $ when (res == Right True) $
                  modify $ over prefetchComicInfo (fmap . fmap $ (\x -> x { CI.title = t' }))
                return $ (,False) <$> res
              (_, True) ->
                fmap (False,) <$> scheduleCrawl c
              _ -> return $ Right (False, False)
        )
    splices = do
      "authorCidAction" ## eitherDefer stdSqlErrorSplice authorSplice . (authorAction =<<)
      -- prefetchComicInfo is certain to be a Just Right at this point
      "title" ## const . return . yieldRuntimeText .
        lift $ (CI.title . either undefined id . fromJust) <$> view prefetchComicInfo

renderAuthorClaim
  :: RuntimeAppHandler MyData
renderAuthorClaim n = manyWithSplices runChildren splices $ do
  usr <- n
  info <- lift $ view prefetchComicInfo
  return $ do
    info' <- either (const Nothing) Just =<< info
    let a = (author usr `elem`) . map Just . CI.authors $ info'
        c = CI.cid info'
    return $ if a then Nothing else Just (uid usr, fromIntegral c)
  where
    splices = do
      "alreadyClaimed" ## ifCSplice isNothing
      "mayClaim" ## deferMany
        (eitherDefer stdSqlErrorSplice (withSplices runChildren claimSplice) . (claim =<<))
    claim (u, c) =
      run $ statement (u, c) $ Statement sql
      (contrazip2 (EN.param EN.uid) (EN.param EN.int4))
      (DE.singleRow $ DE.column DE.text) False
    sql = "INSERT INTO claim_token (uid, cid) VALUES ($1, $2) \
          \ON CONFLICT (uid, cid) DO UPDATE SET uid=EXCLUDED.uid RETURNING token"
    claimSplice = do
      "claimCode" ## pureSplice . textSplice $ HTML.text
