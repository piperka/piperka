{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Splices.Providers (renderProviders) where

import Control.Monad.Trans
import Data.Map.Syntax
import Data.Text (Text)
import Heist

import Application
import Piperka.Heist
import Piperka.OAuth2.Providers

renderProviders
  :: Splice AppHandler
renderProviders = do
  manyWith runChildren providerSplices providerAttrSplices (map snd <$> lift getProviders)

providerSplices
  :: Splices (RuntimeAppHandler (Text, Text))
providerSplices = do
  "name" ## pureSplice . textSplice $ snd

providerAttrSplices
  :: Splices (RuntimeSplice AppHandler (Text, Text) -> AttrSplice AppHandler)
providerAttrSplices = "href" ## \n t -> do
  i <- fst <$> n
  return [("href", t <> i)]
