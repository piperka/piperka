{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Splices.Random where

import Contravariant.Extras.Contrazip
import Control.Monad.Trans
import Data.Map.Syntax
import Hasql.Session hiding (run, sql)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Statement
import Snap.Snaplet.Hasql

import Application
import Piperka.Auth
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (intToText, getCid)

renderRandomButton
  :: Splice AppHandler
renderRandomButton = eitherDefer stdSqlErrorSplice
  (withSplices (callTemplate "_randomButton") ("randomCid" ## pureSplice . textSplice $ id)) $ do
  cid <- fmap (fromIntegral . snd) <$> lift getCid
  usr <- fmap uid <$> lift currentUserPlain
  run $ statement (cid, usr) $ Statement
    "WITH updating_comics AS (\
    \SELECT cid FROM comics JOIN crawler_config USING (cid) \
    \WHERE cid <> coalesce($1,0) \
    \AND cid NOT IN (SELECT cid FROM subscriptions WHERE uid=$2) \
    \AND cid NOT IN (SELECT cid FROM disinterest WHERE uid=$2) \
    \AND (update_value > 0.000001 \
    \ OR cid IN (SELECT cid FROM hiatus_status WHERE active AND code=1))) \
    \SELECT cid FROM updating_comics \
    \OFFSET floor(random() * (SELECT count(*) FROM updating_comics)) LIMIT 1"
    (contrazip2 (EN.nullableParam EN.int4) (EN.nullableParam EN.uid))
    (DE.singleRow $ DE.column $ intToText <$> DE.int4) True
