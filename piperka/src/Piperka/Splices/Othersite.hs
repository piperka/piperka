{-# LANGUAGE RecordWildCards #-}

module Piperka.Splices.Othersite where

import Control.Monad.Trans
import qualified Data.IntSet as IntSet
import Data.IORef

import Application
import Piperka.Heist
import Piperka.UserID

othersiteUpdates
  :: AppInit
  -> RuntimeAppHandler MyData
othersiteUpdates (AppInit {..}) n = checkedSplice $ do
  u <- (\(UserID u) -> fromIntegral u) . uid <$> n
  liftIO $ IntSet.member u <$> readIORef othersiteUserUpdates
