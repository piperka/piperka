{-# LANGUAGE OverloadedStrings #-}

module Piperka.Splices.UserDeleted (renderUserDeletedComics) where

import Control.Applicative
import Data.Int
import Data.Map.Syntax
import Data.Text (Text)
import qualified Data.Text as T
import Data.Vector (Vector)
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Heist
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql (run)

import Application
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (formatTime', intToText)

renderUserDeletedComics
  :: RuntimeAppHandler MyData
renderUserDeletedComics =
  eitherDefer
  stdSqlErrorSplice
  (withSplices runChildren
   ("row" ## manyWithSplices runChildren userDeletedSplices)) .
  (run . fetchUserDeletedComics . uid =<<)

userDeletedSplices
  :: Splices (RuntimeAppHandler (Int32, Text, Maybe UTCTime, Text))
userDeletedSplices = mapV (pureSplice . textSplice) $ do
  "cid" ## intToText . \(c,_,_,_) -> c
  "title" ## HTML.text . \(_,t,_,_) -> t
  "time" ## maybe "" (T.pack . formatTime') . \(_,_,t,_) -> t
  "reason" ## \(_,_,_,r) -> r

fetchUserDeletedComics
  :: UserID
  -> Session (Vector (Int32, Text, Maybe UTCTime, Text))
fetchUserDeletedComics = flip statement $ Statement sql
  (EN.param EN.uid) (DE.rowVector decoder) True
  where
    decoder = (,,,)
      <$> DE.column DE.int4
      <*> DE.column DE.text
      <*> (liftA (fmap $ localTimeToUTC utc) $ DE.nullableColumn DE.timestamp)
      <*> DE.column DE.text
    sql = "SELECT cid, title, removed_on, reason \
          \FROM graveyard JOIN subscriptions USING (cid) \
          \WHERE uid=$1 ORDER BY removed_on DESC NULLS LAST"
