{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Splices.Schedule where

import Control.Monad
import Data.Int
import Data.Map.Syntax
import Data.Text (Text)
import Heist
import Heist.Splices
import qualified HTMLEntities.Text as HTML
import Snap.Snaplet.Hasql

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Update.Statements
import Piperka.Util (intToText)

upcomingSchedule
  :: RuntimeAppHandler MyData
upcomingSchedule n =
  eitherDefer stdSqlErrorSplice
  (deferManyElse (callTemplate "/include/index/_noUpcomingSchedule") $
   withSplices runChildren scheduleSplices) $ do
  run . fetchUpcomingSchedule . uid =<< n

scheduleSplices
  :: Splices (RuntimeAppHandler (Int, [(Int32, Text)]))
scheduleSplices = do
  let hoursDue p f = maybeSplice
        (withSplices runChildren ("hours" ## pureSplice . textSplice $ intToText)) .
        fmap (\(h,_) -> guard (p h) >> return (f h))
  "pastDue" ## hoursDue (< -1) negate
  "pastDue1" ## ifCSplice ((== -1) . fst)
  "due0" ## ifCSplice ((== 0) . fst)
  "due1" ## ifCSplice ((== 1) . fst)
  "due" ## hoursDue (>1) id
  "upcomingComics" ## manyWithSplices runChildren upcomingSplices . fmap snd

upcomingSplices
  :: Splices (RuntimeAppHandler (Int32, Text))
upcomingSplices = mapV (pureSplice . textSplice) $ do
  "cid" ## intToText . fst
  "title" ## HTML.text . snd
