module Piperka.Decoders (uid, authid, opid) where

import Hasql.Decoders

import Piperka.Auth.Types
import Piperka.UserID

uid :: Value UserID
uid = UserID <$> int4

authid :: Value AuthID
authid = AuthID <$> int4

opid :: Value ProviderID
opid = ProviderID <$> int4
