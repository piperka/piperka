{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}

module Piperka.Account.Splices
  ( accountSplices
  , accountAttrSplices
  , deleteSplices
  ) where

import Control.Monad
import Control.Monad.Trans
import Data.Bool
import Data.Functor
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Map.Syntax
import Heist
import Heist.Splices
import qualified HTMLEntities.Text as HTML
import Snap
import Snap.Snaplet.Session
import Text.XmlHtml

import Application hiding (holdBookmark)
import Piperka.Account.Types
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (intToText)

accountSplices
  :: RuntimeSplice AppHandler (Maybe AccountUpdateError, (AccountData, UserPrefs))
  -> Splices (Splice AppHandler)
accountSplices n = mapV ($ n) $ do
  let runId f = ifCSplice
        (not . null . filter (f . identification) . providers . fst . snd)
  "writeup" ## pureSplice . textSplice $ HTML.text . fromMaybe "" .
    writeup . userAccount . fst . snd
  "haveRemovableProviders" ## runId isJust
  "haveAttachableProviders" ## runId isNothing
  "oauth2Providers" ## renderProviders . fmap (providers . fst . snd)
  "hasError" ## const $ callTemplate "_accountError"
  "accountValidationError" ## maybeSplice accountErrorSplice . fmap fst
  "content" ## const $ runChildren
  "authenticateWith" ## const $ callTemplate "_authenticateWith"
  "providerName" ## const $ return $ yieldRuntimeText $ do
    dat <- lift $ withTop messages $ getFromSession "p_attach_name"
    return $ fromMaybe "" dat

accountErrorSplice
  :: RuntimeSplice AppHandler AccountUpdateError
  -> Splice AppHandler
accountErrorSplice = stdConditionalSplice accountError . (fmap (,()))
  where
    accountError (AccountSqlError _) =
      ("maybeSqlError", WithParam (\ ~(AccountSqlError e) _ -> return e)
                        (flip withSplices sqlErrorSplices))
    accountError AccountPasswordMissing = ("passwordMissing", Simple)
    accountError AccountPasswordWrong = ("wrongPassword", Simple)
    accountError AccountNewPasswordMismatch = ("passwordMismatch", Simple)

accountAttrSplices
  :: RuntimeSplice AppHandler (UserAccountSettings, UserPrefs)
  -> Splices (Text -> RuntimeSplice AppHandler [(Text, Text)])
accountAttrSplices n = mapV ($ n) $ do
  "value" ## valueSplice
  "columns" ## settingAttrSplice .
    fmap (columns . snd)
  "privacy" ## settingAttrSplice .
    fmap (privacy . fst)
  "holdBookmark" ## settingAttrSplice .
    fmap (holdBookmark . bookmarkSettings . fst)
  "sortBookmark" ## settingAttrSplice .
    fmap (bookmarkSort . bookmarkSettings . fst)
  "offsetMode" ## settingAttrSplice .
    fmap (offsetMode . bookmarkSettings . fst)
  "stickyShowHiddenMode" ## settingAttrSplice .
    fmap (stickyShowHidden . snd)
  "hasNoPassword" ## hasSplice . fmap (not . hasPassword . fst)

valueSplice
  :: RuntimeSplice AppHandler (UserAccountSettings, UserPrefs)
  -> Text
  -> RuntimeSplice AppHandler [(Text, Text)]
valueSplice n "new_windows" = newExternWindows . snd <$> n >>= \c ->
  return $ if c then [("checked", "")] else []
valueSplice n "rows" = rows . snd <$> n >>= \r ->
  return $ [("value", T.pack $ show r)]
valueSplice n "email" = email . fst <$> n >>= \e ->
  return $ maybeToList $ e >>= \e' -> return ("value", e')
valueSplice _ _ = error "unknown value"

settingAttrSplice
  :: (Eq a, Read a)
  => RuntimeSplice AppHandler a
  -> AttrSplice AppHandler
settingAttrSplice n t = do
  val <- n
  let val' = read $ T.unpack t
  val' `seq` return $ if val == val' then [("checked", "")] else []

hasSplice
  :: RuntimeSplice AppHandler Bool
  -> Text
  -> RuntimeSplice AppHandler [(Text, Text)]
hasSplice n t = do
  checkVal <- n
  return $ if checkVal then [(t, "1")] else []

renderProviders
  :: RuntimeAppHandler [ProviderData]
renderProviders n = do
  filterOwn <- maybe False (read . T.unpack) . getAttribute "filter"
               <$> getParamNode
  filterOwn `seq` manyWithSplices runChildren oauth2Splices $
    (if filterOwn then filter (isJust . identification) else id) <$> n

oauth2Splices
  :: Splices (RuntimeSplice AppHandler ProviderData -> Splice AppHandler)
oauth2Splices = do
  "label" ## pureSplice . textSplice $ snd . provider
  "name" ## pureSplice . textSplice $ fst . provider
  "identification" ## pureSplice . textSplice $ fromMaybe "" . identification
  "hasIdentification" ## checkedSplice . fmap (isJust . identification)

deleteSplices
  :: Splices (RuntimeAppHandler (DeleteRequest, Maybe DeleteState))
deleteSplices = do
  "active" ## checkedSplice . fmap (isJust . snd)
  "csrfFail" ## checkedSplice . fmap
    (\(req, _) -> not (csrfOk req) && (certain req || cancel req))
  "initiated" ## checkedSplice . fmap
    (\(req, st) -> certain req && csrfOk req && case st of
        Just (DeleteWaiting _) -> True
        _ -> False
    )
  "finished" ## checkedSplice . fmap
    (\(req, st) -> csrfOk req && final req && certain req && case st of
        Just DeleteAvailable -> True
        _ -> False
    )
  "dayPassed" ## checkedSplice . fmap
    ((\case { Just DeleteAvailable -> True ; _ -> False }) . snd)
  "timeLeft" ## manyWithSplices runChildren timeLeftSplices . fmap
    ((\case
         Just (DeleteWaiting t) -> Just ((ceiling @Float $ realToFrac t) :: Int)
         _ -> Nothing) . snd)
  where
    runIfDivPositive n = manyWithSplices runChildren timePartSplices . fmap
      (($>) @Maybe <$> guard . (>n) <*> flip div n)
    timeLeftSplices = do
      "hours" ## runIfDivPositive 3600
      "minutes" ## runIfDivPositive 60 . fmap (flip rem 3600)
    timePartSplices = mapV (pureSplice . textSplice) $ do
      "n" ## intToText
      "plural" ## bool "" "s" . (>1)
