{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Account.Delete where

import Control.Error.Util (hush)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Text.Encoding
import Data.Time.Clock
import qualified Data.UUID
import Hasql.Session hiding (run)
import Snap
import Snap.Snaplet.Hasql

import Application
import Piperka.Account.Query
import Piperka.Account.Splices
import Piperka.Account.Types
import Piperka.Error.Splices
import Piperka.Heist

getDeleteRequest
  :: MyData
  -> AppHandler DeleteRequest
getDeleteRequest u =
  DeleteRequest
  <$> ((== Just "I am sure") <$> getPostParam "iamsure")
  <*> ((== Just "1") <$> getPostParam "final")
  <*> ((== Just "1") <$> getPostParam "cancel")
  <*> ((== Just (ucsrfToken u)) . ((Data.UUID.fromText <=< hush . decodeUtf8') =<<) <$>
       getPostParam "csrf_ham"
      )

renderAccountDelete
  :: RuntimeAppHandler MyData
renderAccountDelete =
  eitherDefer stdSqlErrorSplice
  (withSplices runChildren deleteSplices) . (act =<<)
  where
    act usr = lift $ do
      delete <- getDeleteRequest usr
      runExceptT $ (delete,) <$> actDeleteRequest usr delete

deleteStatus
  :: NominalDiffTime
  -> Maybe DeleteState
deleteStatus t
  | t < nominalDay = Just $ DeleteWaiting $ nominalDay - t
  | t >= nominalDay && t < 7 * nominalDay = Just DeleteAvailable
  | otherwise = Nothing

actDeleteRequest
  :: MyData
  -> DeleteRequest
  -> ExceptT QueryError AppHandler (Maybe DeleteState)
actDeleteRequest usr req = do
  let u = uid usr
  dState <-
    if | cancel req ->
           pure Nothing
       | certain req && not (final req) ->
           pure $ Just $ DeleteWaiting nominalDay
       | otherwise ->
           (deleteStatus =<<) <$> ExceptT (run $ statement u accountDeleteLeft)
  if | not $ csrfOk req -> pure ()
     | cancel req ->
       ExceptT $ run $ statement u accountDeleteCancel
     | dState == Just DeleteAvailable && certain req && final req ->
       ExceptT $ run $ statement u accountDeleteFinish
     | certain req ->
       ExceptT $ run $ statement u accountDeleteStart
     | otherwise -> pure ()
  pure dState
