{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Piperka.Account.Query
  ( getAccountSettings
  , updateUnpriv
  , tryUpdatePriv
  , validatePriv
  , getUserEmail
  , accountDeleteStart
  , accountDeleteCancel
  , accountDeleteLeft
  , accountDeleteFinish
  ) where

import Control.Applicative
import Control.Error.Util (hush)
import Control.Monad
import Control.Monad.Trans (MonadIO)
import Control.Monad.Trans.Except
import Contravariant.Extras.Contrazip
import Data.Functor.Contravariant
import Data.Maybe
import qualified Data.Text as T
import Data.Text (Text)
import Data.Time (NominalDiffTime)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import Snap.Snaplet.Hasql (run, HasHasql)
import Unsafe.Coerce (unsafeCoerce)

import Application hiding (holdBookmark)
import Piperka.Account.Types hiding (provider)
import Piperka.Auth.Types
import qualified Piperka.Decoders as DE
import qualified Piperka.Encoders as EN
import Piperka.Listing.Types (columnsToInt)
import Piperka.OAuth2.Providers
import Piperka.Profile.Types (intToPrivacy, privacyToInt)

getAccountSettings
  :: UserID
  -> AppHandler (Either QueryError AccountData)
getAccountSettings u = do
  prov <- map (fmap ProviderData) <$> getProviders
  runExceptT $ do
    res1 <- ExceptT $ run $ statement u stmt1
    ids <- ExceptT $ run $ statement u stmt2
    return $ AccountData res1 $ map (\(i,p) -> p $ lookup i ids) prov
  where
    stmt1 = Statement sql1 encode (DE.singleRow decode1) True
    stmt2 = Statement sql2 encode (DE.rowList decode2) True
    encode = EN.param EN.uid
    decode1 = UserAccountSettings
              <$> (liftA intToPrivacy $ DE.column DE.int4)
              <*> DE.column DE.bool
              <*> DE.nullableColumn DE.text
              <*> DE.nullableColumn DE.text
              <*> (BookmarkOptions
                   <$> DE.column DE.int4
                   <*> DE.column DE.bool
                   <*> DE.column DE.bool)
    decode2 = (,)
              <$> (DE.column DE.opid)
              <*> DE.column DE.text
    sql1 = "SELECT privacy, \
           \uid IN (SELECT uid FROM login_method_passwd WHERE uid IS NOT NULL), \
           \email, writeup, bookmark_sort, offset_bookmark_by_one, hold_bookmark \
           \FROM users WHERE uid=$1"
    sql2 = "SELECT opid, identification FROM login_method_oauth2 WHERE uid=$1"

checkPassword
  :: Statement (UserID, Text) Bool
checkPassword = Statement sql encode (DE.singleRow $ DE.column DE.bool) True
  where
    encode = contrazip2 (EN.param EN.uid) (EN.param EN.text)
    sql = "SELECT (hash = crypt($2, hash)) AS pwmatch \
          \FROM users JOIN login_method_passwd USING (uid) WHERE uid=$1"

updateUnpriv
  :: (HasHasql m, MonadIO m)
  => UserID
  -> Bool
  -> AccountUpdate
  -> m (Either QueryError ())
updateUnpriv u updateBrowser a = run $ statement (u, a) stmt1 >>
  if updateBrowser then statement (u, a) stmt2 else return ()
  where
    stmt1 = Statement sql1 encode1 DE.unit True
    stmt2 = Statement sql2 encode2 DE.unit True
    encode1 = contrazip2 (EN.param EN.uid) $ mconcat
              [ holdBookmark . bookmarkSettings' >$< EN.param EN.bool
              , bookmarkSort . bookmarkSettings' >$< EN.param EN.int4
              , offsetMode . bookmarkSettings' >$< EN.param EN.bool
              ]
    encode2 = contrazip2 (EN.param EN.uid) $ mconcat
              [ newWindows >$< EN.param EN.bool
              , rows' >$< EN.param EN.int4
              , columnsToInt . columns' >$< EN.param EN.int4
              , stickyShowHidden' >$< EN.param EN.bool
              ]
    sql1 = "UPDATE users SET hold_bookmark=$2, bookmark_sort=$3, \
           \offset_bookmark_by_one=$4 WHERE uid=$1"
    sql2 = "UPDATE users SET new_windows=$2, display_rows=$3, \
           \display_columns=$4, sticky_show_hidden=$5 WHERE uid=$1"

updatePriv
  :: Statement (UserID, PrivData) ()
updatePriv = Statement sql encode DE.unit True
  where
    scrub = (=<<) (\x -> if T.null x then Nothing else Just x)
    encode = contrazip2 (EN.param EN.uid)
             ((contramap (scrub . email') (EN.nullableParam EN.text)) <>
              (contramap (privacyToInt . privacy') (EN.param EN.int4)) <>
              (contramap (scrub . writeup') (EN.nullableParam EN.text)))
    sql = "UPDATE users SET email=$2, privacy=$3, writeup=$4 WHERE uid=$1"

updatePassword
  :: Statement (UserID, PrivData) ()
updatePassword = Statement sql encode DE.unit True
  where
    encode = contrazip2 (EN.param EN.uid)
      (contramap (fromJust . newPassword) (EN.param EN.text))
    sql = "SELECT auth_create_password($2, $1)"

deletePassword
  :: Statement UserID ()
deletePassword = Statement sql (EN.param EN.uid) DE.unit True
  where
    sql = "DELETE FROM login_method_passwd WHERE uid=$1"

deleteOAuth2Login
  :: Statement (UserID, [ProviderID]) ()
deleteOAuth2Login = Statement sql encode DE.unit True
  where
    encode = contrazip2 (EN.param EN.uid)
      (EN.param $ EN.array $ EN.dimension foldl $ EN.element EN.opid)
    sql = "DELETE FROM login_method_oauth2 WHERE uid=$1 AND opid = ANY ($2 :: int[])"

validatePriv
  :: (HasHasql m, MonadIO m)
  => UserID
  -> PrivData
  -> ValidateMethod
  -> m (Either (Either AccountUpdateError NeedsValidation) ())
validatePriv u a val = runExceptT $ do
  let pwFail = case a of
        (UpdateAccount n n' _ _ _ _ _) -> let
          pwChange = maybe False (not . T.null) n
          pwMismatch = n /= n'
          in pwChange && pwMismatch
        _ -> False
  case (val, pwFail) of
    (_, True) -> throwE $ Left AccountNewPasswordMismatch
    (OAuth2 provider, _) -> throwE $ Right $ NeedsValidation provider a
    (Password p, _) ->
      if T.null p then throwE $ Left AccountPasswordMissing else do
        pwOk <- withExceptT (Left . AccountSqlError) $
          ExceptT $ run $ statement (u, p) checkPassword
        if pwOk then return () else throwE $ Left AccountPasswordWrong
    (Trusted, _) -> return ()

tryUpdatePriv
  :: (HasHasql m, MonadIO m)
  => UserID
  -> PrivData
  -> m (Either QueryError ())
tryUpdatePriv u a@(UpdateAccount _ _ _ _ _ _ _) = run $ do
  if passwordless a
    then statement u deletePassword
    else when (maybe False (not . T.null) $ newPassword a) $
         statement (u, a) updatePassword
  when (not $ null $ oauth2Removes a) $
    statement (u, oauth2Removes a) deleteOAuth2Login
  statement (u, a) updatePriv

tryUpdatePriv u (AttachProvider p token) =
  run $ statement (p, token, u) stmt
  where
    stmt = Statement sql encode (DE.unit) True
    encode = contrazip3 (EN.param EN.opid) (EN.param EN.text) (EN.param EN.uid)
    sql = "INSERT INTO login_method_oauth2 \
          \(opid, identification, uid) VALUES ($1, $2, $3)"

getUserEmail
  :: (HasHasql m, MonadIO m)
  => UserID
  -> m Text
getUserEmail = (fromMaybe "" . hush <$>) . run . flip statement stmt
  where
    stmt = Statement sql (EN.param EN.uid) (DE.singleRow $ DE.column DE.text) False
    sql = "SELECT email FROM users WHERE uid=$1"

accountDeleteStart
  :: Statement UserID ()
accountDeleteStart = Statement sql (EN.param EN.uid) DE.unit False
  where
    sql = "INSERT INTO user_delete_request (uid) VALUES ($1) \
          \ON CONFLICT (uid) DO UPDATE SET requested_at=now()"

accountDeleteCancel
  :: Statement UserID ()
accountDeleteCancel = Statement sql (EN.param EN.uid) DE.unit False
  where
    sql = "DELETE FROM user_delete_request WHERE uid=$1"

accountDeleteLeft
  :: Statement UserID (Maybe NominalDiffTime)
accountDeleteLeft = Statement sql (EN.param EN.uid) (DE.rowMaybe decode) False
  where
    decode = DE.column (unsafeCoerce <$> DE.interval)
    sql = "SELECT now()-requested_at FROM user_delete_request WHERE uid=$1"

accountDeleteFinish
  :: Statement UserID ()
accountDeleteFinish = Statement "CALL delete_user($1)" (EN.param EN.uid) DE.unit False
