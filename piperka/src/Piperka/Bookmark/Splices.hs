{-# LANGUAGE OverloadedStrings #-}
module Piperka.Bookmark.Splices where

import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Map.Syntax
import Data.Text (Text)
import Hasql.Session hiding (run)
import Heist
import Heist.Splices
import Network.URI.Encode (decodeText)
import Snap hiding (with)
import Snap.Snaplet.Hasql

import Application
import Piperka.Action.Statements
import Piperka.Bookmark.Types
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (intToText)

renderBookmark
  :: RuntimeAppHandler Text
renderBookmark =
  eitherDefer stdSqlErrorSplice renderBookmarkResult . resolveBookmark
  where
    resolveBookmark n = n >>= \url -> do
      let decodedURL = decodeText url
      wantHere <- (== Just "1") <$> lift (getParam "wantHere")
      runExceptT $ do
        res <- ExceptT $ run $ statement (url, wantHere) bookmarkFetch
        case res of
          [] -> if decodedURL /= url
                then ExceptT $ run $ statement (decodedURL, wantHere) bookmarkFetch
                else return []
          xs -> return xs

itemSplices
  :: Splices (RuntimeAppHandler BookmarkResult)
itemSplices = mapV (pureSplice . textSplice) $ do
  "cid" ## \(c, _, _) -> intToText c
  "title" ## \(_, t, _) -> t
  "maybeOrd" ## \(_, _, x) ->
    maybe "" (("&ord=" <>) . intToText . (\(o, _, _) -> o)) x

itemPageSplices
  :: Splices (RuntimeAppHandler BookmarkResult)
itemPageSplices =
  itemSplices <> mapV (. fmap (\(_, _, x) -> x)) pageSplice
  where
    pageSplice :: Splices (RuntimeAppHandler (Maybe (Int, Int, Bool)))
    pageSplice = "result" ## manyWithSplices runChildren pageSplices
    pageSplices :: Splices (RuntimeAppHandler (Int, Int, Bool))
    pageSplices = do
      "newest" ## newest True
      "notNewest" ## newest False
      "ord" ## pureSplice . textSplice $
        intToText . (\(o, s, _) -> o+if s > 0 then 2 else 1)
    newest v n = do
      spl <- runChildren
      flip bindLater n $ \(_, _, x) ->
        if x == v then codeGen spl else return mempty

itemPageAttrSplices
  :: Splices (RuntimeSplice AppHandler BookmarkResult -> AttrSplice AppHandler)
itemPageAttrSplices = do
  "bookmarkData" ## const . fmap
    (\(c, _, pg) ->
       [("data-cid", intToText c)] <>
       maybe [] (\(o, _, _) -> [("data-ord", intToText o)]) pg)

renderBookmarkResult
  :: RuntimeAppHandler [BookmarkResult]
renderBookmarkResult = withSplices runChildren $ do
  "success" ## with itemPageSplices . fmap successF
  "multiple" ## deferMany (with multipleSplices) . fmap multipleF
  "recognized" ## with itemSplices . fmap recognizedF
  "failed" ## ifCSplice null
  where
    with splices = manyWith runChildren splices itemPageAttrSplices
    multipleSplices = "item" ## withSplices runChildren itemSplices
    successF [x@(_, _, Just _)] = Just x
    successF _ = Nothing
    multipleF xs@(_:_:_) = Just xs
    multipleF _ = Nothing
    recognizedF [x@(_, _, Nothing)] = Just x
    recognizedF _ = Nothing
