module Piperka.Bookmark.Types where

import Data.Text (Text)

type BookmarkResult = (Int,Text,(Maybe (Int, Int, Bool)))
