{-# LANGUAGE OverloadedStrings #-}
module Piperka.Bookmark.Bookmarklet where

import Data.Map.Syntax
import Data.Text (Text)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (run)
import Hasql.Statement
import Snap.Snaplet.Hasql

import Application
import qualified Piperka.Encoders as EN
import Piperka.Error.Splices
import Piperka.Heist

renderWithBookmarklet
  :: RuntimeAppHandler MyData
renderWithBookmarklet =
  eitherDefer stdSqlErrorSplice
  (withSplices runChildren ("token" ## pureSplice . textSplice $ id)) .
  (run . getBookmarkletToken . uid =<<)

getBookmarkletToken
  :: UserID
  -> Session Text
getBookmarkletToken = flip statement $ Statement
  "SELECT bookmarklet_token FROM users WHERE uid=$1"
  (EN.param EN.uid) (DE.singleRow $ DE.column DE.text) True
