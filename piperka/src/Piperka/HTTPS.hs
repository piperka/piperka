{-# LANGUAGE OverloadedStrings #-}

module Piperka.HTTPS (sslRedirectHandler) where

import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Snap hiding (path)

import Application

data Protocol = HTTP | HTTPS
  deriving (Show, Eq)

sslRedirectHandler
  :: ByteString
  -> AppHandler ()
sslRedirectHandler hostname = do
  rq <- getRequest
  let forwardedProtocol = getHeader "X-Forwarded-Proto" rq
      protocol = case forwardedProtocol of
        Just "http" -> Just HTTP
        Just "https" -> Just HTTPS
        _ -> Nothing
      path = rqPathInfo rq
      query = rqQueryString rq
      isReader = path ` elem` ["reader", "reader/"]
      redirURI = ("/" <>) .
        (if B.null query then id else (<> "?" <> query)) $
        if path == "updates.html" then "" else path
      -- Use cache-control for now to verify that it works
      redir scheme =
        (modifyResponse $ addHeader "Cache-Control" "max-age=3600") >>
        redirect' (scheme <> hostname <> redirURI) 301
  case (protocol, isReader) of
    (Just HTTPS, True) -> redir "http://"
    (Just HTTP, False) -> redir "https://"
    _ -> return ()
