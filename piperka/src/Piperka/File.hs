module Piperka.File where

import System.Directory (doesFileExist)

import Application

searchSiteFile
  :: SiteIdentity
  -> FilePath
  -> IO FilePath
searchSiteFile site path = do
  let siteSpecific = "site/" <> sitePath site <> path
      commonFile = "site/common/" <> path
  exists <- doesFileExist siteSpecific
  return $ if exists then siteSpecific else commonFile
