{-# LANGUAGE DeriveGeneric #-}

module Piperka.Auth.Types where

import Data.Binary
import Data.Int
import GHC.Generics

newtype AuthID = AuthID { unAuthid :: Int32 }
  deriving (Generic)

instance Binary AuthID

-- OAuth2 provider
newtype ProviderID = ProviderID { unProviderid :: Int32 }
  deriving (Eq, Ord, Generic)

instance Binary ProviderID
