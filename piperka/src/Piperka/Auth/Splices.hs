{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Auth.Splices (authErrorSplices, getSSOSeed) where

import Control.Concurrent
import Control.Monad.Trans
import qualified Data.IntMap as IntMap
import Data.IORef
import Data.Maybe (fromJust, isJust)
import qualified Data.Text as T
import Data.Map.Syntax
import Hasql.Session (QueryError)
import Heist
import qualified Text.XmlHtml as X
import Snap.Snaplet.CustomAuth(AuthFailure(..), CreateFailure(..))
import Snap.Snaplet.CustomAuth.OAuth2(AuthFailure(..), OAuth2Failure(..))
import System.Random

import Application
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.Util (intToText)

authErrorSplices
  :: Splices (RuntimeAppHandler (AuthFailure QueryError))
authErrorSplices = do
  let getLoginFail (Login x) = Just x
      getLoginFail _ = Nothing
      getCreateFail (Create x) = Just x
      getCreateFail _ = Nothing
      getActionFail (Action x) = Just x
      getActionFail _ = Nothing
      getOtherFail (UserError x) = Just x
      getOtherFail _ = Nothing
      authDefer f splices = deferMany
        (withSplices runChildren splices) . fmap f

  "loginFailure" ## authDefer getLoginFail onErr
  "createFailure" ## authDefer getCreateFail $ onErr <> providerError
  "actionFailure" ## authDefer getActionFail onErr
  "otherwise" ## deferMany stdSqlErrorSplice . fmap getOtherFail

onErr
  :: (Read e, Eq e)
  => Splices (RuntimeAppHandler e)
onErr = "on" ## \n -> do
  test <- (==) . read . T.unpack . fromJust . (X.getAttribute "err") <$> getParamNode
  s <- runChildren
  test `seq` flip bindLater n $ \x -> if test x then codeGen s else return mempty

providerError
  :: Splices (RuntimeAppHandler CreateFailure)
providerError = "providerError" ##
  let getProviderError (OAuth2Failure (ProviderError e)) = Just e
      getProviderError _ = Nothing in
    deferMany
    (withSplices runChildren ("error" ## renderError)) . fmap getProviderError
  where
    renderError n = do
      def <- runChildren
      t <- deferMany (return . yieldRuntimeText) n
      flip bindLater n $ \x -> codeGen $ if isJust x then t else def

getSSOSeed
  :: AppInit
  -> RuntimeAppHandler MyData
getSSOSeed (AppInit {..}) u = pure $ yieldRuntimeText $ do
  MyData{..} <- u
  fmap intToText . liftIO $ do
    rnd <- randomIO
    -- Just ignore the case when old value exists.  The possible
    -- failure case is that another user's SSO fails.
    atomicModifyIORef crossSiteSSO $
      (,()) . IntMap.insert rnd (siteIdentity, uid, ucsrfToken)
    forkIO $ threadDelay 60000000 >> atomicModifyIORef crossSiteSSO
      ((,()) . IntMap.delete rnd)
    pure rnd
