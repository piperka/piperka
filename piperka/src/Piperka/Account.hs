{-# LANGUAGE OverloadedStrings #-}

module Piperka.Account (renderAccountForm, accountUpdateHandler, getHideInactive, getUserEmail) where

import Control.Error.Util (bool)
import Control.Lens
import Control.Monad.State
import Data.Maybe
import Snap
import Snap.Snaplet.CustomAuth
import Snap.Snaplet.CustomAuth.OAuth2
import Snap.Snaplet.Session

import Application
import Backend ()
import Piperka.Account.Action
import Piperka.Account.Splices
import Piperka.Account.Types
import Piperka.Account.Query
import Piperka.Error.Splices
import Piperka.Heist
import Piperka.OAuth2.Providers

renderAccountForm
  :: RuntimeAppHandler MyData
renderAccountForm =
  eitherDefer stdSqlErrorSplice
  (\n' -> withLocalSplices
          (accountSplices n')
          (accountAttrSplices $ (over _1 userAccount . snd) <$> n') $ do
      chl <- runChildren
      attach <- callTemplate "_accountAttach"
      return $ yieldRuntime $ do
        isAttach <- fmap isJust $ lift $ withTop messages $
          getFromSession "p_attach"
        codeGen $ if isAttach then attach else chl
  ) . (act =<<)
  where
    act usr = do
      pref <- lift $ view $ activePrefs
      err <- lift $ withTop' id $ view accountUpdateError
      let upd = maybe (Right pref) Left err
      accs <- lift $ getAccountSettings $ uid usr
      return $ accs >>= \a -> Right $
        either (\e -> (Just e, (a, pref))) (\p -> (Nothing, (a, p))) upd

accountUpdateHandler
  :: AppHandler ()
accountUpdateHandler = do
  full <- maybe pass return =<< withTop auth currentUser
  prov <- getProviders
  let usr = user full
  (== (Just "Cancel")) <$> getParam "cancel_attach" >>= flip bool cancelAttach
    (do
        upd <- accountUpdates usr
        case upd of
          Left (Right (NeedsValidation p a)) -> withTop apiAuth $ do
            setUser usr
            let providerName = fst . fromJust $ lookup p prov
            saveAction True providerName $ AccountPayload a
            redirectToProvider providerName
            return ()
          Left (Left e) -> modify $ set accountUpdateError $ Just e
          Right p -> maybe (pure ()) (modify . set activePrefs) p
    )

getHideInactive
  :: AppHandler Bool
getHideInactive = do
  p <- view activePrefs
  if stickyShowHidden p then pure False else (/= Just "1") <$> getParam "show_hidden"
