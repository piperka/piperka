{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Recommend.Thread
  ( RecommendHandle
  , Model
  , readModel
  , startRecommendProcess
  , refreshRecommends
  , getDimensions
  , getRecommends
  , hasRecommends
  , getRelatedCount
  , getRelated
  ) where

import Control.Concurrent
import Control.Exception
import Control.Monad
import Data.Int
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Maybe
import Data.Vector (Vector)
import Hasql.Connection
import Hasql.Session
import qualified Numeric.LinearAlgebra as H
import Numeric.Recommender.ALS

import Piperka.Recommend.Statements
import Piperka.UserID
import Piperka.Util (withSyslog, syslog, Priority(..))

type ModelVariables =
  (ALSModel UserID Int, IntMap [(Int, Bool)])

newtype Model = Model (Int, ModelVariables)

data RecommendHandle = RecommendHandle
  { conn :: Settings
  , searchPath :: Session ()
  , recommendConfig :: ALSParams
  , recommendIterations :: Int
  , recommendModel :: MVar ModelVariables
  }

startRecommendProcess
  :: Settings
  -> Int
  -> ALSParams
  -> Session ()
  -> IO RecommendHandle
startRecommendProcess c it reccfg sp = do
  model <- newEmptyMVar
  let h = RecommendHandle c sp reccfg it model
  refreshRecommends h
  return h

refreshRecommends
  :: RecommendHandle
  -> IO ()
refreshRecommends RecommendHandle{..} = do
  let eitherRun (Left e) = syslog Error $ "acquiring DB for \
                                          \recommend refresh failed: " <> show e
      eitherRun (Right c) = getAllSubscriptions searchPath c >>= either refreshError
        (\x -> do
            let model = buildModelRated recommendConfig
                        (fromIntegral . (\(UserID u) -> u))
                        (UserID . fromIntegral) id id x
                res = recommend model recommendIterations
            done <- results model !! (recommendIterations - 1) `seq`
              tryPutMVar recommendModel (model, res)
            when (not done) $ void $ swapMVar recommendModel (model, res)
        )
      refreshError e = syslog Error $ "refresh recommend DB failure: " <> show e
  withSyslog $
    bracket (acquire conn)
    (either (const $ return ()) release) eitherRun

getAllSubscriptions
  :: Session ()
  -> Connection
  -> IO (Either QueryError (Vector (UserID, (Int, Double))))
getAllSubscriptions sp = run (sp >> fetchAllSubscriptions)

readModel
  :: RecommendHandle
  -> IO Model
readModel RecommendHandle{..} = (Model . (recommendIterations,)) <$> readMVar recommendModel

getDimensions
  :: Model
  -> IntMap [Int]
getDimensions (Model (rIter, (model, _))) =
  IntMap.fromList . zip [1..] . (map . map) fst $ byDimensions model rIter

getRecommends
  :: Bool
  -> Maybe Int
  -> UserID
  -> Model
  -> [Int32]
getRecommends filt top usr (Model (_, (ALSModel{..}, recomm))) =
  maybe id take top .
  map (fromIntegral . fst) . (if filt then filter snd else id) .
  join . maybeToList $
  encodeUser usr >>= flip IntMap.lookup recomm

hasRecommends
  :: UserID
  -> Model
  -> Bool
hasRecommends usr (Model (_, (ALSModel{..}, recomm))) = isJust $
  encodeUser usr >>= flip IntMap.lookup recomm

getRelatedCount
  :: Model
  -> Int
getRelatedCount (Model (_, (ALSModel{..}, _))) =
  snd . H.size . itemFeature $ head results

getRelated
  :: Int
  -> Int
  -> Model
  -> [(Int, Double)]
getRelated lim cid (Model (rIter, (model@ALSModel{..}, _))) = fromMaybe [] $ do
  pos <- encodeItem cid
  take lim . filter ((> 0.01) . snd) <$> IntMap.lookup pos (related model rIter)
