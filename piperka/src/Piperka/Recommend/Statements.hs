{-# LANGUAGE OverloadedStrings #-}

module Piperka.Recommend.Statements where

import Data.Functor.Contravariant
import Data.Maybe
import Data.Vector (Vector)
import Hasql.Decoders as DE
import Hasql.Encoders as EN
import Hasql.Session hiding (run, sql)
import Hasql.Statement

import qualified Piperka.Decoders as DE
import Piperka.UserID (UserID)

fetchNewestLim
  :: Int
  -> Session Int
fetchNewestLim i = statement i $ Statement
  "SELECT cid FROM comics ORDER BY cid DESC LIMIT 1 OFFSET $1"
  (EN.param $ fromIntegral >$< EN.int4)
  (DE.singleRow $ DE.column $ fromIntegral <$> DE.int4) True

fetchAllSubscriptions
  :: Session (Vector (UserID, (Int, Double)))
fetchAllSubscriptions = statement () $ Statement
  "SELECT uid, cid, update_value, COALESCE(num, 1), code, schedule IS NOT NULL \
  \FROM subscriptions LEFT JOIN comic_remain_frag_cache USING (uid, cid) \
  \JOIN users USING (uid) JOIN comics USING (cid) JOIN crawler_config USING (cid) \
  \LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid) \
  \LEFT JOIN inferred_schedule USING (cid) \
  \WHERE countme \
  \AND uid IN (SELECT uid FROM subscriptions GROUP BY uid HAVING count(cid) >= 5)"
  EN.unit
  (DE.rowVector ((,)
                 <$> (DE.column $ DE.uid)
                 <*> ((,)
                      <$> (DE.column $ fromIntegral <$> DE.int4)
                      <*> score))) True
  where
    score = do
      upd <- DE.column DE.float4
      unread <- DE.column DE.int4
      code <- DE.nullableColumn DE.int2
      sched <- DE.column DE.bool
      pure $ (if (upd >= 0.000001) && isNothing code
               -- Prefer frequent updates with active readership
              then (if unread == 0 then (+0.1) else id)
              else subtract 0.07) .
             -- Prefer regular updates
             maybe (if sched then (+0.04) else id)
             -- "Completed" is better than the other reasons
             (\c -> subtract $ if c == 1 then 0.02 else 0.1) code $ 1
