{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}

module Piperka.Auth where

import Control.Applicative
import Control.Error.Util (hush)
import Control.Monad.State
import Control.Monad.Trans.Maybe
import Control.Lens
import Data.ByteString.Builder
import qualified Data.ByteString.Char8 as C
import Data.ByteString.Lazy (toStrict)
import Data.List (find)
import Data.Maybe (isJust, isNothing)
import qualified Data.IntMap as IntMap
import Data.IORef
import Data.Time
import Data.UUID (UUID, toASCIIBytes, toText)
import qualified Hasql.Decoders as DE
import qualified Hasql.Encoders as EN
import Hasql.Session hiding (sql, run)
import Hasql.Statement
import Snap
import Snap.Snaplet.CustomAuth
import Snap.Snaplet.Hasql
import Snap.Snaplet.Heist

import Application
import Backend ()
import Piperka.Action
import qualified Piperka.Encoders as EN
import Piperka.Listing.Types (ViewColumns(OneColumn), intToColumns, columnsToInt)
import Piperka.Mobile (getMobi)

authHandler
  :: Bool
  -> AppHandler ()
  -> AppHandler ()
authHandler alwaysMinimal action = do
  failed <- liftIO $ newIORef False
  let loginFailed = withTop' id $ liftIO $ writeIORef failed True
  useMinimal <- if alwaysMinimal then return True
    else any isJust <$> mapM getParam ["min", "redir"]
  modify $ set minimal useMinimal
  uprefs <- case useMinimal of
    True -> fmap getPrefs <$> withTop apiAuth (combinedLoginRecover loginFailed)
    False -> do
      u <- withTop auth (combinedLoginRecover loginFailed)
      processAction u >>=
        (\(act, usr) -> do
            modify $ set actionResult act
            maybe (pure ()) (withTop auth . setUser) usr)
      pure $ getPrefs <$> u
  isFailed <- liftIO $ readIORef failed
  maybe (return ()) (setCSRFCookie . ucsrfToken) =<< currentUserPlain
  mobi <- getMobi
  if isFailed then cRender "loginFailed_" else do
    ck <- rqCookies <$> getRequest
    -- Ignore database stored prefs for mobile
    let pf = if mobi
             then maybe defaultMobileUserPrefs (\x -> x { columns = OneColumn }) $
                  userPrefsFromCookies mobi ck
             else maybe defaultUserPrefs id $
                  userPrefsFromCookies mobi ck <|> uprefs
    modify $ set activePrefs pf
    action
  when (not useMinimal && isJust uprefs) $ do
    p <- view activePrefs
    stamp <- addUTCTime (5*365*24*60*60) <$> liftIO getCurrentTime
    modifyResponse $ userPrefsToCookies p stamp mobi

setCSRFCookie
  :: UUID
  -> AppHandler ()
setCSRFCookie token = modifyResponse $ addResponseCookie $
  Cookie "csrf_ham" (toASCIIBytes token)
  Nothing Nothing (Just "/") False False

userPrefsFromCookies
  :: Bool
  -> [Cookie]
  -> Maybe UserPrefs
userPrefsFromCookies mobi xs =
  let ck n f = find ((== ((if mobi then ("m_" <>) else id) n)) . cookieName) xs >>=
               C.readInt . cookieValue >>= f . fst
  in UserPrefs
     <$> (ck "pref_rows" $ \x -> guard (x > 1) >> pure (fromIntegral x))
     <*> (if mobi then pure OneColumn else ck "pref_cols" $ pure . intToColumns)
     <*> (ck "pref_extern" $ \x -> pure $ if x == 1 then True else False)
     <*> (ck "pref_sticky_show_hidden" $ pure . (== 1))

userPrefsToCookies
  :: UserPrefs
  -> UTCTime
  -> Bool
  -> Response
  -> Response
userPrefsToCookies UserPrefs{..} t mobi =
  let ck k v = addResponseCookie $ Cookie ((if mobi then ("m_" <>) else id) k)
               v (Just t) Nothing (Just "/") True True
      ck' k = ck k . toStrict . toLazyByteString
  in (ck' "pref_rows" $ intDec $ fromIntegral rows) .
     (if mobi then id else ck' "pref_cols" $ intDec $ columnsToInt columns) .
     (ck "pref_sticky_show_hidden" (if stickyShowHidden then "1" else "0")) .
     (ck "pref_extern" (if newExternWindows then "1" else "0"))

currentUserPlain
  :: AppHandler (Maybe MyData)
currentUserPlain =
  withTop apiAuth currentUser >>=
  maybe ((fmap user) <$> withTop auth currentUser) (return . Just)

mayCreateAccount
  :: AppHandler ()
  -> AppHandler ()
mayCreateAccount action = maybe action id =<< runMaybeT
  (do
      -- Do nothing if there's a session cookie
      guard =<< (lift $ not <$> withTop auth isSessionDefined)
      guard =<< (lift $ (== (Just "Create account")) <$> getParam "action")
      return $
        withTop auth createAccount >>=
        (either
          (const $ return "newuser.html")
          -- Prime user to a mobile setting on account creation
          (const $ do
              mobi <- snd <$> view mobile
              modify $ set mobile (Just mobi, mobi)
              now <- liftIO getCurrentTime
              modifyResponse $ addResponseCookie $ Cookie "mobi"
                (if mobi then "1" else "0")
                (Just $ addUTCTime (5*365*24*60*60) now)
                Nothing (Just "/") True False
              return "welcome_"
          )) >>=
        cRender
  )

ssoLogin
  :: AppInit
  -> AppHandler ()
ssoLogin (AppInit {..}) = do
  notLoggedIn <- isNothing <$> withTop auth currentUser
  when notLoggedIn $ fmap (const ()) . runMaybeT $ do
    key <- MaybeT $ (fmap fst . C.readInt =<<) <$> getPostParam "sso"
    rqCsrf <- MaybeT $ getPostParam "csrf_ham"
    (otherSite, uid, csrf) <- MaybeT $
      IntMap.lookup key <$> liftIO (readIORef crossSiteSSO)
    guard $ otherSite /= siteIdentity && rqCsrf == toASCIIBytes csrf
    session <- MaybeT $ fmap hush . run $ statement uid loginDirect
    usr <- MaybeT $ withTop auth $ do
      usr <- hush <$> recover (toText session)
      modify $ \mgr -> mgr { activeUser = usr }
      return $ user <$> usr
    lift $ do
      now <- liftIO getCurrentTime
      modifyResponse $
        addResponseCookie $
        Cookie Application.sessionCookieName (toASCIIBytes $ usession usr)
        (Just $ addUTCTime (5*365*24*60*60) now) Nothing (Just "/") True True
      setCSRFCookie $ ucsrfToken usr
      liftIO $ atomicModifyIORef crossSiteSSO $ (,()) . IntMap.delete key
  where
    loginDirect :: Statement UserID UUID
    loginDirect = Statement "SELECT p_session FROM do_login($1)"
      (EN.param EN.uid) (DE.singleRow (DE.column DE.uuid)) True
