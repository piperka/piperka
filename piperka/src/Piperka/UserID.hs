module Piperka.UserID where

import Data.Binary
import Data.Int

newtype UserID = UserID Int32
  deriving (Eq)

instance Binary UserID where
  put (UserID u) = put u
  get = UserID <$> get
