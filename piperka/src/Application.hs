{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}

------------------------------------------------------------------------------
-- | This module defines our application's state type and an alias for its
-- handler monad.
module Application
  ( MyData(..)
  , UserPrefs(..)
  , UserWithStats(..)
  , MasterApp(..)
  , App(..)
  , AppHandler
  , RuntimeAppHandler
  , UserID
  , WebAuth
  , ApiAuth
  , AnyAuth
  , AppInit(..)
  , ModeratorLevel(..)
  , SiteIdentity(..)
  , AnnounceEvent(..)
  , sitePath
  , sitePiperka
  , siteTeksti
  , setSearchPath
  , auth
  , apiAuth
  , heist
  , db
  , messages
  , taglookup
  , extlookup
  , httpManager
  , crawlHandle
  , updateWatcherChan
  , mobile
  , activePrefs
  , suppressError
  , minimal
  , autoAds
  , accountUpdateError
  , actionResult
  , submitResult
  , prefetchComicInfo
  , recommendHandle
  , recommendComics
  , siteAnnouncement
  , defaultUserPrefs
  , defaultMobileUserPrefs
  , defaultUserStats
  , getPrefs
  , sessionCookieName
  ) where

------------------------------------------------------------------------------

import Control.Concurrent.Chan (Chan)
import Control.Concurrent.MVar (MVar)
import Control.Lens
import Data.ByteString (ByteString)
import Data.IntMap (IntMap)
import Data.IntSet (IntSet)
import Data.IORef (IORef)
import Data.Text as T
import Data.Text.ICU (Regex)
import Data.Int
import Data.UUID
import Control.Monad.State
import qualified Hasql.Session
import Heist (RuntimeSplice)
import Heist.Compiled (Splice)
import Network.HTTP.Client (Manager)
import Snap.Snaplet
import Snap.Snaplet.Heist
import Snap.Snaplet.CustomAuth hiding (sessionCookieName)
import Snap.Snaplet.Hasql
import Snap.Snaplet.Session
import Web.WebPush (VAPIDKeys)

import Crawler.Command.Types
import Piperka.Account.Types (AccountUpdateError)
import Piperka.Action.Types
import Piperka.API.Submit.Types (SubmitResult)
import Piperka.ComicInfo.Types
import Piperka.Listing.Types (ViewColumns(..))
import Piperka.Recommend.Thread (RecommendHandle)
import Piperka.UserID
------------------------------------------------------------------------------

data SiteIdentity = Piperka | Teksti
  deriving (Show, Read, Eq)

sitePath
  :: SiteIdentity
  -> FilePath
sitePath Piperka = "piperka/"
sitePath Teksti = "teksti/"

setSearchPath
  :: SiteIdentity
  -> Hasql.Session.Session ()
setSearchPath site = Hasql.Session.sql $ "SET search_path TO " <> searchPath site <> ",public"
  where
    searchPath Piperka = "piperka"
    searchPath Teksti = "teksti"

data ModeratorLevel = Moderator | Admin
  deriving (Show, Read, Ord, Eq)

data MyData = MyData
  { usession :: UUID
  , uid :: UserID
  , uname :: T.Text
  , ucsrfToken :: UUID
  , moderator :: Maybe ModeratorLevel
  , author :: Maybe Int32
  , lastUpdateDisclaimer :: Bool
  , prefs :: UserPrefs
  }

data UserPrefs = UserPrefs
  { rows :: Int32
  , columns :: ViewColumns
  , newExternWindows :: Bool
  , stickyShowHidden :: Bool
  } deriving (Show)

data UserWithStats = UserWithStats
  { newComics :: Int32
  , unreadCount :: (Int32,Int32)
  , unreadCountHidden :: (Int32, Int32)
  , holdBookmark :: Bool
  , offsetBookmarkMode :: Bool
  , user :: MyData
  , modStats :: Maybe (Int32, Int32, Int32)
  }

class HasPrefs u where
  getPrefs :: u -> UserPrefs

instance HasPrefs MyData where
  getPrefs = prefs

instance HasPrefs UserPrefs where
  getPrefs = id

instance HasPrefs UserWithStats where
  getPrefs = prefs . user

data AnnounceEvent
  = ComicAdded Int Text
  | ComicRetired Int Text Text
  | RepoUpdated Text [(Text, Text)]

type WebAuth = AuthManager UserWithStats Hasql.Session.QueryError App
type ApiAuth = AuthManager MyData Hasql.Session.QueryError App
type AnyAuth u = AuthManager u Hasql.Session.QueryError App

data MasterApp = MasterApp
  { _sitePiperka :: Snaplet App
  , _siteTeksti :: Snaplet App
  }

data App = App
  { _heist :: Snaplet (Heist App)
  , _auth :: Snaplet WebAuth
  , _apiAuth :: Snaplet ApiAuth
  , _db :: Snaplet Hasql
  , _messages :: Snaplet SessionManager
  , _extlookup :: Int -> Text -> Maybe ExternalEntry
  , _taglookup :: [Int] -> [ComicTag]
  , _httpManager :: Manager
  , _crawlHandle :: IORef (IntMap Crawl)
  -- Gets a token after each hourly crawler run.
  , _updateWatcherChan :: Chan ()
  , _mobile :: (Maybe Bool, Bool)
  , _activePrefs :: UserPrefs
  -- Used when there's a template specific way of presenting an error.
  , _suppressError :: Bool
  , _minimal :: Bool
  , _autoAds :: Maybe Bool
  , _accountUpdateError :: Maybe AccountUpdateError
  , _actionResult :: Maybe (Maybe ActionError, Maybe Action)
  , _submitResult :: Maybe SubmitResult
  , _prefetchComicInfo :: Maybe (Either ComicInfoError ComicInfo)
  , _recommendHandle :: Maybe RecommendHandle
  , _recommendComics :: MVar [(Int, Double)]
  , _siteAnnouncement :: Chan AnnounceEvent
  }

data AppInit = AppInit
  { extFormPart :: Text
  , extMobiFormPart :: Text
  , extValidation :: IntMap Regex
  , tagFormPart :: Text
  , hiatusPart :: Text
  , hiatusNames :: [(Int, Text)]
  , vapidKeys :: VAPIDKeys
  , appHostname :: Text
  , siteIdentity :: SiteIdentity
  , othersiteUserUpdates :: IORef IntSet
  , crossSiteSSO :: IORef (IntMap (SiteIdentity, UserID, UUID))
  , adAskOptOut :: Bool
  , adsDisabled :: Bool
  , webhookSecret :: Maybe ByteString
  , scriptHash :: [(Text, Text)]
  }

defaultUserPrefs :: UserPrefs
defaultUserPrefs = UserPrefs
  { rows = 40
  , columns = ThreeColumn
  , newExternWindows = False
  , stickyShowHidden = False
  }

defaultMobileUserPrefs :: UserPrefs
defaultMobileUserPrefs = UserPrefs
  { rows = 18
  , columns = OneColumn
  , newExternWindows = True
  , stickyShowHidden = False
  }

defaultUserStats :: MyData -> UserWithStats
defaultUserStats usr = UserWithStats 0 (0, 0) (0, 0) False False usr Nothing

makeLenses ''App

makeLenses ''MasterApp

instance HasHeist App where
  heistLens = subSnaplet heist

instance HasHasql (Handler App v) where
  getHasqlState = withTop db get
  setHasqlState s = withTop db $ put s

instance HasHasql (RuntimeSplice (Handler App v)) where
  getHasqlState = lift $ withTop db get
  setHasqlState = lift . withTop db . put

instance HasHasql (Handler MasterApp App) where
  getHasqlState = with db get
  setHasqlState = with db . put

sessionCookieName :: ByteString
sessionCookieName = "p_session"

------------------------------------------------------------------------------
type AppHandler = Handler App App
type RuntimeAppHandler a = RuntimeSplice AppHandler a -> Splice AppHandler
