{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections #-}

module Snap.Snaplet.Hasql where

import Control.Concurrent
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.ByteString
import qualified Data.Configurator
import Data.IORef
import qualified Hasql.Connection as C
import qualified Hasql.Encoders as E
import qualified Hasql.Decoders as D
import qualified Hasql.Session as S
import Hasql.Statement
import Snap.Snaplet

class HasHasql m where
  getHasqlState :: m Hasql
  setHasqlState :: Hasql -> m ()

type MaybeConnection = Either C.ConnectionError C.Connection

type MaybeConnectionInitialized = Either C.ConnectionError (C.Connection, Maybe S.QueryError)

data Hasql = HasqlSettings (IO MaybeConnectionInitialized) (MaybeConnection -> IO ()) Int
           | HasqlConnection (IORef (Maybe C.Connection))
             (IO MaybeConnectionInitialized) (MaybeConnection -> IO ()) Int

hasqlInit :: C.Settings -> Maybe (S.Session a) -> SnapletInit b Hasql
hasqlInit s sesInit = makeSnaplet "hasql" "Hasql Snaplet" Nothing $ do
  cfg <- getSnapletUserConfig
  let acq = maybe
        (fmap (, Nothing) <$> C.acquire s)
        (\ini -> C.acquire s >>= either
                 (return . Left)
                 (\c -> Right . (c,) . either Just (const Nothing) <$> S.run ini c))
        sesInit
  (poolSize, poolVar, syncVar) <- liftIO $ do
    poolSize <- Data.Configurator.lookupDefault 5 cfg "pool"
    poolVar <- newEmptyMVar
    syncVar <- newEmptyMVar
    replicateM_ poolSize $ forkIO $ forever $
      takeMVar syncVar >> acq >>= putMVar poolVar
    replicateM_ poolSize $ putMVar syncVar ()
    return (poolSize, poolVar, syncVar)
  return $ HasqlSettings (takeMVar poolVar)
    (\c -> either (const $ return ()) C.release c >> putMVar syncVar ()) poolSize

commit :: S.Session ()
commit = S.statement () $ Statement "commit" E.unit D.unit True

begin :: S.Session ()
begin = S.statement () $ Statement "begin" E.unit D.unit True

ping :: S.Session ()
ping = S.statement () $ Statement "select where false" E.unit D.unit True

run :: (HasHasql m, MonadIO m) => S.Session a -> m (Either S.QueryError a)
run session = do
  state <- getHasqlState
  case state of
   HasqlConnection ref acquire release poolSize -> liftIO $ runExceptT $ do
     let tryPool :: Int -> S.QueryError -> ExceptT S.QueryError IO C.Connection
         tryPool 0 = ExceptT . return . Left
         tryPool n = const $ do
           c <- liftIO acquire
           either (\e -> (liftIO $ release $ fst <$> c) >> tryPool (n-1) e) (lift . return) =<<
             (liftIO $ runExceptT $ do
                 (c', initErr) <- catchE (ExceptT $ return c)
                   (throwE . S.QueryError "" [] . S.ClientError)
                 maybe (return ()) throwE initErr
                 _ <- ExceptT $ S.run ping c'
                 return c')
     let initSession = do
           c <- tryPool (poolSize+1) (S.QueryError "" [] $ S.ClientError (Just ""))
           liftIO $ writeIORef ref $ Just c
           ExceptT $ liftIO $ S.run begin c
           return c
     c <- maybe initSession return =<< liftIO (readIORef ref)
     ExceptT $ liftIO (S.run session c)
   HasqlSettings _ _ _ -> error "connection IORef not initialized"

wrapDbOpen :: (HasHasql (Handler b v)) => Initializer b v ()
wrapDbOpen = wrapSite bracketDbOpen

addRoutesDbOpen :: (HasHasql (Handler b v)) =>
                   [(ByteString, Handler b v ())] -> Initializer b v ()
addRoutesDbOpen = addRoutes . over (mapped._2) bracketDbOpen

bracketDbOpen :: (HasHasql (Handler b v)) => Handler b v a -> Handler b v a
bracketDbOpen site = do
  s' <- getHasqlState
  case s' of
    HasqlSettings s r poolSize ->
      bracketHandler (newIORef Nothing)
      (maybeRelease r)
      (workSite s r poolSize)
    HasqlConnection _ _ _ _ -> error "should not happen"
  where
    -- TODO: What to do if commit fails?
    maybeRelease release ref = readIORef ref >>=
      maybe (return ()) (\c -> S.run commit c >> (release $ Right c))
    workSite s r poolSize ref = do
      setHasqlState (HasqlConnection ref s r poolSize)
      x <- site
      setHasqlState (HasqlSettings s r poolSize)
      return x
