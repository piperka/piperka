{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

------------------------------------------------------------------------------
-- | This module is where all the routes and handlers are defined for your
-- site. The 'app' function is the initializer that combines everything
-- together and is exported by this module.
module Site
  ( app
  ) where

------------------------------------------------------------------------------
import Control.Concurrent
import Control.Concurrent.Async
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Crypto.Hash.MD5
import Data.ByteString (ByteString, readFile)
import qualified Data.ByteString as B
import qualified Data.ByteString.Base16 as Base16
import qualified Data.ByteString.Char8 as B8
import Data.ByteString.UTF8 (fromString)
import qualified Data.Configurator
import Data.Int (Int32)
import Data.IntMap (IntMap)
import Data.IntSet (IntSet)
import Data.IORef (newIORef, IORef)
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Data.UUID (UUID)
import Hasql.Session (sql)
import Heist
import qualified Network.HTTP.Client as HTTP.Client
import Network.HTTP.Client.TLS
import Numeric.Recommender.ALS (ALSParams(..))
import Prelude hiding (readFile)
import Snap.Core
import Snap.Snaplet
import Snap.Snaplet.CustomAuth hiding (sessionCookieName)
import Snap.Snaplet.Hasql
import Snap.Snaplet.Heist
import Snap.Snaplet.Session.Backends.CookieSession
import Snap.Util.FileServe
import System.Directory (createDirectoryIfMissing, listDirectory)
import System.Environment (getProgName)
import System.Posix.Syslog (Priority(Emergency, Notice), setlogmask)
import Text.Read (readMaybe)
import Web.WebPush

import Application
import Crawler.DB
import Piperka.Account
import Piperka.API
import Piperka.Auth (authHandler, currentUserPlain, mayCreateAccount, ssoLogin)
import Piperka.ComicInfo.Hiatus
import Piperka.ComicInfo.Tag
import Piperka.ComicInfo.External
import Piperka.ComicInfo.Xslt
import Piperka.Discord
import Piperka.File
import Piperka.HTTPS
import Piperka.Mobile
import Piperka.OAuth2
import Piperka.OAuth2.Providers
import Piperka.Recommend.Thread
import Piperka.Splices
import Piperka.Update.Handlers
import Piperka.UserID

instance MonadFail (Initializer a b) where
  fail = error

------------------------------------------------------------------------------
-- | The application's routes.
routes
  :: ByteString
  -> DBConfig
  -> VAPIDKeys
  -> AppInit
  -> Chan AnnounceEvent
  -> IO [(ByteString, Handler App App ())]
routes hostname dbConfig vapid ini ann = do
  let apiRoutes =
        (maybe id ((:) . ("/s/gitlabWebhook",) . gitlabWebhook ann) $ webhookSecret ini) $
        [ ("/s/cinfo/:cid", comicInfo)
        , ("/s/sortedlist", sortedList hostname)
        , ("/s/qsearch", quickSearch)
        , ("/s/tagslist/:tagid", tagList)
        , ("/s/uprefs", userPrefs hostname)
        , ("/s/archive/:cid", dumpArchive hostname)
        , ("/s/rss/:cid", rssFeed hostname)
        , ("/s/profile", profileSubmission)
        , ("/s/attachProvider/:provider", attachProvider)
        , ("/s/submit", receiveSubmit ini)
        , ("newuser", mayCreateAccount (return ()))
        , ("/s/login", apiLogin)
        , ("/s/createAccount", apiCreateAccount)
        , ("/s/deleteAccount", deleteAccount)
        , ("/s/export", exportUserData)
        , ("/s/lookup", lookupPage hostname)
        , ("/s/lookupPositions", lookupPositions)
        , ("/s/recommend", recommendList hostname)
        , ("/s/recommendDims", recommendDims)
        , ("/s/related", related)
        , ("/s/updateWatch", updateWatch)
        , ("/s/push", updatePush vapid)
        , ("/s/editAuthor", editAuthor)
        , ("/s/random", randomPage)
        -- Moderator interface
        , ("/s/autoFix", autoFixer)
        , ("/s/sinfo/:sid", readUserEdit)
        , ("/s/sinfo2/:sid", readSubmit)
        , ("/s/dropsubmit/:sid", dropUserEdit)
        , ("/s/viewsubmitbanner/:sid", viewSubmitBanner)
        , ("/s/hiatus", controlHiatus ini)
        , ("/s/genentry/:sid", readGenentry site)
        , ("/s/generateComicTitles", generateComicTitles site)
        , ("/s/crawler", crawler dbConfig)
        ]
  let specialTemplates = [ "account.html"
                         , "newuser.html"
                         , "include/cinfo"
                         , "reader"
                         ]
  templateRoutes <-
    (map fromString .
     filter (not . (`elem` specialTemplates)) .
     filter ((/= "_") . take 1) .
     filter ((/= "_") . take 1 . reverse) .
     map (reverse . drop 4 . reverse) .
     filter ((== ".tpl") . reverse . take 4 . reverse) .
     filter ((> 4) . length)
    ) <$>
    ((<>)
      <$> listDirectory "snaplets/heist/templates"
      <*> listDirectory ("snaplets/" <> sitePath site <> "snaplets/heist/site_templates"))
  return $
    ([ ("reader", bracketDbOpen $ cRender "reader")
     ] <>) $
    mapped._2 %~ bracketDbOpen $ apiRoutes <>
    (map (\x -> (x, mobileMode $ authHandler False $ cRender x)) templateRoutes) <>
    [ ("account.html", mobileMode $ authHandler False $
        accountUpdateHandler >> cRender "account.html")
    , ("updates.html", do
          rq <- getRequest
          let q = rqQueryString rq
          redirect' (if B.null q then "/" else "/?" <> q) 301)
    , ("newuser.html", mobileMode $ authHandler False $
        mayCreateAccount $ cRender "newuser.html")
    , ("include/cinfo", mobileMode $ authHandler True $ cRender "include/cinfo")
    , ("", ifTop renderIndex)
    , ("index.html", renderIndex)
    ]
  where
    site = siteIdentity ini
    renderIndex = do
      mobileMode $ authHandler False $ do
        didRedir <- mayRedir
        when (not didRedir) $ do
          ssoLogin ini
          cRender "index"
      -- Refresh the data on which updates the user has not seen.  No
      -- need to delay serving the request for it.
      currentUserPlain >>= maybe (return ())
        (\u -> do
            p <- rqServerPort <$> getRequest
            mgr <- view httpManager
            -- TODO: Is there some way to short cut this?
            liftIO $ forkIO $ do
              let req = HTTP.Client.parseRequest_ $
                    "http://localhost" <>
                    "/refreshUser/" <> show @SiteIdentity site <>
                    "/" <> show @Int32 ((\(UserID x) -> x) $ uid u)
              HTTP.Client.httpNoBody
                (req { HTTP.Client.method = "HEAD"
                     , HTTP.Client.port = p
                     }) mgr
              return ()
            return ()
        )
staticRoutes
  :: ByteString
  -> SiteIdentity
  -> [(ByteString, Handler App App ())]
staticRoutes hostname site =
  ((mapped._2 %~ (\x -> serveFile x >>
                   modifyResponse (addHeader "Cache-Control" "public, max-age=31536000")) $
    [ ("/site.css", "site/common/static/site.css")
    , ("/piperka.js", "site/common/static/piperka.js")
    , case site of
        Piperka -> ("/images/paprika.png", "site/piperka/static/images/paprika.png")
        Teksti -> ("/images/kirja.png", "site/teksti/static/images/kirja.png" )
    , ("/favicon.png", "site/" <> sitePath site <> "static/favicon.png")
    ] )<>) .
  ((mapped._2 %~ (\x -> serveFile ("data/" <> sitePath site <> x) >>
                        modifyResponse (addHeader "Access-Control-Allow-Origin"
                                        ("http://" <> hostname))) $
    [ ("/d/comictitles.json", "d/comictitles.json")
    , ("/d/comics_ordered_rated.json", "d/comics_ordered_rated.json")
    ] )<>) $
  mapped._2 %~ serveDirectory $
  (if site == Piperka then (("blog", "data/blog"):) else id)
  [ ("d", "data/" <> sitePath site <> "d")
  , ("banners", "data/" <> sitePath site <> "banners")
  , ("/d/readershistory", "data/" <> sitePath site <> "readershistory")
  , ("", "site/" <> sitePath site <> "static")
  , ("", "site/common/static")
  ]

data ParLabels a b c d e = L1 !a | L2 !b | L3 !c | L4 !d | L5 !e

generateMD5
  :: SiteIdentity
  -> FilePath
  -> IO (Either String Text)
generateMD5 site p = do
  file <- searchSiteFile site $ "static" <> p
  content <- readFile file
  return $ Right $ decodeUtf8 $ Base16.encode $ hash content

app :: SnapletInit MasterApp MasterApp
app = makeSnaplet "master" "Piperka container" Nothing $ do
  cfg <- getSnapletUserConfig
  liftIO $ Data.Configurator.lookup cfg "syslogLevel" >>=
    setlogmask . enumFromTo Emergency . fromMaybe Notice . (readMaybe =<<)
  sitesEnabled <- map read <$>
    (liftIO $ Data.Configurator.lookupDefault ["Piperka"] cfg "sitesEnabled")
  sso <- liftIO $ newIORef mempty
  pUpdate <- liftIO $ (,) <$> newIORef mempty <*> newChan
  tUpdate <- liftIO $ (,) <$> newIORef mempty <*> newChan
  addRoutes [ ("crawlDone/piperka", withTop sitePiperka $ crawlDone pUpdate)
            , ("crawlDone/teksti", withTop siteTeksti $ crawlDone tUpdate)
            , ("refreshUser/:site/:uid", refreshUserUpdates)
            ]
  p <- embedSnaplet "piperka" sitePiperka $ if Piperka `elem` sitesEnabled
    then subApp Piperka sso (snd pUpdate) (fst tUpdate)
    else nullApp
  t <- embedSnaplet "teksti" siteTeksti $ if Teksti `elem` sitesEnabled
    then nameSnaplet "teksti" $ subApp Teksti sso (snd tUpdate) (fst pUpdate)
    else nullApp
  return $ MasterApp p t

nullApp
  :: SnapletInit App App
nullApp = makeSnaplet "piperka" "null piperka" Nothing $
  return $ App undefined undefined undefined undefined undefined
  (const $ const Nothing) (const []) undefined undefined undefined
  (Nothing, False) defaultUserPrefs
  False False Nothing Nothing Nothing Nothing Nothing Nothing
  undefined undefined

subApp
  :: SiteIdentity
  -> IORef (IntMap (SiteIdentity, UserID, UUID))
  -> Chan ()
  -> IORef IntSet
  -> SnapletInit App App
subApp site sso uw otherUpdates = makeSnaplet "piperka" "Piperka application." Nothing $ do
  let primarySearchPath = case site of
        Piperka -> "piperka"
        Teksti -> "teksti"
  let jsFiles =
        [ "/piperka.js"
        , "/qsearch.js"
        , "/moderate.js"
        , "/site.css"
        , "/qsearch.css"
        ]
  cfg <- getSnapletUserConfig
  hostname <- liftIO $ Data.Configurator.lookupDefault "piperka.net" cfg "hostname"
  let hostname' = encodeUtf8 hostname
  when (B.take 3 hostname' == "dev") $ liftIO $
    createDirectoryIfMissing True $ "run/shared/" <> B8.unpack primarySearchPath
  adOptOut <- liftIO $ Data.Configurator.lookupDefault False cfg "adAskOptOut"
  adsDis <- liftIO $ Data.Configurator.lookupDefault False cfg "forceDisableAds"
  conn <- liftIO $ Data.Configurator.lookupDefault "postgresql://piperka@/piperka" cfg "db"
  let dbConfig = DBConfig conn primarySearchPath
  mgr <- liftIO newTlsManager
  ((L1 (elookup, ev)): (L2 tlookup): (L3 tfp): (L3 efp): (L3 efmp): (L3 hp): (L5 hn):jsHash) <- liftIO $
    (map $ either error id) <$> mapConcurrently id
                ([ fmap L1 <$> generateExternal site
                 , fmap L2 <$> generateTag site
                 , fmap L3 <$> generatePart site "tags"
                 , fmap L3 <$> generatePart site "epedias"
                 , fmap L3 <$> generatePart' site "epediasMobi" "epedias"
                 , fmap L3 <$> generatePart site "hiatus"
                 , fmap L5 <$> generateHiatus site
                 ] <> map (\x -> fmap L4 <$> generateMD5 site x) jsFiles)
  vapid <- liftIO . fmap readVAPIDKeys $ VAPIDKeysMinDetails
    <$> Data.Configurator.lookupDefault 0 cfg "vapid.privateNumber"
    <*> Data.Configurator.lookupDefault 0 cfg "vapid.publicCoordX"
    <*> Data.Configurator.lookupDefault 0 cfg "vapid.publicCoordY"
  whs <- liftIO $ fmap encodeUtf8 <$> Data.Configurator.lookup cfg "webhookSecret"
  let initData = AppInit efp efmp ev tfp hp hn vapid hostname site
        otherUpdates sso adOptOut adsDis whs $
        zip (map T.pack jsFiles) $ map (\ ~(L4 x) -> x) jsHash
  let authSettings =  defAuthSettings
        & authCookieLifetime .~ Just (fromInteger 5*365*24*60*60)
  m <- nestSnaplet "messages" messages $
       initCookieSessionManager "site_key.txt" "messages" Nothing (Just 3600)
  let m' = subSnaplet messages
  a <- nestSnaplet "auth" auth $ authInit Nothing $ authSettings
       & authName .~ "auth"
  prov <- liftIO $ getProviders' cfg
  a' <- nestSnaplet "apiAuth" apiAuth $
        authInit (Just $ piperkaOAuth2 m' mgr prov) $ authSettings
        & authName .~ "apiAuth"
  h <- nestSnaplet "" heist $ heistInit' "templates" $
       emptyHeistConfig
       & hcLoadTimeSplices .~ defaultLoadTimeSplices
       & hcNamespace .~ "h"
       & hcCompiledSplices .~ (piperkaSplices initData)
       & hcAttributeSplices .~ piperkaAttrSplices
       & hcTemplateLocations .~ [loadTemplates $ "snaplets/" <> sitePath site
                                 <> "snaplets/heist/site_templates"]
  d <- nestSnaplet "" db $ hasqlInit conn $ Just $ sql $
       "set statement_timeout = 60000; \
       \set search_path to " <> primarySearchPath <> ",public;"
  wrapSite ((sslRedirectHandler hostname') >>)
  ann <- liftIO newChan
  addRoutes =<< (liftIO $ routes hostname' dbConfig vapid initData ann)
  addRoutes $ staticRoutes hostname' site
  addRoutes [("", bracketDbOpen $ authHandler False $
               modifyResponse (setResponseCode 404) >> cRender "404_")]
  ch <- liftIO $ newIORef mempty
  liftIO $ Data.Configurator.lookup cfg "discordSecret" >>=
    maybe (pure ()) (void . sendDiscordEvents site ann)
  recommendEnabled <- liftIO $ Data.Configurator.lookup cfg "recommend.enabled" >>=
    maybe ((/= "<interactive>") <$> getProgName) pure
  rc <- liftIO $ newMVar []
  rcm <- if recommendEnabled then liftIO $ do
    nIter <- Data.Configurator.lookupDefault 10 cfg "recommend.nIterations"
    param <- ALSParams
      <$> Data.Configurator.lookupDefault 0.1 cfg "recommend.lambda"
      <*> Data.Configurator.lookupDefault 10 cfg "recommend.alpha"
      <*> pure 0
      <*> Data.Configurator.lookupDefault 10 cfg "recommend.nFactors"
      <*> Data.Configurator.lookupDefault 200 cfg "recommend.parChunk"
    rcm <- startRecommendProcess conn nIter param (setSearchPath site)
    refresh <- Data.Configurator.lookupDefault 300000000 cfg "recommend.refresh"
    void $ liftIO $ forkIO $ forever $
      threadDelay refresh >> refreshRecommends rcm
    return $ Just rcm
    else return Nothing
  return $ App h a a' d m elookup tlookup mgr ch uw
    (Nothing, False) defaultUserPrefs
    False False Nothing Nothing Nothing Nothing Nothing rcm rc
    ann
