{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module Backend ( oauth2Login, oauth2Check ) where

import Piperka.Listing.Types

import Contravariant.Extras.Contrazip
import Control.Applicative
import Control.Monad (when)
import Control.Monad.Trans
import Control.Monad.Trans.Except
import qualified Data.Binary
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Char (isSpace)
import Data.Int
import Data.Functor
import Data.Functor.Contravariant
import qualified Data.Text as T
import Data.Text (Text)
import Data.UUID
import Snap
import Snap.Snaplet.CustomAuth
import Snap.Snaplet.Hasql
import qualified Hasql.Encoders as EN
import qualified Hasql.Decoders as DE
import Hasql.Session hiding (run, sql)
import Hasql.Statement
import PostgreSQL.ErrorCodes (unique_violation)

import Application
import Piperka.Auth.Types
import qualified Piperka.Decoders as DE
import qualified Piperka.Encoders as EN
import Piperka.Util (getParamText)

encodeLoginPasswd :: EN.Params (T.Text, T.Text)
encodeLoginPasswd =
  contramap fst (EN.param EN.text) <>
  contramap snd (EN.param EN.text)

userRow :: DE.Row UUID -> DE.Row MyData
userRow x =
  MyData
  <$> x
  <*> DE.column DE.uid
  <*> DE.column DE.text
  <*> DE.column DE.uuid
  <*> (DE.nullableColumn DE.int4 <&> \case
          Just 1 -> Just Moderator
          Just 2 -> Just Admin
          _ -> Nothing)
  <*> DE.nullableColumn DE.int4
  <*> DE.column DE.bool
  <*> prefsRow

prefsRow :: DE.Row UserPrefs
prefsRow =
  UserPrefs
  <$> DE.column DE.int4
  <*> (liftA intToColumns (DE.column DE.int4))
  <*> DE.column DE.bool
  <*> DE.column DE.bool

userStatsRow :: DE.Row UUID -> DE.Row (Maybe (Int32, Int32, Int32) -> UserWithStats)
userStatsRow x =
  (\(a,b,c,d,e) -> UserWithStats a (b,c) (d,e))
  <$> (DE.column $ DE.composite ((,,,,)
                                <$> DE.field DE.int4
                                <*> DE.field DE.int4
                                <*> DE.field DE.int4
                                <*> DE.field DE.int4
                                <*> DE.field DE.int4))
  <*> DE.column DE.bool
  <*> DE.column DE.bool
  <*> userRow x

userDefaultRow :: Text -> DE.Row MyData
userDefaultRow n =
  MyData
  <$> DE.column DE.uuid
  <*> DE.column DE.uid
  <*> pure n
  <*> DE.column DE.uuid
  <*> pure Nothing
  <*> pure Nothing
  <*> pure False
  <*> pure defaultUserPrefs

statsDefaultRow :: Text -> DE.Row UserWithStats
statsDefaultRow n =
  defaultUserStats
  <$> userDefaultRow n

doLogout
  :: (MonadIO m, HasHasql m)
  => Text
  -> m ()
doLogout t = do
  let tokenUuid = fromText t
  case tokenUuid of
   Nothing -> return ()  -- Don't know what to do in this case
   Just t' -> do
     run $ statement t' deleteSession
     return ()
  where
    deleteSession = Statement sql (EN.param EN.uuid) DE.unit True
    sql = "delete from p_session where ses=$1"

doRecover
  :: (MonadIO m, HasHasql m)
  => Text
  -> (UUID -> Session (Maybe u))
  -> m (Either (AuthFailure QueryError) u)
doRecover t sess = do
  let tokenUuid = fromText t
  case tokenUuid of
    Nothing -> return $ Left $ Login NoSession
    Just token -> do
      usr <- run $ sess token
      return $ either (Left . UserError)
        (maybe (Left $ Login SessionRecoverFail) Right) usr

doPrepare
  :: (HasHasql m, MonadIO m, HasUserID u)
  => Maybe u
  -> Text
  -> m (Either QueryError AuthID)
doPrepare u p = run $ statement (p, extractUid <$> u) stmt
  where
    stmt = Statement sql encode decode True
    encode = contrazip2 (EN.param EN.text) (EN.nullableParam EN.uid)
    decode = DE.singleRow $ DE.column DE.authid
    sql = "select auth_create_password($1, $2)"

doCancelPrepare
  :: (HasHasql m, MonadIO m)
  => AuthID
  -> m ()
doCancelPrepare u = (run $ statement u stmt) >> return ()
  where
    stmt = Statement sql encode DE.unit True
    encode = EN.param EN.authid
    sql = "delete from login_method where lmid=$1"

validName
  :: Text
  -> Bool
validName name = len > 1 && len < 40 && isTrimmed
  where
    len = T.length name
    isTrimmed = not (isSpace (T.head name) || isSpace (T.last name))

doCreate
  :: (HasHasql m, MonadIO m, MonadSnap m)
  => Text
  -> AuthID
  -> DE.Result b
  -> m (Either (Either QueryError CreateFailure) b)
doCreate u loginId decode = runExceptT $ do
  email <- lift $ getParamText "email"
  when (not $ validName u) $ throwE $ Right InvalidName
  usr <- lift $ run $ statement (u, email, loginId) stmt
  either (throwE . maybeDuplicate) return usr
    where
      stmt = Statement sql encode decode True
      encode = contrazip3 (EN.param EN.text) (EN.nullableParam EN.text) (EN.param EN.authid)
      sql = "select p_session, uid, csrf_ham from auth_create($1, $2, $3)"
      maybeDuplicate e =
        if isDuplicateSqlError e then Right DuplicateName else Left e

attach
  :: (HasHasql m, MonadIO m, HasUserID u)
  => u
  -> AuthID
  -> m (Either QueryError ())
attach u i =  run $ statement (extractUid u, i) stmt
  where
    stmt = Statement sql encode DE.unit True
    encode = contrazip2 (EN.param EN.uid) (EN.param EN.authid)
    sql = "insert into login_method (uid, lmid) values ($1, $2)"

isDuplicateSqlError
  :: QueryError
  -> Bool
isDuplicateSqlError (QueryError _ _ (ResultError (ServerError c _ _ _))) = c == unique_violation
isDuplicateSqlError _ = False

maybeModStats
  :: Maybe (Maybe (Int32, Int32, Int32) -> UserWithStats)
  -> Session (Maybe UserWithStats)
maybeModStats usr = do
  let usr' = fmap ($ Nothing) usr
      getModStats = flip statement $ Statement sql (EN.param EN.uid)
        (DE.singleRow $
         (,,)
         <$> DE.column DE.int4
         <*> DE.column DE.int4
         <*> DE.column DE.int4
        ) True
  maybe (return usr') (\(f, u) -> Just . f . Just <$> getModStats u) $
    usr' >>= moderator . user >> (,) <$> usr <*> (uid . user <$> usr')
  where
    sql = "SELECT (SELECT count(*) FROM ONLY user_edit JOIN comics USING (cid)), \
          \coalesce((SELECT extract(days FROM now() - last_moderate) \
          \ FROM moderator WHERE uid=$1), 0)::int, \
          \ (SELECT count(*) FROM ticket WHERE tid NOT IN (SELECT tid FROM ticket_resolve))"

commonUserParams
  :: ByteString
commonUserParams = "  uid\
             \, name\
             \, csrf_ham\
             \, level\
             \, auid\
             \, last_update_disclaimer \
             \, display_rows\
             \, display_columns\
             \, new_windows \
             \, sticky_show_hidden \
             \"

-- For use with partial HTML render and AJAX calls
instance IAuthBackend MyData AuthID QueryError App where
  preparePasswordCreate = doPrepare
  cancelPrepare = doCancelPrepare
  create u i = doCreate u i $ DE.singleRow $ userDefaultRow u
  attachLoginMethod = attach
  login u pwd = run $
    statement (u, pwd) $ Statement sql
    encodeLoginPasswd
    (DE.rowMaybe . userRow $ DE.column DE.uuid) True
    where
      sql = "select p_session, " <> commonUserParams <>
            "from auth_login($1, $2) join users using (uid) \
            \left join moderator using (uid)"
  logout = doLogout
  recover t = doRecover t $ \u -> statement u $ Statement sql
    (EN.param EN.uuid)
    (DE.rowMaybe $ userRow $ pure u) True
    where
      sql = "select " <> commonUserParams <>
            "from recover_session($1) join users using (uid) \
            \left join moderator using (uid)"
  getUserId = return . toStrict . Data.Binary.encode . uid
  isDuplicateError = return . isDuplicateSqlError

-- Side effect: updates last read stats on login/recover
instance IAuthBackend UserWithStats AuthID QueryError App where
  preparePasswordCreate = doPrepare
  cancelPrepare = doCancelPrepare
  create u i = doCreate u i $ DE.singleRow $ statsDefaultRow u
  attachLoginMethod = attach
  login u pwd = run $
    (statement (u, pwd) $ Statement sql
     encodeLoginPasswd
     (DE.rowMaybe . userStatsRow $ DE.column DE.uuid) True) >>= maybeModStats
      where
        sql = "select (select (new_comics, total_new, new_in, total_new_hide, new_in_hide) from \
              \get_and_update_stats(uid, true)), hold_bookmark, offset_bookmark_by_one, \
              \p_session, " <> commonUserParams <>
              "from auth_login($1, $2) join users using (uid) left join \
              \moderator using (uid)"
  logout = doLogout
  recover t = doRecover t $ \u ->
    (statement u $ Statement sql
     (EN.param EN.uuid)
     (DE.rowMaybe . userStatsRow $ pure u) True) >>= maybeModStats
      where
        sql = "select (select (new_comics, total_new, new_in, total_new_hide, new_in_hide) from \
              \get_and_update_stats(uid, false)), hold_bookmark, offset_bookmark_by_one, "
              <> commonUserParams <>
              "from recover_session($1) join users using (uid) left join \
              \moderator using (uid)"
  getUserId = return . toStrict . Data.Binary.encode . uid . user
  isDuplicateError = return . isDuplicateSqlError

oauth2Login
  :: (HasHasql m, MonadIO m)
  => ProviderID
  -> Text
  -> m (Either QueryError (Maybe MyData))
oauth2Login provider token = do
  run $ statement (provider, token) stmt
  where
    stmt = Statement sql encode decode True
    encode = contrazip2 (EN.param EN.opid) (EN.param EN.text)
    decode = DE.rowMaybe $ userRow (DE.column DE.uuid)
    sql = "select p_session, " <> commonUserParams <>
          "from auth_oauth2($1, $2) join users using (uid) \
          \left join moderator using (uid)"

oauth2Check
  :: (HasHasql m, MonadIO m)
  => ProviderID
  -> Text
  -> m (Either QueryError (Maybe ByteString))
oauth2Check provider token = do
  run $ statement (provider, token) stmt
  where
    stmt = Statement sql encode decode True
    encode = contrazip2 (EN.param EN.opid) (EN.param EN.text)
    decode = DE.rowMaybe $ (toStrict . Data.Binary.encode <$> DE.column DE.int4)
    sql = "select uid from login_method_oauth2 where \
          \uid is not null and opid = $1 and identification = $2"

instance UserData MyData where
  extractUser MyData{..} = AuthUser
    { name = uname
    , session = toASCIIBytes usession
    , csrfToken = toASCIIBytes ucsrfToken
    }

instance UserData UserWithStats where
  extractUser UserWithStats{..} = AuthUser
    { name = uname user
    , session = toASCIIBytes $ usession user
    , csrfToken = toASCIIBytes $ ucsrfToken user
    }

class HasUserID u where
  extractUid :: u -> UserID

instance HasUserID MyData where
  extractUid u = uid u

instance HasUserID UserWithStats where
  extractUid u = uid $ user u
