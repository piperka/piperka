<h:piperka title="Author comic page" prefetch="Info" ads="False">
  <h:withCid>
    <h:exists>
      <h:ifLoggedIn>
	<h:isCidAuthor>
	  <h:authorCidAction>
	    <h:titleUpdate>
	      Title updated.
	    </h:titleUpdate>
	    <h:crawlScheduled>
	      Crawl scheduled.
	    </h:crawlScheduled>
	  </h:authorCidAction>
	  <h2>Author page for <h:title/></h2>
	  <h3>Change title</h3>
	  <p>
	    <h:csrfForm method="post">
	      <input type="hidden" name="cid" value="${h:cid}"/>
	      Title: <input type="text" name="newTitle" value="${h:title}" pattern="^\S.*\S$" maxlength="100"/>
	      <button type="submit">Submit</button>
	    </h:csrfForm>
	    Other displayed data about the <h:comicWord/> is editable
	    on the
	    <a href="edit_info.html?cid=${h:cid}">edit info</a> page.
	    Please read the <a href="moderator_policy.html">moderator
	    policy</a> first (it's not long).
	  </p>
	  <h3>Crawler</h3>
	  <p>
	    <h:siteName/> defaults to roughly daily checks on a site if it's
	    been long silent.
	  </p>
	  <h:csrfForm method="post">
	    <input type="hidden" name="cid" value="${h:cid}"/>
	    <input type="hidden" name="crawlNow" value="1"/>
	    <button type="submit">
	      Schedule a crawl on next hour
	    </button>
	  </h:csrfForm>
	</h:isCidAuthor>
      </h:ifLoggedIn>
    </h:exists>
  </h:withCid>
</h:piperka>
