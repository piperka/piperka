<h:piperka minifiable="True">
  <h:ifLoggedOut>
    <h2>Welcome.</h2>
    <p>
      This is a <h:comicWord web="True"/> tracking and bookmarking
      service.
    </p>
    <h:site site="Piperka">
      <p>
	You might find this site useful if you like to read many
	<h:comicWord web="True"/>s, like <h:kaolSubs/> as your humble
	web master does.
      </p>
    </h:site>
    <h:mobile>
      <h:autoAd>
	<div id="adbox">
	  <h:adCode id="mobile"/>
	</div>
      </h:autoAd>
      <h:adAskOptOut>
	<h:autoAd enabled="Nothing">
	  <p class="ads-question script show">
	    Disable <a href="ad_settings.html">ads</a>?<br/>
	    <button name="ads" value="0">Yes please</button>
	    <button name="ads" value="1">I don't mind them</button>
	  </p>
	</h:autoAd>
    </h:adAskOptOut>
    </h:mobile>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <h:ifMinimal>
      <h:explicitStats>
	<span class="scripthidden" id="newin"><h:unread/></span>
	<span class="scripthidden" id="newcomics"><h:newComics/></span>
      </h:explicitStats>
    </h:ifMinimal>
    <h:mobile>
      <h:adAskOptOut>
	<h:autoAd enabled="Nothing">
	  <p class="ads-question script show">
	    Disable <a href="ad_settings.html">ads</a>?<br/>
	    <button name="ads" value="0">Yes please</button>
	    <button name="ads" value="1">I don't mind them</button>
	  </p>
	</h:autoAd>
      </h:adAskOptOut>
    </h:mobile>
    <span id="page-controls">
      <h2>Set bookmark</h2>
      <h:ifMod>
	<h:onlyWithStats>
	  <h:modStats>
	    <h:haveModCount>
	      <h:nag>
	        <p>
	          It's <h:modDays/> days since you last
	          <a href="/moderate.html">moderated</a>. Pretty please?
		</p>
	      </h:nag>
	    </h:haveModCount>
	  </h:modStats>
	</h:onlyWithStats>
      </h:ifMod>
      <form method="post" action="/">
	<input type="hidden" name="csrf_ham" value="${h:csrf}"/>
	<p id="bookmark-line">URL: <input type="text" name="bookmark"/>
	  <button name="action" value="set_bookmark">Set</button>
	  <label for="wantbookmarkhere">Set bookmark here</label>
	  <input type="checkbox" id="wantbookmarkhere" name="wantbookmarkhere"/>
	  <h:site site="Piperka">
	    <a href="help.html#bookmarkhere" rel="help">?</a>
	  </h:site>
	  <a href="bookmarklet.html">Get a bookmarklet</a>
	</p>
      </form>
      <p>
	<a href="revert.html">Revert</a> updates.
	<span class="script">
	  |
	  <label for="openinreader">Open <h:comicWord/> in <h:siteName/> Reader</label>
	  <input type="checkbox" id="openinreader"/>
	  | <a href="recommend.html">View <h:siteName/>'s recommendations</a>
	  <span id="updatewatch" class="scripthidden">
	    |
	    <label for="updatewatch-toggle">Watch updates</label>
	    <input type="checkbox" id="updatewatch-toggle"/>
	    <span class="scripthidden" id="update-push-container">
	      |
	      <label for="update-push-toggle">Notifications</label>
	      <input type="checkbox" id="update-push-toggle" data-pushkey="${h:vapidPublic}"/>
	    </span>
	  </span>
	  <div class="script" id="watch-messages">
	    <span class="updatewatch-message" id="updatewatch-msg-upd">Updating...</span>
	    <span class="updatewatch-message" id="updatewatch-msg-reconnecting">Reconnecting...</span>
	    <span class="updatewatch-message error" id="updatewatch-msg-connfailed">
	      Watch failed.  Toggle to manually try reconnecting or
	      just reload the page.
	    </span>
	    <span class="updatewatch-message" id="updatewatch-msg-custom"/>
	  </div>
	</span>
      </p>
    </span>
    <p class="script show">
      <button id="unreadInNewTabs">Open in tabs</button>

      every <h:comicWord/> with at most

      <input type="number" id="minNewTab" value="5"/>

      unread pages.
      <span class="script" id="unreadTabOpenResult">Opened <span/> tab(s).</span>
    </p>
    <h2>Updated <h:comicWord/>s</h2>
    <h:updateStatus/>
    <span id="updates">
      <h:listing mode="Update" />
    </span>
    <h:mobile>
      <h:autoAd enabled="Just True">
	<div id="adbox">
	  <h:adCode id="mobile"/>
	</div>
      </h:autoAd>
    </h:mobile>
    <h:othersiteUpdates>
      <div id="othersite-updates">
	You have new unread pages on <h:otherSiteSSO/>.
      </div>
    </h:othersiteUpdates>
    <div>
      Inactive bookmarks are hidden from the default view.

      <a href="?show_hidden=1">Show all</a>.
    </div>
    <span id="pre-expected-updates"/>
    <table id="expected-updates">
      <tr>
	<th colspan="2">
	  Expected updates
	</th>
      </tr>
      <h:upcomingSchedule>
	<tr>
	  <td class="hour">
	    <h:pastDue>
	      <h:hours/> hours past due
	    </h:pastDue>
	    <h:pastDue1>
	      1 hour past due
	    </h:pastDue1>
	    <h:due0>
	      In &lt; 1 hour
	    </h:due0>
	    <h:due1>
	      In 1 hour
	    </h:due1>
	    <h:due>
	      In <h:hours/> hours
	    </h:due>
	  </td>
	  <td class="commaseparated">
	    <h:upcomingComics>
	      <a href="info.html?cid=${h:cid}"><h:title/></a>
	    </h:upcomingComics>
	  </td>
	</tr>
      </h:upcomingSchedule>
    </table>
  </h:ifLoggedIn>
</h:piperka>
