<div id="${h:id}" class="scripthidden show">
  <form>
    <label for="qinput">Quick search: </label><input id="qinput" type="input"/><button type="submit">Go!</button>
    <input type="hidden" name="sorttype" value="${h:sort}"/>
    <label for="showallresults">Show all results </label><input type="checkbox" id="showallresults"/>
  </form>
</div>
