<div id="welcome" title="About ${h:siteName} Reader">
  <h2><h:siteName/> Reader</h2>
  <p>
    <h:siteName/> Reader uses <h:siteName/>'s <h:comicWord/> archive
    to display their archives in an embedded form.  It offers a
    unified navigation interface, preloads subsequent pages when
    reading them in order and allows updating bookmarks on
    <h:siteName/> directly.
  </p>
  <p>
    This feature is still under development.  Hopefully the buttons on
    the Reader bar are self-explanatory enough.  The Reader won't know
    it if you navigate away within the embedded <h:comicWord/>.  More
    options for controlling how the content is displayed are available
    from a menu available in the top left corner.
  </p>
  <p id="nocomicselected">
    To try it out, you'll need to select a <h:comicWord/>.  Go back to
    the <a href="https://${h:hostname}/browse.html">main site</a> and
    select Reader from a <h:comicWord/>'s info page.
  </p>
  <p>
    Click "Archive" to see a list of <h:comicWord/>'s pages.  Clicking
    a row there takes you that page.
  </p>
  <h3>Actions as logged in user</h3>
  <p class="user">Hello, <span id="namehere"/>.</p>
  <p>
    Select a <h:comicWord/> by clicking "My <h:comicWord
    capital="True"/>s" button.  You can move your bookmark in the
    archive dialog by clicking an already selected row.  The "Next"
    button automatically updates the bookmark if the "autoupdate"
    option is selected.
  </p>
</div>
<div class="script" id="userreq-failed">
  <h2>Authorization failed</h2>
  <p>
    Looks like you're logged in but accessing your user data failed.
    Please check that third party cookies are enabled and try again.
  </p>
  <p>
    A bit longer explanation on what's going on: Browsers generally
    disallow embedding insecure content in a secure context.  That's
    why Reader uses HTTP as its protocol instead of HTTPS as the rest
    of the site does, to avoid browsers' mixed content protections.
    But that leads to another browser security policy, third party
    cookies.  Accessing your bookmark data with HTTPS is considered
    such use.  <h:siteName/> with HTTP is a different thing from
    <h:siteName/> with HTTPS, as far as that's considered.
  </p>
</div>
