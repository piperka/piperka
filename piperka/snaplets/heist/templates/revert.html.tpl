<h:piperka title="Revert" minifiable="True">
  <h2>Revert updates</h2>
  <h:ifLoggedIn>
    <h:siteName/> can't keep track of whether our redirects were
    successful.  It keeps records of your last redirects for
    <h:comicWord/>s.
    <h:listing mode="Revert" action="/">
      <after>
	<input type="submit" name="do" value="Revert"/>
      </after>
    </h:listing>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    This page would list your recently used redirects and allow you to
    revert them if you were logged on.
  </h:ifLoggedOut>
</h:piperka>
