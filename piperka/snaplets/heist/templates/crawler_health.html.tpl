<h:piperka ads="False" title="Crawler health check">
  <h:ifMod>
    <h2>Crawler health check</h2>
    <h3>Persistent crawl errors</h3>
    <table>
      <thead>
	<tr>
	  <th>CID</th>
	  <th>Title</th>
	  <th>Origin</th>
	  <th>Error</th>
	  <th>Last success</th>
	</tr>
      </thead>
      <tbody>
	<h:healthCrawlerFailures>
	  <tr>
	    <td><h:cid/></td>
	    <td><a href="info.html?cid=${h:cid}"><h:title/></a></td>
	    <td>
	      <h:hasOrigin>
		<a title="${h:origin}" href="${h:origin}" target="origin"><h:originText/></a>
	      </h:hasOrigin>
	    </td>
	    <td title="${h:error}"><h:errorAbbrev/></td>
	    <h:hasLastSuccess>
	      <td><h:lastSuccess/></td>
	    </h:hasLastSuccess>
	  </tr>
	</h:healthCrawlerFailures>
      </tbody>
    </table>
  </h:ifMod>
  <extra>
    <h:ifMod>
      <h3>Initial redirects</h3>
      <table id="base-redirects">
	<thead>
	  <tr>
	    <th>CID</th>
	    <th>Autofix</th>
	    <th>Title</th>
	    <th>base</th>
	    <th>target</th>
	    <th>redirect</th>
	  </tr>
	</thead>
	<tbody>
	  <h:healthBaseRedirects>
	    <tr>
	      <td><h:cid/></td>
	      <td>
		<h:ifMod level="Admin">
		  <h:ifAutoFix>
		    <button data-cid="${h:cid}">Autofix</button>
		  </h:ifAutoFix>
		  <a href="mod_crawler.html?cid=${h:cid}&firstPage=${h:redirectEnc}" target="crawler">crawler</a>
		  <a href="mod_morgue.html?cid=${h:cid}" target="_blank">morgue</a>
		  <span class="message"/>
		  <span class="error"/>
		</h:ifMod>
	      </td>
	      <td><h:title/></td>
	      <td><h:base/></td>
	      <td><h:target/></td>
	      <td><a h:redirectHref=""><h:redirect/></a></td>
	    </tr>
	  </h:healthBaseRedirects>
	</tbody>
      </table>
    </h:ifMod>
  </extra>
</h:piperka>
