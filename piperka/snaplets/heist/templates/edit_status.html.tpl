<h:piperka ads="False" title="Edit ${h:comicWord} hiatus/completion status">
  <h:withCidTitle>
    <h2>Hiatus/completion status for <h:title/></h2>
    <p>
      Please read <a href="status_explanation.html">this page</a>
      first.  Edits on this page won't change the <h:comicWord/>'s
      status directly but will submit edits to the moderators' queue.
    </p>
    <h:ifMod>
      <p>
	Wouldn't you rather use
	the <a href="mod_status.html?cid=${h:cid}">moderator
	version</a> of this page?
      </p>
    </h:ifMod>
    <form id="user_hiatus" method="POST" data-cid="${h:cid}">
      <input type="hidden" name="hiatus_user_edit" value="1"/>
      <p>
	<select name="status">
	  <option value="" disabled selected></option>
	  <h:hiatusClassifications>
	    <option value="${h:id}"><h:name/></option>
	  </h:hiatusClassifications>
	</select>
      </p>
      <p>
	<label for="hiatus_description">
	  Comment (hiatus statuses are supposed to be simple things
	  so leaving this empty is ok but if you'd like to use a few
	  words to give a reason to the mods then this is for it)
	</label>
	<input type="text" id="hiatus_description" name="reason" maxlength="100"/>
      </p>
      <button type="submit" disabled>Submit</button>
      <noscript>
	<p>
	  Using this form requires JavaScript.  Just as a basic bot
	  preventition measure.  Surely you aren't one?
	</p>
      </noscript>
    </form>
  </h:withCidTitle>
</h:piperka>
