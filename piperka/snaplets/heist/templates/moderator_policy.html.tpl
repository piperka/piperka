<h:piperka ads="False" title="Moderator policy">
  <h2>Moderator policy</h2>
  <p>
    This is a policy document for moderators.  Brief and simple.
  </p>
  <p>
    The <h:comicWord/> descriptions and tags are meant to give short
    introduction for <h:comicWord/>s.  If you think they can be
    improved on then feel free to.
  </p>
  <p>
    <h:comicWord capital="True"/> listed on <h:siteName/> may be
    mature.  Some very much so.  But descriptions and banners on
    <h:siteName/> itself should remain SFW.
  </p>
  <p>
    Some <h:comicWord/> blurbs on the authors' sites may be written
    using first person nouns.  It's fine for their use but I'd advise
    against copying them as is to <h:siteName/>.  It's not like I'm
    drawing their comics.
  </p>
  <h:ifMod>
    <p>
      Moderators' <h:comicWord/> edits
      are <a href="edit_history.html">logged</a>.  If you feel like
      not volunteering for moderating anymore then please let me know.
    </p>
  </h:ifMod>
</h:piperka>
