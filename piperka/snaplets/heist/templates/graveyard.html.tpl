<h:piperka title="Removed comics" minifiable="True">
  <h2>Removed comics</h2>
  <p>
    Domains expire, <h:comicWord/>s get taken offline or are made
    impossible to index.  Such <h:comicWord/>s end up listed on this
    page.
    <h:ifLoggedIn>
      For a list of formerly listed <h:comicWord/>s that you were
      subscribed to, see the <a href="/my_removed.html">My removed
      <h:comicWord/>s</a> page.
    </h:ifLoggedIn>
  </p>
  <h:listing mode="Graveyard">
    <h:hilightButton/>
  </h:listing>
</h:piperka>
