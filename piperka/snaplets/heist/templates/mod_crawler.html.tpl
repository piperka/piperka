<h:piperka ads="False">
  <h:notMod level="Admin">
    Admin only page.
  </h:notMod>
  <h:ifLoggedIn>
    <h:ifMod level="Admin">
      <h2>Crawler control</h2>
      <a id="genentry">genentry</a>
      <h:crawlerControl>
	<form id="crawler_config">
	  <p>
	    <label for="cid">cid</label>
	    <input type="number" name="cid" id="cid" value="${h:cid}"/>
	    <label for="parser_type">parser type</label>
	    <input type="number" name="parser_type" id="parser_type" value="${h:parserType}"/>
	    <button type="button" id="auto_discover" disabled>AutoDiscover</button>
	    <span id="parser_candidates"/>
	  </p>
	  <p>
	    <label for="auto_url">auto url</label>
	    <input type="text" name="auto_url" id="auto_url" style="width: 60em"/>
	    <button type="button" id="auto_url_act">AutoURL</button>
	    <button type="button" id="gocomics_helper">GoComics helper</button>
	  </p>
	  <p>
	    <label for="url_base">url_base</label>
	    <input type="text" name="url_base" id="url_base" style="width: 20em" value="${h:urlBase}"/>
	    <label for="url_tail">url_tail</label>
	    <input type="text" name="url_tail" id="url_tail" value="${h:urlTail}"/>
	    <label for="homepage">homepage</label>
	    <input type="text" name="homepage" id="homepage" value="${h:homepage}"/>
	    <label for="fixed_head">fixed_head</label>
	    <input type="text" name="fixed_head" id="fixed_head" value="${h:fixedHead}"/>
	  </p>
	  <p>
	    <label for="extra_url" style="width: 40m">extra_url</label>
	    <input type="text" name="extra_url" id="extra_url" value="${h:extraURL}"/>
	    <label for="extra_data">extra_data</label>
	    <input type="text" name="extra_data" id="extra_data" value="${h:extraData}"/>
	    <label for="crawl_delay">Delay</label>
	    <input type="number" step="0.1" min="0" value="0" id="crawl_delay" name="delay"/>
	  </p>
	  <p>
	    Linear updates <label for="lnrStart">from</label>
	    <input type="number" id="lnrStart" name="lnrStart" value="1"/>
	    <label for="lnrEnd">to</label>
	    <input type="number" id="lnrEnd" name="lnrEnd"/>
	    <label for="lnrPad">pad</label>
	    <input type="number" id="lnrPad" name="lnrPad"/>
	    <button type="button" id="lnrAdd">Add linear</button>
	  </p>
	  <p>
	    <label for="insert_page">insert page to end</label>
	    <input type="text" name="insert_page" id="insert_page" style="width: 60em"/>
	    <button type="button" id="insert_page_act">Insert</button>
	  <p>
	    <button type="button" id="start" h:sourceMode="False">Start</button>
	    <button type="button" id="halt" disabled>Halt</button>
	    <button type="button" id="clear">Clear</button>
	    <button type="button" id="save" h:sourceMode="True">Save</button>
	  </p>
	  <h:sourceMode>
	    <input id="source" type="hidden" name="source" value="${h:source}"/>
	  </h:sourceMode>
	  <span id="msg"/>
	</form>
      </h:crawlerControl>
    </h:ifMod>
  </h:ifLoggedIn>
  <extra>
    <h:ifMod level="Admin">
      <ul id="menu">
	<li id="menu-remove"><div>Remove</div></li>
	<li id="menu-crop"><div>Crop</div></li>
      </ul>
      <h:crawlerArchive>
	<div id="crawler_archives" class="nowrap">
	  <div style="float: left; width: 40%">
	    <table style="width: 100%">
	      <thead>
		<tr>
		  <td colspan="3">Current</td>
		</tr>
		<tr>
		  <th scope="col">ord</th>
		  <th scope="col">page</th>
		  <th scope="col">bookm</th>
		  <th scope="col">tgt</th>
		</tr>
	      </thead>
	      <tbody id="existing_archive">
		<h:archiveRows>
		  <tr h:id="src-" h:archiveClass="">
		    <td><h:ord/></td>
		    <td>
		      <h:archiveLink>
			<a href="${h:href}" target="archive_page"><h:name/></a>
		      </h:archiveLink>
		    </td>
		    <h:ifBookmarks>
		      <td class="bookmark_handle"><h:bookmarks/></td>
		      <td/>
		    </h:ifBookmarks>
		  </tr>
		</h:archiveRows>
	      </tbody>
	    </table>
	  </div>
	  <canvas id="bookmark_transfers" width="150" height="150" style="float: left"></canvas>
	  <div style="float: left; width: 35%">
	    <table style="width: 100%">
	      <thead>
		<tr>
		  <td colspan="2">Crawl</td>
		</tr>
		<tr>
		  <th scope="col">ord</th>
		  <th scope="col">page</th>
		</tr>
	      </thead>
	      <tbody id="crawl_results">
		<h:archiveRows source="True">
		  <h:archiveLink>
		    <tr h:id="tgt-" class="existingarchive bookmark_drag">
		      <td><h:ord/></td>
		      <td><a href="${h:href}" target="archive_page"><h:name/></a></td>
		    </tr>
		  </h:archiveLink>
		</h:archiveRows>
	      </tbody>
	    </table>
	    <span id="tgt-current">
	      <div class="current bookmark_drag" h:currentId="">current</div>
	    </span>
	  </div>
	</div>
      </h:crawlerArchive>
    </h:ifMod>
  </extra>
</h:piperka>
