<h:piperka title="Edit history">
  <h:submit mode="History">
    <h:ifLoggedIn>
      <h:ifMod>
	<h2>Edit history</h2>
	<table id="user-edits">
	  <thead>
	    <tr>
	      <th>sid</th>
	      <th>Comic</th>
	      <th>Stamp</th>
	      <th>From IP</th>
	      <th>User</th>
	    </tr>
	  </thead>
	  <tbody>
	    <h:listOfEdits mode="HistoryList">
	      <tr id="${h:sidId}">
		<td><h:sid/></td>
		<td class="cid" id="${h:cidId}"><h:title/></td>
		<td><h:addedOn/></td>
		<td><h:fromIP/></td>
		<td><h:name/></td>
	      </tr>
	    </h:listOfEdits>
	  </tbody>
	</table>
	<h2>Current Entry</h2>
	<div id="current-entry"/>
	<h:csrfForm class="submitcomic script" id="editinfo">
	  <input type="hidden" name="formtype" value="editinfo"/>
	  <input type="hidden" name="user_sid" value=""/>
	  <input type="hidden" name="cid" value=""/>
	  Title: <a id="info-title"></a>
	  <p>
	    CID: <span id="info-cid"/>
	  </p>
	  <div style="height: 60px" id="useredit-banner"/>
	  <h:submitForm/>
	</h:csrfForm>
      </h:ifMod>
    </h:ifLoggedIn>
    <h:notMod>
      <h2>Nothing to see</h2>
      <p>
	Moderator privilege required to view this page.
      </p>
    </h:notMod>
  </h:submit>
</h:piperka>
