<h:piperka title="Welcome">
  <h2>Welcome to Piperka</h2>
  <p>
    You have successfully created an account.  To get started, use the
    <h:mobile mode="Desktop">
      links on the side bar
    </h:mobile>
    <h:mobile>
      menu
    </h:mobile>
    to find <h:comicWord web="True"/> and subscribe to them to follow
    updates.
  </p>
  <h3>App</h3>
  <h:site site="Piperka">
    <p>
      If you're looking for other ways to use Piperka, it's got an app
      as well.
    </p>
    <span id="google-badge">
      <a href='https://play.google.com/store/apps/details?id=net.piperka.client&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='/images/google-play-badge.png'/></a>
    </span>
    <p class="legalese">
      Google Play and the Google Play logo are trademarks of Google LLC.
    </p>
  </h:site>
</h:piperka>
