<h:piperka title="Claim comic" prefetch="Info" ads="False">
  <h:withCid>
    <h:exists>
      <h2>Claim <h:title/></h2>
      <h:ifLoggedIn>
	<h:authorClaim>
	  <h:alreadyClaimed>
	    You have already claimed this <h:comicWord/>.
	  </h:alreadyClaimed>
	  <h:mayClaim>
	    <p>
	      Claiming <h:comicWord/> is a semi-manual process at this
	      moment.  Please include the following code on your site
	      somewhere where it's clearly not on a user editable
	      portion of the site.  Like as a separate file or in the
	      head section or, if nothing else, in a comment by an
	      account that's marked as admin or author.  Then let me
	      know your user name and where you put it on your site
	      and I'll go and verify it myself.
	    </p>
	    <textarea rows="2" cols="100" readonly><h:claimCode/></textarea>
	  </h:mayClaim>
	</h:authorClaim>
	<p>
	  I'd be grateful if you'd link back to <h:siteName/>, as well.
	  <h:site site="Piperka">
	    I have a few button images available, feel free to change
	    them as you like to better fit your site.
	  </h:site>
	</p>
	<h:site site="Piperka">
	  <div id="piperka-buttons">
	    <img src="/images/banner-117x30.png"/>
	    <img src="/images/banner-88x31.png"/>
	    <img src="/images/banner-200x40.png"/>
	  </div>
	</h:site>
      </h:ifLoggedIn>
      <h:ifLoggedOut>
	Claiming a <h:comicWord/> requires a user account on <h:siteName/>.
	Please log in or create an account.
      </h:ifLoggedOut>
      <p>
	As <h:comicWord web="True"/>s may be collaborative efforts,
	multiple user accounts may be tied to a single <h:comicWord/>.
      </p>
    </h:exists>
  </h:withCid>
</h:piperka>
