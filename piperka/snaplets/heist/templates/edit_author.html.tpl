<h:piperka>
  <h2>Edit author</h2>
  <h:editAuthor>
    <div id="editAuthor">
      <h:csrfForm id="editAuthorForm" method="POST">
	<h:hasId>
	  <input type="hidden" name="auid" value="${h:auid}"/>
	</h:hasId>
	<h:noId>
	  <input type="hidden" name="auid"/>
	</h:noId>
	<div>
	  Name: <input type="text" name="name" value="${h:name}"/>
	</div>
	<table>
	  <thead>
	    <tr>
	      <th scope="col">CID</th>
	      <th scope="col">Title</th>
	    </tr>
	  </thead>
	  <tbody id="comics">
	    <h:comics>
	      <tr data-cid="${h:cid}" h:class="graveyard">
		<td><h:cid/><input type="hidden" name="cid" value="${h:cid}"/></td>
		<td><a href="info.html?cid=${h:cid}" target="_blank"><h:title/></a></td>
		<td><button type="button" class="delete">Delete</button></td>
	      </tr>
	    </h:comics>
	  </tbody>
	</table>
	<div>
	  <h:ifMod>
	    <button type="submit">
	      <h:hasId>
		Update
	      </h:hasId>
	      <h:noId>
		Create
	      </h:noId>
	    </button>
	    <span class="error" id="error"/>
	  </h:ifMod>
	  <h:notMod forbid="False">
	    For now, edits to author data are a moderator action only.
	  </h:notMod>
	</div>
      </h:csrfForm>
      <h:qSearch/>
    </div>
  </h:editAuthor>
</h:piperka>
