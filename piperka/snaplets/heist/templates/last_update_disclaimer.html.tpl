<h:piperka title="Disclaimer: last update">
  <h2>About last update dates</h2>
  <p>
    Knowing when a <h:comicWord/> has last updated is a nice thing to
    know.  Alas, <h:siteName/> doesn't, as such, have that data.
    Instead, it has the date when the crawler last found a new page.
    Often, those two dates are very close to each other if not the
    same, but not always.  Which is why I'm not showing them as a
    default.  There might be any number of reasons why the time stamp
    would get an update on <h:siteName/>'s end alone, it's just not
    necessarily an accurate measure of when a site has updated.
  </p>
  <p>
    If I just showed a date, people would simply take it as the last
    update date for an archive, and if <h:siteName/> showed something
    that didn't match with the site's state, then people would just
    conclude that <h:siteName/> is broken.  I just don't want people
    to be confused about what the data I'm showing represents.  That's
    why I'm making you read this disclaimer before seeing them.
  </p>
  <p>
    Piperka has a last update date stored but they're maintained on a
    best effort basis and may or may not reflect what you'd expect.
    Ideally, sites would have archive page creation dates in a nice
    machine readable format universally available and I could just
    show that.  But they don't really do that.
  </p>
  <p>
    Please, don't send me manual updates for the last update dates.
  </p>
  <h:ifLoggedOut>
    <p>
      Viewing last update dates is restricted to logged in users.
    </p>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <h:lastUpdateDisclaimer maySet="True">
      <h:setFailed>
	<p>
	  Did you check the checkbox?  Please try again.
	</p>
      </h:setFailed>
      <h:seenDisclaimer accepted="False">
	<h:csrfForm method="POST">
	  <p>
	    <label for="seenDisclaimer">
	      I have seen this disclaimer
	    </label>
	    <input type="checkbox" name="seenDisclaimer" id="seenDisclaimer" value="1"/>
	  </p>
	  <p>
	    <button type="submit">Submit</button>
	  </p>
	</h:csrfForm>
      </h:seenDisclaimer>
      <h:seenDisclaimer accepted="True">
	<p>
	  You have enabled seeing last update dates.
	  <h:hasCid>
	    <a href="/info.html?cid=${h:cid}">Return</a>.
	  </h:hasCid>
	</p>
      </h:seenDisclaimer>
    </h:lastUpdateDisclaimer>
  </h:ifLoggedIn>
</h:piperka>
