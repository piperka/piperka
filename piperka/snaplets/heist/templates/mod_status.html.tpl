<h:piperka ads="False" title="Status">
  <h:ifMod>
    <h2>Comic hiatus/completion status</h2>
    <p>
      Please read <a href="status_explanation.html">this page</a>
      first.
    </p>
    <h:hiatus>
      <h:single>
	<h:withCidTitle>
	  <h3>Set status for <h:title/></h3>
	  <span id="hiatus_single">
	    <button id="clear_hiatus_single">Clear</button>
	    <select>
	      <option value=""></option>
	      <h:hiatusClassifications>
		<option value="${h:id}"><h:name/></option>
	      </h:hiatusClassifications>
	    </select>
	  </span>
	  <span id="hiatus_error" class="error"/>
	</h:withCidTitle>
      </h:single>
      <h:all>
	<p>
	  This page shows 100 comics identified to receive no updates.
	  If the comic should be ticketed to get updates running again
	  instead of classifying, please click defer and do so.  It'll
	  remove it from this list and it'll return in two weeks if
	  other conditions for it to show up still apply.  It's
	  possible to mark comics not shown on this page.  If a comic
	  gets updated by the hourly crawl run then the hiatus status
	  is cleared.
	</p>
	<table id="hiatus-table">
	  <tr>
	    <th>cid</th>
	    <th>Title</th>
	    <th>Defer</th>
	    <th>Action</th>
	  </tr>
	  <h:pendingHiatus>
	    <tr data-cid="${h:cid}">
	      <td><h:cid/></td>
	      <td><a href="info.html?cid=${h:cid}" target="hiatus"><h:title/></a></td>
	      <td><button class="defer">Defer</button></td>
	      <td>
		<select>
		  <option value=""></option>
		  <h:hiatusClassifications>
		    <option value="${h:id}"><h:name/></option>
		  </h:hiatusClassifications>
		</select>
	      </td>
	      <td class="result" />
	    </tr>
	  </h:pendingHiatus>
	</table>
      </h:all>
    </h:hiatus>
  </h:ifMod>
</h:piperka>
