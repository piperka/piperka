<h:piperka>
  <h2>Piperka Tour</h2>
  <div id="tour">
    <p>
      Navigation: <a href="javascript:" rel="prev" id="main_prev">🡄 Previous page</a>
      <a href="javascript:" rel="next" id="main_next">Next page 🡆</a>
    </p>
    <p>
      Page <span id="on_page">0</span>/<span id="pages_total">0</span>
    </p>
    <div id="tour_container">Loading...</div>
  </div>
  <noscript>
    <p>
      The tour requires JavaScript.  Mind you, Piperka is mostly
      usable without it.  Just not this part.
    </p>
  </noscript>
</h:piperka>
