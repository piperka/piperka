<!DOCTYPE html>
<html lang="en" h:maybeOg="info.html">
  <head>
    <h:mobile>
      <meta name="viewport" content="width=device-width, initial-scale=1">
    </h:mobile>
    <h:withPrefetchInfo>
      <meta property="og:title" content="${h:title}"/>
      <meta property="og:type" content="website"/>
      <meta property="og:description" content="${h:description}"/>
    </h:withPrefetchInfo>
    <meta charset="UTF-8"/>
    <title><h:title><h:siteName cap="True"/> <h:unreadStats/></h:title></title>
    <h:stylesheet rel="stylesheet" type="text/css" href="/site.css"/>
    <h:ifPage names='["top.html","browse.html","edit_author.html"]'>
      <h:stylesheet rel="stylesheet" type="text/css" href="/qsearch.css"/>
    </h:ifPage>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <h:ifPage name="index" not="True">
      <h:ifProd dev="True">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
      </h:ifProd>
      <h:ifProd>
	<link rel="stylesheet" href="/i/jquery-ui.css"/>
      </h:ifProd>
    </h:ifPage>

    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE"/>
    <h:ifProd dev="True">
      <script defer src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
      <h:ifPage name="index" not="True">
	<script defer src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
      </h:ifPage>
    </h:ifProd>
    <h:ifProd>
      <script defer src="/i/jquery.min.js"></script>
      <h:ifPage name="index" not="True">
	<script defer src="/i/jquery-ui.min.js"></script>
      </h:ifPage>
    </h:ifProd>
    <h:ifPage name="info.html">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.9/jquery.jqplot.min.css"/>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.9/jquery.jqplot.min.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.9/plugins/jqplot.dateAxisRenderer.min.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.9/plugins/jqplot.canvasTextRenderer.min.js"></script>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/jqPlot/1.0.9/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
    </h:ifPage>
    <h:ifMod>
      <h:script defer src="/moderate.js"></h:script>
    </h:ifMod>
    <h:ifMod level="Admin">
      <h:script defer src="/crawler.js"></h:script>
    </h:ifMod>
    <h:ifPage name="info.html">
      <h:script defer src="/viewarchive.js"/>
    </h:ifPage>
    <h:ifPage names='["top.html","browse.html","edit_author.html"]'>
      <h:script defer src="/qsearch.js"></h:script>
    </h:ifPage>
    <h:ifPage name="tour.html">
      <h:stylesheet rel="stylesheet" type="text/css" href="/tour.css"/>
      <h:script defer src="/tour.js"></h:script>
    </h:ifPage>
    <h:script defer src="/piperka.js"></h:script>
    <h:autoAd>
      <h:adCode id="head"/>
    </h:autoAd>
  </head>

  <body h:mobileClass="mobile" id="${h:siteId}">
    <div id="header" class="hiliteBG">
      <h:mobile mode="Desktop">
	<h:autoAd>
	  <div style="float: right" id="hdr-float">
	    <h:adCode id="header"/>
	  </div>
	  <div class="aa">
	    <a href="/">
	      <h:site site="Piperka">
		<img id="site-logo" src="/images/paprika.png" alt="Piperka"/>
	      </h:site>
	      <h:site site="Teksti">
		<img id="site-logo" src="/images/kirja.png" alt="Teksti"/>
	      </h:site>
	    </a>
	    <h1 style="margin-left:250px; position: absolute;"><h:siteName/></h1>
	  </div>
	</h:autoAd>
	<h:noAutoAd>
	  <a href="/">
	    <h:site site="Piperka">
	      <img id="site-logo" src="/images/paprika.png" alt="Piperka">
	    </h:site>
	    <h:site site="Teksti">
	      <img id="site-logo" src="/images/kirja.png" alt="Teksti"/>
	    </h:site>
	  </a>
	  <h1><h:siteName/></h1>
	</h:noAutoAd>
      </h:mobile>
      <h:mobile>
	<a href="/">
	  <h:site site="Piperka">
	    <img id="site-logo" src="/images/paprika.png" alt="Piperka">
	  </h:site>
	  <h:site site="Teksti">
	    <img id="site-logo" src="/images/kirja.png" alt="Teksti"/>
	  </h:site>
	</a>
	<h1><h:siteName/></h1>
	<svg id="menu-button" viewbox="0 0 85 85">
	  <text x="10" y="65">☰</text>
	  <h:ifLoggedIn>
	    <circle cx="15" cy="70" r="10" h:newComicsClass="newcomics"/>
	  </h:ifLoggedIn>
	</svg>
	<h:ifLoggedIn>
	  <span id="unread-header"><h:unreadStats parens="False"/></span>
	</h:ifLoggedIn>
      </h:mobile>
    </div>
    <h:mobile>
      <div id="popup-container"/>
    </h:mobile>

    <div class="container">
      <div class="sidebar hiliteBG">
	<div class="control">
	  <p>
	    <h:site site="Piperka">
	      <h:ifLoggedIn>
		<a href="/support.html">Support Piperka</a><br/>
	      </h:ifLoggedIn>
	    </h:site>
	    <a href="/about.html">About this site</a>
	    <br/><a href="https://piperka.net/blog/">Blog</a>
	    <br/><a href="/top.html">Most popular</a>
	    <br/><a href="/submit.html">Submit a <h:comicWord/></a>
	    <h:ifLoggedIn>
	      <h:isAuthor>
		<br/><a href="/my_authored.html">Author</a>
	      </h:isAuthor>
	    </h:ifLoggedIn>
	    <br/><span id="browseComics">
	      <a href="/browse.html">Browse <h:comicWord/>s</a> <span id="newcomics"><h:newComics/></span>
	    </span>
	    <h:ifLoggedIn>
	      <p>You are logged in as <a href="${h:profileLink}"><h:loggedInUser/></a></p>
	      <a href="/">Check updates</a> <span id="newin" h:stickyShowHidden=""><h:unreadStats/></span>
	      <br/><a href="/account.html">Your account</a>
	      <br/><hr/><a href="/?action=logout&csrf_ham=${h:csrf}">Logout</a>
	      <h:ifMod>
		<hr/>
		<a href="/moderate.html">Moderate</a>
		<h:modStats>
		  <h:haveModCount>(<h:modCount/>)</h:haveModCount>
		</h:modStats>
		<br/><a href="/mod_status.html">Hiatus status</a>
	      </h:ifMod>
	      <h:ifMod level="Admin">
		<br/><a href="/mod_ticket.html">Tickets</a> (<h:modStats><h:unresolvedTickets/></h:modStats>)
	      </h:ifMod>
	    </h:ifLoggedIn>
	    <h:ifLoggedOut>
	      <form method="post" action="/" style="padding-bottom:10px">
		<p>
		  User:
		  <a id="loginwith" href="loginwith.html">Login with...</a>
		  <br/><input class="login" type="text" name="_login" maxlength="40" autocomplete="username" required/>
		  <br/>Password:<br/>
		  <input class="login" type="password" name="_password" maxlength="40" autocomplete="current-password" required/>
		  <input id="loginsubmit" type="submit" name="action" value="Login"/>
		</p>
	      </form>
	      <br/><a href="/newuser.html">Create account</a>
	      <br/><a href="/lost.html">Lost password?</a>
	    </h:ifLoggedOut>
	    <h:mobile>
	      <br/><a href="javascript::" class="selectMobile" data-sel="0">Desktop site</a>
	    </h:mobile>
	    <h:mobile mode="LatentMobile">
	      <br/><a href="javascript::" class="selectMobile" data-sel="1">Mobile site</a>
	    </h:mobile>
	  </p>
	</div>
	<h:mobile mode="Desktop">
	  <h:autoAd>
	    <h:adCode id="sidebar"/>
	  </h:autoAd>
	</h:mobile>
      </div>
      <div id="maincornerback"></div>
      <div class="main">
	<h:site site="Teksti">
	  <h:mobile mode="Desktop">
	    <h:adAskOptOut>
	      <h:autoAd enabled="Nothing">
		<div id="smalladbg">
		  <span class="ads-question script show">
		    Disable <a href="ad_settings.html">ads</a>?<br/>
		    <button name="ads" value="0">Yes please</button>
		    <button name="ads" value="1">I don't mind them</button>
		  </span>
		</div>
	      </h:autoAd>
	    </h:adAskOptOut>
	    <h:ifPage names='["info.html","top.html"]' not="True">
	      <h:autoAd enabled="Just True">
		<div id="smalladbg">
		  <h:adCode id="corner"/>
		</div>
	      </h:autoAd>
	    </h:ifPage>
	  </h:mobile>
	</h:site>
	<div id="notmuch" style="width:1px; height:1px;"></div>
	<h:ifHasMessage>
	  <div class="error" id="message">
	    <h:message/>
	  </div>
	</h:ifHasMessage>
	<h:action>
	  <h:logout>
	    You have successfully logged out.
	  </h:logout>
	  <h:bookmark>
	    <h:success>
	      <h:result>
		Bookmark
		for <a href="/info.html?cid=${h:cid}"><h:title/></a> set
		on <h:newest>newest page</h:newest><h:notNewest>page
		  <h:ord/></h:notNewest>.
	      </h:result>
	    </h:success>
	    <h:multiple>
	      <p>Bookmark matches multiple <h:comicWord/>s.</p>
	      <p>
		<ul>
		  <h:item>
		    <li><a href="/info.html?cid=${h:cid}"><h:title/></a></li>
		  </h:item>
		</ul>
	    </h:multiple>
	    <h:recognized>
	      <h3>Failed to set bookmark</h3>
	      <p>
		Looks like the URL you submitted might be for
		<a href="/info.html?cid=${h:cid}"><h:title/></a> but it
		failed to match with any page.  The error has been
		logged.
	      </p>
	      <h:ifLoggedIn>
		<p>
		  If you wish, you can set the bookmark for this
		  <h:comicWord/>: <h:subscribeForm action="/"/>
		</p>
	      </h:ifLoggedIn>
	    </h:recognized>
	    <h:failed>
	      <h3>Failed to set bookmark</h3>
	      <p>
		For some reason, the URL failed to match with a
		<h:comicWord/> in the database.  The error has been
		logged.
	      </p>
	      <p>
		The bookmarking algorithm is unfortunately, by
		necessity, quite ad hoc.  It tries its best effort
		with pages from <h:comicWord/>s' archives, but it may
		fail if you try to give the URL of the
		<h:comicWord/>'s front page.  You might have better
		luck with choosing the <h:comicWord/> directly from
		the list, especially since <h:comicWord/>s may be
		hosted on several sites and <h:siteName/>'s keeping
		track of only one.
	      </p>
	    </h:failed>
	  </h:bookmark>
	  <h:ifLoggedIn>
	    <h:csrfFail>
	      You tried to
	      <h:describe>
		<logout>log out</logout>
		<bookmark>
		  <h:result>
		    set bookmark
		    for <a href="/info.html?cid=${h:cid}"><h:title/></a>
		    on <h:newest>newest page</h:newest>
		    <h:notNewest>page <h:ord/></h:notNewest>
		  </h:result>
		</bookmark>
		<subscribe>
		  subscribe
		  to <a href="/info.html?cid=${h:cid}"><h:title/></a>
		</subscribe>
		<unsubscribe>
		  unsubscribe
		  from <a href="/info.html?cid=${h:cid}"><h:title/></a>
		</unsubscribe>
		<revert>
		  revert updates
		</revert>
		<reject>
		  mark your disinterest
		  on <a href="/info.html?cid=${h:cid}"><h:title/></a>
		</reject>
		<unreject>
		  clear your disinterest mark on
		  on <a href="/info.html?cid=${h:cid}"><h:title/></a>
		</unreject>
	      </h:describe>
	      but this action failed the check to see that this
	      request originated from <h:siteName/>.  Please verify
	      that you meant to do this.
	      <h:csrfForm method="post">
		<h:actionInputs>
		  <input type="hidden" name="${h:name}" value="${h:value}"/>
		</h:actionInputs>
		<input type="submit" value="Submit"/>
	      </h:csrfForm>
            </h:csrfFail>
	  </h:ifLoggedIn>
	  <h:unknownAction>
	    You requested an action from <h:siteName/> that failed a
	    precondition, like trying to subscribe to a <h:comicWord/>
	    not listed on <h:siteName/>.  You shouldn't be seeing this
	    error unless you did something special like edited the
	    post data yourself or had the extraordinary luck of doing
	    something concurrently with some maintenance work.  Feel
	    free to contact the admin if this persists.
	  </h:unknownAction>
	  <h:sqlErr/>
	</h:action>
      </div>
    </div>
    <h:extra/>
    <div class="legalese" id="footer">
      <h:siteName domain="True"/> copyright Kari Pahula
      &lt;<a href="mailto:kaol@piperka.net">kaol@piperka.net</a>&gt;
      2005-2024. Descriptions are user submitted and <h:siteName/>
      claims no copyright over them.  Banners copyright their
      respective authors.  <br/>
      <a href="/privacy_policy.html">Privacy policy</a> |

      <h:mobile mode="ReallyDesktop">
	<a href="javascript::" class="selectMobile" data-sel="1">Mobile site</a>
	|
      </h:mobile>
      <a href="/ad_settings.html">Ad settings</a>
      |
      <h:ifLoggedIn>
	<a href="https://discord.gg/HBXfvDw">Discord</a>
	|
      </h:ifLoggedIn>
      <h:otherSiteSSO/>
    </div>
  </body>
</html>
