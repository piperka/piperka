<h:piperka minifiable="True">
  <div id="tour" class="script">
    <div>
      <div class="tour_info">
	<p>
	  I shouldn't ask you to make an account just to see what
	  Piperka is about.  On this page, I'm showing a tour of its
	  interface.  Use the navigation links at the top of this page
	  or find a target to click on the page to proceed.
	</p>
	<p>
	  This is the main view on Piperka, with some bits omitted for
	  now.  On it, you can see all your subscribed comics which
	  have updates for them.  Clicking on the links redirects you
	  to the first unread page.  Click on "Stickfigure Hero" to
	  see what happens.
	</p>
	<p>
	  None of the comics mentioned in the tour are existing
	  comics.  At least not as of now.
	</p>
      </div>
      <div class="tour_page">
	<h2>Updated comics</h2>
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (15 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
		<li><a href="javascript:" rel="next" class="target">Stickfigure Hero</a> (3 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  You have left Piperka.  Nothing you do here will have any
	  effect on your account on Piperka.  Enjoy the nice art and
	  then click on the link below to return to Piperka.
	</p>
      </div>
      <div class="tour_page">
	<div>
	  <span class="fake_browser_button">🡠</span>
	  <span class="fake_browser_button">🡢</span>
	  <span class="fake_browser_button">🗘</span>
	  <textarea readonly rows="1" cols="80" class="fake_browser_address">https://stickfigurehero.invalid/1</textarea>
	</div>
	<h2 id="stickTitle">Stickfigure Hero Begins</h2>
	<img id="comicpage" src="/images/tutorial/comic1.svg"/>
	<div id="stickfigure_nav">
	  <a href="javascript:"><img src="/images/tutorial/first.png" class="disabled" id="first"/></a>
	  <a href="javascript:"><img src="/images/tutorial/prev.png" class="disabled" id="prev"/></a>
	  <a href="javascript:"><img src="/images/tutorial/next.png" id="next"/></a>
	  <a href="javascript:"><img src="/images/tutorial/last.png" id="last"/></a>
	</div>
      </div>
      <div class="tour_footer">
	<a href="javascript:" rel="next" class="target">Return to Piperka</a>.
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  As you can see, Stickfigure Hero is no longer listed on the
	  page.  What Piperka does along with the redirect is that it
	  updates your bookmark to the newest page.  The updates page
	  only lists those comics which have unread content.  Now
	  click on revert.
	</p>
      </div>
      <div class="tour_page">
	<h2>Updated comics</h2>
	<a href="javascript:" rel="next" class="target">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (15 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  You can view your last redirects on this page and cancel
	  them.  Select the checkbox for Stickfigure Hero and click
	  "Revert".
	</p>
      </div>
      <div class="tour_page">
	<h2>Revert updates</h2>
	<p>
	  Piperka can't keep track of whether our redirects were
	  successful.  It keeps records of your last redirects for
	  comics.
	</p>
	<div id="piperka_list">
	  <div class="list1">
	    <ul>
	      <li>
		<input type="checkbox" id="recent">
		<label for="recent">
		  Stickfigure Hero (10 minutes ago)
		</label>
	      </li>
	      <li>
		<input type="checkbox" id="recent2">
		<label for="recent2">
		  Land of Hiatus (10 years ago)
		</label>
	      </li>
	    </ul>
	  </div>
	  <button type="button" rel="next" disabled>Revert</button>
	</div>
      </div>
      <div class="script tour_footer" id="recent2_blurb">
	On the live site, there'd be nothing preventing from
	reverting many comics on the same go.  But for this
	demonstration, just mark Stickfigure Hero.
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  As you can see, Stickfigure Hero is back on the list, with
	  bookmark on the same place as when we started.  Now click on
	  it again.
	</p>
      </div>
      <div class="tour_page">
	<h2>Updated comics</h2>
	<a href="javascript:">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (15 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
		<li><a href="javascript:" rel="next" class="target">Stickfigure Hero</a> (3 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Note that the redirect link took you to the first page
	  again, since the revert resetted your bookmark.
	</p>
	<p>
	  Piperka can also set bookmarks using comics' archive page
	  addresses.  Let's use that now and navigate to the second
	  page ("Stickfigure Hero's Tea Time") and copy its address.
	</p>
      </div>
      <div class="tour_page">
	<span class="fake_browser_button">🡠</span>
	<span class="fake_browser_button">🡢</span>
	<span class="fake_browser_button">🗘</span>
	<textarea readonly rows="1" cols="80" class="fake_browser_address">https://stickfigurehero.invalid/1</textarea>
	<h2 id="stickTitle">Stickfigure Hero Begins</h2>
	<img id="comicpage" src="/images/tutorial/comic1.svg"/>
	<div id="stickfigure_nav">
	  <a href="javascript:"><img src="/images/tutorial/first.png" class="disabled" id="first"/></a>
	  <a href="javascript:"><img src="/images/tutorial/prev.png" class="disabled" id="prev"/></a>
	  <a href="javascript:"><img src="/images/tutorial/next.png" id="next"/></a>
	  <a href="javascript:"><img src="/images/tutorial/last.png" id="last"/></a>
	</div>
      </div>
      <div class="script tour_footer" id="copy_complete">
	Let's cut the chase.  Controlling your clipboard is a finicky
	thing so let's just assume that you've copied the address from
	above.  Now, <a href="javascript:" rel="next"
	class="target">return to Piperka</a>.
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Now, let's paste the address for the second page to the
	  bookmark field (if you didn't then here it is
	  again: <tt>https://stickfigurehero.invalid/2</tt>) and click
	  "Set".
	</p>
	<p>
	  The "set bookmark here" checkbox does nothing in this
	  demonstration, we'll get to what it does in a bit.  The
	  bookmarklet link would lead to instructions on how to add a
	  bookmarklet to your browser which would make this all a one
	  step process.  We'll skip that on this tour.
	</p>
      </div>
      <div class="tour_page">
	<h2>Set bookmark</h2>
	<p>
	  URL:
	  <input type="text" name="bookmark"/>
	  <button id="bookmark_button">Set</button>
	  <label for="wantbookmarkhere">Set bookmark here</label>
	  <input type="checkbox" id="wantbookmarkhere" name="wantbookmarkhere"/>
	  <a href="javascript:">Get a bookmarklet</a>
	</p>
	<h2>Updated comics</h2>
	<a href="javascript:">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (15 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Success!  Note that Stickfigure Hero now reads as having "1
	  new" instead of 3.  Piperka sets the bookmark on one page
	  after the page you've submitted it, since you may not want
	  to reread the page.
	</p>
	<p>
	  Using the "set bookmark here" checkbox controls this
	  behavior and it would set the bookmark so the redirect would
	  lead to the page you've read already.  There's also a
	  setting on the "Your account" page to make the redirects go
	  one page backwards.
	</p>
	<p>
	  Click on Stickfigure Hero to see what the redirect does now.
	</p>
      </div>
      <div class="tour_page">
	<p>
	  Bookmark for <a href="javascript:">Stickfigure Hero</a> set
	  on page 2.
	</p>
	<h2>Set bookmark</h2>
	<p>
	  URL:
	  <input type="text" name="bookmark" readonly/>
	  <button rel="next">Set</button>
	  <label for="wantbookmarkhere">Set bookmark here</label>
	  <a href="javascript:">Get a bookmarklet</a>
	</p>
	<h2>Updated comics</h2>
	<a href="javascript:">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (15 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
		<li><a href="javascript:" class="target" rel="next">Stickfigure Hero</a> (1 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
      </div>
    </div>
    <div data-stickPage="3">
      <div class="tour_info">
	<p>
	  As you can see Piperka took you to the next unread page.
	</p>
      </div>
      <div class="tour_page">
	<span class="fake_browser_button">🡠</span>
	<span class="fake_browser_button">🡢</span>
	<span class="fake_browser_button">🗘</span>
	<textarea readonly rows="1" cols="80" class="fake_browser_address">https://stickfigurehero.invalid/3</textarea>
	<h2 id="stickTitle">YACP</h2>
	<img id="comicpage" src="/images/tutorial/comic3.svg"/>
	<div id="stickfigure_nav">
	  <a href="javascript:"><img src="/images/tutorial/first.png" id="first"/></a>
	  <a href="javascript:"><img src="/images/tutorial/prev.png" id="prev"/></a>
	  <a href="javascript:"><img src="/images/tutorial/next.png" class="disabled" id="next"/></a>
	  <a href="javascript:"><img src="/images/tutorial/last.png" class="disabled" id="last"/></a>
	</div>
	<div class="tour_footer">
	  Now, <a href="javascript:" rel="next" class="target">return
	  to Piperka again</a>.
	</div>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Using the redirect button updated the bookmark to the newest
	  page and it's gone from this list again.  Now, let's just
	  see what happens when time passes.
	</p>
      </div>
      <div class="tour_page">
	<h2>Updated comics</h2>
	<a href="javascript:">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (15 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
	<p>
	  <a href="javascript:" rel="next" class="target">Tick tock (a
	  few days or a week or whatever later)</a>...
	</p>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Stickfigure Hero has updated and has a total of four
	  glorious pages now!  A Cat Comic has a couple of new pages
	  as well.
	</p>
      </div>
      <div class="tour_page">
	<h2>Updated comics</h2>
	<a href="javascript:">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (17 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
		<li><a href="javascript:">Stickfigure Hero</a> (1 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
	<p>
	  Let's have a look at the rest of things on the updates
	  page.  <a href="javascript:" rel="next" class="target">Go
	  on</a>...
	</p>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<dl>
	  <dt>Open comic in Piperka Reader</dt>
	  <dd>
	    The default mode of Piperka redirects you to the comic
	    site and leaves Piperka behind altogether.  The Reader
	    mode embeds the web comic into itself and offers unified
	    controls, navigation controls to the whole archive and
	    page prefetching.  It's handy if you're up for longer
	    archive binges.
	  </dd>
	  <dt>View Piperka's recommendations</dt>
	  <dd>
	    Piperka can suggest you comics once you've picked a few
	    comics to read on your own.
	  </dd>
	  <dt>Watch updates</dt>
	  <dd>
	    This option makes the open tab wait for updates to happen.
	    It'll show a small exclamation mark on the favicon if
	    there's been an update.
	  </dd>
	  <dt>Notifications</dt>
	  <dd>
	    This makes Piperka send push notifications to your desktop
	    or mobile device when you have updates.
	  </dd>
	</dl>
      </div>
      <div class="tour_page">
	<p>
	  Open comic in Piperka Reader <input type="checkbox"/> |
	  <a href="javascript:">View Piperka's recommendations</a> |
	  Watch updates <input type="checkbox"/> |
	  Notifications <input type="checkbox"/>
	<p>
	<h2>Updated comics</h2>
	<a href="javascript:">Revert</a> updates.
	<span id="updates">
	  <div id="piperka_list">
	    <div class="list1">
	      <ul>
		<li><a href="javascript:">A Cat Comic</a> (17 new)</li>
		<li><a href="javascript:">Serious Overload</a> (10 new)</li>
	      </ul>
	    </div>
	  </div>
	</span>
	<p>
	  Next up, <a href="javascript:" rel="next"
	  class="target">let's have a look at the sidebar</a>.
	</p>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Many of these are pretty self-explanatory, but let's go
	  through them step by step.
	</p>
	<dl>
	  <dt>Support Piperka</dt>
	  <dd>Patreon & stuff</dd>
	  <dt>About this site</dt>
	  <dd>FAQ & links to some parts of the site</dd>
	  <dt>Blog</dt>
	  <dd>Site updates</dd>
	  <dt>Most popular</dt>
	  <dd>List of comic pages, sorted by popularity</dd>
	  <dt>Submit a comic</dt>
	  <dd>Request to add a new comic</dd>
	  <dt>Browse comics</dt>
	  <dd>Alphabetical list of comics, with other sort options as well</dd>
	  <dt>You are logged in as ...</dt>
	  <dd>Link to your profile page</dd>
	  <dt>Check updates</dt>
	  <dd>
	    The main page, which we've already covered.  The numbers
	    next to it are short hand for the number of unread pages
	    and the amount of comics which have unread pages.
	  </dd>
	  <dt>Your account</dt>
	  <dd>
	    Settings like display options, and password and
	    federated login (OAuth2)
	  </dd>
	</dl>
      </div>
      <div class="tour_page">
	<div class="sidebar hiliteBG">
	  <p>
	    <a href="javascript:">Support Piperka</a><br/>
	    <a href="javascript:">About this site</a><br/>
	    <a href="javascript:">Blog</a><br/>
	    <a href="javascript:">Most popular</a><br/>
	    <a href="javascript:">Submit a comic</a><br/>
	    <a href="javascript:">Browse comics</a><br/>
	  </p>
	  <p>
	    You are logged in as <a href="javascript:">username</a>
	  </p>
	  <p>
	    <a href="javascript:">Check updates</a> (27 new in 2)<br/>
	    <a href="javascript:">Your account</a><br/>
	  </p>
	</div>
	<p>
	  Next up, let's <a href="javascript:" rel="next"
	  class="target">explain a typical comic listing a bit</a>.
	</p>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  Quick search is what it says.  Technically, it uses a regexp
	  but you can pretty much just use a name as is.  The checkbox
	  controls whether to limit the amount of results.  It is
	  possible to make a search that lists all comics on Piperka
	  in a query.  Which may slow down your browser.
	</p>
	<p>
	  The tag highlight button makes the listing mark comics using
	  selected tags.
	</p>
	<p>
	  The first comic checkbox controls whether to set the
	  bookmark at the start or the end of the archives.  If you're
	  already up to date with a comic then uncheck it.
	</p>
	<p>
	  The listing entries may be a bit dense on the first look.
	  On the right edge there's an estimate on the update schedule
	  of the comic.  The day letters get their color from the hour
	  of the day, at UTC time.  The bluer the color the earlier
	  the update.  Otherwise, there may be an estimate of the
	  amount of updates in a week, or in a month.
	</p>
	<p>
	  Comics may also have been flagged to be on hiatus, or such.
	  That's also displayed in the listings.  The white colored
	  corner signifies that the comic is getting frequent updates.
	</p>
	<p>
	  Piperka has also a bit of flair for social features.  You
	  may mark an interest on other users' subscriptions and
	  there's that green "F" letter for comics which your friends
	  read.
	</p>
      </div>
      <div class="tour_page">
	<h2>Most popular webcomics</h2>
	<p>
	  Quick search:

	  <input type="input" readonly/><button>Go!</button>

	  Show all results <input type="checkbox" readonly/>
	</p>
	<p>
	  <button>Highlight comics with tag...</button>
	</p>
	<p>
	  Bookmark the first comic <input type="checkbox" readonly/>
	</p>
	<div id="piperka_list">
	  <div class="list1">
	    <ol>
	      <li class="update-high">
		<span class="update-info">
		  <span class="schedule-letters"><span class='no-update'>_</span><span class="s0">T</span><span class="s22">W</span><span class='no-update'>_</span><span class="s23">F</span><span class='no-update'>_</span><span class='no-update'>_</span></span>
		</span>
		<button class="plus">+</button>
		<a href="javascript:">Nerdy comic</a>
		<a href="javascript:" class="followee">F</a>
	      </li>
	      <li class="update-high">
		<span class="update-info">
		  <span class="schedule-letters"><span class='no-update'>_</span><span class='no-update'>_</span><span class='no-update'>_</span><span class='no-update'>_</span><span class='no-update'>_</span><span class='no-update'>_</span><span class="s18">S</span></span>
		</span>
		<button class="plus">+</button>
		<a href="javascript:">Explicit comic</a>
	      </li>
	      <li class="update-high">
		<span class="update-info">
		  <span class="schedule-letters"><span class="s8">M</span><span class='no-update'>_</span><span class="s8">W</span><span class='no-update'>_</span><span class="s8">F</span><span class='no-update'>_</span><span class='no-update'>_</span></span>
		</span>
		<button class="plus">+</button>
		<a href="javascript:">Science-fantasy school comic</a>
	      </li>
	      <li class="update-high">
		<span class="update-info">5 times a week</span>
		<button class="plus">+</button>
		<a href="javascript:">Slice-of-life comic</a>
	      </li>
	      <li>
		<button class="plus">+</button>
		<a href="javascript:">RPG comic</a>
	      </li>
	    </ol>
	  </div>
	</div>
      </div>
      <div class="tour_footer">
	<p>
	  That's a handful.  <a href="javascript:" rel="next"
	  class="target">Let's go have a look at your profile
	  page</a>.
	</p>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  The main use of your profile page is to see the list of
	  subscriptions.
	</p>
	<p>
	  Your profile can be set as private, friends only or public.
	  In friends only mode, you'll need to approve people to see
	  your profile, on their profile pages.  The default is
	  friends only and it can be set on your account page.
	</p>
	<p>
	  You can view your others' profiles if they have allowed it.
	</p>
      </div>
      <div class="tour_page">
	<h2>Comic picks of username</h2>
	<p>
	  You have <a href="javascript:">3 followers and you follow 2
	    people</a>.
	</p>
	<p>
	  Total comic pages read by you: 1500 in 4 comics.
	</p>
	<div id="piperka_list">
	  <div class="list1">
	    <ul>
	      <li><button class="minus">-</button><a href="javascript:">A Cat Comic</a></li>
	      <li><button class="minus">-</button><a href="javascript:">Land of Hiatus</a></li>
	      <li><button class="minus">-</button><a href="javascript:">Serious Overload</a></li>
	      <li><button class="minus">-</button><a href="javascript:">Stickfigure Hero</a></li>
	    </ul>
	  </div>
	</div>
      </div>
      <div class="tour_footer">
	<p>
	  <a href="javascript:" rel="next">That's all</a>.
	</p>
      </div>
    </div>
    <div>
      <div class="tour_info">
	<p>
	  This concludes the tour.  There's still more to Piperka but
	  this should give you a gist of it.
	</p>
      </div>
    </div>
  </div>
</h:piperka>
