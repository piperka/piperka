<h:csrfForm method="post" h:action="">
  <h:subscribeControl>
    <h:ifLoggedIn>
      <div>
	<label for="atfirst">Bookmark the first <h:comicWord/> page</label>
	<input id="atfirst" type="checkbox" name="start_at_first" checked="checked"/>
      </div>
    </h:ifLoggedIn>
  </h:subscribeControl>
  <h:navigateControl>
    <div id="piperka_list" data-total="${h:totalCount}">
      <h:columnMode col="OneColumn">
	<div class="list1">
	  <h:column column="1"/>
	</div>
      </h:columnMode>
      <h:columnMode col="TwoColumn">
	<div class="list2" id="left">
	  <h:column column="1"/>
	</div>
	<div class="list2" id="right">
	  <h:column column="2"/>
	</div>
      </h:columnMode>
      <h:columnMode col="ThreeColumn">
	<div class="list3" id="leftmost">
	  <h:column column="1"/>
	</div>
	<div class="list3" id="middle">
	  <h:column column="2"/>
	</div>
	<div class="list3" id="rightmost">
	  <h:column column="3"/>
	</div>
      </h:columnMode>
      <h:columnMode col="FourColumn">
	<div class="list4" id="col1">
	  <h:column column="1"/>
	</div>
	<div class="list4" id="col2">
	  <h:column column="2"/>
	</div>
	<div class="list4" id="col3">
	  <h:column column="3"/>
	</div>
	<div class="list4" id="col4">
	  <h:column column="4"/>
	</div>
      </h:columnMode>
    </div>
  </h:navigateControl>
  <h:afterListing/>
</h:csrfForm>
