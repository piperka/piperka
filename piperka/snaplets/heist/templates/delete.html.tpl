<h:piperka title="Delete account" ads="False">
  <h1>Account deletion</h1>
  <p>
    Account deletion policy: From the initial request, there's a 24
    hour waiting period.  After that, requesting it again will finish
    it.  The request may be canceled at any point and they expire
    after a week.
  </p>
  <p>
    Deleted user names remain reserved.  Bookmarks and email address
    (if you provided one) are deleted immediately and logs of site
    activity with IP address with no other user identifying data may
    be retained for a longer time.
  </p>
  <p>
    This policy applies both to this web site as well as to the
    Piperka Client app.
  </p>
  <h2>Your request</h2>
  <h:ifLoggedIn>
    <h:accountDelete>
      <h:csrfFail check="True">
	<p>
	  CSRF check failed.  Did you make the request from somewhere
	  else than this page?
	</p>
      </h:csrfFail>
      <h:initiated check="True">
	<p>
	  Account delete initiated.
	</p>
      </h:initiated>
      <h:finished check="True">
	<p>
	  Account deleted.  The user session was still valid for this
	  render but if you reload the page the site state will show
	  you unlogged.  Any other sessions you may have had with
	  other browsers/devices have been deleted.
	</p>
	<p>
	  Farewell.
	</p>
      </h:finished>
      <h:finished check="False">
	<h:active check="False">
	  <p>
	    If you want to start the delete procedure, type in "I am
	    sure" to the input box.
	  </p>
	  <p>
	    <h:csrfForm method="post">
	      <input type="text" name="iamsure"/>
	      <button type="submit">Submit</button>
	    </h:csrfForm>
	  </p>
	</h:active>
	<h:active check="True">
	  <h:dayPassed check="False">
	    Your account delete request is pending.  Time left:
	    <h:timeLeft>
	      <h:hours><h:n/> hour<h:plural>s</h:plural></h:hours>
	      <h:minutes><h:n/> minute<h:plural>s</h:plural></h:minutes>
	    </h:timeLeft>.
	  </h:dayPassed>
	  <h:dayPassed check="True">
	    <p>
	      The waiting period is over and your account may be
	      deleted now.  Please confirm by typing in "I am sure" to
	      the input box.
	    </p>
	    <p>
	      <h:csrfForm method="post">
		<input type="text" name="iamsure"/>
		<input type="hidden" name="final" value="1"/>
		<button type="submit">Submit</button>
	      </h:csrfForm>
	    </p>
	  </h:dayPassed>
	  <p>
	    You may cancel the request by pressing this button:
	    <h:csrfForm method="post">
	      <input type="hidden" name="cancel" value="1"/>
	      <button type="submit">Cancel</button>
	    </h:csrfForm>
	  </p>
	</h:active>
      </h:finished>
    </h:accountDelete>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    <p>
      You are not logged in.
    </p>
  </h:ifLoggedOut>
</h:piperka>
