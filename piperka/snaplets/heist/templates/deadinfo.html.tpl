<h:piperka prefetch="Dead" title="Removed ${h:comicWord}">
  <h:withCid dead="True">
    <h:exists>
      <h:comicInfo/>
      <h3>Actions</h3>
      <ul>
	<li><a href="ticket.html?cid=${h:cid}">Open ticket</a></li>
      </ul>
    </h:exists>
  </h:withCid>
</h:piperka>
