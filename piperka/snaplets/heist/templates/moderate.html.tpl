<h:piperka ads="False" title="Moderate">
  <h:submit mode="Moderate">
    <h:ifLoggedIn>
      <h:ifMod>
	<h2>Moderate</h2>
	<h:ifMod level="Admin">
	  <span id="refresh-comiclists">
	    <button type="button">Refresh <h:comicWord/> lists</button>
	    <span/>
	  </span>
	</h:ifMod>
	<a href="moderator_policy.html">Moderator policy</a>.
	<h3>Hiatus status edits</h3>
	<table id="user-hiatus-edits">
	  <tr>
	    <th>uhid</th>
	    <th><h:comicWord capital="True"/></th>
	    <th>Submitted on</th>
	    <th>User</th>
	    <th>Current</th>
	    <th>Proposed</th>
	    <th>Reason</th>
	    <th>Action</th>
	  </tr>
	  <h:listOfStatusEdits>
	    <tr data-uhid="${h:uhid}" data-cid="${h:cid}" data-code="${h:code}">
	      <td><h:uhid/></td>
	      <td><a href="info.html?cid=${h:cid}" target="status-edit"><h:title/></a></td>
	      <td><h:addedOn/></td>
	      <td><h:name/></td>
	      <td><h:currentHiatus/></td>
	      <td><h:proposedHiatus/></td>
	      <td><h:reason/></td>
	      <td>
		<button name="use">Use</button>
		<button name="discard">Discard</button>
	      </td>
	      <td class="msg"/>
	    </tr>
	  </h:listOfStatusEdits>
	</table>
	<h3><h:comicWord capital="True"/> entry edits</h3>
	<table id="user-edits">
	  <tr>
	    <th>sid</th>
	    <th><h:comicWord capital="True"/></th>
	    <th>Submitted on</th>
	    <th>From IP</th>
	    <th>User</th>
	  </tr>
	  <h:ifLoggedIn>
	    <h:listOfEdits>
	      <tr id="${h:sidId}" h:class="youCare">
		<td><h:sid/></td>
		<td class="cid" id="${h:cidId}"><h:title/></td>
		<td><h:addedOn/></td>
		<td><h:fromIP/></td>
		<td><h:name/></td>
	      </tr>
	    </h:listOfEdits>
	  </h:ifLoggedIn>
	</table>
	<h2>Current Entry</h2>
	<div id="current-entry"/>
	<h:csrfForm class="submitcomic script" id="editinfo">
	  <input type="hidden" name="formtype" value="editinfo"/>
	  <input type="hidden" name="user_sid" value=""/>
	  <input type="hidden" name="cid" value=""/>
	  Title: <a id="info-title"></a>
	  <p>
	    CID: <span id="info-cid"/>
	  </p>
	  <div style="height: 60px" id="useredit-banner"/>
	  <h:submitForm/>
	</h:csrfForm>
      </h:ifMod>
    </h:ifLoggedIn>
    <h:notMod>
      <h2>403 Go away</h2>
      <p>
	This page is meant for moderators, not for mere mortals.
      </p>
    </h:notMod>
  </h:submit>
</h:piperka>
