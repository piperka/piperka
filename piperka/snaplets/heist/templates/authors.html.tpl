<h:piperka>
  <h2>Authors</h2>
  <p>
    <h:siteName/> mainly tries to maintain this list to keep track of
    comics which have common authors.  If you'd like to have your name
    and comics listed here then head to their info pages to claim
    them.
  </p>
  <table id="authors">
    <thead>
      <tr>
	<th scope="col">Name</th>
	<th scope="col">Comics</th>
      </tr>
    </thead>
    <tbody>
      <h:authors>
	<h:hasId>
	  <tr data-auid="${h:auid}">
	    <td>
	      <h:ifMod>
		<a href="edit_author.html?auid=${h:auid}"><h:name/></a>
	      </h:ifMod>
	      <h:notMod forbid="False"><h:name/></h:notMod>
	    </td>
	    <td class="commaseparated">
	      <h:comics>
		<a href="info.html?cid=${h:cid}"><h:title/></a>
	      </h:comics>
	    </td>
	  </tr>
	</h:hasId>
      </h:authors>
    </tbody>
  </table>
  <h:ifMod>
    <a href="edit_author.html">Add new</a>
  </h:ifMod>
</h:piperka>
