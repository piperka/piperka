<h2>No readers for <h:title/></h2>
<p>
  Looks like nobody admits
  reading <a href="info.html?cid=${h:cid}">this comic</a>.
</p>
<h:hidden>
  <p>
    Still, there are <h:total/> readers.
  </p>
</h:hidden>
