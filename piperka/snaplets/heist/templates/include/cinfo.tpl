<h:comicInfo>
  <h:banner>
    <p>
      <img id="info-banner" src="${h:bannerUrl}"/>
    </p>
  </h:banner>
  <h2><h:title/></h2>
  <p>
    Subscriptions: <h:readersLink><h:subscriptions/></h:readersLink>
  </p>
  <p>
    Total pages:
    <h:ifPage name="info.html">
      <a href="javascript::" id="info-archive">
	<h:hasFragments><h:fragmentCount/>/</h:hasFragments><h:pageCount><i>unknown</i></h:pageCount>
      </a>
    </h:ifPage>
    <h:ifPage name="info.html" not="True">
      <h:hasFragments><h:fragmentCount/>/</h:hasFragments><h:pageCount><i>unknown</i></h:pageCount>
    </h:ifPage>
    <h:ifKnowPages>
      <true>
	| <h:externA href="${h:firstPageUrl}" h:deadRel="">First page</h:externA>
	| <h:externA href="${h:lastPageUrl}" h:deadRel="">Last known page</h:externA>
	<h:ifFixedHead>
	  (excluding front page)
	</h:ifFixedHead>
	| <a href="/s/rss/${h:cid}">RSS</a>
      </true>
      <false>
	| No pages in index
      </false>
    </h:ifKnowPages>
  </p>
  <p>
    Homepage: <h:externA href="${h:homepage}" h:deadRel=""><h:homepageText/></h:externA>
  </p>
  <h:ifExternLinks>
    <p>
      This comic on:
      <h:externLink>
	<h:externA href="${h:url}" title="${h:description}"><h:siteName/></h:externA>
      </h:externLink>
    </p>
  </h:ifExternLinks>
  <h:ifHasDates>
    <p class="infoStamps">
      <h:ifAddDate>
	<span>Added on: <h:addDate/></span>
      </h:ifAddDate>
      <h:ifUpdateDate>
	<h:ifLoggedIn>
	  <span>
	    Last updated:
	    <h:lastUpdateDisclaimer>
	      <h:seenDisclaimer accepted="False">
		<a href="/last_update_disclaimer.html?cid=${h:cid}">Read disclaimer</a>
	      </h:seenDisclaimer>
	      <h:seenDisclaimer accepted="True">
		<h:lastUpdateDate/>
	      </h:seenDisclaimer>
	    </h:lastUpdateDisclaimer>
	  </span>
	</h:ifLoggedIn>
      </h:ifUpdateDate>
      <h:ifBookmarkUpdated>
	<span>
	  Your bookmark for the comic last moved on: <h:bookmarkUpdated/>
	</span>
      </h:ifBookmarkUpdated>
    </p>
  </h:ifHasDates>
  <h:ifAuthors>
    <p>
      Other comics by the same author(s):
      <span class="commaseparated">
	<h:common>
	  <a href="info.html?cid=${h:cid}"><h:title/></a>
	</h:common>
      </span>
    </p>
  </h:ifAuthors>
  <h:inferredSchedule>
    Update schedule (UTC):
    <h:indeterminate>
      <i>irregular / schedule inference inconclusive</i>
    </h:indeterminate>
    <h:weekly>
      <h:once>Once a week</h:once>
      <h:multi><h:num/> times a week</h:multi>
    </h:weekly>
    <h:monthly>
      <h:once>Once a month</h:once>
      <h:multi><h:num/> times a month</h:multi>
    </h:monthly>
    <h:updates>
      <h:mon>
	Monday
      </h:mon>
      <h:tue>
	Tuesday
      </h:tue>
      <h:wed>
	Wednesday
      </h:wed>
      <h:thu>
	Thursday
      </h:thu>
      <h:fri>
	Friday
      </h:fri>
      <h:sat>
	Saturday
      </h:sat>
      <h:sun>
	Sunday
      </h:sun>
      <h:hour/>:00
      <h:notLast>|</h:notLast>
    </h:updates>
  </h:inferredSchedule>
  <h:hiatusStatus>
    <p>
      Comic status (since <h:stamp/>): <a href="status_explanation.html"><h:status/></a>
    </p>
  </h:hiatusStatus>
  <h:dead>
    <p>
      Removed on: <h:removeDate/>
    </p>
    <p>
      Reason given: <i><h:reason/></i>
    </p>
  </h:dead>
  <p>
    Categories:
    <h:categories>
      <span h:description="" h:class=""><h:name/></span>
    </h:categories>
  </p>
  <div>
    <h:description/>
  </div>
  <h:ifPage name="info.html">
    <div id="archivePages" class="script">
      <span id="currentpagemarker" class="marker">Viewing</span>
      <span id="bookmarkmarker" class="marker">Bookmark</span>
      <div id="archivedialog" title="Archive">
	<table>
	  <thead>
	    <tr>
	      <th>#</th>
	      <th class="page">Page</th>
	    </tr>
	  </thead>
	  <tbody/>
	</table>
      </div>
      <div id="thumbdialog" title="Thumbnails">
      </div>
    </div>
  </h:ifPage>
</h:comicInfo>
