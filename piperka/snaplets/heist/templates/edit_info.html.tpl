<h:piperka title="Edit ${h:comicWord} information">
  <h:submit mode="Edit">
    <h:hasCid>
      <h:found>
	<h2>Edit <h:comicWord/> information</h2>
	Submissions will enter the moderation queue.
	<p>
	  For issues other than info page edits, please open a ticket
	  instead.
	</p>
	<h:csrfForm class="submitcomic">
	  <input type="hidden" name="formtype" value="editinfo"/>
	  <input type="hidden" name="cid" value="${h:cid}"/>
	  Title: <h:title/>
	  <p>Home page: <h:homepage/></p>
	  <h:submitForm/>
	</h:csrfForm>
      </h:found>
    </h:hasCid>
  </h:submit>
</h:piperka>
