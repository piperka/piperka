<h:piperka title="Support Comics" minifiable="True">
  <h2>Support Comics</h2>
  <h:ifLoggedIn>
    <p>
      <h:siteName/> wouldn't amount to much anything without
      <h:comicWord/> authors and their work.  This page has a list of
      <h:comicWord/>s which you are subscribed to and which have
      Patreon or Ko-fi pages recorded for them.
    </p>
    <h:listing mode="Support"/>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    This page shows a list of user's <h:comicWord/>s that they have
    subscribed to which have a Patreon or Ko-fi link associated with
    them.  You are not logged in so there's nothing to show.
  </h:ifLoggedOut>
</h:piperka>
