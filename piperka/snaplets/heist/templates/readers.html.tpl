<h:piperka title="Readers">
  <h:readers>
    <h:positive>
      <h2>People subscribed to <h:title/></h2>
      <p>
	These people have subscribed to
	<a href="info.html?cid=${h:cid}"><h:title/></a> and have their
	profiles set public<h:ifLoggedIn> or shared it with
	you</h:ifLoggedIn>.
      </p>
      <ul>
	<h:list>
	  <li><a href="profile.html?name=${h:userURL}" h:passive=""><h:user/></a></li>
	</h:list>
      </ul>
      <p>
	<h:total/> in all.
      </p>
    </h:positive>
  </h:readers>
</h:piperka>
