<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title><h:siteName/> Reader (beta)</title>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="https://${h:hostname}/reader.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"/>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://${h:hostname}/viewarchive.js"></script>
    <script src="https://${h:hostname}/reader.js"></script>
  </head>

  <body>
    <span id="currentpagemarker" class="marker">Viewing</span>
    <span id="bookmarkmarker" class="marker">Bookmark</span>
    <span id="readingmarker" class="marker">Now reading</span>
    <div id="moreoptions" class="script">
      <br/>
      <span title="Height fixing enables more options for how to display the embedded page.">
	<label for="fixiframe">Fix iframe height</label> <input type="checkbox" id="fixiframe">
      </span>
      <span id="withfix" class="defaultHidden">
	<br/>
	<span id="fixheight">
	  Fix height at <input type="text"><button>Set</button> (Now: <span>2000</span>)
	</span>
	<br/>
	<label for="lockselect">Lock page position </label> <input type="checkbox" id="lockselect" checked="1">
	<input type="text" id="lockheight" value="0" readonly="1" title="Uncheck lock and scroll the embedded page to change the value.">
	<br/>
	<label for="arrownavigate">Bind left/right arrow</label><input type="checkbox" id="arrownavigate">
      </span>
    </div>
    <div id="navigation">
      <span>
	<span id="logo"><h:siteName/> Reader <span class="beta">beta</span></span>
	| <button id="first" disabled="1">First</button>
	<button id="prev" disabled="1">Prev</button>
	<button id="next" disabled="1">Next</button>
	<button id="current" disabled="1">Current</button>
	| <button id="archive" disabled="1">Archive</button>
	<span class="user">| <button id="nextcomic">Next <h:comicWord capital="True"/></button> <button id="mycomics">My <h:comicWord capital="True"/>s</button>
	  <label for="autoupdate">autoupdate</label><input type="checkbox" id="autoupdate" checked="1"/></span>
	| <span id="title"></span>
	| <div id="pagecount">Page <span id="pagenum"></span> / <span id="pagetotal"></span></div>
	<a href="https://${h:hostname}/" title="Return to ${h:siteName}"><h:site site="Piperka">P</h:site><h:site site="Teksti">T</h:site></a> <a id="tocomic" title="Exit to ${h:comicWord}">&darr;</a>
      </span>
      <form>
	<input type="text" name="url" id="url"/>
      </form>
    </div>
    <div id="discover">
      Discover mode | <a id="info-page" target="info-page" data-base="https://${h:hostname}/info.html?cid=">view entry on <h:siteName/></a>
      | <input type="checkbox" name="discover-blur-nsfw" id="discover-blur-nsfw" checked="1"/>
      <label for="discover-blur-nsfw">Blur nsfw</label>
      <button id="discover-next" disabled="1">Discover next</button>
      Next: <span id="discover-next-name"/><span class="beta" id="discover-next-nsfw">nsfw</span>
    </div>
    <div id="container">
      <div id="reader" tabindex="1000">
	<h:bookmark>
	  <h:success>
	    <h:result>
	      <span id="bookmarkResult" h:bookmarkData=""/>
	    </h:result>
	  </h:success>
	  <h:multiple>
	    <div>
	      <h2>Multiple matches</h2>
	      The URL you submitted matched with multiple comics.
	      <ul>
		<h:item>
		  <li><a href="/reader/?cid=${h:cid}${h:maybeOrd}"><h:title/></a></li>
		</h:item>
	      </ul>
	    </div>
	  </h:multiple>
	  <h:recognized>
	    <div>
	      <h2>No page match</h2>
	      <p>
		The URL you submitted failed to match with an archive
		page in the database.  Click to view the <h:comicWord/>:
		<a href="/reader/?cid=${h:cid}"><h:title/></a>.
	      </p>
	    </div>
	  </h:recognized>
	  <h:failed>
	    <div>
	      <h2>No match</h2>
	      <p>
		The URL you submitted failed to match any
		${h:comicWord}.  Sorry.
	      </p>
	    </div>
	  </h:failed>
	</h:bookmark>
      </div>
      <div id="nsfw-container">
	<div>
	  <div id="nsfw-message">NSFW. Click to view.</div>
	</div>
      </div>
    </div>
    <div id="archivePages" class="defaultHidden">
      <div id="archivedialog" title="Archive">
	<table>
	  <thead>
	    <tr>
	      <th>#</th>
	      <th class="page">Page</th>
	    </tr>
	  </thead>
	  <tbody/>
	</table>
      </div>
      <div id="thumbdialog" title="Thumbnails">
      </div>
    </div>
  </body>
</html>
