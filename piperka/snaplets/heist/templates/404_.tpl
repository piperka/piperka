<h:piperka title="404">
  <h2>Page not found</h2>
  <p>
    Oops.  No such page is found on <h:siteName/>.
  </p>
</h:piperka>
