<h:piperka title="My ${h:comicWord}s" ads="False">
  <h2>My <h:comicWord/>s - author</h2>
  <h:ifLoggedIn>
    <h:isAuthor>
      <h:author>
	<p>
	  You have been registered as an author on <h:siteName/>.  As
	  of now, that allows direct edits to <h:comicWord/>s' titles,
	  descriptions and tags and some control over the crawler
	  runs.

	  I would ask you to read
	  the <a href="moderator_policy.html">moderator policy</a>
	  before making updates to the description (it's not long).
	</p>
	<h:authorAction>
	  <h:nameUpdated>
	    Display name updated.
	  </h:nameUpdated>
	</h:authorAction>
	<h3>Display name</h3>
	<h:withAuthored>
	  <h:csrfForm method="POST">
	    Name (empty to hide from
	    the <a href="authors.html">authors list</a>):

	    <input type="text" name="authorName" value="${h:authorName}" maxlength="100"/>
	    <input type="submit" name="submit" value="Update"/>
	  </h:csrfForm>
	  <p>
	    <h:comicWord capital="True"/>s that you have been
	    registered to as an author.
	  </p>
	  <table>
	    <tr>
	      <th>CID</th>
	      <th>Title</th>
	      <th>Info</th>
	    </tr>
	    <h:authoredComics>
	      <tr>
		<td><h:cid/></td>
		<td><a href="authored.html?cid=${h:cid}"><h:title/></a></td>
		<td><a href="info.html?cid=${h:cid}">Info</a></td>
	      </tr>
	    </h:authoredComics>
	  </table>
	</h:withAuthored>
      </h:author>
    </h:isAuthor>
    <h:isNotAuthor>
      <p>
	You haven't been registered as an author.  Nothing to see
	here.
      </p>
    </h:isNotAuthor>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    <p>
      Oh come on, you haven't even logged in.  Nothing to see here.
    </p>
  </h:ifLoggedOut>
</h:piperka>
