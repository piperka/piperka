<h:site site="Piperka">
  <h:ifLoggedIn>
    <a href="/sso_redir.html?csrf_ham=${h:csrf}">Teksti</a>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    <a href="https://${h:devPrefix}teksti.eu/">Teksti</a>
  </h:ifLoggedOut>
</h:site>
<h:site site="Teksti">
  <h:ifLoggedIn>
    <a href="/sso_redir.html?csrf_ham=${h:csrf}">Piperka</a>
  </h:ifLoggedIn>
  <h:ifLoggedOut>
    <a href="https://${h:devPrefix}piperka.net/">Piperka</a>
  </h:ifLoggedOut>
</h:site>
