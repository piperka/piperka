<h:piperka>
  <h:site site="Piperka"><h2>Redirecting to Teksti</h2></h:site>
  <h:site site="Teksti"><h2>Redirecting to Piperka</h2></h:site>
  <h:ifLoggedOut>
    <p>
      As a convenience, this page offers redirects to <h:siteName/>'s
      twin site which automatically log the user in to it.  But you
      aren't logged in.
    </p>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <h:csrfProtected>
      <h:if>
	<h:site site="Piperka">
	  <h:csrfForm action="https://${h:devPrefix}teksti.eu/" method="POST" id="sso">
	    <input type="hidden" name="sso" value="${h:SSOSeed}"/>
	    <p>
	      If you aren't automatically redirected to Teksti, click
	      <button type="submit">here</button>.
	    </p>
	  </h:csrfForm>
	</h:site>
	<h:site site="Teksti">
	  <h:csrfForm action="https://${h:devPrefix}piperka.net/" method="POST" id="sso">
	    <input type="hidden" name="sso" value="${h:SSOSeed}"/>
	    <p>
	      If you aren't automatically redirected to Piperka, click
	      <button type="submit">here</button>.
	    </p>
	  </h:csrfForm>
	</h:site>
      </h:if>
      <h:if check="False">
	<p>
	  <a href="/sso_redir.html?csrf_ham=${h:csrf}">Click here</a>
	  to go to
	  <h:site site="Piperka">Teksti</h:site>
	  <h:site site="Teksti">Piperka</h:site> with SSO.
	</p>
      </h:if>
    </h:csrfProtected>
  </h:ifLoggedIn>
</h:piperka>
