<span class="update-info">
  <h:schedule><span class="schedule-letters"><h:letters><h:present><span h:class=""><h:letter/></span></h:present><h:missing><span class="no-update">_</span></h:missing></h:letters></span></h:schedule>
  <h:weekly>
    <h:once>Weekly</h:once>
    <h:multi><h:num/> times a week</h:multi>
  </h:weekly>
  <h:monthly>
    <h:once>Monthly</h:once>
    <h:multi><h:num/> times a month</h:multi>
  </h:monthly>
  <h:reason><h:msg/></h:reason>
</span>
