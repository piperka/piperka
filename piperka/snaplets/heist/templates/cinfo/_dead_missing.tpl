<p>
  I'm the message that tells you about this page being the page that
  shows some details about a comic formerly listed on Piperka.  But
  either you gave no <i>cid</i> or it is not used and you'll see no
  comic, only me.
</p>
<p>
  Click on the side bar to browse the comic list.
</p>
