<h:piperka prefetch="Info">
  <h:isRandom>
    <h:randomComicButton/>
  </h:isRandom>
  <h:statusEdited>
    <p>
      <h:success>
	Status edit successfully submitted to the moderator queue.
      </h:success>
      <h:failure>
	Status edit failed.  Please try again or contact me if this
	persists.
      </h:failure>
    </p>
  </h:statusEdited>
  <h:withCid>
    <h:exists>
      <h:mobile mode="Desktop">
	<h:related/>
	<div class="script chart" id="readerchart"></div>
      </h:mobile>
      <h:comicInfo/>
      <h3>Actions</h3>
      <ul>
	<li><a href="edit_info.html?cid=${h:cid}">Edit information</a></li>
	<h:ifLoggedIn>
	  <h:ifSubscribed>
	    <false>
	      <li>
		<h:subscribeForm/>
	      </li>
	      <li>
		<h:csrfForm method="post">
		  <input type="hidden" name="reject" value="${h:cid}"/>
		  <input type="submit" name="action" value="Reject ${h:comicWord}"/>
		</h:csrfForm>
	      </li>
	    </false>
	    <true>
	      <li>
		<h:csrfForm method="post">
		  <input type="hidden" name="unsubscribe" value="${h:cid}"/>
		  <input type="submit" name="action" value="Unsubscribe"/>
		</h:csrfForm>
	      </li>
	    </true>
	    <rejected>
	      <li>
		<h:csrfForm method="post">
		  <input type="hidden" name="unreject" value="${h:cid}"/>
		  <input type="submit" name="action" value="Unreject ${h:comicWord}"/>
		</h:csrfForm>
	      </li>
	    </rejected>
	  </h:ifSubscribed>
	</h:ifLoggedIn>
	<li><a href="http://${h:hostname}/reader/?cid=${h:cid}">View in <h:siteName/> Reader</a>
	<h:ifMapped>
	  <li><a href="/map/?cid=${h:cid}">View on <h:siteName/> Map</a></li>
	</h:ifMapped>
	<li><a href="ticket.html?cid=${h:cid}">Open ticket</a></li>
	<h:ifLoggedIn>
	  <li><a href="/my_tickets.html">My tickets</a></li>
	</h:ifLoggedIn>
	<h:notMod forbid="False">
	  <li><a href="edit_status.html?cid=${h:cid}">Hiatus/completion status</a></li>
	</h:notMod>
	<h:notCidAuthor>
	  <li><a href="claim.html?cid=${h:cid}">Claim <h:comicWord/></a></li>
	</h:notCidAuthor>
      </ul>
      <h:ifLoggedIn>
	<h:ifMod>
	  <h3>Moderator</h3>
	  <ul>
	    <li><a href="mod_status.html?cid=${h:cid}">Hiatus status control</a></li>
	  </ul>
	</h:ifMod>
	<h:ifMod level="Admin">
	  <h3>Admin</h3>
	  <ul>
	    <li><a href="mod_crawler.html?cid=${h:cid}">Crawler</a></li>
	    <li><a href="mod_morgue.html?cid=${h:cid}">Morgue</a></li>
	  </ul>
	</h:ifMod>
	<h:isCidAuthor>
	  <h3>Author</h3>
	  <ul>
	    <h:site site="Piperka">
	      <li><a href="author_comic.html?cid=${h:cid}">Author page</a></li>
	    </h:site>
	  </ul>
	</h:isCidAuthor>
      </h:ifLoggedIn>
      <h:mobile>
	<h:related/>
	<div class="script chart" id="readerchart"></div>
      </h:mobile>
      <h:crawlErrors>
	<h3>Crawl errors</h3>
	<p>
	  The last 5 crawl errors during the last 30 days.  Having
	  this empty doesn't necessarily imply that there isn't
	  something wrong with the crawler.  I'll go through these
	  eventually but I don't mind if you ask me to check whether
	  the crawler's doing the right thing.
	</p>
	<table id="crawlerr">
	  <tr>
	    <th>Page order</th>
	    <th>Time</th>
	    <th>URL</th>
	    <th colspan="2">HTTP status</th>
	  </tr>
	  <h:rows>
	    <tr>
	      <td><h:ord/></td>
	      <td><h:time/></td>
	      <td><h:url/></td>
	      <td><h:code/></td>
	      <td><h:msg/></td>
	    </tr>
	  </h:rows>
	</table>
      </h:crawlErrors>
    </h:exists>
  </h:withCid>
</h:piperka>
