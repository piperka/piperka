<h:item type="UserMode">
  <li h:freqClass="">
    <h:mobile mode="Desktop">
      <h:summaryRow/>
      <h:subscribed>
	<button class="minus" name="unsubscribe" value="${h:cid}" type="submit">-</button>
      </h:subscribed>
      <h:unsubscribed>
	<button class="plus" name="subscribe" value="${h:cid}" type="submit">+</button>
      </h:unsubscribed>
      <h:rejected>
	<button class="reject">x</button>
      </h:rejected>
      <h:isNew>(new)</h:isNew>
      <a href="info.html?cid=${h:cid}"><h:title/></a>
      <h:followee>
	<a href="readers.html?cid=${h:cid}" class="followee">F</a>
      </h:followee>
    </h:mobile>
    <h:mobile mode="Mobile">
      <h:subscribed>
	<button class="minus" name="unsubscribe" value="${h:cid}" type="submit">-</button>
      </h:subscribed>
      <h:unsubscribed>
	<button class="plus" name="subscribe" value="${h:cid}" type="submit">+</button>
      </h:unsubscribed>
      <h:rejected>
	<button class="reject">x</button>
      </h:rejected>
      <span class="li">
	<h:isNew>(new)</h:isNew>
	<a href="info.html?cid=${h:cid}"><h:title/></a>
	<h:summaryRow/>
      </span>
    </h:mobile>
  </li>
</h:item>
<h:item type="ListingMode">
  <li h:freqClass="">
    <h:summaryRow/>
    <a href="info.html?cid=${h:cid}"><h:title/></a>
  </li>
</h:item>
