<p>
  <h:comicWord capital="True"/>s you are subscribed to that haven't
  had their bookmark positions changed for 14 days (by you but
  possibly also by site admin due to maintenance) and which have 50 or
  more unread pages will be hidden from the default updates view.
</p>
