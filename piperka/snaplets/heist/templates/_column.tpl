<h:listingMode type="[Top,Recommend]">
  <ol h:startNum="">
    <h:listingStdItem/>
  </ol>
</h:listingMode>
<h:listingMode type="[Browse,Profile]">
  <ul class="list">
    <h:listingStdItem/>
  </ul>
</h:listingMode>
<h:listingMode type="[Update]">
  <ul class="list">
    <h:item type="UpdateMode">
      <h:holdbookmark check="True">
	<li h:class="booklink" id="c${h:cid}" data-cid="${h:cid}" data-unread="${h:new}">
	  <h:externA href="${h:directLink}"><h:title/></h:externA>
	  (<h:new/> new)
	</li>
      </h:holdbookmark>
      <h:holdbookmark check="False">
	<li h:class="bookredir" data-cid="${h:cid}" data-unread="${h:new}">
	  <h:externA href="/?redir=${h:cid}&amp;csrf_ham=${h:csrf}${h:offsetBackParam}"><h:title/></h:externA>
	  (<h:new/> new)
	</li>
      </h:holdbookmark>
    </h:item>
  </ul>
</h:listingMode>
<h:listingMode type="[Graveyard]">
  <ul class="list">
    <h:item type="ListingMode">
      <li>
	<button class="script null" value="${h:cid}">&nbsp;</button>
	<a href="deadinfo.html?cid=${h:cid}"><h:title/></a>
      </li>
    </h:item>
  </ul>
</h:listingMode>
<h:listingMode type="[Revert]">
  <ul class="list">
    <h:item type="RevertMode">
      <li>
	<input type="checkbox" name="revert" h:id="id"/>
	<label h:id="for">
	  <h:title/>
	  <h:time div="86400">(<h:tick/> day<h:plural/> ago)</h:time>
	  <h:time div="3600" max="24">(<h:tick/> hour<h:plural/> ago)</h:time>
	  <h:time div="60" max="60">(<h:tick/> minute<h:plural/> ago)</h:time>
	  <h:time div="1" max="60">(<h:tick/> second<h:plural/> ago)</h:time>
	</label>
      </li>
    </h:item>
  </ul>
</h:listingMode>
<h:listingMode type="[Support]">
  <ul class="list">
    <h:item type="SupportMode">
      <li>
	<h:patreon>
	  <h:externA class="supportAuthor" href="https://www.patreon.com/${h:page}">Patreon</h:externA>
	</h:patreon>
	<h:kofi>
	  <h:externA class="supportAuthor" href="https://ko-fi.com/${h:page}">Ko-fi</h:externA>
	</h:kofi>
	<h:summaryRow/>
	<a href="info.html?cid=${h:cid}"><h:title/></a>
      </li>
    </h:item>
  </ul>
</h:listingMode>
