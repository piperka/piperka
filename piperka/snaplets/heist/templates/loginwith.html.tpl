<h:piperka title="Login with...">
  <h3>Login with...</h3>
  <p>
    If you don't have an account associated with a login provider yet,
    you'll be prompted to enter an account name afterwards to create a
    new account.
  </p>
  <ul class="plain">
    <h:providers>
      <li>
	<a h:href="/apiAuth/oauth2login/"><h:name/></a>
      </li>
    </h:providers>
  </ul>
</h:piperka>
