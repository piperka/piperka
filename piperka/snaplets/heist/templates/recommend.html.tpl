<h:piperka title="Recommended comics">
  <h2>Recommended comics</h2>
  <h:randomComicButton/>
  <h:ifLoggedOut>
    <p>
      This page would show personal recommendations.  Log in to see
      your recommendations.
    </p>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <p>
      Show results among
      <a href="recommend.html">all <h:comicWord/>s</a> | last
      <a href="recommend.html?newest=200">200</a> /
      <a href="recommend.html?newest=500">500</a> /
      <a href="recommend.html?newest=1000">1000</a> /
      <a href="recommend.html?newest=2000">2000</a> added <h:comicWord/>s.
    </p>
    <p>
      <a href="http://${h:hostname}/reader/?discover=recommend${h:newest}">View
      in discover mode</a>.
    </p>
    <h:recommend>
      <h:tooFew>
	<p>
	  Please subscribe to at least 5 <h:comicWord/>s to help the
	  recommendation algorithm.  If you have already done so and
	  this problem persists, try again later or contact me.
	</p>
      </h:tooFew>
      <h:canRecommend>
	<p>
	  Recommendations are recomputed every five minutes.
	</p>
	<h:listing mode="Recommend"/>
      </h:canRecommend>
    </h:recommend>
  </h:ifLoggedIn>
</h:piperka>
