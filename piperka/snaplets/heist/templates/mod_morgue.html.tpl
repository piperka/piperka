<h:piperka ads="False" title="Morgue actions">
  <h:ifLoggedIn>
    <h:ifMod level="Admin">
      <h:graveyard>
	<isComic>
	  <h2><h:title/></h2>
	  <h:alive>
	    <h:csrfForm method="POST">
	      <input type="hidden" name="morgue" value="bury"/>
	      The <h:comicWord/> is alive.
	      <h3>Remove</h3>
	      <p>
		<label for="reason">Reason</label>
		<input type="text" id="reason" name="reason" style="width: 60em"/>
	      </p>
	      <p>
		<button type="submit">Bury</button>
	      </p>
	    </h:csrfForm>
	    <h:csrfForm method="POST">
	      <input type="hidden" name="morgue" value="merge"/>
	      <h3>Merge into</h3>
	      <p>
		<label for="merge_into">Merge into</label>
		<input type="number" name="merge_into" id="merge_into"/>
		<a target="_blank" id="merge_target_link">target</a>
	      </p>
	      <p>
		<button type="submit">Merge</button>
	      </p>
	    </h:csrfForm>
	  </h:alive>
	  <h:dead>
	    <h:csrfForm method="POST">
	      <input type="hidden" name="morgue" value="dig"/>
	      The <h:comicWord/> is dead.
	      <p>
		<i><h:reason/></i>
	      </p>
	      <p>
		<button type="submit">Dig</button>
	      </p>
	    </h:csrfForm>
	  </h:dead>
	</isComic>
	<processed>
	  <h:alive>
	    <a href="info.html?cid=${h:cid}"><h:comicWord capital="True"/> revived</a>
	    <a href="mod_crawler.html?cid=${h:cid}">Crawler</a>
	  </h:alive>
	  <h:merged>
	    Comic merged
	  </h:merged>
	  <h:dead>
	    <a href="deadinfo.html?cid=${h:cid}"><h:comicWord capital="True"/> retired</a>
	    <h:csrfForm method="POST" action="mod_ticket.html">
	      <input type="hidden" name="cid" value="${h:cid}"/>
	      <input type="hidden" name="close_ticket" value="1"/>
	      <input type="hidden" name="to_close" value="all"/>
	      <input type="hidden" name="resolve_msg" value="Removed"/>
	      <button type="submit">Close all tickets with "Removed"</button>
	    </h:csrfForm>
	  </h:dead>
	  <h:undead>
	    Brains.
	  </h:undead>
	</processed>
	<noComic>
	  No such <h:comicWord/> or no cid given.
	</noComic>
      </h:graveyard>
    </h:ifMod>
  </h:ifLoggedIn>
  <h:notMod level="Admin">
    This page is for admin use only.
  </h:notMod>
</h:piperka>
