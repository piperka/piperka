<h:piperka title="Comic statuses">
  <h2><h:comicWord capital="True"/> completion status</h2>
  <p>
    <h:siteName/> tries to make it visible whether a <h:comicWord/>
    gets updates and if it does, when.  This part can pretty much be
    automated from the crawler events.
  </p>
  <p>
    When <h:comicWord/>s aren't updating, it can be for a variety of
    reasons.  Some <h:comicWord/>s get completed.  Some get abandoned.
    Some end up on a hiatus that extends from months to years.  It's a
    pretty tall order for the crawler to tell by itself which fate
    befell to any particular <h:comicWord/>.  It won't even try but
    any <h:comicWord/>s that have been suspiciously quiet get flagged
    and a moderator will pick an explanation.
  </p>
  <p>
    There are plenty of <h:comicWord/>s listed on <h:siteName/> that
    were abandoned many years ago.  If they drop from the Internet
    then it's an easy call to remove them from <h:siteName/>'s end as
    well but there's still plenty of other <h:comicWord/>s that are
    still online but won't amount to much but old curiosites for
    someone up to a bit of reminiscing.  I reckon it's fine to have
    those still listed as long as they are marked appropriately.
  </p>
  <p>
    If you think that this list of reasons is incomplete then please
    let me know.  If you think that a particular <h:comicWord/> needs
    to have it's reason updated or would like to expedite the process
    for a particular unmarked <h:comicWord/> then please contact me.
    There isn't yet any mechanism for flagging those via <h:siteName/>
    itself.  <h:ifMod>But moderators like you can do so directly
    already.</h:ifMod>  If a <h:comicWord/> has been marked with a
    status but it should be receiving updates then please open a
    ticket.
  </p>
  <h3>List of statuses</h3>
  <h:hiatusExplanation/>
</h:piperka>
