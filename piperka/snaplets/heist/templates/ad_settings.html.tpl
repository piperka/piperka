<h:piperka title="Ad settings" ads="False">
  <h2>Ad settings</h2>
  <h:site site="Piperka">
    <p>
      Piperka uses <a href="https://www.comicad.net/?r=FkHGi45h">Comic
      Ad Network</a> for advertising.  As the name implies, they're an
      ad network that focuses on web comics.  They don't try to
      profile or track you either but collect just simple impression
      and click statistics.
    </p>
    <p>
      Still, in the spirit of letting you control what's running in
      your own browser, I'm giving you the choice of opting out of
      them.  Though I recommend to just leave them be, you may even
      see a web comic that'd match your interests on them.
    </p>
  </h:site>
  <h:site site="Teksti">
    <p>
      Teksti uses third party opt out advertising.  Ad revenue helps
      to run the site but as they're much more likely to be
      personalized and targeted rather than content based I'll rather
      give you the choice of just disabling them.  Saving you the
      trouble of even using an ad blocker.
    </p>
    <p>
      If you don't mind seeing some ads then leave them
      unblocked.  <a href="privacy_plicy.html#ads">Please review the
      privacy policy on third party ads if you like first</a> (it's
      the usual boilerplate about third party cookies and such).  Your
      choice on seeing ads is stored as a cookie on your browser.  If
      you'd like to change your setting later, delete the cookie or
      click the link to this page on the footer.
    </p>
  </h:site>
  <h:ifLoggedIn>
    <p>
      <h:siteName/> can also be found
      on <a href="https://www.patreon.com/kaol">Patreon</a>
      and <a href="https://liberapay.com/kaol">Liberapay</a>.  If
      that'd be your thing.
    </p>
  </h:ifLoggedIn>
  <h:adsEnabledForSite check="False">
    <p>
      <h:siteName/> doesn't currently run ads on it.  I'm considering
      my options at the moment.  You may see and change your opt out
      status on this page but it doesn't affect anything at the
      moment.
    </p>
  </h:adsEnabledForSite>
  <h:autoAdChoice>
    <span class="ads-question">
      <div>
	<input type="radio" id="ads_disable" name="ads"
	       value="0" h:checked="False"/>
	<label for="ads_disable">Disable third party ads</label>
      </div>
      <div>
	<input type="radio" id="ads_enable" name="ads"
	       value="1" h:checked="True"/>
	<label for="ads_enable">Enable third party ads</label>
      </div>
      <button class="script show">Submit</button><span class="message"/>
      <noscript>
	Changing ad settings requires JavaScript which you seem to
	have disabled.  Seeing them requires JavaScript as well so I
	suppose we're fine.
      </noscript>
    </span>
  </h:autoAdChoice>
</h:piperka>
