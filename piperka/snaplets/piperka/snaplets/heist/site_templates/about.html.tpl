<h:piperka title="About">
  <h2>About Piperka</h2>
  <h3>What is Piperka?</h3>
  <p>
    It works as a plain old web comic list and there's a top list
    built from user subscriptions too.  But those things are not the
    site's main focus.
  </p>
  <p>
    It also helps you keep track of comics.  It tells you what comics
    have updated since your last visit and keeps bookmarks for you.
    You can input it an archive page's address and Piperka will match
    it with a comic and a page.  Piperka runs a crawler which
    periodically checks on webcomics to seek for new pages and adds
    them to its index if it finds any.
  </p>
  <h3>What kinds of comics does Piperka keep track of?</h3>
  <p>
    Generally anything that has a linear archive with navigation
    buttons with distinct, permanent addresses for archive pages.
    Piperka's crawler acts much like a human who returns to visit the
    last page to see if the page's changed to add a link to an update.
  </p>
  <h3>Does Piperka do hotlinking?</h3>
  <p>
    No.
  </p>
  <p>
    The usual form of function for Piperka is to offer redirect links
    which update the user's bookmark and take the user's browser to
    the comic's own archive.
  </p>
  <h3>I don't want to be listed on Piperka!</h3>
  <p>
    Are you sure?  Piperka doesn't copy any of the webcomic's contents
    (save for the URLs of their archives).  I would expect that having
    your comic listed on Piperka would only bring you more readers.
  </p>
  <p>
    That said, just send me a message if you don't want to be listed
    in here and I'll remove your comic.  We don't keep anyone here
    against their will.
  </p>
  <h3>What do you do with users' email addresses?</h3>
  <p>
    Password recovery.  Piperka can also, if requested, send you an
    email when a comic has been added.  I won't give them to anyone
    else.
  </p>
  <h3>How often does the site update?</h3>
  <p>
    The update job is ran hourly, though only a portion of the comics
    are checked on each run.  It tries to act intelligently and be
    efficient yet timely.  With varying success.
  </p>
  <p>
    The update job may be thwarted by badly timed net outages and the
    comic sites may change their archive structure.  The scripts are
    not infallible.  Normally an update to a comic should show within
    a day at Piperka, but if they seem to be absent for longer than
    that let us know and we'll investigate.  I've written
    a <a href="/blog/2011/crawler-exposed/">blog post</a> that covers
    more detail about what the crawler does.
  </p>
  <h3>Can I use RSS?</h3>
  <p>
    All the comics are provided with an RSS feed as well.  Though a
    word of warning, they're pretty bare bones and if the comic has
    its own RSS you're most likely better off by using it.  The latest
    entry on them has a time stamp from the last successful crawl
    which may or may not match with their actual publishing time.
    Please don't send me corrections if they don't match.  As with
    everything else, the service is provided as is with not
    guarantees.
  </p>
  <p>
    If you're a comic author and your own publishing platform doesn't
    provide one, you're welcome to link to Piperka's feed.  Giving
    credit would be appreciated if you do.  If you just want to keep
    an eye on how well Piperka catches your updates without actively
    using Piperka then this could be the option for you.
  </p>
  <p>
    Or if you're just on the fence about whether to create an account
    or not, you can start with just this and come back later to get
    one if you like.  It comes with recommendations and other
    features.
  </p>
  <h3>Is there an app?</h3>
  <p>
    Why, yes.
  </p>
  <span id="google-badge">
    <a href='https://play.google.com/store/apps/details?id=net.piperka.client&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='/images/google-play-badge.png'/></a>
  </span>
  <p class="legalese">
    Google Play and the Google Play logo are trademarks of Google LLC.
  </p>
  <h3>What does Piperka run on?</h3>
  <p>
    Debian GNU/Linux, on a VPS
    at <a href="https://upcloud.com/signup/?promo=427J6P">UpCloud</a>.
    The pages are served
    by <a href="http://snapframework.com/">Snap</a> with pages
    generated
    by <a href="https://hackage.haskell.org/package/heist">Heist</a>
    library.  The database is PostgreSQL and the crawler's written in
    Haskell with parsing done with Perl.  Most of the software stack
    is free software and available
    at <a href="https://gitlab.com/piperka">Gitlab</a>.
  </p>
  <h3>How do I contact you?</h3>
  <p>
    My email is at the bottom of the page.  Piperka can be found
    on <a href="https://discord.gg/HBXfvDw">Discord</a> and IRC is an
    option, you can find me on #piperka on OFTC.  If you have bug
    reports or development ideas, then Gitlab tickets would be the
    place.  The preferred way of telling me about comic specific
    issues is via the on site ticket system, which can be accessed on
    the per comic info page.
  </p>
  <h3>Piperka? Paprika? Pepper?</h3>
  <p>
    When I started working on this site, I just called it "subscribe".
    I had a quick look at the respective domain name, subscribe.com
    and saw that there was no such server yet.  Later on I had a
    closer look and found out that the name was actually reserved, by
    yet another domain hoarder, just not one that had set up a "this
    domain is for sale" web page for it.  Hence I was sent to look for
    names elsewhere.  And I looked.  And I looked.
  </p>
  <p>
    Finally, I was looking for various terms around paprikas and
    finally found one of them free.  "Piperka" is pepper in Bulgarian,
    even though I have no personal connection to the country.
  </p>
  <p>
    I had had a friend take an image of those paprikas long before I
    thought of a name for the site.  The yellow one was cut into
    slices and eaten with bread and cheese, the rest went into a
    pizza.
  </p>
</h:piperka>
