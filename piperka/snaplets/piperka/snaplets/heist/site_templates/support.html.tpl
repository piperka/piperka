<h:piperka title="Support" ads="False">
  <h2>Support Piperka</h2>
  <h:ifLoggedOut>
    <p>
      This is rather embarrassing.  There's nothing secret on this
      page as such.  But I'd still rather only show it to my
      registered users.
    </p>
  </h:ifLoggedOut>
  <h:ifLoggedIn>
    <p>
      I may have used a plural pronoun somewhere on Piperka but it's
      only for effect, it's nearly all done by just me, Kari Pahula.
      If you like what Piperka is about, there are ways you can help
      it.
    </p>
    <p>
      Running Piperka costs money and I put in a lot of work towards
      it, if you don't mind me saying.  Though I want to make it clear
      that using Piperka is and will remain free and I won't think any
      less of you if you never contribute anything.  I won't consider
      putting any site features behind a pay wall.  I'm not offering
      any specific returns, rewards or services if you choose to
      donate but if you like what I'm doing and would like me to do
      more of it then please consider doing so.
    </p>
    <h3>Donations</h3>
    <p>
      You can find me
      on <a href="https://www.patreon.com/kaol">Patreon</a>
      or <a href="https://liberapay.com/kaol">Liberapay</a>.  Either
      is fine but if you're a EU resident then I would advise using
      the latter since Patreon takes VAT and I can't reimburse it
      currently.  It's questionable how what I'm doing would
      constitute a sale anyway since you're getting the exact same
      service for free.  It's not about avoiding taxes either, I do
      report this as taxable income.  Anyway.  There's no way of
      avoiding transaction fees, especially if the sums are small.
    </p>
    <p>
      <script src="https://liberapay.com/kaol/widgets/button.js"></script>
      <noscript><a href="https://liberapay.com/kaol/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
      <a href="https://www.patreon.com/bePatron?u=3993722" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
    </p>
    <h3>Ads</h3>
    <p>
      Piperka features opt out advertising.  If you don't mind seeing
      some ads then <a href="ad_settings.html">you can leave them
      unblocked</a>.
    </p>
    <h3>Affiliate programs</h3>
    <p>
      Patreon has
      a <a href="https://patreon.com/invite/wkoryo">referral
      program</a>.  Joining them via the invite link and getting
      patrons would get both of us a bonus.
    </p>
    <p>
      Piperka's currently hosted at UpCloud
      and <a href="https://upcloud.com/signup/?promo=427J6P">they have
      a referral program</a>.  Using that link and depositing at least
      $10 will give you $25 credit for your hosting costs and $50 for
      me.
    </p>
    <p>
      I'm using Payoneer and they have
      a <a href="https://share.payoneer.com/nav/BZeRz7vXujKAJN-tb9frmQCeTMLFwtHXIIqnCLox7KqvMz5anahBppckFBqt-MYkl_iITTGJBhBiddsFkF-TUw2">referral
      program</a> as well.
    </p>
    <h3>Support artists</h3>
    <p>
      Piperka would amount to nothing without authors who diligently
      work on their comics.  For your
      convenience, <a href="support_authors.html">here's a list of
      comics that you're subscribed to which have Patreon and/or Ko-fi
      links recorded for them</a>.
    </p>
    <h3>Other</h3>
    <p>
      The usual: If you find Piperka useful, tell your friends about
      it.  Piperka's source code is open source and I'm sure I could
      think of something for you to do if you're up to it and not
      averse of Haskell.  If there's some particular feature you'd
      like then I may be persuaded to add it if you code
      it.  <a href="https://gitlab.com/piperka">The source code</a> is
      hosted on GitLab.
    </p>
    <p>
      Finally, a word about Finnish law.
    </p>
    <div lang="fi">
      <p>
	Lopuksi sananen rahankeräyslaista, kun se usein nousee esiin
	nettikeskusteluissa verkkosivujen lahjoitusmahdollisuuksista,
	ja siitä miten minä sitä tulkitsen.  Siitä huolimatta että
	kerään vastikkeetonta rahaa, olen sitä mieltä että
	rahankeräyslaki ei koske Piperkaa, koska lain määritelmässä
	puhutaan yleisöön, eli ennalta määrittelemättömään joukkoon
	ihmisiä kohdistuvasta toiminnasta.  Jotain kickstartermaista
	kampanjointia välttäisin kun sen luonne olisi enemmän ilmiön
	herättelyä ja uusien ihmisten tavoittelua, mutta se että
	löydyn hakutoiminnolla parilta sivustolta ja pelkästään
	käyttäjille näytettävän linkin takaa omalta sivustoltani
	nähdäkseni tuskin täyttäisi tätä ehtoa.
      </p>
      <p>
	<a href="https://effi.org/blog/2009-02-10-Tapani-Tarvainen.html">Electronic
	Frontier Finland ry</a> on omalta osaltaan joutunut
	rahankeräyslain vuoksi menemään hovioikeuteen asti missä
	todettiin että verkkosivuilla lahjoitusmahdollisuuden
	tarjoaminen ei riitä rahankeräysrikoksen tunnusmerkiksi.
	Suomen lainsäädäntö ei ole niin ennakkotapauspainotteinen kuin
	jossain muualla mutta laskisin tämän silti tukevan omaa
	tulkintaani.
      </p>
    </div>
  </h:ifLoggedIn>
</h:piperka>
