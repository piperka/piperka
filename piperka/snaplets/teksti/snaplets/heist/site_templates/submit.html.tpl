<h:piperka title="Submit">
  <h:submit mode="Submit">
    <span class="hideafterdone">
      <h2 id="submit">Submit a web fiction</h2>
      <p>
	<h:csrfForm class="submitcomic script show">
	  <input type="hidden" name="formtype" value="submit" />
	  <table>
	    <tbody>
	      <tr>
		<td>Title:</td>
		<td><input type="text" size="60" name="title"/></td>
	      </tr>
	      <tr>
		<td>Home page:</td>
		<td><input type="text" size="100" name="url" value="${h:preHomepage}"/></td>
	      </tr>
	      <tr>
		<td>First page:</td>
		<td><input type="text" size="100" name="first_page"/></td>
	      </tr>
	    </tbody>
	  </table>
	  <p>
	    You don't need to provide a first page, but if you want to
	    save me a few seconds, then feel free to.  If the site's
	    navigation makes the URL in your browser's location bar
	    stay the same when you get to the first page, then the
	    web fiction might not be suitable for Teksti.
	  </p>
	  <h:submitForm/>
	</h:csrfForm>
    </span>

    <div class="noscript">
      Submitting web fictions requires JavaScript.  Please enable it.
    </div>

    <div id="msgdiv"/>
  </h:submit>
</h:piperka>
