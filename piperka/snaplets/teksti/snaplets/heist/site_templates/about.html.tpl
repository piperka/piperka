<h:piperka title="About">
  <h2>About Teksti</h2>
  <h3>What is Teksti?</h3>
  <p>
    On one level, it's a ranking list for web fiction and other
    textual content.  But that's not the main focus of the site.
  </p>
  <p>
    It's a bookmarking and tracking service.  It allows keeping
    bookmarks on their archives and it maintains a user specific web
    page of updates.  It doesn't download or host any of their content
    but redirects the user to their sites instead.
  </p>
  <h3>What kinds of web fictions does Teksti keep track of?</h3>
  <p>
    Generally any textual fiction that has a linear archive with
    navigation buttons with distinct, permanent addresses for archive
    pages.  Most anything that uses some sort of a blog software is
    fine.
  </p>
  <h3>What do you do with users' email addresses?</h3>
  <p>
    Password recovery.  Teksti can also, if requested, send you an
    email when a web fiction has been added.  I won't give them to
    anyone else.
  </p>
  <h3>How often does the site update?</h3>
  <p>
    The update job is ran hourly, though only a portion of the comics
    are checked on each run.  It tries to act intelligently and be
    efficient yet timely.  With varying success.
  </p>
  <h3>What's the deal with Piperka and Teksti?</h3>
  <p>
    Teksti is a twin of <a href="https://piperka.net/">Piperka</a>.
    It even shares the user database with it so if you have an account
    on one you have one on the other one as well.  They share most of
    their code and most of the differences are just cosmetic and
    content based.
  </p>
  <p>
    Piperka is for web comics and Teksti is for web fiction.
  </p>
  <h3>Texty? Teksti?</h3>
  <p>
    Teksti is pronounced like "texty".  It's Finnish for "text".  The
    language is a bit like a certain fruit company except it likes to
    stick the "i" at the end.
  </p>
  <p>
    The image is of my old cookbook.
  </p>
</h:piperka>
