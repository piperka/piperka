<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"/>
  <xsl:template match="description">
    <div class="description">
      <xsl:apply-templates />
    </div>
  </xsl:template>
  <xsl:template match="/">
    <html>
      <head>
	<title>
	  Piperka classifications of series non-update status
	</title>
      </head>
      <body>
	<dl>
	  <xsl:apply-templates />
	</dl>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="reason">
    <dt>
      <xsl:apply-templates select="name"/>
    </dt>
    <dd>
      <xsl:apply-templates select="description"/>
    </dd>
  </xsl:template>
</xsl:stylesheet>
