stickTitles = ['', 'Stickfigure Hero Begins', "Stickfigure Hero's Tea Time", 'YACP'];

$(document).ready(function(){
    var pages = [];
    var page = 0;
    var pagesTotal = $('#pages_total');
    var onPage = $('#on_page');
    var mainNext = $('#main_next');
    var mainPrev = $('#main_prev');
    var container = $('#tour_container');
    var tour = $('#tour');
    var stickPage = 1;
    var firstLoad = true;
    var pageChange = function() {
	if (firstLoad) {
	    container.empty();
	    container.append($(pages[page]).clone());
	    container.show();
	    firstLoad = false;
	} else {
	    container.effect({
		effect: 'fold',
		duration: 400,
		queue: false,
		complete: function() {
		    container.empty();
		    container.append($(pages[page]).clone());
		    container.show();
		}
	    });
	}
    };
    var setPage = function(fromBack) {
	stickPage = 1;
	if (page == 0) {
	    mainPrev.addClass('disabled');
	} else {
	    mainPrev.removeClass('disabled');
	}
	if (page == pages.length-1) {
	    mainNext.addClass('disabled');
	} else {
	    mainNext.removeClass('disabled');
	}
	onPage.text(page+1);
	var action;
	if (fromBack || firstLoad) {
	    action = function(pg) {
		history.replaceState(page, '', pg);
	    };
	} else {
	    action = function(pg) {
		history.pushState(page, '', pg);
	    };
	}
	if (page == 0) {
	    action('tour.html');
	} else {
	    action('#'+(page+1));
	}
	pageChange();
    };
    window.onhashchange = pageChange;
    tour.on('click', '*[rel="next"]', function(ev) {
	if (page < pages.length-1) {
	    ++page;
	    setPage(false);
	}
    });
    tour.on('click', '*[rel="prev"]', function(ev) {
	if (page > 0) {
	    --page;
	    setPage(false);
	}
    });

    // Navigation for the mock comic
    tour.on('click', '#stickfigure_nav', function(ev) {
	if (ev.originalEvent.target.classList.contains('disabled'))
	    return false;
	switch(ev.originalEvent.target.id) {
	case 'first':
	    stickPage = 1;
	    break;
	case 'prev':
	    stickPage--;
	    break;
	case 'next':
	    stickPage++;
	    break;
	case 'last':
	    stickPage = 3;
	    break;
	default:
	    return false;
	}
	container.find('#stickfigure_nav img').removeClass('disabled')
	if (stickPage == 1) {
	    container.find('#stickfigure_nav img#first').addClass('disabled');
	    container.find('#stickfigure_nav img#prev').addClass('disabled');
	} else if (stickPage == 3) {
	    container.find('#stickfigure_nav img#next').addClass('disabled');
	    container.find('#stickfigure_nav img#last').addClass('disabled');
	}
	container.find('#comicpage').attr('src', '/images/tutorial/comic'+stickPage+'.svg');
	container.find('.fake_browser_address').text('https://stickfigurehero.invalid/'+stickPage);
	container.find('#stickTitle').text(stickTitles[stickPage]);
	if (stickPage == 2) {
	    container.find('#copy_complete').show('slow');
	}
    });

    // Actions for revert page
    tour.on('click', '#recent', function(ev) {
	var button = container.find('button');
	if (ev.target.checked) {
	    button.removeAttr('disabled');
	} else {
	    button.attr('disabled', true);
	}
    });
    tour.on('click', '#recent2', function(ev) {
	container.find('#recent2_blurb').show();
	return false;
    });

    var bookmarkPaste = function(ev,a,b,c) {
	var but = container.find('#bookmark_button');
	var val;
	if (ev.originalEvent.type == 'paste') {
	    val = ev.originalEvent.clipboardData.getData('text/plain');
	} else {
	    val = ev.target.value;
	}
	if (val == 'https://stickfigurehero.invalid/2') {
	    but.addClass('target');
	    but.attr('rel', 'next');
	}
    }
    tour.on('change', 'input[name="bookmark"]', bookmarkPaste);
    tour.on('paste', 'input[name="bookmark"]', bookmarkPaste);

    // Parse fragment and set page if applicable
    var parseFragment = function() {
	var fragmentPage = parseInt(window.location.hash.substring(1));
	if (!isNaN(fragmentPage) && fragmentPage >= 1 && fragmentPage <= pages.length) {
	    page = fragmentPage-1;
	    return true;
	}
	return false;
    };

    // Load tour content
    $.ajax({
	url: '/tour2.html?min=1',
	dataType: 'html',
	method: 'GET'
    }).then(function(resp, b, c) {
	var doc = $(resp);
	doc.children().each(function(i, el) {
	    pages[i] = el;
	});
	pagesTotal.text(pages.length)
	container.empty();
	parseFragment();
	setPage(false);
    });

    window.onpopstate = function(ev) {
	if (ev.state === null) {
	    if (!parseFragment()) {
		page = 0;
	    }
	} else {
	    page = ev.state;
	}
	setPage(true);
    }
});
