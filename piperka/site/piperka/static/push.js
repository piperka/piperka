self.addEventListener('notificationclick', function(e) {
    var notification = e.notification;
    var action = e.action;

    if (action !== 'close') {
	clients.openWindow('https://piperka.net');
    }
    notification.close();
});

self.addEventListener('push', function(e) {
    var options;
    var title;
    if (e.data) {
	options = e.data.json();
	title = options.title;
    } else {
	options = {
	    tag: 'unread',
	    icon: '/favicon.png'
	};
	title = 'New unread comics';
    }
    e.waitUntil(
	self.registration.showNotification(title, options)
    );
});
