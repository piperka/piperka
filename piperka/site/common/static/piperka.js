function getCid() {
    var cidRegex = /[?&]cid=(\d+)/;
    var cid = cidRegex.exec(document.location);
    return cid ? cid[1] : null;
}

function readUnreadMode() {
    const newin = document.getElementById('newin');
    if (newin && newin.dataset.stickyShowHidden === "1")
	return 1;
    return /[?&]show_hidden=1/.exec(document.location) ? 1 : 2;
}

const getunread = readUnreadMode();
var csrf_ham = /csrf_ham=([0-9a-z-]+)/.exec(document.cookie);
csrf_ham = csrf_ham ? csrf_ham[1] : null;
const cid = getCid();

function getUpdatedComics() {
    var res = [];
    document.querySelectorAll('li').forEach(function(li, i) {
	if (li.dataset.cid !== undefined) {
	    res.push({cid: parseInt(li.dataset.cid),
		      n: parseInt(li.dataset.unread),
		      href: li.children[0].href
		     });
	}
    });
    return res;
}

(function( $ ){
    $.fn.tagSelect = function() {
	$(this).on('change', function(){
	    $('.maincategory').hide();
	    var id = 'cat-'+$(this).find(':selected').val();
	    $('#'+id).show();
	});
	return this;
    };

    $.fn.updatedComics = function(a,b,c) {
	var cids = {};
	$(this).find('li').each(function(x,y) {
	    var cid = $(this).data('cid');
	    if (cid) {
		cids[cid] = true;
	    }
	});
	return cids;
    }

    return this;
})( jQuery );

function submitSuccess(reply) {
    if (typeof reply != 'undefined') {
	$('#msgdiv').html('');
	if (reply.ok) {
	    $('.hideafterdone').slideUp();
	    $('.removeafterdone').remove();
	    $('#msgdiv').hide().html(reply.msg).slideDown();
	} else if (reply.errmsg) {
	    enableSubmitcomic();
	    $('.submitcomic button[type=submit]').removeAttr('disabled');
	    $('#msgdiv').hide().html(reply.errmsg).slideDown();
	}
    }
}

function submitError() {
    enableSubmitcomic();
    $('.submitcomic button[type=submit]').removeAttr('disabled');
    $('#msgdiv').hide().html("Unexpected error").slideDown();
}

function enableSubmitcomic() {
    var submitButton = $('.submitcomic button[type=submit]');
    submitButton.removeAttr('disabled');
    $('.submitcomic').one('submit', function() {
	submitButton.attr('disabled', 1);
	var options = {url: '/s/submit',
		       type: 'POST',
		       dataType: 'json',
		       error: submitError,
		       success: submitSuccess
		      };
	if (window.FormData) {
	    options.data = new FormData(this);
	    options.processData = false;
	    options.contentType = false;
	} else {
	    options.data = $(this).serialize();
	}
	$.ajax(options);
	return false;
    });
    if (window.FormData) {
	$('.script.hasformdata').show();
    } else {
	$('.script.noformdata').show();
    }
}

function setTagsEpedias(rpy) {
    $('#tagdiff .diff').empty();
    var origtags = {};
    $.each(rpy.origtags, function(idx, tagid){
	origtags[tagid] = true;
    });
    $('#tags-container')
	.off('change', 'input[type=checkbox]')
	.find('input[type=checkbox]').prop('checked', false);
    $('#tags-container').on('change', 'input[type=checkbox]', function() {
	var tagid = this.getAttribute('value');
	const desc = $('label[for='+this.getAttribute('id')+']').text();
	if (desc.startsWith('site:'))
	    return;
	if ((origtags[tagid] && this.checked) || (!origtags[tagid] && !this.checked)) {
	    $('span#difftag-'+tagid).remove();
	} else {
	    var diffelem = document.createElement('span');
	    diffelem.id = 'difftag-'+tagid;
	    diffelem.textContent = desc;
	    if (origtags[tagid]) {
		$('#removedtags').show();
		$('#removedtags .diff').append(diffelem);
	    } else {
		$('#addedtags').show();
		$('#addedtags .diff').append(diffelem);
	    }
	}
    });
    var initialNewtags = {};
    $.each(rpy.tags, function(idx, tagid){
	$('#cat-sel-'+tagid).click();
	initialNewtags[tagid] = true;
    });
    $.each(rpy.origtags, function(idx, tagid){
	if (!initialNewtags[tagid])
	    $('#cat-sel-'+tagid).trigger('change');
    });
    $.each(rpy.epedias, function(idx, dat) {
	var epid = dat.epid || dat[0];
	var entry = dat.entry || dat[1];
	$('input[name=epedia-entry-'+epid+']').val(entry);
    });
}

function resetSubmitForm() {
    $('.submitcomic input[type=text]').val('');
    $('.submitcomic textarea').val('');
    $('.submitcomic input[type=checkbox]').removeAttr('checked');
    $('.submitcomic').off('change');
    $('#tagdiff .diff').html('');
}

function updateUnread(unread) {
    if (unread.total_new > 0) {
	var newin = $('#newin').show();
	newin.text('('+unread.total_new+' new in '+unread.new_in+')');
	var newinHeader = $('#unread-header').show();
	newinHeader.text(unread.total_new+' new in '+unread.new_in);
    } else {
	$('#newin, #unread-header').hide();
    }
}

// Also shows the account settings section that sets this.
function getBookForward() {
    const key = 'alwaysUseForward';
    let hasStorage = false;
    try {
	const x = '__storage_test__';
	window.localStorage.setItem(x, x);
	window.localStorage.removeItem(x);
	hasStorage = true;
    } catch (e) {}
    if (hasStorage) {
	const alwaysUse = !!window.localStorage.getItem(key)
	const el = document.getElementById('alwaysForwardBtn');
	if (el) {
	    el.checked = alwaysUse;
	    addEventListener('change', function(event) {
		if (event.target.checked) {
		    window.localStorage.setItem(key, '1');
		} else {
		    window.localStorage.removeItem(key);
		}
	    });
	}

	$('#alwaysForwardBtnSection').show();
	if (alwaysUse) {
	    return '.booklink,.bookredir';
	}
    }
    return '.booklink';
}

$(document).ready(function(){
    var popup = $('.mobile #popup-container');
    var menubuttonsvg = $('.mobile #menu-button');
    var repage;
    const bookForward = getBookForward();
    $('.noscript').hide();
    $('.script.show').show();
    $('.scripthidden.show').removeClass('scripthidden');
    $(bookForward).prepend($('<button type="button" class="bookmarkforward">&gt;</button>')
			   .click(function(){
			       var par = $(this).parent()
			       const cid = par.data('cid');
			       $.ajax({url: '/s/uprefs',
				       data: {
					   bookmark: [cid, 'max'],
					   csrf_ham: csrf_ham,
					   getunread: getunread
				       }
				      })
				   .done(function(rpy) {
				       if (rpy && rpy.ok) {
					   par.fadeOut('fast');
					   updateUnread(rpy);
				       }
				   });
			   }));
    if ($('.submitcomic').length > 0) {
	if (cid != null && $('.submitcomic input[name="cid"]').length > 0) {
	    $.ajax({url:'/s/cinfo/'+cid,
		    dataType: 'json',
		    success:function(rpy){
			if (rpy != null) {
			    rpy.origtags = rpy.tags;
			    setTagsEpedias(rpy);
			    enableSubmitcomic();
			}
		    }});
	} else {
	    enableSubmitcomic();
	}
    }

    var hilitedialog;
    var loadedtags = new Object;
    var tagcounts = new Object;
    $('form:has(#piperka_list)').on('hilite', '#piperka_list', function(){
	$('#piperka_list button').each(function(){
	    var cid = this.getAttribute('value');
	    if (tagcounts[cid] != undefined) {
		var li = $(this).parent();
		if (tagcounts[cid] > 0) {
		    li.addClass('hilite');
		} else {
		    li.removeClass('hilite');
		}
	    }
	});
    });
    $('#tagselect').tagSelect().trigger('change');
    $('button.hilitetags').on('click', function() {
	if (hilitedialog == undefined) {
	    $.ajax({url: '/d/tags.xml',
		    dataType: 'html',
		    success: function(content){
			hilitedialog = $('#hilitedialog').html(content);
			hilitedialog.find('#tagselect').tagSelect().trigger('change');
			var lastTop, lastLeft;
			hilitedialog.dialog({appendTo: '#notmuch', dialogClass: 'isover', minWidth:200, width:400, open: function(){
			    if (lastTop != undefined)  {
				var style = this.parentNode.style;
				style.top = lastTop;
				style.left = lastLeft;
			    }
			}, close: function(){
			    var style = this.parentNode.style;
			    lastTop = style.top;
			    lastLeft = style.left;
			}});
			hilitedialog.on('change', 'input', function() {
			    function updateTagcounts(tagid, diff) {
				$.each(loadedtags[tagid], function() {
				    if (tagcounts[this] == undefined) {
					tagcounts[this] = 0;
				    }
				    tagcounts[this] += diff;
				});
				$('#piperka_list').trigger('hilite');
			    }
			    var tagid = this.getAttribute('value');
			    var diff = this.checked ? 1 : -1;
			    if (loadedtags[tagid] == undefined) {
				hilitedialog.find('.tagselector').hide();
				$.ajax({url:'/s/tagslist/'+tagid,
					method:'GET',
					dataType:'json',
					success:function(rpy){
					    hilitedialog.find('.tagselector').show();
					    if (rpy != null) {
						if (rpy.cids) {
						    loadedtags[tagid] = rpy.cids;
						}
					    }
					    updateTagcounts(tagid, diff);
					}});
			    } else {
				updateTagcounts(tagid, diff);
			    }
			});
		    }});
	} else {
	    hilitedialog.dialog('open');
	}
    });

    // Reader history chart
    if ($('.chart').length > 0) {
	var rhist = $('#readerchart');
	if (cid != null && rhist.length > 0) {
	    $.when($.ajax({url:'/d/readershistory/'+cid, method:'GET', dataType:'json'}))
		.then(function(rpy){
		    chart = rpy;
		    if (rpy.length > 1) {
			var day = new Date(), dateTicks = [];
			for (var i=0; i < 5; ++i) {
			    dateTicks[4-i] = day.toISOString().substring(0,10);
			    day.setTime(day.getTime()-7*24*60*60*1000);
			}
			var maxReader = 0, minReader;
			$.each(rpy, function() {
			    var reader = this[1];
			    minReader = minReader == null || reader < minReader ? reader : minReader;
			    maxReader = maxReader < reader ? reader : maxReader;
			});
			var readerTicks = [minReader-1 < 0 ? 0 : minReader-1], lastTick = minReader-1, j=1;
			for (var i=0; i < 5; ++i) {
			    var num = Math.floor(minReader + (maxReader-minReader)/5*i);
			    if (num != lastTick) {
				lastTick = num;
				readerTicks[j++] = num;
			    }
			}
			readerTicks[j++] = maxReader;
			readerTicks[j] = maxReader+1;
			rhist.show();
			$.jqplot('readerchart', [chart],
				 {axes:{xaxis:{renderer: $.jqplot.DateAxisRenderer,
					       ticks:dateTicks,
					       tickRenderer: $.jqplot.CanvasAxisTickRenderer,
					       tickOptions: {angle: -30, formatString: '%m-%d'}
					      },
					yaxis:{ticks:readerTicks,
					       tickOptions:{formatString: '%i'}}
				       },
				  title:"Reader history"
				 });
		    }
		});
	}
    }

    if ($('#quicksearch').length > 0) {
	var form = $('#quicksearch form');
	form.on('submit', function() {
	    var cid = form.data('cid');
	    var sorttype = $('#quicksearch input[name=sorttype]').attr('value');
	    if (cid) {
		$.getJSON('/s/qsearch', {cid: cid, sorttype: sorttype}, function(rpy) {
		    if (rpy && rpy.offset != undefined) {
			if (repage != undefined) {
			    repage(rpy.offset);
			} else {
			    if (sorttype == 'top') {
				document.location = '/top.html?offset='+rpy.offset;
			    } else {
				document.location = '/browse.html?sort='+sorttype+'&offset='+rpy.offset;
			    }
			}
		    }
		});
	    }
	    return false;
	});
	$('#quicksearch').qsearch({
	    select: function(event, ui) {
		form.data('cid', ui.item.cid);
		form.submit();
	    }});
    }

    // AJAX subscribe buttons
    $('form:has(#piperka_list)').on('click', 'button', function(event) {
	event.preventDefault();
	var name = this.getAttribute('name');
	var subscid = this.getAttribute('value');
	var button = this;
	if (name == 'subscribe') {
	    var startAtFirst = $('#atfirst:checked').length > 0;
	    $.ajax({url: '/s/uprefs',
		    method: 'POST',
		    data: {bookmark: [subscid, startAtFirst ? 0 : 'max'], csrf_ham: csrf_ham, getunread: getunread},
		    success: function(rpy) {
			if (rpy && rpy.ord != undefined) {
			    $(button).attr('name', 'unsubscribe').attr('class', 'minus').text('-');
			    updateUnread(rpy);
			}
		    }
		   });
	} else if (name == 'unsubscribe') {
	    $.ajax({url: '/s/uprefs',
		    method: 'POST',
		    data: {bookmark: [subscid, 'del'], csrf_ham: csrf_ham, getunread:getunread},
		    success: function(rpy) {
			if (rpy && rpy.ok) {
			    $(button).attr('name', 'subscribe').attr('class', 'plus').text('+');
			    updateUnread(rpy);
			}
		    }
		   });
	}
    });

    // AJAX paginate
    if (!!(window.history && history.pushState) && $('.paginate').length > 0) {
	var b = {start: $('.paginate .start'),
		 prev: $('.paginate .prev'),
		 next: $('.paginate .next'),
		 end: $('.paginate .end')};
	var getOffsetRegexp = /[?&].*offset=([0-9]+)/;
	var getOffset = function(url) {
	    var matches = getOffsetRegexp.exec(url);
	    if (matches != undefined && matches.length > 1)
		return parseInt(matches[1]);
	    else
		return null;
	}
	var getMaxcidRegexp = /&_max=([0-9]+)/;
	var getMaxcid = function(url) {
	    var matches = getMaxcidRegexp.exec(url);
	    if (matches && matches.length > 1)
		return parseInt(matches[1]);
	    else
		return null;
	};
	var offset = getOffset(window.location.href);
	var sorttype;
	if (window.location.pathname == '/top.html') {
	    sorttype = 'top';
	} else {
	    sorttype = /[?&]sort=([a-z]+)/.exec(window.location.search);
	    if (sorttype) {
		sorttype = sorttype[1];
		switch (sorttype) {
		case 'top': case 'name': case 'new': case 'update': case 'hot': case 'recommend':
		    break;
		default:
		    sorttype = 'name';
		}
	    } else {
		sorttype = 'name';
	    }
	}
	var prev = getOffset(b.prev.attr('href'));
	var container = $('#piperka_list');
	if (offset == undefined) {
	    offset = 0;
	}
	var total, perPage = getOffset($(b.next).attr('href'));
	if (perPage == undefined) {
	    if (prev != undefined) {
		perPage = offset-prev;
		total = $('#piperka_list li').length+offset;
	    }
	} else {
	    perPage -= offset;
	    total = getOffset(b.end.attr('href'))+perPage;
	}
	if (total != undefined) {
	    var lastAjax;
	    repage = function(event) {
		popup.slideUp();
		menubuttonsvg.removeClass('active');
		var newOffset, newSorttype, href, maxcidMarker = '';
		if (typeof event == 'number') {
		    newOffset = event;
		    newSorttype = sorttype;
		} else {
		    event.stopPropagation();
		    event.preventDefault();
		    var forceUpdate = false;
		    if (event.type == 'popstate') {
			href = document.location.href;
			if (event.originalEvent.state) {
			    forceUpdate = true;
			}
		    } else {
			href = this.getAttribute('href');
			if (href == undefined)
			    return;
		    }
		    newOffset = getOffset(href);
		    if (window.location.pathname == '/top.html') {
			newSorttype = 'top';
		    } else {
			newSorttype = /\?.*sort=([a-z]+)/.exec(href);
			if (newSorttype)
			    newSorttype = newSorttype[1];
			else
			    newSorttype = 'name';
			switch (newSorttype) {
			case 'top': case 'name': case 'new': case 'update': case 'hot': case 'recommend':
			    if (newSorttype != sorttype) {
				if (newSorttype == 'name') {
				    $('#alphabet_index').show();
				} else if (sorttype == 'name') {
				    $('#alphabet_index').hide();
				}
				$('#quicksearch input[name=sorttype]').attr('value', newSorttype);
			    }
			    break;
			default:
			    newSorttype = sorttype;
			}
		    }
		    if (newOffset == undefined) {
			newOffset = 0;
			var maxcid;
			if (newSorttype == 'new' && (maxcid = getMaxcid(href)))
			    maxcidMarker = '&_max='+maxcid;
		    }
		}
		newOffset = newOffset < 0 ? 0 : newOffset >= total ? (total-1) : newOffset;
		if (!forceUpdate && offset == newOffset && sorttype == newSorttype)
		    return;
		sorttype = newSorttype;
		offset = newOffset;
		if (window.location.pathname == '/top.html') {
		    href = '/top.html?offset='
		} else {
		    var search = window.location.search
			.replace(/[?&]offset=[^&#]*/, '')
			.replace(/[?&]sort=[^&#]*/, '')
			.replace(/[?&]_max=[^&#]*/, '');
		    if (search == '') {
			search = '?'
		    } else {
			search = search.replace(/^./, '?') + '&';
		    }
		    href = window.location.pathname + search + 'sort='+sorttype+'&offset=';
		}
		if (offset == 0) {
		    b.start.removeAttr('href');
		    b.prev.removeAttr('href');
		} else {
		    b.start.attr('href', href + '0');
		    var newPrev = offset-perPage;
		    if (newPrev < 0)
			newPrev = 0;
		    b.prev.attr('href', href + newPrev);
		}
		var newNext = offset+perPage;
		if (newNext >= total) {
		    b.next.removeAttr('href');
		    b.end.removeAttr('href');
		} else {
		    b.next.attr('href', href + newNext);
		    b.end.attr('href', href + (total-perPage));
		}
		container.fadeTo('slow', 0, 'linear');
		if (lastAjax)
		    lastAjax.abort();
		href = href + offset + maxcidMarker;
		lastAjax = $.ajax({url: href,
				   dataType: 'html',
				   data: {min: 1},
				   success: function(content) {
				       lastAjax = null;
				       container.stop();
				       var newContent = $(content);
				       var newContainer = newContent.filter('#piperka_list');
				       if (newContainer.length == 0)
					   newContainer = newContent.find('#piperka_list');
				       let newTotal = newContainer.data('total');
				       if (newTotal && total != newTotal) {
					   total = newTotal;
					   b.end.attr('href', href.substring(0, href.indexOf('offset=')+7)+(total-perPage));
				       }
				       container.replaceWith(newContainer);
				       container = newContainer;
				       if (event.type != 'popstate')
					   history.pushState(true, null, href);
				       container.trigger('hilite');
				   }});
	    };
	    $('.paginate').on('click', 'a', repage);
	    $('#alphabet_index').on('click', 'a', repage);
	    $('#sort_by').on('click', 'a', repage);
	    $(window).on('popstate', repage);
	    href = undefined;
	    $('body').on('click', 'a', function(event) {
		history.replaceState(false, $('title').text());
		return true;
	    });
	}
    }

    $(".followcheck").on('change', 'input[type="checkbox"]', function(){
	var type = this.getAttribute('name');
	var name = this.getAttribute('value');
	var checkbox = this;
	var action;
	if (type == 'followee') {
	    action = this.checked ? 'permit' : 'deny';
	} else if (type == 'follow') {
	    action = this.checked ? 'follow' : 'unfollow';
	}
	if (action == undefined) {
	    this.checked = !this.checked;
	    return;
	}
	var docancel = function() {
	    checkbox.checked = (action == 'permit' || action == 'follow') ? false : true;
	};
	this.setAttribute('disabled', true);
	$.ajax({url: '/s/profile',
		method: 'POST',
		data: {action: action, name: name, csrf_ham: csrf_ham},
		success: function(rpy) {
		    if (!(rpy && rpy.rows && rpy.rows > 0)) {
			docancel();
		    }
		    checkbox.removeAttribute('disabled');
		},
		failure: function(rpy) {
		    docancel();
		    checkbox.removeAttribute('disabled');
		}
	       });
    });

    $('#followee').on('click', 'button', function(){
	var row = $(this).parents('tr');
	var button = this;
	this.setAttribute('disabled', true);
	$.ajax({url: '/s/profile',
		method: 'POST',
		data: {action: 'unfollow', name: this.getAttribute('name'),  csrf_ham: csrf_ham},
		success: function(rpy) {
		    if (rpy.rows != undefined && rpy.rows > 0) {
			row.fadeOut();
		    } else {
			button.removeAttribute('disabled');
		    }
		},
		failure: function(rpy) {
		    button.removeAttribute('disabled');
		}
	       });
    });

    $('span#interest').on('click', 'button', function() {
	var action = this.getAttribute('id') == 'cancel' ? 'unfollow' : 'follow';
	var button = this;
	button.setAttribute('disabled', true);
	$.ajax({url: '/s/profile',
		method: 'POST',
		data: {action: action, name: this.getAttribute('name'), csrf_ham: csrf_ham},
		success: function(rpy) {
		    if (rpy.rows != undefined && rpy.rows > 0) {
			window.location.href = window.location.href;
		    } else {
			button.removeAttribute('disabled');
		    }
		}
	       });
    });

    if ($('#ticket').length > 0 && $('#ticket').tabs().length > 0) {
	$('input[name="jsok"]').val(1);
    }

    // Account management field disables and enables
    var oldPasswdInput = $('input[name="_password"]');
    $('#authenticate_with').on('change', function() {
	if ($(this).val() == 'Password') {
	    oldPasswdInput.removeAttr('disabled');
	} else {
	    oldPasswdInput.attr('disabled', 1);
	}
    });
    var passwdInput = $('input[name="new_passwd"], input[name="new_passwd_retype"]');
    $('#only_oauth2').on('change', function() {
	var checkbox = this;
	if (this.checked) {
	    passwdInput.attr('disabled', 1);
	} else {
	    passwdInput.removeAttr('disabled');
	}
    });

    // Switch links on updates page to point to Reader
    var readerCheck = $('#openinreader');
    if (readerCheck.length > 0) {
	readerCheck.parent().show();
	readerCheck.on('readerChange', function() {
	    var rg = /redir=(\d+)/;
	    var links = $('#piperka_list a');
	    if (readerCheck[0].checked) {
		links.each(function(idx, item) {
		    var cid = rg.exec(item.href)[1];
		    $(item).data('orig', [item.href, item.getAttribute('target')]);
		    item.href = 'http://'+window.location.hostname+'/reader/?cid='+cid;
		    item.setAttribute('target', 'reader');
		});
	    } else {
		links.each(function(idx, item) {
		    var orig = $(item).data('orig');
		    item.href = orig[0];
		    item.setAttribute('target', orig[1]);
		});
	    }
	});
	readerCheck.on('change', function() {
	    readerCheck.trigger('readerChange');
	});
    }

    var archive;
    $('#info-archive').on('click', function() {
	var userreq, subscriptions;
	if (!archive) {
	    $('#archivedialog').pPageDialog();
	    $('#thumbdialog').pThumbDialog();
	    $('#archivedialog').on('archive_select', 'tr:has(td)', function(ev, existing) {
		if (!existing) {
		    var info = $('#archivedialog').data();
		    const page = $(this).data('page');
		    var url;
		    if (page === null) {
			url = info.fixed_head ? info.fixed_head : info.homepage;
		    } else {
			url = info.url_base+$(this).data('page')+info.url_tail;
		    }
		    window.open(url, 'ArchivePage');
		}
	    })
	    archive = makePageDialog(cid);
	}
	$.when(archive)
	    .then(function(archive) {
		$('#archivedialog').dialog('open');
	    });
	return false;
    });

    var updateWatch = $('#updatewatch');
    var supportsWebSockets = 'WebSocket' in window || 'MozWebSocket' in window;
    $('.updatewatch-message').hide().removeClass('scripthidden');
    var updateWatchMsg = function(id) {
	$('.updatewatch-message').hide();
	$('#'+id).show();
    };
    var updateWatchCustomMsg = function(msg) {
	$('.updatewatch-message').hide();
	$('#updatewatch-msg-custom').text(msg).show();
    };
    if (csrf_ham && updateWatch.length > 0 && supportsWebSockets) {
	$('#watch-messages').removeClass('script');
	var tgl = $('#updatewatch-toggle');
	tgl.prop('checked', false);
	var schedule = [10000, 20000, 40000, 100000]
	var updatedComics = $('#piperka_list').updatedComics();
	var icon = $('link[rel="icon"]');
	window.onfocus = function() {
	    icon.attr('href', '/favicon.png');
	};
	var watchFailed = function() {
	    updateWatchMsg('updatewatch-msg-connfailed');
	    tgl.prop('checked', false);
	};
	var watchUpdating = function() {
	    updateWatchMsg('updatewatch-msg-upd');
	};
	var watchReconn = function() {
	    updateWatchMsg('updatewatch-msg-reconnecting');
	};
	function initUpdateWatchWs(reconnect) {
	    ws = new WebSocket(wsUri);
	    ws.onerror = function(ev) {
		if (tgl.prop('checked')) {
		    watchFailed();
		}
		console.log({x: 'error', ev: ev})
	    };
	    ws.onopen = function(ev) {
		$('.updatewatch-message').hide();
	    };
	    ws.onclose = function(ev) {
		if (!tgl.prop('checked')) {
		    return;
		}
		if (schedule[reconnect]) {
		    $('#updatewatch-msg-reconnecting').show();
		    window.setTimeout(initUpdateWatchWs, schedule[reconnect], reconnect+1);
		} else {
		    $('#updatewatch-msg-connfailed').show();
		}
	    };
	    ws.onmessage = function(ev) {
		watchUpdating();
		$.ajax({url: '/?min=1',
			dataType: 'html',
			success: function(content) {
			    $('.updatewatch-message').hide();
			    var newContent = $(content);
			    $('#piperka_list').replaceWith(newContent.find('#piperka_list'));
			    $('#newin').text(newContent.filter('#newin').text());
			    var newComics = newContent.filter('#newcomics').children().detach();
			    if (newComics.length > 0) {
				$('#menu-button circle').addClass('newcomics');
			    } else {
				$('#menu-button circle').removeClass('newcomics');
			    }
			    $('#newcomics').empty().append(newComics);
			    var newExpected = newContent.filter('#expected-updates');
			    var oldExpected = $('#expected-updates');
			    if (oldExpected.length) {
				oldExpected.replaceWith(newExpected);
			    } else if (newExpected.length) {
				$('#pre-expected-updates').after(newExpected);
			    }
			    if (readerCheck[0].checked) {
				readerCheck.trigger('readerChange');
			    }
			    var newUpdated = $('#piperka_list').updatedComics();
			    var anyNew = Object.keys(newUpdated).some(function(cid) {
				return !updatedComics[cid];
			    });
			    if (anyNew) {
				icon.attr('href', '/favicon-updates.png');
			    }
			    updatedComics = newUpdated;
			},
			failure: function() {
			    watchFailed();
			}
		       });
	    };
	    return ws;
	}

	updateWatch.removeClass('scripthidden');
	var wsUri = 'wss://'+window.location.host+'/s/updateWatch/init/'+csrf_ham;
	tgl.on('change', function(ev) {
	    $('.updatewatch-message').hide();
	    if (ev.target.checked) {
		initUpdateWatchWs(0);
	    } else {
		ws.close();
	    }
	});
    }
    var supportsNotification = 'Notification' in window && navigator.serviceWorker;
    if (csrf_ham && updateWatch.length > 0 && supportsNotification) {
	$('#update-push-container').removeClass('scripthidden');
	var tgl2 = $('#update-push-toggle');
	var updateSubscription = function(subscription) {
	    var keys = subscription.toJSON().keys;
	    var call = $.ajax({url: '/s/push/subscribe',
			       method: 'POST',
			       data: {'csrf_ham': csrf_ham,
				      'endpoint': subscription.endpoint,
				      'auth': keys.auth,
				      'p256dh': keys.p256dh
				     }
			      });
	    return call;
	};
	// Determine if notifications start as enabled
	navigator.serviceWorker.getRegistration('/push.js')
	    .then(function(registration) {
		if (registration && Notification.permission == 'granted') {
		    // SW has been registered, proceed to check if
		    // there's an existing push subscription.
		    registration.pushManager.getSubscription()
			.then(function(subscription) {
			    if (subscription) {
				updateSubscription(subscription)
				    .catch(function(e) {
					tgl2.prop('checked', false);
					throw e;
				    })
				    .then(function() {
					tgl2.prop('checked', true);
				    });
			    }
			});
		} else {
		    // Explicit uncheck since browsers may restore an
		    // old state for the checkbox.
		    tgl2.prop('checked', false);
		}
	    });

	tgl2.on('change', function(ev) {
	    $('.updatewatch-message').hide();
	    if (ev.target.checked) {
		// Request permission
		Notification.requestPermission(function(status) {
		    if (Notification.permission == 'granted') {
			var errored = false;
			navigator.serviceWorker.register('/push.js')
			    .then(function(reg) {
				var key = atob(document.getElementById('update-push-toggle').dataset.pushkey);
				var buf = new Uint8Array(key.length);
				for (i = 0; i < key.length; ++i) {
				    buf[i] = key.charCodeAt(i);
				}
				var options = {userVisibleOnly: true,
					       applicationServerKey: buf};
				reg.pushManager.subscribe(options)
				    .catch(function(err) {
					console.log(err);
					updateWatchCustomMsg('Push subscription failed on browser');
					errored = true;
					tgl2.prop('checked', false);
					throw err;
				    })
				    .then(updateSubscription)
				    .catch(function(err) {
					if (errored)
					    return;
					console.log(err);
					updateWatchCustomMsg('Push subscription failed on server');
					tgl2.prop('checked', false);
				    });
			    });
		    } else {
			updateWatchCustomMsg('No permission for notifications');
			tgl2.prop('checked', false);
		    }
		});
	    } else {
		// Unsubscribe
		navigator.serviceWorker.getRegistration('/push.js')
		    .then(function(registration) {
			if (!registration)
			    return;
			registration.pushManager.getSubscription()
			    .then(function(subscription) {
				if (!subscription)
				    return;
				var endpoint = subscription.endpoint;
				subscription.unsubscribe()
				    .then(function(success) {
					$.ajax({url: '/s/push/unsubscribe',
						method: 'POST',
						data: {'csrf_ham': csrf_ham,
						       'endpoint': endpoint
						      }
					       });
				    });
			    });
		    });
	    }
	});
    }

    var editAuthor = $('#editAuthor');
    if (editAuthor.length > 0) {
	var comicTable = $('#comics');
	$('#editAuthorForm').on('submit', function(ev) {
	    $.ajax({url: '/s/editAuthor',
		    method: 'POST',
		    data: $(this).serialize()
		   })
		.done(function(res) {
		    $('#editAuthorForm input[name=auid]').val(res.auid);
		    $('#editAuthorForm button[type=submit]').text('Update');
		    $('#error').text();
		})
		.fail(function() {
		    $('#error').text('update/create failed');
		});
	    return false;
	});
	$('#editAuthorForm').on('click', 'button.delete', function(ev) {
	    this.parentElement.parentElement.remove();
	});
	$('#editAuthorSearch').qsearch({
	    select: function(event, ui) {
		var form = this.parentElement;
		form.dataset.cid = ui.item.cid;
		form.dataset.label = ui.item.label;
		$(form).submit();
	    }});
	$('#editAuthorSearch form').on('submit', function() {
	    var cid = this.dataset.cid;
	    var label = this.dataset.label;
	    if ($('#editAuthorForm input[name="cid"][value="'+cid+'"]').length > 0)
		return false;
	    $('<tr/>')
		.data('cid', cid)
		.append($('<td/>')
			.text(cid)
			.append($('<input/>')
				.attr('type', 'hidden')
				.attr('name', 'cid')
				.attr('value', cid))
		       )
		.append($('<td/>')
			.append($('<a/>')
				.text(label)
				.attr('target', '_blank')
				.attr('href', 'info.html?cid='+cid))
		       )
		.append($('<td/>')
			.append($('<button>Delete</button>')
				.attr('type', 'button')
				.attr('class', 'delete'))
		       )
		.appendTo($('#comics'));
	    return false;
	});
    }

    // Epedias paste event
    $('#epedias').on('paste', 'input[type=text]', function(ev) {
	var paste = ev.originalEvent.clipboardData.getData('text/plain');
	var base = this.dataset.base;
	var tail = this.dataset.tail;
	if (paste.startsWith(base)) {
	    paste = paste.slice(base.length);
	}
	if (tail && paste.endsWith(tail)) {
	    paste = paste.slice(0, paste.length-tail.length);
	}
	this.value=paste;
	return false;
    });

    $('.ads-question input').on('change', function() {
	$('.ads-question span.message').text('');
    });
    $('.ads-question button').on('click', function(el) {
	var consent;
	var refresh = false;
	if (el.target.name == 'ads') {
	    consent = el.target.value;
	    refresh = true;
	} else {
	    consent = $('.ads-question input:checked').val();
	}
	if (typeof(consent) != 'undefined') {
	    document.cookie="ads="+consent+";path=/;max-age=157680000; Secure; SameSite=Strict";
	    if (refresh) {
		document.location = 'ad_settings.html';
	    } else {
		$('.ads-question span.message').text('Done');
	    }
	}
    });

    const unreadInNewTabs = document.getElementById('unreadInNewTabs');
    if (unreadInNewTabs) {
	const minNewTab = document.getElementById('minNewTab');
	const unreadTabOpenResult = document.getElementById('unreadTabOpenResult');
	unreadInNewTabs.onclick = function() {
	    const lim = minNewTab.valueAsNumber;
	    const tabs = getUpdatedComics()
		  .filter(function(c) {return c.n <= lim && c.n > 0;})
		  .map(function(update) {
		      return window.open(update.href, "Piperka Update "+update.cid);
		  });
	    if (tabs[0]) {
		tabs[0].focus();
	    }
	    unreadTabOpenResult.style.display = "inline";
	    unreadTabOpenResult.children[0].textContent = tabs.length;
	};
    }

    // Mobile site
    $('.selectMobile').on('click', function() {
	var mobi = this.dataset.sel;
	document.cookie="mobi="+mobi+";path=/;max-age=157680000; Secure; SameSite=Strict";
	if (mobi == "1" && this.tagName == "BUTTON") {
	    $('#uncommittedMobileQuestion').hide(600);
	} else {
	    document.location=document.location;
	}
    });

    var popupVisible = false;
    $('.mobile #page-controls').children().detach().appendTo(popup);
    popup.append('<hr/>');
    $('.mobile .sidebar').children().detach().appendTo(popup);
    $('.mobile #menu-button').on('click', function() {
	popup.slideToggle(300);
	menubuttonsvg.toggleClass('active');
    });

    if (csrf_ham && /^https:\/\/[^/]+\/sso_redir.html/.exec(document.documentURI)) {
	$('form#sso').submit();
    }

    var userHiatus = $('#user_hiatus');
    if (userHiatus.length > 0) {
	var hiatusButton = $('#user_hiatus button');
	userHiatus.attr('action', '/info.html?cid='+userHiatus.data('cid'));
	$('#user_hiatus select').on('change', function(ev) {
	    hiatusButton.prop('disabled', ev.target.value === '');
	});

	userHiatus.on('submit', function() {
	    return !hiatusButton.prop('disabled');
	});
    }

    // Moderator interface
    if ($.fn.pModerate) {
	$('#user-edits tbody').pModerate();
	$('#user-submits tbody').pSubmissions();
	$('#refresh-comiclists').pRefreshComicLists();
    }
});
