(function($) {
    var qsearchRowsDefault = 10;
    var comics;

    $.fn.qsearch = function(options){
	var form = this.find('form');
	var qsearchButton = this.find('button').attr('disabled', 1);
	var lastSelect = null;
	var qsearchRows = qsearchRowsDefault;
	if (comics == null) {
	    comics = $.getJSON('/d/comics_ordered2.json');
	}
	this.find('#showallresults').on('change', function(ev) {
	    qsearchRows = ev.target.checked ? 100000 : qsearchRowsDefault;
	});

	var qinput_source = function(req, resp){
	    var hits = [];
	    $.when(comics).then(function(rpy) {
		var matches = {};
		var reqlen = req.term.length;
		var req_lc = req.term.toLowerCase();
		var matcher = function(rpy, f) {
		    rpy.every(function(el) {
			if (matches[el[0]])
			    return true;
			var candidates = [el[1]];
			if (el[2]) {
			    candidates = candidates.concat(el[2]);
			}
			if (candidates.some(f)) {
			    hits.push({label: el[1], cid: el[0]});
			    matches[el[0]] = 1;
			    if (hits.length >= qsearchRows) {
				return false;
			    }
			}
			return true;
		    });
		};
		// First, exact matches
		matcher(rpy, function(m) {
		    return m.substr(0, reqlen).toLowerCase() == req_lc;
		});
		// Then, grab regexp results
		if (hits.length < qsearchRows) {
		    var req_regexp;
		    var validRegexp = true;
		    try {
			req_regexp = new RegExp(req.term, 'i');
		    } catch (err) {
			validRegexp = false;
		    }
		    if (validRegexp) {
			matcher(rpy, function(m) {
			    return req_regexp.test(m);
			});
		    }
		}
		if (hits.length > 0) {
		    form.data('cid', hits[0].cid);
		    qsearchButton.removeAttr('disabled');
		} else {
		    form.data('cid', null);
		    qsearchButton.attr('disabled', 1);
		    lastSelect = null;
		}
		resp(hits);
	    }, function() {
		resp([]);
	    })};

	var optSelect = options.select;
	var optSubmit = options.submit;
	delete options.select;
	var qinput = this.find('input[type="input"]');
	var settings = $.extend({
	    minLength: 1,
	    delay: 100,
	    autoFocus: true,
	    select: function(event, ui) {
		lastSelect = ui;
		if (optSelect)
		    optSelect.apply(this, arguments);
	    },
	    source: qinput_source
	    }, options);

	qinput.autocomplete(settings);
	form.on('submit', function(ev){
	    if (lastSelect && optSubmit)
		optSubmit.call(qinput[0], ev, lastSelect);
	    return false;
	});
	return this;
    };
})(jQuery);
