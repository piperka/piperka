var csrf_ham = /csrf_ham=([0-9a-z-]+)/.exec(document.cookie);
csrf_ham = csrf_ham ? csrf_ham[1] : null;

var bookmarkMoves = {};
var ws;
var bookmarks = {};
var afterReset = null;

// This is called after WebSocket has been initialized.
// websocket-snap seems to cause concurrent ajax calls to freeze if it
// is initializing at the time and this adds order to the calls.
function loadCrawlParams() {
    if (cid !== null) {
	$('#cid').val(cid);
	var archiveRow = document.getElementById('src-0');
	while (archiveRow) {
	    var bookm = archiveRow.children[2];
	    if (bookm) {
		var idx = /src-(\d+)/.exec(archiveRow.id)[1];
		bookmarks[idx] = parseInt(bookm.textContent);
	    }
	    archiveRow = archiveRow.nextElementSibling;
	}
	var archiveTable = document.getElementById('existing_archive');
	$(archiveTable)
	    .find('tr.has_bookmarks')
	    .draggable({helper: 'clone',
			handle: '.bookmark_handle',
			scope: 'bookmark_transfer',
		       });
	var resultsTable = document.getElementById('crawl_results');
	$(resultsTable).trigger('archive');
    }

}

function crawlerRedrawCanvas() {
    var canvas = document.getElementById('bookmark_transfers');
    // Resize
    var results = document.getElementById('crawl_results').parentNode;
    var archive = document.getElementById('existing_archive').parentNode;
    var height = results.clientHeight;
    var height2 = archive.clientHeight;
    canvas.height = height > height2 ? height : height2;
    var ctx = canvas.getContext('2d');
    var canvasTop = canvas.getBoundingClientRect().top;
    ctx.beginPath();
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    Object.keys(bookmarks).forEach(function(idx) {
	document.getElementById('src-'+idx).children[3].innerText = '';
    });
    Object.keys(bookmarkMoves).forEach(function(src) {
	var tgt = bookmarkMoves[src];
	var tgtEl = document.getElementById('tgt-'+tgt);
	if (!tgtEl)
	    return;
	var srcEl = document.getElementById('src-'+src);
	// Canvas element has a browser set upper limit on dimensions.
	// Use this column as a fallback.
	srcEl.children[3].innerText = tgt;
	var srcRect = srcEl.getBoundingClientRect();
	var tgtRect = tgtEl.getBoundingClientRect();
	ctx.moveTo(0, srcRect.top-canvasTop+10);
	ctx.lineTo(150, tgtRect.top-canvasTop+10);
    });
    ctx.stroke();
}

function addCrawlRow(txt) {
    var resultsTable = document.getElementById('crawl_results');
    var urlBase = $('#url_base').val();
    var urlTail = $('#url_tail').val();
    var crawlRow = document.createElement('tr');
    var idx = resultsTable.lastElementChild && resultsTable.lastElementChild.id && /tgt-(\d+)/.exec(resultsTable.lastElementChild.id)[1];
    idx = !!idx ? parseInt(idx)+1 : 0;
    crawlRow.id = 'tgt-'+idx
    crawlRow.classList.add('bookmark_drag');
    var ord = document.createElement('td');
    ord.appendChild(document.createTextNode(idx));
    var name = document.createElement('td');
    var link = document.createElement('a');
    link.setAttribute('href', urlBase+txt+urlTail);
    link.setAttribute('target', 'archive_page');
    link.appendChild(document.createTextNode(txt));
    name.appendChild(link);
    crawlRow.appendChild(ord);
    crawlRow.appendChild(name);
    resultsTable.appendChild(crawlRow);
    // Current row gets its id updated to one past
    var current = document.getElementById('tgt-current');
    current.firstElementChild.id = 'tgt-'+(idx+1);
    $(resultsTable).trigger('archive');
    if (idx > 1) {
	document.getElementById('auto_discover').removeAttribute('disabled');
    }
}

function removeCrawlRow(ord) {
    // Remove row and rename ids of following elements
    var tr = document.getElementById('tgt-'+ord);
    tr.id = '';
    var el, i;
    for (el = tr.nextElementSibling, i = ord; el; el = el.nextElementSibling) {
	el.firstElementChild.innerText = i;
	el.id = 'tgt-'+(i++);
    }
    tr.remove();
    var current = document.getElementById('tgt-current');
    current.firstElementChild.id = 'tgt-'+i;
    // Adjust bookmarkMoves
    var newBookmarks = {};
    Object.keys(bookmarks).forEach(function(x) { newBookmarks[x] = parseInt(x) });
    $.extend(newBookmarks, bookmarkMoves);
    Object.keys(newBookmarks).forEach(function(i) {
	i = parseInt(i);
	var bookmark = newBookmarks[i];
	if (document.getElementById('tgt-'+(bookmark-1)) && bookmark >= ord && i > 0) {
	    --newBookmarks[i];
	}
    });
    Object.keys(newBookmarks).forEach(function(i) {
	i = parseInt(i);
	if (newBookmarks[i] == i) {
	    delete newBookmarks[i];
	}
    });
    bookmarkMoves = newBookmarks;
    crawlerRedrawCanvas();
}

function cropCrawl(ord) {
    var tr = document.getElementById('tgt-'+ord);
    var originalId = tr.id;
    while (tr.nextSibling)
	tr.nextSibling.remove();
    if (tr.parentNode.id != 'tgt-current')
	tr.remove();
    var current = document.getElementById('tgt-current');
    current.firstElementChild.id = originalId;
    if (ord < 1) {
	document.getElementById('auto_discover').setAttribute('disabled', '');
    }
}

function resetCrawl(f) {
    ws.send(JSON.stringify({cmd: 'reset'}));
    // Reset bookmark moves
    bookmarkMoves = {};
    var resultsTable = document.getElementById('crawl_results');
    $(resultsTable).trigger('archive')
    document.getElementById('auto_discover').setAttribute('disabled', '');
    if (f) {
	afterReset = f;
    }
}

function serializeData() {
    var payload = {};
    var serialized = $('#crawler_config')
	.serializeArray()
	.forEach(function(x) {
	    payload[x.name] = x.value;
	});
    payload.parser_type = parseInt(payload.parser_type);
    return payload;
}

$(document).ready(function() {
    if (!document.getElementById('crawler_config')) {
	return;
    }
    var firstPage = /firstPage=([^&]+)/.exec(document.location.href);
    if (firstPage) {
	document.getElementById('auto_url').value = decodeURIComponent(firstPage[1]);
    }
    var wsUri = 'wss://'+window.location.host+'/s/crawler/init/'+csrf_ham+(cid ? ('?cid='+cid) : '');
    ws = new WebSocket(wsUri);
    ws.onerror = function(ev) { console.log({x: 'error', ev: ev}) };
    ws.onopen = function(ev) {
	ws.send(csrf_ham);
	console.log({x: 'open', ev: ev})
	loadCrawlParams();
    };
    ws.onclose = function(ev) {
	console.log({x: 'close', ev: ev})
    };
    ws.onmessage = function(ev) {
	console.log({x: 'message', ev: ev});
	var result = JSON.parse(ev.data);
	if (!result)
	    return;
	if (result && result.ev) {
	    if (result.ev.done) {
		actionElementIds.forEach(function(x) {
		    document.getElementById(x).removeAttribute('disabled');
		});
		document.getElementById('halt').setAttribute('disabled', '');
	    } else if (result.ev.gocomics) {
		const root = 'https://www.gocomics.com/'+result.homepage+'/';
		document.getElementById('url_base').value = root;
		document.getElementById('url_tail').value = '';
		document.getElementById('homepage').value = root;
		document.getElementById('parser_type').value = 1003;
	    }
	}
	if (result && result.discover) {
	    document.getElementById('auto_discover').removeAttribute('disabled');
	    var suggestions = document.getElementById('parser_candidates');
	    while (suggestions.firstChild)
		suggestions.removeChild(suggestions.firstChild);
	    result.discover.forEach(function(cid) {
		var el = document.createElement('a');
		el.appendChild(document.createTextNode(cid));
		el.setAttribute('href', 'javascript::');
		suggestions.appendChild(el);
		suggestions.appendChild(document.createTextNode(' '));
	    });
	}
	if (result && result.add !== undefined) {
	    addCrawlRow(result.add);
	}
	if (result && result.remove !== undefined) {
	    removeCrawlRow(result.remove);
	}
	if (result && result.mark_removal !== undefined) {
	    // TODO figure out bookmark moves for this
	    removeCrawlRow(result.mark_removal);
	}
	if (result && result.reset !== undefined) {
	    cropCrawl(0);
	    if (afterReset) {
		afterReset();
		afterReset = null;
	    }
	}
	if (result && result.crop !== undefined) {
	    cropCrawl(result.crop);
	}
	if (result.msg) {
	    $('#msg').append($('<span class="message"/>').text(result.msg));
	}
	if (result.err) {
	    $('#msg').append($('<span class="error"/>').text(result.msg));
	}
    };
    $('#parser_candidates').on('click', 'a', function() {
	document.getElementById('parser_type').value = this.innerText;
    });
    $('#crawl_results')
	.on('contextmenu', 'tr', function(ev) {
	    var x = ev.originalEvent.x+window.pageXOffset;
	    var y = ev.originalEvent.y+window.pageYOffset;
	    $('#menu')
		.css({top: y, left: x})
		.show()
		.data('target', ev.currentTarget);
	    return false;
	});
    window.onclick = function() {
	$('#menu').hide();
    };
    $('#menu')
	.hide()
	.menu({
	    select: function(ev, ui) {
		var target = $('#menu')
		    .hide()
		    .data('target');
		var ord = parseInt(/tgt-(\d+)/.exec(target.id)[1]);
		switch(ev.currentTarget.id) {
		case 'menu-remove':
		    ws.send(JSON.stringify({cmd: 'remove', ord: ord}));
		    break;
		case 'menu-crop':
		    ws.send(JSON.stringify({cmd: 'crop', ord: ord}))
		    break;
		}
	    }
	});

    var actionElementIds = ['start', 'auto_url_act', 'insert_page_act', 'save'];
    $('#start')
	.on('click', function(ev) {
	    actionElementIds.forEach(function(x) {
		document.getElementById(x).setAttribute('disabled', '');
	    });
	    var vals = serializeData();
	    var payload = serializeData();
	    payload.cmd = 'start';
	    if (!payload.extra_data)
		delete payload.extra_data;
	    if (!payload.extra_url)
		delete payload.extra_url;
	    ws.send(JSON.stringify(payload));
	    $('#halt')
		.removeAttr('disabled')
		.on('click', function(ev) {
		    ws.send(JSON.stringify({cmd: 'halt'}));
		});
	    return false;
	});
    $('#auto_url_act')
	.on('click', function(ev) {
	    const baseEl = document.getElementById('url_base');
	    const tailEl = document.getElementById('url_tail');
	    const parserTypeEl = document.getElementById('parser_type');
	    var el = document.getElementById('auto_url');
	    if (el.selectionStart == el.selectionEnd) {
		const webtoons = /^https:\/\/(?:www|m)(\.webtoons\.com\/[^\/]+\/[^\/]+\/[^\/]+\/)(.+title_no=(\d+).+)$/.exec(el.value);
		if (webtoons) {
		    baseEl.value = 'https://www'+webtoons[1];
		    tailEl.value = '';
		    parserTypeEl.value = 978;
		    document.getElementById('homepage').value = 'https://www'+webtoons[1]+'list?title_no='+webtoons[3];
		    document.getElementById('crawl_delay').value = 1;
		    resetCrawl(function() {
			ws.send(JSON.stringify({'cmd': 'add', 'pages': [webtoons[2]]}));
		    });
		    return;
		}
		const tapas = /^https:\/\/(?:m\.)?tapas\.io\/episode\/(\d+)$/.exec(el.value);
		if (tapas) {
		    baseEl.value = 'https://tapas.io/episode/';
		    tailEl.value = '';
		    parserTypeEl.value = 1095;
		    resetCrawl(function() {
			ws.send(JSON.stringify({'cmd': 'add', 'pages': [tapas[1]]}));
		    });
		    return;
		}
		return;
	    }
	    baseEl.value=el.value.substring(0, el.selectionStart);
	    tailEl.value=el.value.substring(el.selectionEnd);
	    resetCrawl(function() {
		var page = el.value.substring(el.selectionStart, el.selectionEnd);
		ws.send(JSON.stringify({'cmd': 'add', 'pages': [page]}));
		if (page == '1') {
		    document.getElementById('lnrStart').value = '2';
		}
	    });
	});
    $('#insert_page_act')
	.on('click', function(ev) {
	    var el = document.getElementById('insert_page');
	    var urlBase = document.getElementById('url_base').value;
	    var urlTail = document.getElementById('url_tail').value;
	    var re = (new RegExp('^'+urlBase+'(.+)'+urlTail+'$')).exec(el.value);
	    ws.send(JSON.stringify({'cmd': 'add', 'pages': [re ? re[1] : el.value]}));
	});
    $('#lnrAdd')
	.on('click', function(ev) {
	    var i = parseInt(document.getElementById('lnrStart').value);
	    var end = parseInt(document.getElementById('lnrEnd').value);
	    var pad = parseInt(document.getElementById('lnrPad').value);
	    var pages = [];
	    for (; i <= end; ++i) {
		var page = String(i);
		if (pad > 0 && page.length < pad) {
		    page = "0".repeat(pad-page.length)+page;
		}
		pages.push(page);
	    }
	    ws.send(JSON.stringify({'cmd': 'add', 'pages': pages}));
	});
    $('#clear')
	.on('click', function(ev) {
	    resetCrawl();
	});
    $('#save')
	.on('click', function(ev) {
	    const source = document.getElementById('source');
	    var payload = {cmd: source ? 'sourceSave' : 'save', bookmarkMoves: bookmarkMoves};
	    var sid = /sid=(\d+)/.exec(document.location.href);
	    payload.cid = parseInt(document.getElementById('cid').value);
	    payload.homepage = document.getElementById('homepage').value;
	    payload.fixedHead = document.getElementById('fixed_head').value;
	    // Save mode gets these from the crawl parameters
	    if (source) {
		payload.sourceCid = parseInt(source.value);
		payload.url_base = document.getElementById('url_base').value;
		payload.url_tail = document.getElementById('url_tail').value;
		if (document.getElementById('extra_url').value)
		    payload.extra_url = document.getElementById('extra_url').value;
		if (document.getElementById('extra_data').value)
		    payload.extra_data = document.getElementById('extra_data').value;
		payload.parser_type = parseInt(document.getElementById('parser_type').value);
	    }
	    ws.send(JSON.stringify(payload));
	    if (sid) {
		sid = sid[1];
		$('#genentry')
		    .show()
		    .attr('href', 'genentry.html?sid='+sid+'&url_base='+
			  encodeURIComponent(document.getElementById('url_base').value)+
			  '&url_tail='+
			  encodeURIComponent(document.getElementById('url_tail').value)+
			  '&extra_url='+
			  encodeURIComponent(document.getElementById('extra_url').value)+
			  '&extra_data='+
			  encodeURIComponent(document.getElementById('extra_data').value)+
			  '&parser_type='+document.getElementById('parser_type').value);
	    }
	});
    $('#auto_discover').click(function(x) {
	var results = $('#crawl_results tr.bookmark_drag');
	var len = results.length;
	var payload = {cmd: 'discover'};
	payload.url_base = document.getElementById('url_base').value;
	payload.url_tail = document.getElementById('url_tail').value;
	payload.source = results[len-2].firstElementChild.nextElementSibling.innerText;
	payload.target = results[len-1].firstElementChild.nextElementSibling.innerText;
	ws.send(JSON.stringify(payload));
	document.getElementById('auto_discover').setAttribute('disabled', '');
    });
    $('#gocomics_helper').click(function(ev) {
	resetCrawl(function() {
	    var url = document.getElementById('auto_url').value;
	    ws.send(JSON.stringify({'cmd': 'gocomics', 'url': url}));
	});
    });
    $('#crawl_results').on('archive', function(x) {
	var resultsTable = document.getElementById('crawl_results');
	var len = $('#crawl_results tr.bookmark_drag').length;
	if (len > 1) {
	    document.getElementById('auto_discover').removeAttribute('disabled');
	} else {
	    document.getElementById('auto_discover').setAttribute('disabled', '');
	}
	var lastDiff;
	$(resultsTable.parentNode.parentNode)
	    .find('.bookmark_drag')
	    .droppable({
		scope: 'bookmark_transfer',
		activate: function(ev, ui) {
		    if (lastDiff !== undefined) {
			var src = parseInt(/src-(\d+)/.exec(ui.draggable[0].id)[1]);
			var suggest = src + lastDiff;
			$('#tgt-'+suggest).addClass('suggest');
		    }
		},
		drop: function(ev, ui) {
		    $('#crawler_archives .suggest').removeClass('suggest');
		    var src = parseInt(/src-(\d+)/.exec(ui.draggable[0].id)[1]);
		    var tgt = parseInt(/tgt-(\d+)/.exec(ev.target.id)[1]);
		    if (src == tgt) {
			delete bookmarkMoves[src];
		    } else {
			bookmarkMoves[src] = tgt;
		    }
		    lastDiff = tgt - src;
		    crawlerRedrawCanvas();
		}
	    });
    });
});
