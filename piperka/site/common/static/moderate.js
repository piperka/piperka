var csrf_ham = /csrf_ham=([0-9a-z-]+)/.exec(document.cookie);
csrf_ham = csrf_ham ? csrf_ham[1] : null;

(function( $ ){
    $.fn.pModerate = function() {
	$(this).on('click', 'tr', function() {
	    $('.hideafterdone').show();
	    $('#msgdiv').html('');
	    $('#addedtags').hide().find('.diff').html('');
	    $('#removedtags').hide().find('.diff').html('');
	    $('#currententry').html('');
	    $('.submitcomic').off('submit');
	    var thisrow = $(this).on('delete', function(){
		$('.submitcomic').hide();
		$(this).remove();
		$('#current-entry').html('');
		$('#nummod').text(($('#nummod').text())-1);
	    });
	    $('#editinfo').one('submit', function(){thisrow.trigger('delete')});
	    enableSubmitcomic();
	    $('.submitcomic').show();
	    var sid = thisrow.attr('id').substring(4);
	    var cid = thisrow.find('.cid').attr('id').substring(4);
	    $('#useredit-banner').html('<img src="/s/viewsubmitbanner/'+sid+'">');
	    $('#info-cid').text(cid);
	    $('#info-title').attr('href', 'info.html?cid='+cid).text(thisrow.find('.cid').text());
	    resetSubmitForm();
	    $('.submitcomic input[name=cid]').val(cid);
	    $('.submitcomic input[name=user_sid]').val(sid);
	    $('#user-edits tr').removeClass('hilite removeafterdone');
	    thisrow.addClass('hilite removeafterdone');
	    $('#removeedit').off('click');
	    $('#removeedit').one('click', function(){
		$.ajax({url:'/s/dropsubmit/'+sid,
			data:{'csrf_ham':csrf_ham},
			type:'POST',
			success:function(rpy){
			    if (rpy.ok) {
				thisrow.trigger('delete');
			    } else if (rpy.errmsg) {
				$('#msgdiv').hide().html(rpy.errmsg).slideDown();
			    }
			}});
	    });
	    $.ajax({url:'/include/cinfo',
		    data:{'cid':cid},
		    dataType:'html',
		    success:function(rpy){
			$('#current-entry').html(rpy);
		    }});
	    $.ajax({url:'/s/sinfo/'+sid,
		    dataType: 'json',
		    success:function(rpy){
			if (rpy != null) {
			    if (rpy.ok) {
				setTagsEpedias(rpy);
				if (rpy.oldbanner) {
				    $('.oldbanner').show().find('span').text(rpy.banner);
				} else {
				    $('.oldbanner').hide();
				}
				if (rpy.newbanner) {
				    $('.hasbanner.script').show();
				    var bannerCheckbox = $('.hasbanner input[type=checkbox]')[0];
				    if (bannerCheckbox)
					bannerCheckbox.checked = true;
				} else {
				    $('.hasbanner').hide();
				}
				$('.submitcomic input[name=banner]').val(rpy.banner);
				$('.submitcomic textarea[name=description]').val(rpy.description);
			    } else if (rpy.errmsg) {
				$('#msgdiv').hide().html(rpy.errmsg).slideDown();
			    }
			}
		    }});
	});
	return this;
    };
    $.fn.setChildFields = function(rpy) {
	var mainField = $(this);
	$.each(rpy, function(k,v){
	    var field = mainField.find('#'+k)[0];
	    if (typeof field != 'undefined') {
		switch (field.tagName.toLowerCase()) {
		case 'span':
		case 'textarea':
		    $(field).text(v);
		    break;
		case 'a':
		    field.innerText = v;
		    field.setAttribute('href', v);
		    break;
		case 'input':
		    var type = field.getAttribute('type');
		    if (type == 'checkbox') {
			if (v) {
			    field.setAttribute('checked', 1);
			} else {
			    field.removeAttribute('checked');
			}
		    } else if (type == 'text') {
			field.setAttribute('value', v);
		    }
		    break;
		}
	    }
	});
	return this;
    };
    $.fn.pSubmissions = function() {
	$(this).on('click', 'tr', function() {
	    var sid = this.getAttribute('id').substring(4);
	    $('#submission-banner').html('<img src="/s/viewsubmitbanner/'+sid+'">');
	    $.ajax({url:'/s/sinfo2/'+sid,
		    dataType: 'json',
		    success: function(rpy){
			$('#genentry-link')[0].setAttribute('href', 'genentry.html?sid='+sid);
			$('#crawler-link')[0].setAttribute('href', 'mod_crawler.html?sid='+sid);
			$('#submission-entry').setChildFields(rpy);
		    }});
	});
	return this;
    };
    $.fn.pRefreshComicLists = function() {
	var msg = $(this).find('span');
	var but = $(this).find('button');
	$(this).on('click', 'button', function() {
	    but.prop('disabled', 1);
	    $.ajax({url: '/s/generateComicTitles',
		    data: {csrf_ham: csrf_ham},
		    method: 'POST'
		   })
		.done(function() {
		    msg.text('Done');
		})
		.fail(function() {
		    msg.text('Fail');
		})
		.always(function() {
		    but.prop('disabled', 0);
		});
	});
    };
})( jQuery );

$(document).ready(function(){
    var sid = /sid=(\d+)/.exec(document.location.href);
    if (sid) {
	sid = sid[1];
    }
    var genentry = $('#genentry-form');
    if (genentry.length > 0) {
	genentry.show();
	$('#tagdiff .script').show();
	$('#submission-banner').html('<img src="/s/viewsubmitbanner/'+sid+'">');
	$.ajax({url:'/s/genentry/'+sid,
		dataType: 'json',
		success: function(rpy) {
		    if (rpy.newbanner) {
			$('.hasbanner.script').show();
			$('.hasbanner input[type=checkbox]')[0].checked = true;
		    } else {
			$('.hasbanner').hide();
		    }
		    rpy.origtags = [];
		    setTagsEpedias(rpy);
		    if (!/^http/.test(rpy.homepage)) {
			rpy.homepage = 'http://'+rpy.homepage;
		    }
		    $('#genentry-form').setChildFields(rpy);
		    $('#homepage_link').attr('href', rpy.homepage);
		    var urlBase = /&url_base=([^&]+)/.exec(document.location.href);
		    if (urlBase) {
			$('#url_base').val(decodeURIComponent(urlBase[1]));
		    }
		    var urlTail = /&url_tail=([^&]+)/.exec(document.location.href);
		    if (urlTail) {
			$('#url_tail').val(decodeURIComponent(urlTail[1]));
		    }
		    var parserType = /&parser_type=(\d+)/.exec(document.location.href);
		    if (parserType) {
			$('#parser_id').val(parserType[1]);
		    }
		    var extraData = /&extra_data=([^&]+)/.exec(document.location.href);
		    if (extraData) {
			$('#extra_data').val(decodeURIComponent(extraData[1]));
		    }
		    var extraUrl = /&extra_url=([^&]+)/.exec(document.location.href);
		    if (extraUrl) {
			$('#extra_url').val(decodeURIComponent(extraUrl[1]));
		    }
		}});
    }
    var crawler = $('#crawler_config');
    if (crawler.length > 0) {
	if (sid) {
	    $('#genentry').show();
	    $('#genentry').attr('href', 'genentry.html?sid='+sid);
	    $.ajax({url:'/s/genentry/'+sid,
		    dataType: 'json',
		    success: function(rpy) {
			var page = rpy.first_page ? rpy.first_page : rpy_homepage;
			$('#auto_url').val(page);
		    }});
	} else {
	    $('#genentry').hide();
	}
    }

    var cid = /cid=(\d+)/.exec(document.location.href);
    var hiatusTable = $('#hiatus-table');
    if (cid) {
	cid = cid[1];
	var markDone = function() {
	    $('#hiatus_single').replaceWith('Done');
	};
	var markError = function(err,b,c,d) {
	    $('#hiatus_error').text('error');
	};
	$('#clear_hiatus_single').on('click', function() {
	    $.ajax({url:'/s/hiatus',
		    method: 'POST',
		    dataType: 'text',
		    data: {'cid': cid, 'clear': 1, 'csrf_ham': csrf_ham}
		   })
		.then(markDone)
		.catch(markError);
	});
	$('#hiatus_single').on('input', function(event) {
	    if (event.target.value) {
		$.ajax({url:'/s/hiatus',
			method: 'POST',
			dataType: 'text',
			data: {'cid': cid, 'status': event.target.value, 'csrf_ham': csrf_ham}
		       })
		    .then(markDone)
		    .catch(markError);
	    }
	});
    } else if (hiatusTable.length > 0) {
	var markDone = function(res) {
	    return function() {
		res.append('<span class="message">Success</span>');
	    };
	}
	var markError = function(res) {
	    return function() {
		res.append('<span class="error">Error</span>');
	    };
	}
	hiatusTable.on('input', 'tr', function(event) {
	    var res = $(event.currentTarget).find('.result');
	    res.empty();
	    if (event.target.value) {
		$.ajax({url: '/s/hiatus',
			method: 'POST',
			dataType: 'text',
			data: {'cid': event.currentTarget.dataset.cid,
			       'status': event.target.value,
			       'csrf_ham': csrf_ham}
		       })
		    .then(markDone(res))
		    .catch(markError(res));
	    }
	});
	hiatusTable.on('click', 'button', function(event) {
	    var row = $(event.currentTarget).closest('tr');
	    var res = row.find('.result');
	    res.empty();
	    $.ajax({url: '/s/hiatus',
		    method: 'POST',
		    dataType: 'text',
		    data: {'cid': row[0].dataset.cid,
			   'defer': 1,
			   'csrf_ham': csrf_ham}
		   })
		.then(markDone(res))
		.catch(markError(res));
	});
    }
    $('#user-hiatus-edits').on('click', 'button', function(ev) {
	var row = $(ev.target).parents('tr')
	var data = row[0].dataset;
	var payload = {
	    cid: parseInt(data.cid),
	    uhid: parseInt(data.uhid),
	    csrf_ham: csrf_ham
	};
	if (ev.target.name == 'use') {
	    payload.status = data.code;
	}
	var msg = row.children('.msg');
	msg.text('');
	$.ajax({url: '/s/hiatus',
		method: 'POST',
		dataType: 'text',
		data: payload
	       })
	    .then(function() {
		row.find('button').prop('disabled', 'true');
		msg.text('Success');
	    }).catch(function() {
		msg.text('Error');
	    });
    });

    $('#base-redirects').on('click', 'button', function(ev) {
	var button = ev.currentTarget;
	$.ajax({url: '/s/autoFix',
		method: 'POST',
		data: {'cid': button.dataset.cid,
		       'csrf_ham': csrf_ham}
	       })
	    .then(function(resp) {
		var msg = $(button.parentElement).find('.message').text('');
		var err = $(button.parentElement).find('.error').text('');
		button = button
		if (resp && resp.success) {
		    msg.text('success');
		    button.setAttribute('disabled', 'disabled');
		} else if (resp && !resp.success) {
		    msg.text('nothing done');
		} else {
		    err.text('fail');
		}
	    }).catch(function(obj,b,c) {
		$(button.parentElement).find('.message').text('');
		$(button.parentElement).find('.error').text(c);
	    });
    });
});
