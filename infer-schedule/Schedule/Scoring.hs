{-# LANGUAGE TupleSections #-}

module Schedule.Scoring where

import Control.Monad
import Data.Maybe (listToMaybe)
import Data.List (foldl', sort, sortOn, group)
import Data.Time.Clock

-- Weighed occurrences of update hours.  Returns a list of 168 items.
weeklySchedule :: UTCTime -> Int -> [UTCTime] -> [Double]
weeklySchedule start weeks xs = map
  (\t -> sum $ map (score . fst) $ filter ((==t) . snd) ps) [0..(7*24-1)]
  where
    weekPoint :: UTCTime -> (Int, Int)
    weekPoint x = ((floor (diffUTCTime x start)) `div` (60*60)) `divMod` (7*24)
    score week = 0.95 ** fromIntegral (weeks - week)
    ps = map weekPoint $ dropWhile (< start) xs

weeklyUpdateCount :: UTCTime -> UTCTime -> Int -> [UTCTime] -> Maybe Int
weeklyUpdateCount = updateCount (7*24*60*60) 2
  (\xs un -> case fst $ head xs of
      -- If the most frequent weekly count is 1 then require that 3/4
      -- of weeks have an update
      1 -> let populatedWeeks = sum $ map snd xs
           in populatedWeeks >= 3*un `div` 4
      _ -> snd (head xs) > un `div` 3
      ) Nothing

monthlyUpdateCount :: UTCTime -> UTCTime -> Int -> [UTCTime] -> Maybe Int
monthlyUpdateCount start comicStart months xs = do
  let monthSec = 30*24*60*60
  guard $ (not $ null xs) && (last xs >= addUTCTime (3*fromIntegral monthSec) start)
  updateCount monthSec 3 (\ys un -> snd (head ys) > un `div` 2) (Just 4)
    start comicStart months xs

updateCount :: Int -> Int -> ([(Int, Int)] -> Int -> Bool) -> Maybe Int
            -> UTCTime -> UTCTime -> Int -> [UTCTime] -> Maybe Int
updateCount spanSec minSpans divF maxCount start comicStart spans xs = do
  let un = min spans $ spans - floor (diffUTCTime comicStart start) `div` spanSec
  guard $ un >= minSpans
  a <- fst <$> listToMaybe spanUpdates
  guard $ divF spanUpdates un
  maybe (return ()) (guard . (>= a)) maxCount
  return a
  where
    updates = map (\x -> floor (diffUTCTime x start) `div` spanSec) $ dropWhile (< start) xs
    spanUpdates = reverse . sortOn snd . map (\ys -> (head ys, length ys)) .
                  group . sort . map length $ group updates

scoreWeek :: [Double] -> [Double]
scoreWeek xs = let xs' = scanr (:) [] $ cycle xs
                   -- Give the leading edge a boost to try to catch an
                   -- earlier update spot
                   score x | x < 0 = 1.1 ** (x+5)
                           | otherwise = 1.1 ** (-5*x+5)
  in take 168 . drop 84 $
     map (sum . map (\(i, x) -> x*score i) . zip [-84..83]) xs'

data ScheduleFail = TooLow Double | TooSmooth Double | TooManyPeaks [Int] | TooDensePeaks [Int]
  deriving (Show, Eq)

findPeaks :: Maybe Int -> [Double] -> Either ScheduleFail [Int]
findPeaks weeklyUpdates xs = do
  when (hi < peakLimit) $ Left $ TooLow hi
  when (range < minRange) $ Left $ TooSmooth range
  when (length peaks > 7) $ Left $ TooManyPeaks peaks
  when (12 > (minimum $ map (uncurry (-)) $
              (head peaks, last peaks - 168):zip (tail peaks) peaks)) $
    Left $ TooDensePeaks peaks
  return peaks
  where
    peakLimit = 4.0
    minRange = 4.0
    lo = minimum xs
    hi = maximum xs
    range = hi-lo
    cutoff | maybe False (>= 6) weeklyUpdates = lo + 2*range/5
           | otherwise = lo+2*range/3
    initialPeakLen = length $ takeWhile (>cutoff) xs
    groupConsecutive :: [(Int, a)] -> [[(Int, a)]]
    groupConsecutive [] = []
    groupConsecutive ys@(((i, _)):_) = reverse . map reverse . snd $ foldl'
      (\(j, yss@(ys':yss')) a@(j', _) -> if j' == j
        then (j+1, (a:ys'):yss')
        else (j'+1, [a]:yss)) (i, [[]]) ys
    peakIndex :: [(Int, Double)] -> Int
    peakIndex = fst . foldr (\a b -> if snd a > snd b then a else b) (0, 0.0)
    peaks :: [Int]
    peaks = sort . map (`rem` 168) . map peakIndex . groupConsecutive .
      filter ((>cutoff) . snd) $
      zip [initialPeakLen..initialPeakLen+167]
      (take 168 $ drop initialPeakLen $ cycle xs)
