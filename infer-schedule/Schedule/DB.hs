{-# LANGUAGE OverloadedStrings #-}

module Schedule.DB where

import Contravariant.Extras.Contrazip
import Control.Monad
import Data.Functor.Contravariant
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap as IntMap
import Data.List (foldl', groupBy, sort)
import Data.Time.Clock
import Data.Time.LocalTime
import Hasql.Decoders as D
import Hasql.Encoders as E
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement

getUpdateDates
  :: UTCTime
  -> UTCTime
  -> Session (IntMap (UTCTime, [UTCTime]))
getUpdateDates begin end = statement (begin, end) $
  Statement sql (contrazip2 tm tm) decoder True
  where
    sql = "SELECT cid, coalesce(added_on, '2005-01-01'), array_agg(stmp), array_agg(ord) FROM (\
          \ SELECT cid, run_id, date_trunc('hour', min(stamp)) AS stmp, max(ord) AS ord, \
          \ count(CASE WHEN event_type='parse' AND event->'parse'->>'type' = 'result' \
          \  THEN 1 ELSE NULL END) - \
          \ CASE WHEN sum( \
          \  CASE WHEN event_type='parse' AND COALESCE((event->'parse'->>'auto') :: boolean, false) \
          \  THEN 1 ELSE 0 END)>0 THEN 1 ELSE 0 END - \
          \ max(CASE WHEN event_type='terminal' \
          \  THEN COALESCE((event->>'head_crop') :: smallint, old_head_crop_count) \
          \  ELSE NULL END) - \
          \ CASE WHEN sum((event_type='terminal' \
          \  AND event->'end'->>'type' IN ('faulty', 'http_error'))::int)>0 \
          \ THEN 1 ELSE 0 END AS new_pages \
          \ FROM crawler_log JOIN crawler_config USING (cid) JOIN comics USING (cid) \
          \ WHERE stamp >= $1 AND stamp < $2 \
          \ GROUP BY cid, run_id\
          \) AS a JOIN comics USING (cid) \
          \WHERE new_pages > 0 GROUP BY cid, added_on ORDER BY cid"
    tm = E.param $ utcToLocalTime utc >$< E.timestamp
    decoder = fmap IntMap.fromAscList . D.rowList $
      (,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> ((,)
           <$> D.column (localTimeToUTC utc <$> D.timestamp)
           <*> (fmap (map (fst . head) . groupBy (\a b -> snd a == snd b) . sort) $
                zip
                <$> D.column (D.array (D.dimension replicateM
                                       (localTimeToUTC utc <$> D.element D.timestamp)))
                <*> D.column (D.array (D.dimension replicateM $ D.element D.int4))))

saveUpdateSchedule
  :: [(Int, [Int], Maybe Int, Maybe Int)]
  -> Session ()
saveUpdateSchedule sched = do
  S.sql "DELETE FROM inferred_schedule"
  mapM_ (flip statement $ Statement sql
         (contrazip4 (E.param $ fromIntegral >$< E.int4)
          (E.param $ E.array (E.dimension foldl' (E.element $ fromIntegral >$< E.int2)))
          (E.nullableParam $ fromIntegral >$< E.int2)
          (E.nullableParam $ fromIntegral >$< E.int2)
         ) D.unit True) sched
  where
    sql = "INSERT INTO inferred_schedule \
          \(cid, schedule, weekly_updates, monthly_updates) VALUES \
          \($1, $2 :: smallint[], $3, $4)"
