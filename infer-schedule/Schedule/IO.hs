{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}

module Schedule.IO where

import Control.Monad.Trans.Except
import Data.Aeson
import Data.IntMap (IntMap, toList)
import Data.List (intercalate)
import Data.Time.Clock
import Data.Time.Calendar.WeekDate
import Data.Time.Format (defaultTimeLocale, formatTime, parseTimeM)
import Text.Printf

import Crawler.DB
import Schedule.Scoring (ScheduleFail(..))
import Schedule.DB

formatTime' :: UTCTime -> String
formatTime' = formatTime defaultTimeLocale "%F %T"

parseTime' :: MonadFail m => String -> m UTCTime
parseTime' = parseTimeM False defaultTimeLocale "%F %T"

newtype UpdateTimesOne = UpdateTimesOne { unOne :: (UTCTime, [UTCTime]) }

instance ToJSON UpdateTimesOne where
  toJSON (UpdateTimesOne (addedOn, xs)) =
    object [ "addedOn" .= formatTime' addedOn
           , "updates" .= map formatTime' xs
           ]

instance FromJSON UpdateTimesOne where
  parseJSON = withObject "Comic" $ \v ->
    UpdateTimesOne
    <$> ((,)
          <$> (parseTime' =<< v .: "addedOn")
          <*> (mapM parseTime' =<< v .: "updates")
        )

newtype UpdateTimesStorable = UpdateTimesStorable { unStore :: (UTCTime, UTCTime, IntMap UpdateTimesOne) }

instance ToJSON UpdateTimesStorable where
  toJSON (UpdateTimesStorable (weeksStart, monthsStart, comics)) =
    object [ "weeksStart" .= formatTime' weeksStart
           , "monthsStart" .= formatTime' monthsStart
           , "comics" .= comics
           ]

instance FromJSON UpdateTimesStorable where
  parseJSON = withObject "UpdateTimes" $ \v ->
    UpdateTimesStorable
    <$> ((,,)
         <$> (parseTime' =<< v .: "weeksStart")
         <*> (parseTime' =<< v .: "monthsStart")
         <*> v .: "comics"
        )

getUpdateTimes :: DBConfig -> UTCTime -> Int -> Int
             -> IO (Either String (UTCTime, UTCTime, IntMap (UTCTime, [UTCTime])))
getUpdateTimes settings stamp weeks months = do
  let (year, weeknum, _) = toWeekDate . utctDay $ stamp
      end = UTCTime (fromWeekDate year weeknum 1) (fromInteger 0)
      ws = addUTCTime (fromIntegral (-weeks)*7*nominalDay) end
      ms = addUTCTime (fromIntegral (-months)*30*nominalDay) end
  runExceptT $ withExceptT show $ (ws,ms,) <$> (withConn settings $ getUpdateDates ms end)

storeInferredSchedules
  :: DBConfig
  -> IntMap ([Int], Maybe Int, Maybe Int)
  -> IO (Either String ())
storeInferredSchedules settings =
  fmap (either (Left . show) Right) .
  runExceptT . withConnTransactionally settings .
  saveUpdateSchedule . map (\(a,(b,c,d)) -> (a,b,c,d)) . toList

saveUpdateTimes
  :: FilePath
  -> (UTCTime, UTCTime, IntMap (UTCTime, [UTCTime]))
  -> IO ()
saveUpdateTimes file = encodeFile file .
  UpdateTimesStorable .
  (\(a,b,x) -> (a,b,fmap UpdateTimesOne x))

loadUpdateTimes
  :: FilePath
  -> IO (Either String (UTCTime, UTCTime, IntMap (UTCTime, [UTCTime])))
loadUpdateTimes = (fmap . fmap) ((\(a,b,x) -> (a,b,fmap unOne x)) . unStore) . eitherDecodeFileStrict

-- Dump dat files for computed schedules for gnuplot.  Uses fixed file
-- names, see the supplied gnuplot file.
dumpPlots
  :: (Either ScheduleFail [Int], [Double], Maybe Int, Maybe Int)
  -> IO ()
dumpPlots (res, weeklyScores, _, _) = do
  let toRadian :: Int -> Double
      toRadian = (*pi) . (*2) . (/168) . fromIntegral
  writeFile "weeklyScores.dat" . intercalate "\n" .
    map (uncurry (printf "%.6f %.6f")) $
    zip (map toRadian [0..167]) weeklyScores
  case res of
    Left err -> do
      print err
      writeFile "inferredSchedule.dat" ""
    Right xs ->
      writeFile "inferredSchedule.dat" . intercalate "\n" $
      map (printf "%.6f 100" . toRadian) xs
