{-# LANGUAGE OverloadedStrings #-}

module Schedule where

import Control.Parallel.Strategies
import Data.Time (UTCTime, getCurrentTime)
import qualified Data.IntMap as IntMap
import Options.Applicative

import Crawler.DB
import Schedule.IO
import Schedule.Scoring

data InferScheduleOptions = InferScheduleOptions
  { dbOpt :: DBConfig
  , loadTimes :: Maybe FilePath
  , saveTimes :: Maybe FilePath
  , plot :: Maybe Int
  , stamp :: Maybe UTCTime
  }

main :: IO ()
main = do
  scheduleOptions <- execParser opts
  updatesAt <- maybe getCurrentTime return $ stamp scheduleOptions
  let weeks = 12
      months = 5
      settings = dbOpt scheduleOptions
  inp@(wStart, mStart, inputSched) <- either error id <$> maybe
    (getUpdateTimes settings updatesAt weeks months)
    loadUpdateTimes (loadTimes scheduleOptions)
  let schedules =
        (\(ini, xs) ->
           let weekly = weeklyUpdateCount wStart ini weeks xs
               weeklyScores = scoreWeek $ weeklySchedule wStart weeks xs
           in ( findPeaks weekly weeklyScores
              , weeklyScores
              , weekly
              , monthlyUpdateCount mStart ini months xs
              )
        ) <$> inputSched
  case (saveTimes scheduleOptions, plot scheduleOptions) of
    (Just outputFile, _) -> saveUpdateTimes outputFile inp
    (_, Just cid) -> dumpPlots (schedules IntMap.! cid)
    _ -> (storeInferredSchedules settings .
          withStrategy (parTraversable rdeepseq) .
          fmap (\(x,_,b,c) -> (either (const []) id $ x, b, c)) $ schedules) >>=
         either print (const $ return ())
  where
    parser :: Parser InferScheduleOptions
    parser = InferScheduleOptions
      <$> dbArg
      <*> (optional $ strOption (long "load"
                                 <> help "Load input data from file instead of using DB"))
      <*> (optional $ strOption (long "save"
                                 <> help "Save input data from DB and quit"))
      <*> (optional $ option auto (long "plot"
                                   <> help "Dump plot data for one comic and quit"))
      <*> (optional $ option (maybeReader parseTime')
           (long "at"
             <> help "Timestamp for when to gather schedule data"))
    dbArg = DBConfig
      <$> strOption (long "db" <> value "postgresql://kaol@/piperka")
      <*> strOption (long "schema" <> value "piperka")
    opts = info (parser <**> helper) $
      fullDesc <> progDesc "Piperka Schedule infer"
