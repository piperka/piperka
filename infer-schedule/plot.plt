set polar
set ttics add ("monday" 0)
set ttics add ("tuesday" 51.4)
set ttics add ("wednesday" 102.9)
set ttics add ("thursday" 154.3)
set ttics add ("friday" 205.7)
set ttics add ("saturday" 257.1)
set ttics add ("sunday" 308.6)
set grid r polar 51.4
set rrange [0:20]
# generate with infer-schedule --plot
plot "weeklyScores.dat" with lines, "inferredSchedule.dat" with impulses
