#! /usr/bin/perl -w

# Static blog generator.  Takes blog posts and templates and generates
# an index of them and runs XSLT on them.

use strict;

use File::Find::Object;
use XML::LibXML;
use Date::Parse;
use DateTime;
use File::Copy;
use File::Temp qw/ tempdir /;
use File::Path qw/ make_path /;
use Getopt::Std;

my %opts;
getopts('xd:D', \%opts);
my $xsltmode = $opts{x};
my $devel = $opts{D};
my $dest = $opts{d};
$dest = '/srv/piperka.net/files/blog/' if !defined $dest;
my $tempdir = tempdir('/tmp/generate_blog.XXXXXXX', CLEANUP => 1);

# Go through all the blog posts and mark up some data
my $tree = File::Find::Object->new({}, '/home/kaol/src/blog');
my @posts;
while (my $r = $tree->next()) {
    if ($r =~ m<\.xml$>) {
	next if $r =~ m<(?:\b|/)template/>;
	# load the file
	my $dom = XML::LibXML->load_xml(location => $r);
	# XPath context to query it
	if (defined $dom) {
	    my $node = $dom->getDocumentElement;
	    if (defined $node && $node->nodeName eq 'post') {
		my $xpc = XML::LibXML::XPathContext->new($node);
		my $date_str = $xpc->findvalue('/post/meta/date');
		my $date = str2time($date_str);
		if (defined $date) {
		    my $dt = DateTime->from_epoch(epoch => $date);
		    $r =~ m<([^/]+)\.xml$>;
		    my $target = $dt->year."/$1/";
		    my $fragment = '';
		    my @anchors = $xpc->findnodes('/post/content//a[@name]/@name');
		    if (@anchors > 0) {
			my $anchor = pop @anchors;
			$fragment = '#'.$anchor->to_literal;
		    }
		    push @posts, {file => $r, epoch => $date, target => $target,
				  fragment => $fragment, date => $date_str,
				  title => $xpc->findvalue('/post/meta/title'),
				  dom => $dom,
		    };
		    make_path($tempdir.'/'.$target);
		} else {
		    print STDERR "no date found for $r\n";
		}
	    } else {
		print STDERR "skipping file not recognized as blog post $r\n";
	    }
	}
    } elsif ($r =~ m<\.xslt$>) {
	copy($r, $tempdir);
    } elsif ($r =~ m<\.(?:jpg|png|gif)$>) {
	$r =~ m<.*?/blog/(.*)/(.*)>;
	make_path($tempdir.'/'.$1);
	copy($r, $tempdir.'/'.$1);
    }
}

# Now, build an index from the matching posts
my $idxdom = XML::LibXML::Document->new();
my $idx = XML::LibXML::Element->new('index');
if ($devel) {
    $idx->setAttribute('noads', 1);
}
$idxdom->setDocumentElement($idx);

my $ord = 1;

foreach my $post (sort {$a->{epoch} <=> $b->{epoch}} @posts) {
    my $node = XML::LibXML::Element->new('post');
    $node->setAttribute('href', $post->{target});
    $node->setAttribute('fragment', $post->{fragment});
    $node->setAttribute('title', $post->{title});
    $node->setAttribute('date', $post->{date});
    $node->setAttribute('ord', $ord);
    $idx->appendChild($node);
    $post->{dom}->getDocumentElement->setAttribute('ord', $ord);
    $post->{dom}->toFile($tempdir.'/'.$post->{target}.'index.xml');
    ++$ord;
}

$idxdom->toFile("$tempdir/blogidx.xml", 2);

# Now, copy the template files in place
$tree = File::Find::Object->new({}, '/home/kaol/src/blog/template/');
while (my $r = $tree->next()) {
    next if $r =~ m<~$>;
    if ($r =~ m<(.*/)(.+)\.xml$>) {
	my ($dir, $target) = ($1, $2);
	$dir =~ s,.*?template/,,;
	$target =~ s,index$,,;
	make_path($tempdir.'/'.$dir.$target);
	copy($r, $tempdir."/$dir$target/index.xml");
    } elsif ($r =~ m<(?:.*?template/)(.*?)([^/]+\.[^/]+)$>) {
	my ($dir, $file) = ($1, $2);
	if ($dir ne '') {
	    make_path($tempdir.'/'.$dir);
	}
	copy($r, $tempdir.'/'.$dir);
    }
}

my $olddir = `pwd`;
chomp $olddir;

chdir $tempdir;

if (!$xsltmode) {
# next: run xsltproc on everything
    $tree = File::Find::Object->new({}, $tempdir);
    while (my $r = $tree->next()) {
	my $file = $r;
	$file =~ s,^/tmp/[^/]+/,,;
	next unless $file =~ m<\.xml$>;
	if ($file eq 'blogidx.xml') {
	    system "xsltproc -o blog.rss rss.xslt $file";
	    system "xsltproc -o blog.atom atom.xslt $file";
	} elsif ($file =~ m<(.*index)\.xml>) {
	    system "xsltproc -o $1.html blog.xslt $file";
	}
    }

# Done.
    system "rsync -r --exclude='*.xml' --exclude='*.xslt' --delete-excluded $tempdir/ $dest";
} else {
    system "rsync -r --delete $tempdir/ $dest";
}

chdir '/';
