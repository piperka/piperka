#! /bin/bash

set -e

# A script for dumping data and server config living outside of git.

if [ $UID != 0 ]; then
    echo "run as root"
    exit 1
fi

DIR="$2"
if [ -z "$DIR" ]; then
    if [ "$1" = backup ]; then
	DIR=/home/kaol/backup/$(($(date +%s) / 86400 % 2))
    else
	DIR=/home/kaol/migrate
    fi
fi
DIR=$(realpath $DIR)

function save_cfg {
    tar zcf $DIR/piperka-cfg.tar.gz \
	etc/imapd.conf \
	etc/cyrus.conf \
	etc/exim4/exim.crt \
	etc/exim4/exim.key \
	etc/exim4/passwd.client \
	etc/exim4/dkim_domains \
	etc/exim4/update-exim4.conf.conf \
	etc/exim4/conf.d/main/00_local_macros \
	etc/exim4/conf.d/transport/10_lmtp \
	etc/exim4/private \
	etc/default/saslauthd \
	etc/letsencrypt \
	etc/crossfire \
	home/kaol/dkim \
	home/kaol/old \
	home/kaol/.irssi \
	usr/local/lib/piperka/*.cfg \
	usr/local/lib/piperka/snaplets/*/*/*.cfg \
	var/local/piperka/site_key.txt \
	var/www/piperka \
	var/local/piperka/usr/add
    crontab -l > $DIR/crontab.root
    crontab -l -u kaol > $DIR/crontab.kaol
}

function save_snap {
    tar zcf $DIR/piperka-data.tar.gz \
	var/games/crossfire \
	var/local/piperka/data
}

function save_db {
    # Note: shell pipe is as root so ok
    sudo -u postgres -- pg_dump -F c piperka > $DIR/piperka.db
}

cd /
mkdir -p $DIR
chown kaol: $DIR
case "$1" in
    # Config and random stuff not in git
    cfg)
	save_cfg
	;;
    # Some stuff that changes daily at most.  May be large.
    slow-data)
	tar zcf $DIR/piperka-slow.tar.gz var/local/piperka/usr/data
	;;
    # For getting the data while the server is shut down.  Try to be quick
    snap)
	save_snap
	;;
    db)
	save_db
	;;
    backup)
	save_cfg
	save_snap
	save_db
	# slow-data contents are rsynced as is
	;;
    *)
	echo "usage: tar_server backup|cfg|db|slow-data|snap" 2>&1
	exit 1
	;;
esac

chown kaol: $DIR/*
