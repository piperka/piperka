# Server install and deployment.  You'll probably want a fully
# virtualised environment like a KVM image to run this.  But I
# wouldn't recommend using this for development, I myself use a
# debootstrap-installed image with systemd-nspawn.

# VAR is the home directory of piperka user.  More restricted than the
# regular user.
VAR=/var/local/piperka
SUDOPIPERKA=sudo -u piperka --
# Regular user writable.
LIB=/var/local/piperka/usr
SUDOINST=
# Root installable
INST=/usr/local/lib/piperka
POSTGRES_VERSION=15

ifeq ($(shell hostname),piperka)
  DEV=
  PIPERKA_CERT=/etc/letsencrypt/live/piperka.net/fullchain.pem
  PIPERKA_KEY=/etc/letsencrypt/live/piperka.net/privkey.pem
  TEKSTI_CERT=/etc/letsencrypt/live/teksti.eu/fullchain.pem
  TEKSTI_KEY=/etc/letsencrypt/live/teksti.eu/privkey.pem
else
  DEV=dev.
  PIPERKA_CERT=/etc/ssl/certs/ssl-cert-snakeoil.pem
  PIPERKA_KEY=/etc/ssl/private/ssl-cert-snakeoil.key
  TEKSTI_CERT=/etc/ssl/certs/ssl-cert-snakeoil.pem
  TEKSTI_KEY=/etc/ssl/private/ssl-cert-snakeoil.key
endif

INSTANCES=8000 8001
SITES=piperka teksti
ACTIVE=$(shell readlink /etc/nginx/sites-enabled/piperka | grep -oE '[0-9]+')
ifeq ($(ACTIVE),)
  ACTIVE=8000
endif
ifeq ($(ACTIVE),8000)
  SPARE=8001
else
  SPARE=8000
endif
DEPLOY=$(INST)/instance/$(SPARE)

BINARIES=piperka-snap crawler infer-schedule

EXTRA_SCRIPTS=\
readers_history \
dump_userpicks \
build_world \
tar_server

PERL_MODULES=Piperka/Control.pm Piperka/Parser.pm

# Things directly needed to run Piperka.
PIPERKA_DEPS=\
xsltproc \
postgresql-$(POSTGRES_VERSION) \
postgresql-plperl-$(POSTGRES_VERSION) \
postgresql-contrib-$(POSTGRES_VERSION) \
libfile-find-object-perl \
libxml-libxml-perl \
libtimedate-perl \
liblinux-prctl-perl \
libjson-perl \
libdbd-pg-perl \
libxml-feed-perl \
liburi-encode-perl \
fortune \
fortunes \
fortune-anarchism \
imagemagick \
curl

# Things needed to deploy Piperka.
SERVER_DEPS= \
nginx \
libblas3 \
liblapack3 \
icu-devtools \
inotify-tools \
git \
libjansson4 \
rsync \
darcs

# Not necessarily Piperka related but for miscellanous other things
# running on the prod server and conveniences as well.
ifeq ($(DEV),)
SERVER_DEPS+= \
tmux \
aptitude \
irssi \
emacs-nox \
certbot \
exim4 \
cyrus-imapd \
sasl2-bin \
crossfire-server
endif

BUILD_DEPS= \
make \
gcc \
ghc \
libtinfo5 \
libgmp-dev \
zlib1g-dev \
libblas-dev \
liblapack-dev \
libpq-dev \
libicu-dev \
libjansson-dev \
cabal-install \
pkgconf

ifneq ($(shell stat -c '%U' $(VAR)),piperka)
  CHOWN_PIPERKA_HOME=chown-piperka-home
else
  CHOWN_PIPERKA_HOME=
endif

# Controls whether to use systemctl start/stop/reload which would fail
# under systemd-nspawn (at least).
DETECT_VIRT=$(shell systemd-detect-virt -c)

PG_TARGETS=
ifneq ($(shell psql -w template1 -c 'select 1;' 2>&1 >/dev/null && echo 1),1)
  PG_TARGETS+= pg-user-kaol pg-db
endif
ifneq ($(shell psql -w -U piperka template1 -c 'select 1;' 2>&1 >/dev/null && echo 1),1)
  PG_TARGETS+= pg-user-piperka pg-db
endif

CABAL_DIST=dist-newstyle/build/x86_64-linux/ghc-8.8.4/piperka-0.1/x

$(CABAL_DIST)/piperka-snap/build/piperka-snap/piperka-snap: $(shell find piperka/src crawler/src -name "*.hs") stamp/piperka-cabal
	schroot -c build cabal build piperka-snap

$(CABAL_DIST)/crawler/build/crawler/crawler: $(shell find crawler/src -name "*.hs") stamp/piperka-cabal
	schroot -c build cabal build crawler

$(CABAL_DIST)/infer-schedule/build/infer-schedule/infer-schedule: $(shell find infer-schedule/Schedule crawler/src -name "*.hs") infer-schedule/Schedule.hs stamp/piperka-cabal
	schroot -c build cabal build infer-schedule

stamp/piperka-cabal: piperka.cabal
	mkdir -p stamp
	schroot -c build -- cabal install --only-dependencies --force-reinstalls --overwrite-policy=always
	touch stamp/piperka-cabal

deploy: $(CABAL_DIST)/piperka-snap/build/piperka-snap/piperka-snap /usr/local/bin/forcelayout $(foreach M,$(PERL_MODULES),/usr/local/lib/site_perl/$M) $(CHOWN_PIPERKA_HOME)
	sudo systemctl enable /etc/systemd/user/piperka-$(SPARE).service
ifeq ($(DETECT_VIRT),none)
	sudo systemctl stop piperka-$(SPARE)
else
	-sudo kill `ps a --format pid,exe --no-headers | grep $(SPARE)/piperka-snap | cut -d' ' -f 1`
endif
	sudo cp $(CABAL_DIST)/piperka-snap/build/piperka-snap/piperka-snap $(DEPLOY)
	sudo rsync -r --delete piperka/site/ $(DEPLOY)/site/
	sudo rsync -r --delete piperka/xslt/ $(DEPLOY)/xslt/
	sudo rsync -r --delete piperka/snaplets/heist/templates $(DEPLOY)/snaplets/heist/
	sudo rsync -r --delete piperka/snaplets/piperka/snaplets/heist/site_templates $(DEPLOY)/snaplets/piperka/snaplets/heist/
	sudo rsync -r --delete piperka/snaplets/teksti/snaplets/heist/site_templates $(DEPLOY)/snaplets/teksti/snaplets/heist/
	sudo rsync -a $(LIB)/add/ $(DEPLOY)/
	$(SUDOPIPERKA) touch $(VAR)/instance/$(SPARE)/run/start.stamp
ifeq ($(DETECT_VIRT),none)
	sudo systemctl start piperka-$(SPARE)
else
	cd /usr/local/lib/piperka/instance/$(SPARE) ; $(SUDOPIPERKA) ./piperka-snap --port $(SPARE) --address 127.0.0.1 --hostname $(DEV)piperka.net --proxy=X_Forwarded_For &
endif

	inotifywait -q $(VAR)/instance/$(SPARE)/run/start.stamp
ifeq ($(DETECT_VIRT),none)
	sudo systemctl status piperka-$(SPARE) | cat
endif
	curl -si "http://localhost:$(SPARE)/piperka/" | head -n 1 | grep -q 200

	sudo -- ln -fsT ../sites-available/piperka-$(SPARE) /etc/nginx/sites-enabled/piperka
ifeq ($(DETECT_VIRT),none)
	sudo systemctl reload nginx
else
	sudo /etc/init.d/nginx reload
endif

rollback:
	sudo -- ln -fsT ../sites-available/piperka-$(SPARE) /etc/nginx/sites-enabled/piperka
	sudo systemctl reload nginx

deploy-crawler: $(CABAL_DIST)/crawler/build/crawler/crawler $(foreach M,$(PERL_MODULES),/usr/local/lib/site_perl/$M)
	sudo cp $< /usr/local/bin/

MAINTENANCE_MSG?=Piperka is under maintenance. We'll be back soon.
# hey, silly emacs: '

maintenance:
# Writes short message with timestamp in /var/www/piperka
	sudo lib/write_maintenance "$(MAINTENANCE_MSG)"
	TMP=`mktemp`; sed "\
s/DEV/$(DEV)/g;\
s,PIPERKA_CERT,$(PIPERKA_CERT),;\
s,PIPERKA_KEY,$(PIPERKA_KEY),;\
s,TEKSTI_CERT,$(TEKSTI_CERT),;\
s,TEKSTI_KEY,$(TEKSTI_KEY),;\
" lib/maintenance.nginx.template > $$TMP; sudo cp --remove-destination $$TMP /etc/nginx/sites-enabled/piperka; rm $$TMP
	sudo chmod a+r /etc/nginx/sites-enabled/piperka
ifeq ($(DETECT_VIRT),none)
	sudo systemctl reload nginx
else
	sudo /etc/init.d/nginx reload
endif

# Server install
/etc/schroot/chroot.d/piperka-build: lib/build.schroot.template
	dpkg -s schroot >/dev/null 2>&1 || sudo apt -y install schroot
	sudo sh -c 'sed "s/USER/$(USER)/g;s,LIB,$(LIB),g" lib/build.schroot.template > $@'

/etc/sudoers.d/piperka:
	sudo sh -c 'sed s/USER/$(USER)/g lib/sudoers.template > $@'

$(foreach F,run/shared $(foreach S,$(SITES),run/shared/$S) log,$(VAR)/$F) \
$(foreach I,$(INSTANCES),$(VAR)/instance/$I) \
$(foreach I,$(INSTANCES),$(VAR)/instance/$I/tmp) \
$(foreach I,$(INSTANCES),$(foreach S,$(SITES),$(VAR)/instance/$I/run/$S)): create-piperka-user
	$(SUDOPIPERKA) mkdir -p $@

$(foreach I,$(INSTANCES),$(VAR)/instance/$I/run/shared): $(VAR)/run/shared create-piperka-user
	$(SUDOPIPERKA) ln -fsT $(VAR)/run/shared $@

$(foreach S,$(SITES),$(VAR)/data/$S $(foreach F,d banners, $(VAR)/data/$S/$F)): create-piperka-user
	$(SUDOPIPERKA) mkdir -p $@

$(foreach M,$(PERL_MODULES),/usr/local/lib/site_perl/$M): $(foreach M,$(PERL_MODULES),crawler/lib/crawler/$M)
	sudo install -m 0644 -D $(subst /usr/local/lib/site_perl,crawler/lib/crawler,$@) $@

$(VAR): \
create-piperka-user | \
$(foreach F,run/shared $(foreach S,$(SITES),run/shared/$S) log,$(VAR)/$F) \
$(foreach I,$(INSTANCES),$(VAR)/instance/$I) \
$(foreach I,$(INSTANCES),$(VAR)/instance/$I/tmp) \
$(foreach I,$(INSTANCES),$(foreach S,$(SITES) shared,$(VAR)/instance/$I/run/$S)) \
$(foreach I,$(INSTANCES),/etc/systemd/user/piperka-$I.service) \
$(foreach S,$(SITES),$(VAR)/data/$S $(foreach F,d banners,$(VAR)/data/$S/$F))

$(foreach I,$(INSTANCES),/etc/systemd/user/piperka-$I.service): lib/piperka.service.template
	sudo sh -c 'sed "\
s/DEV/$(DEV)/g;\
s,INST,$(INST),g;\
s/PORT/$(patsubst /etc/systemd/user/piperka-%.service,%,$@)/g"\
 lib/piperka.service.template > $@'


# No site specific blog
$(foreach S,$(SITES),$(foreach F,readershistory,$(LIB)/data/$S/$F)) $(LIB)/data/blog $(LIB)/add:
	sudo mkdir -p $@
	sudo chown "$(USER):" $@

$(LIB): | $(foreach S,$(SITES),$(foreach F,readershistory,$(LIB)/data/$S/$F)) $(LIB)/data/blog $(LIB)/add
	sudo mkdir -p $@
	sudo chown "$(USER):" $@

$(foreach I,$(INSTANCES),/etc/nginx/sites-available/piperka-$I): lib/nginx.config.template
	dpkg -s nginx >/dev/null 2>&1 || sudo apt -y install nginx
	sudo sh -c 'sed "\
s/DEV/$(DEV)/;\
s,PIPERKA_CERT,$(PIPERKA_CERT),;\
s,PIPERKA_KEY,$(PIPERKA_KEY),;\
s,TEKSTI_CERT,$(TEKSTI_CERT),;\
s,TEKSTI_KEY,$(TEKSTI_KEY),;\
s,PORT,$(patsubst /etc/nginx/sites-available/piperka-%,%,$@)," \
lib/nginx.config.template > $@'

$(LIB)/build:
	sudo mkdir -p $(LIB)/build
	dpkg -s debootstrap >/dev/null 2>&1 || sudo apt -y install debootstrap
# Bullseye comes with ghc 9.0 but it's cursed, stick to 8.8 for now.
# It's easier to install Bookworm, pin ghc and upgrade the rest.
	sudo debootstrap --include=ghc bullseye $(LIB)/build
	sudo bash -c 'echo "deb http://deb.debian.org/debian bookworm main" >> $(LIB)/build/etc/apt/sources.list'
	sudo cp lib/99bullseye-ghc $(LIB)/build/etc/apt/preferences.d
	sudo schroot -c build apt update
# For some reason apt upgrade flags /etc/protocols as changed
	sudo schroot -c build -- apt upgrade -y -o 'Dpkg::Options::=--force-confnew'
	sudo schroot -c build -- apt install $(BUILD_DEPS) -y
	schroot -c build cabal update
	sudo chown "$(USER):" $(LIB)

/usr/local/bin/forcelayout: forcelayout/forcelayout
	sudo install forcelayout/forcelayout $@

forcelayout/forcelayout: $(LIB)/build
	cd forcelayout; schroot -c build make

/usr/local/bin/infer-schedule: $(CABAL_DIST)/infer-schedule/build/infer-schedule/infer-schedule
	sudo cp $< $@

install-packages:
ifneq ($(shell dpkg -s $(SERVER_DEPS) $(PIPERKA_DEPS) >/dev/null 2>&1; echo $$?),0)
	sudo apt -y install $(SERVER_DEPS) $(PIPERKA_DEPS)
# On a simple systemd-nspawn environment, the host system's postgres
# shows up on the free port check.  This setting also affects the unix
# domain socket name so reset it just in case.
	sudo sed -ri 's/port = [0-9]+/port = 5432/' /etc/postgresql/$(POSTGRES_VERSION)/main/postgresql.conf
	sudo rm -f /etc/nginx/sites-enabled/default
endif

create-piperka-user:
ifeq ($(shell id -u piperka 2>/dev/null),)
	sudo adduser --system piperka --group --home $(VAR) --disabled-login
	sudo adduser $(USER) piperka
endif

$(foreach I,$(INSTANCES),$(INST)/instance/$I $(INST)/instance/$I/snaplets/heist/templates):
	sudo mkdir -p $@

$(foreach I,$(INSTANCES),$(INST)/instance/$I/log):
	sudo ln -fsT $(VAR)/$(subst $(INST)/instance/8000/,,$(subst 8001,8000,$@)) $@

$(foreach I,$(INSTANCES),$(INST)/instance/$I/run):
	sudo ln -fsT $(subst $(INST),$(VAR),$@) $@

$(foreach I,$(INSTANCES),$(INST)/instance/$I/tmp):
	sudo ln -fsT $(VAR)/instance/$(subst /tmp,,$(subst $(INST)/instance/,,$@))/tmp $@

# Piperka and Teksti share a lot of common templates
$(foreach S,$(SITES),$(foreach I,$(INSTANCES),$(INST)/instance/$I/snaplets/$S/snaplets/heist/templates)):
	sudo ln -fsT ../../../heist/templates/ $@

# Auth stuff is not included in version control
$(foreach I,$(INSTANCES),$(INST)/instance/$I/devel.cfg):
	sudo ln -fsT $(INST)/devel.cfg $@

$(foreach I,$(INSTANCES),$(foreach S,$(SITES),$(INST)/instance/$I/snaplets/$S/devel.cfg)):
	sudo ln -fsT $(INST)/$(subst /devel.cfg,,$(subst $(INST)/instance/8000/snaplets/,,$(subst 8001,8000,$@))).cfg $@

$(foreach I,$(INSTANCES),$(foreach S,$(SITES),$(foreach A,auth apiAuth,$(INST)/instance/$I/snaplets/$S/snaplets/$A))):
	sudo ln -fsT $(shell echo $@ | sed -E 's,.*snaplets/([^/]+)/snaplets/([^/]+).*,$(INST)/snaplets/\1/\2/,') $@

$(foreach S,$(SITES),$(foreach A,auth apiAuth,$(INST)/snaplets/$S/$A)) \
$(foreach I,$(INSTANCES),$(INST)/instance/$I/data $(foreach S,$(SITES),$(INST)/instance/$I/data/$S $(INST)/instance/$I/snaplets/$S/snaplets/heist)):
	sudo mkdir -p $@

$(foreach I,$(INSTANCES),$(foreach S,$(SITES),$(foreach F,d banners,$(INST)/instance/$I/data/$S/$F))):
	sudo ln -fsT $(shell echo $@ | sed -E 's,.*instance/[0-9]+/data/(.*),$(VAR)/data/\1,') $@

$(foreach I,$(INSTANCES),$(foreach S,$(SITES),$(INST)/instance/$I/data/$S/readershistory)):
	sudo ln -fsT $(LIB)/data/$(shell echo $@ | sed -E 's,.*/instance/[0-9]+/data/([^/]+).*,\1,')/readershistory $@

$(foreach I,$(INSTANCES),$(INST)/instance/$I/data/blog):
	sudo ln -fsT $(LIB)/data/blog $@

$(foreach I,$(INSTANCES),$(INST)/instance/$I/site_key.txt):
	sudo ln -fsT $(VAR)/site_key.txt $@

$(INST): | \
$(foreach S,$(SITES),$(foreach A,auth apiAuth,$(INST)/snaplets/$S/$A)) \
$(foreach I,$(INSTANCES),$(INST)/instance/$I $(INST)/instance/$I/data $(INST)/instance/$I/data/blog) \
$(foreach F,tmp run log data devel.cfg site_key.txt snaplets/heist/templates \
  $(foreach S,$(SITES), \
    $(foreach C,snaplets/heist snaplets/heist/templates devel.cfg snaplets/auth snaplets/apiAuth,snaplets/$S/$C) \
    data/$S \
    $(foreach D,d banners readershistory,data/$S/$D) \
  ) \
  ,$(foreach I,$(INSTANCES),$(INST)/instance/$I/$F))

$(foreach E,$(EXTRA_SCRIPTS),$(INST)/bin/$E): $(foreach E,$(EXTRA_SCRIPTS),lib/$E) $(INST)
	sudo mkdir -p $(INST)/bin
	sudo cp $(subst $(INST)/bin,lib/,$@) $@
	sudo chmod +x $(INST)/bin/*

chown-piperka-home:
	sudo chown "piperka:" $(VAR)

/etc/postgresql/$(POSTGRES_VERSION)/main/pg_ident.conf: lib/pg_ident.conf.template
	sudo sh -c 'sed s,USER,$(USER),g lib/pg_ident.conf.template > /etc/postgresql/$(POSTGRES_VERSION)/main/pg_ident.conf'
	sudo chown 'postgres:' /etc/postgresql/$(POSTGRES_VERSION)/main/pg_ident.conf
	sudo chmod 0640 /etc/postgresql/$(POSTGRES_VERSION)/main/pg_ident.conf

/etc/postgresql/$(POSTGRES_VERSION)/main/pg_hba.conf: lib/pg_hba.conf.template
	sudo cp lib/pg_hba.conf.template /etc/postgresql/$(POSTGRES_VERSION)/main/pg_hba.conf
	sudo chown 'postgres:' /etc/postgresql/$(POSTGRES_VERSION)/main/pg_hba.conf
	sudo chmod 0640 /etc/postgresql/$(POSTGRES_VERSION)/main/pg_hba.conf

pg-user-kaol:
	pg_isready || sudo /etc/init.d/postgresql restart
	-cd /; sudo -u postgres createuser $(USER)

pg-user-piperka: pg-user-kaol
	-cd /; sudo -u postgres createuser piperka
	cd /; sudo -u postgres -- psql template1 -c 'grant piperka to $(USER);'

# Call psql -f schema.sql piperka yourself after init (or restore from
# dump if me).
pg-db: /etc/postgresql/$(POSTGRES_VERSION)/main/pg_ident.conf /etc/postgresql/$(POSTGRES_VERSION)/main/pg_ident.conf pg-user-piperka
ifneq ($(shell psql -lt | grep -q " piperka "; echo $$?),0)
	cd /; sudo -u postgres -- createdb piperka -O piperka || true
endif

# May be missing in a systemd-nspawn
check-run-lock:
	test -e /var/lock || { echo -e "/var/lock is broken.\nHint: add [ -e /run/lock ] || mkdir /run/lock to /root/.bashrc" ; /bin/false ; }

ifeq ($(DEV),)
init: \
check-run-lock \
install-packages \
/etc/sudoers.d/piperka \
$(PG_TARGETS) \
/etc/schroot/chroot.d/piperka-build \
$(VAR) $(LIB) $(INST) \
$(foreach I,$(INSTANCES),/etc/nginx/sites-available/piperka-$I) \
$(LIB)/build \
$(foreach E,$(EXTRA_SCRIPTS),$(INST)/bin/$E) \
$(foreach C,pg_ident.conf pg_hba.conf,/etc/postgresql/$(POSTGRES_VERSION)/main/$C) \
/usr/local/bin/infer-schedule \
/usr/local/bin/forcelayout \
deploy-crawler \
$(CABAL_DIST)/piperka-snap/build/piperka-snap/piperka-snap
ifneq (,$(wildcard /etc/nginx/sites-enabled/default))
	sudo rm -f /etc/nginx/sites-enabled/default
endif
else
# Simpler setup, just include the build environment in place and run
# in dev dir instead of deploying anywhere.
init: check-run-lock install-dev-packages  create-piperka-user \
$(PG_TARGETS) $(PIPERKA_DEV_CFG) \
$(foreach C,pg_ident.conf pg_hba.conf,/etc/postgresql/$(POSTGRES_VERSION)/main/$C) \
$(foreach M,$(PERL_MODULES),/usr/local/lib/site_perl/$M) \
piperka/log piperka/data/piperka/d log
	cabal update
	cabal build
endif

PIPERKA_DEV_CFG=\
piperka/snaplets/piperka/devel.cfg \
piperka/snaplets/piperka/snaplets/auth/devel.cfg \
piperka/snaplets/piperka/snaplets/apiAuth/devel.cfg

install-dev-packages:
	sudo cp lib/99bullseye-ghc /etc/apt/preferences.d
	grep -q bookworm /etc/apt/sources.list || sudo sh -c 'echo "deb http://deb.debian.org/debian bookworm main" >> /etc/apt/sources.list'
	sudo apt update
	sudo apt -y dist-upgrade
	sudo apt -y install $(PIPERKA_DEPS) $(BUILD_DEPS)

piperka/log piperka/data/piperka/d:
	mkdir -p $@

log:
	ln -s piperka/log log

blog: /usr/local/
	$(SUDOINST) $(INST)/bin/blog_generate -d $(LIB)/data/blog
	crawler single -u 4488

.PHONY: deploy deploy-crawler rollback maintenance \
init install-dev-packages \
blog install-packages create-piperka-user chown-piperka-home \
check-run-lock \
pg-user-kaol pg-user-piperka pg-db

.SECONDEXPANSION:

$(PIPERKA_DEV_CFG): $$@.example
	cp -n $< $@
