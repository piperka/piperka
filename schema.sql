--
-- PostgreSQL database dump
--

-- Dumped from database version 15.8 (Debian 15.8-0+deb12u1)
-- Dumped by pg_dump version 15.8 (Debian 15.8-0+deb12u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: piperka; Type: SCHEMA; Schema: -; Owner: piperka
--

CREATE SCHEMA piperka;


ALTER SCHEMA piperka OWNER TO piperka;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: comic_remain; Type: TYPE; Schema: public; Owner: kaol
--

CREATE TYPE public.comic_remain AS (
	cid integer,
	num integer
);


ALTER TYPE public.comic_remain OWNER TO kaol;

--
-- Name: crawl_event_type; Type: TYPE; Schema: public; Owner: kaol
--

CREATE TYPE public.crawl_event_type AS ENUM (
    'target',
    'parse',
    'terminal'
);


ALTER TYPE public.crawl_event_type OWNER TO kaol;

--
-- Name: integerpair; Type: TYPE; Schema: public; Owner: piperka
--

CREATE TYPE public.integerpair AS (
	"1" integer,
	"2" integer
);


ALTER TYPE public.integerpair OWNER TO piperka;

--
-- Name: ord_frag; Type: TYPE; Schema: public; Owner: kaol
--

CREATE TYPE public.ord_frag AS (
	ord integer,
	frag integer
);


ALTER TYPE public.ord_frag OWNER TO kaol;

--
-- Name: ord_subord; Type: TYPE; Schema: public; Owner: kaol
--

CREATE TYPE public.ord_subord AS (
	ord integer,
	subord integer
);


ALTER TYPE public.ord_subord OWNER TO kaol;

--
-- Name: redir_url_and_last; Type: TYPE; Schema: public; Owner: kaol
--

CREATE TYPE public.redir_url_and_last AS (
	url text,
	last_ord integer,
	last_subord integer
);


ALTER TYPE public.redir_url_and_last OWNER TO kaol;

--
-- Name: dump_userpicks(date); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.dump_userpicks(stamp date) RETURNS jsonb
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $$SELECT jsonb_build_object('items', (SELECT jsonb_object_agg(cid, jsonb_build_object('cid', cid, 'title', title, 'readers', readers)) FROM (SELECT unnest AS cid, coalesce(title, '<null>') AS title, count(*) AS readers FROM pick_history CROSS JOIN LATERAL unnest(pick_history.cid) LEFT JOIN comics ON (unnest=comics.cid) WHERE pick_history.stamp=dump_userpicks.stamp AND unnest NOT IN (SELECT cid FROM graveyard) GROUP BY unnest, title ORDER BY unnest) AS a), 'picks', (SELECT jsonb_object_agg(coalesce(uid::text, gen_random_uuid()::text), cid) FROM pick_history WHERE pick_history.stamp=dump_userpicks.stamp))$$;


ALTER FUNCTION piperka.dump_userpicks(stamp date) OWNER TO kaol;

--
-- Name: dump_world(date, boolean); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.dump_world(stamp date, update_mapped boolean DEFAULT false) RETURNS jsonb
    LANGUAGE plpgsql
    AS $$
DECLARE
  my_stamp date;
BEGIN
  my_stamp := coalesce(dump_world.stamp, (SELECT max(world_map.stamp) FROM world_map));
  IF update_mapped THEN
    UPDATE comics SET mapped=comics.cid IN (SELECT cid FROM world_map WHERE world_map.stamp=my_stamp);
  END IF;
  RETURN (SELECT jsonb_object_agg(cid, jsonb_build_object('radius', radius, 'weight', weight, 'x', x, 'y', y)) FROM world_map WHERE world_map.stamp=my_stamp);
END;
$$;


ALTER FUNCTION piperka.dump_world(stamp date, update_mapped boolean) OWNER TO kaol;

--
-- Name: input_userpicks(date, jsonb); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.input_userpicks(stamp date, dat jsonb) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM pick_history WHERE pick_history.stamp=input_userpicks.stamp;
  -- The ugly part is basically a cast from jsonb array to int[]
  INSERT INTO pick_history (stamp, uid, cid) SELECT input_userpicks.stamp, key::int, array_agg(value)::int[] FROM (SELECT a.key, jsonb_array_elements(a.value) AS value FROM (SELECT * FROM jsonb_each(dat->'picks')) AS a) AS b GROUP BY key;
END;
$$;


ALTER FUNCTION piperka.input_userpicks(stamp date, dat jsonb) OWNER TO kaol;

--
-- Name: input_world(date, jsonb); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.input_world(stamp date, dat jsonb) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM world_map WHERE world_map.stamp=input_world.stamp;
  INSERT INTO world_map (stamp, cid, radius, weight, x, y) SELECT input_world.stamp, key::int, (value->'radius')::float4, (value->'weight')::smallint, (value->'x')::float8, (value->'y')::float8 FROM jsonb_each(dat);
END;
$$;


ALTER FUNCTION piperka.input_world(stamp date, dat jsonb) OWNER TO kaol;

--
-- Name: scheduled_updates_near(timestamp without time zone, integer, integer); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.scheduled_updates_near(stamp timestamp without time zone DEFAULT CURRENT_TIMESTAMP, hours_before integer DEFAULT 1, hours_after integer DEFAULT 1) RETURNS TABLE(cid integer, schedule timestamp without time zone)
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $$
WITH monday AS (
SELECT date_trunc('day', stamp)+(((extract(isodow FROM stamp)-1) || ' days ago')::interval) AS monday)
SELECT cid, decoded FROM
(SELECT cid, monday+(n || ' hours') :: interval AS decoded FROM monday,
(SELECT cid, n FROM piperka.inferred_schedule, UNNEST(schedule) AS n
UNION
SELECT cid, n+168 FROM piperka.inferred_schedule, UNNEST(schedule) AS n where n<hours_after
UNION
SELECT cid, n-168 FROM piperka.inferred_schedule, UNNEST(schedule) AS n where n>168-hours_before) AS a ORDER BY decoded) AS b
WHERE decoded >= date_trunc('hour', stamp) - (hours_before||' hours')::interval
AND decoded <= stamp + (hours_after||' hours')::interval
$$;


ALTER FUNCTION piperka.scheduled_updates_near(stamp timestamp without time zone, hours_before integer, hours_after integer) OWNER TO kaol;

--
-- Name: site_tag(); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.site_tag() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  site_tag integer;
BEGIN
  DELETE FROM comic_tag WHERE cid=NEW.cid AND tagid IN (63, 64, 65, 66);
  CASE
    WHEN NEW.url_base LIKE 'https://www.webtoons.com/%' THEN
      site_tag := 63;
    WHEN NEW.url_base LIKE 'https://tapas.io/%' THEN
      site_tag := 64;
    WHEN NEW.url_base LIKE 'https://www.theduckwebcomics.com/%' THEN
      site_tag := 65;
    WHEN NEW.url_base ~ '^http://([a-z0-9]+).comicgen(esis)?.com/' THEN
      site_tag := 66;
    ELSE NULL;
  END CASE;
  IF site_tag IS NOT NULL THEN
    INSERT INTO comic_tag (cid, tagid) VALUES (NEW.cid, site_tag);
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION piperka.site_tag() OWNER TO kaol;

--
-- Name: store_userpicks(); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.store_userpicks() RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM pick_history WHERE stamp=current_date;
  INSERT INTO pick_history (stamp, uid, cid) SELECT current_date, uid, array_agg(cid) FROM users JOIN subscriptions USING (uid) WHERE countme GROUP BY uid;
END;
$$;


ALTER FUNCTION piperka.store_userpicks() OWNER TO kaol;

--
-- Name: user_updates(integer, boolean); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.user_updates(uid integer, hide boolean) RETURNS TABLE(r1 integer, r2 boolean, r3 integer, r4 text, r5 real, r6 smallint, r7 smallint[], r8 smallint, r9 smallint)
    LANGUAGE plpgsql STABLE PARALLEL SAFE
    AS $$
DECLARE
  bookmark_sort smallint;
BEGIN
  SELECT users.bookmark_sort FROM users WHERE users.uid=user_updates.uid INTO bookmark_sort;
  CASE WHEN bookmark_sort = 0 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY num DESC, ordering_form(title);
  WHEN bookmark_sort = 1 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY num, ordering_form(title);
  WHEN bookmark_sort = 2 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY ordering_form(title);
  WHEN bookmark_sort = 3 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY last_updated DESC NULLS LAST, ordering_form(title);
  ELSE
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY last_updated NULLS LAST, ordering_form(title);
  end case;
end;
$$;


ALTER FUNCTION piperka.user_updates(uid integer, hide boolean) OWNER TO kaol;

--
-- Name: user_updates_link(integer, boolean); Type: FUNCTION; Schema: piperka; Owner: kaol
--

CREATE FUNCTION piperka.user_updates_link(uid integer, hide boolean) RETURNS TABLE(r1 integer, r2 boolean, r2_ text, r3 integer, r4 text, r5 real, r6 smallint, r7 smallint[], r8 smallint, r9 smallint)
    LANGUAGE plpgsql STABLE PARALLEL SAFE
    AS $$
DECLARE
  bookmark_sort smallint;
BEGIN
  SELECT users.bookmark_sort FROM users WHERE users.uid=user_updates_link.uid INTO bookmark_sort;
  CASE WHEN bookmark_sort = 0 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      COALESCE((SELECT url FROM redir_url_and_last(user_updates_link.uid, cid, CASE WHEN offset_bookmark_by_one THEN -1 ELSE 0 END)), fixed_head, homepage),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates_link.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY num DESC, ordering_form(title);
  WHEN bookmark_sort = 1 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      COALESCE((SELECT url FROM redir_url_and_last(user_updates_link.uid, cid, CASE WHEN offset_bookmark_by_one THEN -1 ELSE 0 END)), fixed_head, homepage),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates_link.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY num, ordering_form(title);
  WHEN bookmark_sort = 2 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      COALESCE((SELECT url FROM redir_url_and_last(user_updates_link.uid, cid, CASE WHEN offset_bookmark_by_one THEN -1 ELSE 0 END)), fixed_head, homepage),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates_link.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY ordering_form(title);
  WHEN bookmark_sort = 3 THEN
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      COALESCE((SELECT url FROM redir_url_and_last(user_updates_link.uid, cid, CASE WHEN offset_bookmark_by_one THEN -1 ELSE 0 END)), fixed_head, homepage),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates_link.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY last_updated DESC NULLS LAST, ordering_form(title);
  ELSE
    RETURN QUERY SELECT num, cid IN (SELECT cid FROM comic_tag WHERE tagid = 13),
      COALESCE((SELECT url FROM redir_url_and_last(user_updates_link.uid, cid, CASE WHEN offset_bookmark_by_one THEN -1 ELSE 0 END)), fixed_head, homepage),
      cid, title, update_value,
      hs.code, inf.schedule, inf.weekly_updates, inf.monthly_updates
      FROM comics AS x LEFT JOIN crawler_config USING (cid)
      LEFT JOIN (SELECT cid, code FROM hiatus_status WHERE active) AS hs USING (cid)
      LEFT JOIN inferred_schedule AS inf USING (cid)
      JOIN comic_remain_frag_cache USING (cid)
      JOIN subscriptions USING (uid, cid)
      JOIN users USING (uid)
      WHERE users.uid=user_updates_link.uid AND num > 0
      AND (NOT hide OR (num <= 50 OR coalesce(greatest(last_set, restored_on) > now() - '14 days' :: interval, false)))
      ORDER BY last_updated NULLS LAST, ordering_form(title);
  end case;
end;
$$;


ALTER FUNCTION piperka.user_updates_link(uid integer, hide boolean) OWNER TO kaol;

--
-- Name: auth_create(text, text, integer); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.auth_create(name text, email text, lmid integer, OUT uid integer, OUT p_session uuid, OUT csrf_ham uuid) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO users (name, email, initial_lmid) VALUES (auth_create.name, auth_create.email, auth_create.lmid)
    RETURNING users.uid INTO uid;
  INSERT INTO piperka.user_site_activity (uid) VALUES (uid);
  INSERT INTO teksti.user_site_activity (uid) VALUES (uid);
  SELECT do_login.p_session, do_login.csrf_ham FROM do_login(uid) INTO p_session, csrf_ham;
END;
$$;


ALTER FUNCTION public.auth_create(name text, email text, lmid integer, OUT uid integer, OUT p_session uuid, OUT csrf_ham uuid) OWNER TO piperka;

--
-- Name: auth_create_password(text, integer); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.auth_create_password(password text, uid integer) RETURNS integer
    LANGUAGE sql
    AS $$
  INSERT INTO login_method_passwd (uid, hash)
    VALUES (auth_create_password.uid, crypt(password, gen_salt('bf', 13)))
    ON CONFLICT (uid) DO UPDATE SET hash=EXCLUDED.hash, stamp=now()
    RETURNING login_method_passwd.lmid;
$$;


ALTER FUNCTION public.auth_create_password(password text, uid integer) OWNER TO piperka;

--
-- Name: auth_login(text, text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.auth_login(name text, password text) RETURNS TABLE(uid integer, p_session uuid, csrf_ham uuid)
    LANGUAGE plpgsql
    AS $$
DECLARE
  pw_matches boolean;
  pw_convert boolean;
  old_hash character(32);
BEGIN
  SELECT users.uid, crypt(auth_login.password, hash)=hash, passwd
    INTO auth_login.uid, pw_matches, old_hash
    FROM users LEFT JOIN login_method_passwd USING (uid)
    WHERE LOWER(auth_login.name)=LOWER(users.name);
  IF pw_matches IS NULL AND old_hash IS NOT NULL THEN
    -- Check for old md5 hash
    BEGIN
      SELECT md5(convert_to(auth_login.password, 'latin1'))=old_hash INTO pw_matches;
    EXCEPTION WHEN untranslatable_character THEN
      SELECT md5(auth_login.password)=old_hash INTO pw_matches;
    END;
    -- Convert to new hash
    pw_convert := TRUE;
  END IF;
  IF pw_matches THEN
    IF pw_convert THEN
      PERFORM auth_create_password(auth_login.password, auth_login.uid);
      UPDATE users set passwd=null WHERE users.uid=auth_login.uid;
    END IF;
    RETURN QUERY SELECT auth_login.uid, do_login.p_session, do_login.csrf_ham FROM do_login(auth_login.uid);
  END IF;
END;
$$;


ALTER FUNCTION public.auth_login(name text, password text) OWNER TO piperka;

--
-- Name: auth_oauth2(integer, text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.auth_oauth2(opid integer, token text) RETURNS TABLE(uid integer, p_session uuid, csrf_ham uuid)
    LANGUAGE plpgsql
    AS $$
BEGIN
  SELECT users.uid INTO auth_oauth2.uid
    FROM users JOIN login_method_oauth2 USING (uid)
    WHERE login_method_oauth2.identification = auth_oauth2.token
    AND login_method_oauth2.opid = auth_oauth2.opid;
  IF auth_oauth2.uid IS NOT NULL THEN
    RETURN QUERY SELECT auth_oauth2.uid, do_login.p_session, do_login.csrf_ham FROM do_login(auth_oauth2.uid);
  END IF;
END;
$$;


ALTER FUNCTION public.auth_oauth2(opid integer, token text) OWNER TO piperka;

--
-- Name: bookmark(text, boolean); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.bookmark(text, boolean) RETURNS TABLE(cid integer, ord integer, subord integer, at_max boolean)
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
DECLARE
regexp text;
match int;
canonical text;
try_home bool default true;
m_ord int;
m_subord int;
m_at_max bool;
bookmark_misses int[];
BEGIN
canonical := canonical_bookmark_form($1);
IF canonical = '' THEN
  RETURN;
END IF;
FOR match IN SELECT * FROM match_comic(canonical) LOOP
  SELECT * INTO m_ord, m_subord, m_at_max FROM match_page(match, canonical, $2);
  IF m_ord IS NOT NULL THEN
    try_home := false;
    RETURN QUERY SELECT match, m_ord, m_subord, m_at_max;
  ELSE
    bookmark_misses := array_append(bookmark_misses, match);
  END IF;
END LOOP;
IF try_home THEN
  RETURN QUERY SELECT *, true FROM match_home(canonical);
  IF NOT FOUND THEN
    LOOP
      m_ord := bookmark_misses[1];
      bookmark_misses := bookmark_misses[2:array_length(bookmark_misses, 1)];
      IF m_ord IS NULL THEN
        EXIT;
      END IF;
      RETURN QUERY SELECT m_ord, null::int, null::int, null::bool;
    END LOOP;
  END IF;
END IF;
END;
$_$;


ALTER FUNCTION public.bookmark(text, boolean) OWNER TO piperka;

--
-- Name: bookmark_and_log(text, boolean, inet, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.bookmark_and_log(text, boolean, inet, integer) RETURNS TABLE(v_cid integer, v_ord integer, v_subord integer, v_at_max boolean)
    LANGUAGE plpgsql
    AS $_$
BEGIN
CREATE TEMPORARY table _results (cid int, ord int, subord int, at_max bool);
INSERT INTO _results SELECT * FROM bookmark($1, $2);
IF (SELECT COUNT(*) FROM _results) = 1 THEN
  INSERT INTO bookmarking_log (at_date, url, want_here, host, uid, cid, ord, subord, is_at_max) SELECT NOW(), $1, $2, $3::inet, $4, * FROM _results;
ELSE
  INSERT INTO bookmarking_log (at_date, url, want_here, host, uid) VALUES (NOW(), $1, $2, $3, $4);
END IF;
RETURN QUERY SELECT * FROM _results;
DROP TABLE _results;
END;
$_$;


ALTER FUNCTION public.bookmark_and_log(text, boolean, inet, integer) OWNER TO kaol;

--
-- Name: bookmark_and_log(text, boolean, text, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.bookmark_and_log(text, boolean, text, integer) RETURNS TABLE(v_cid integer, v_ord integer, v_subord integer, v_at_max boolean)
    LANGUAGE plpgsql
    AS $_$
BEGIN
CREATE TEMPORARY table _results (cid int, ord int, subord int, at_max bool);
INSERT INTO _results SELECT * FROM bookmark($1, $2);
IF (SELECT COUNT(*) FROM _results) = 1 THEN
  INSERT INTO bookmarking_log (at_date, url, want_here, host, uid, cid, ord, subord, is_at_max) SELECT NOW(), $1, $2, $3::inet, $4, * FROM _results;
ELSE
  INSERT INTO bookmarking_log (at_date, url, want_here, host, uid) VALUES (NOW(), $1, $2, $3::inet, $4);
END IF;
RETURN QUERY SELECT * FROM _results;
DROP TABLE _results;
END;
$_$;


ALTER FUNCTION public.bookmark_and_log(text, boolean, text, integer) OWNER TO kaol;

--
-- Name: canonical_bookmark_form(text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.canonical_bookmark_form(text) RETURNS text
    LANGUAGE plperl IMMUTABLE
    AS $_X$
$_ = $_[0];
s,^(https?://)?(www\.)?,,;
s,^airships\.paulgazis\.com/,paulgazis.com/FlyingCloud/,;
s,^scarygoround\.com/index\.php\?date=,scarygoround\.com/\?date=,;
s,^13seconds\.info/,13seconds.com/,;
s,^wigu\.com/overcompensating/,overcompensating\.com/,;
s,^elgoonishshive\.com/,egscomic.com/,;
s,^rampagenetwork\.com/comics/unwinder/,tallcomics.com/,;
s,^([^.]+)\.comicgenesis\.com/,\1.comicgen.com/,;
s,^([^.]+)\.blogspot\.[^/]+,\1.blogspot.com/,;
s,^(?:m\.)?webtoons\.com/(.+?&episode_no=\d+)(.*)$,webtoons.com/\1,;
s,^m\.(?!xkcd\.com),,;
s,//,/,g;
s,/[^/]*/\.\.(.),/\1,g;
s,/\./,/,g;
s,/\.?\./$,/,;
return $_;
$_X$;


ALTER FUNCTION public.canonical_bookmark_form(text) OWNER TO piperka;

--
-- Name: clear_user_remain_frag_cache(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.clear_user_remain_frag_cache() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF tg_table_name='subscriptions' THEN
    IF tg_op='DELETE' THEN
      DELETE FROM comic_remain_frag_cache WHERE uid=OLD.uid AND cid=OLD.cid;
      RETURN NULL;
    ELSE
      DELETE FROM comic_remain_frag_cache WHERE uid=NEW.uid AND cid=NEW.cid;
      RETURN NEW;
    END IF;
  ELSIF tg_table_name='updates' OR tg_table_name='page_fragments' THEN
    IF tg_op='DELETE' THEN
      DELETE FROM comic_remain_frag_cache WHERE cid=OLD.cid;
      RETURN NULL;
    ELSE
      DELETE FROM comic_remain_frag_cache WHERE cid=NEW.cid;
      RETURN NEW;
    END IF;
  END IF;
END;
$$;


ALTER FUNCTION public.clear_user_remain_frag_cache() OWNER TO kaol;

--
-- Name: comic_era_update(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.comic_era_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF TG_OP='UPDATE' AND (NEW.url_base <> OLD.url_base OR NEW.url_tail <> OLD.url_tail) THEN
    INSERT INTO page_era (cid, ord, subord) SELECT NEW.cid, ord, 0 FROM updates	WHERE cid=NEW.cid ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
    INSERT INTO page_era (cid, ord, subord) SELECT NEW.cid, ord, subord FROM page_fragments WHERE cid=NEW.cid ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
-- Not expecing cid changes on comics
  ELSIF TG_OP='DELETE' THEN
    INSERT INTO page_era (cid, ord, subord) SELECT OLD.cid, ord, 0 FROM updates	WHERE cid=OLD.cid ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
    INSERT INTO page_era (cid, ord, subord) SELECT OLD.cid, ord, subord FROM page_fragments WHERE cid=OLD.cid ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.comic_era_update() OWNER TO piperka;

--
-- Name: comic_remain_frag(integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.comic_remain_frag(integer) RETURNS SETOF public.comic_remain
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT cid, max_ord_of(subs.cid)-subs.ord+unread_fragments_correction(uid, cid) AS num FROM subscriptions AS subs WHERE subs.uid=$1$_$;


ALTER FUNCTION public.comic_remain_frag(integer) OWNER TO kaol;

--
-- Name: comics_remain(integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.comics_remain(integer) RETURNS SETOF public.comic_remain
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT cid, max_ord_of(subs.cid)-subs.ord+1 AS num FROM subscriptions AS subs WHERE subs.uid = $1$_$;


ALTER FUNCTION public.comics_remain(integer) OWNER TO kaol;

--
-- Name: create_recovery_key(text, text, character); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.create_recovery_key(name text, email text, hash character) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
  _uid integer;
BEGIN
  _uid := (SELECT users.uid FROM users WHERE LOWER($1)=LOWER(users.name) AND users.email=$2);
  IF _uid IS NULL THEN
    RETURN FALSE;
  END IF;
  DELETE FROM pwdgen_hash WHERE pwdgen_hash.uid=_uid;
  INSERT INTO pwdgen_hash (uid, hash) VALUES (_uid, hash);
  RETURN TRUE;
END;
$_$;


ALTER FUNCTION public.create_recovery_key(name text, email text, hash character) OWNER TO piperka;

--
-- Name: delete_csrf_tokens(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.delete_csrf_tokens() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM p_session WHERE token_for=OLD.ses;
  RETURN NULL;
END
$$;


ALTER FUNCTION public.delete_csrf_tokens() OWNER TO piperka;

--
-- Name: delete_user(integer); Type: PROCEDURE; Schema: public; Owner: kaol
--

CREATE PROCEDURE public.delete_user(IN uid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
  UPDATE users SET deleted_at = now(), passwd=null, privacy=1, email=null WHERE users.uid=delete_user.uid;
  DELETE FROM login_method WHERE login_method.uid=delete_user.uid;
  DELETE FROM subscriptions WHERE subscriptions.uid=delete_user.uid;
  UPDATE bookmarking_log SET uid=null, host=null WHERE bookmarking_log.uid=delete_user.uid;
  DELETE FROM p_session WHERE p_session.uid=delete_user.uid;
  DELETE FROM oauth2_account WHERE oauth2_account.uid=delete_user.uid;
  DELETE FROM piperka.user_site_activity where user_site_activity.uid=delete_user.uid;
  DELETE FROM teksti.user_site_activity where user_site_activity.uid=delete_user.uid;
  DELETE FROM piperka.redirect_log where redirect_log.uid=delete_user.uid;
  DELETE FROM teksti.redirect_log where redirect_log.uid=delete_user.uid;
END;
$$;


ALTER PROCEDURE public.delete_user(IN uid integer) OWNER TO kaol;

--
-- Name: do_login(integer); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.do_login(uid integer, OUT p_session uuid, OUT csrf_ham uuid) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  UPDATE users SET last_login = NOW() WHERE users.uid=do_login.uid;
  SELECT token INTO p_session FROM generate_session(uid, null);
  SELECT token INTO csrf_ham FROM generate_session(uid, p_session);
END;
$$;


ALTER FUNCTION public.do_login(uid integer, OUT p_session uuid, OUT csrf_ham uuid) OWNER TO piperka;

--
-- Name: do_login(text, text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.do_login(in_name text, in_passwd text, OUT o_uid integer, OUT new_comics integer, OUT p_session uuid, OUT csrf_ham uuid) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
SELECT uid INTO o_uid FROM users WHERE LOWER(name)=LOWER(in_name) AND passwd=in_passwd;
IF o_uid IS NOT NULL THEN
  SELECT * FROM do_login(o_uid) INTO o_uid, new_comics, p_session, csrf_ham;
END IF;
END;
$$;


ALTER FUNCTION public.do_login(in_name text, in_passwd text, OUT o_uid integer, OUT new_comics integer, OUT p_session uuid, OUT csrf_ham uuid) OWNER TO piperka;

--
-- Name: drop_sid_friends(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.drop_sid_friends() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM submit_tag WHERE sid=OLD.sid;
  DELETE FROM external_entry_submit WHERE sid=OLD.sid;
  DELETE FROM submit_banner WHERE sid=OLD.sid;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.drop_sid_friends() OWNER TO piperka;

--
-- Name: edit_entry(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.edit_entry(uid_ integer, cid_ integer, sid_ integer) RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
  DELETE FROM comic_tag WHERE cid=$2;
  INSERT INTO comic_tag (cid, tagid) SELECT cid, tagid FROM user_edit JOIN submit_tag USING (sid) WHERE sid=$3;
  DELETE FROM external_entry WHERE cid=$2;
  INSERT INTO external_entry (cid, epid, entry) SELECT cid, epid, entry FROM user_edit JOIN external_entry_submit USING (sid) WHERE sid=$3;
  UPDATE comics SET description=(SELECT description FROM user_edit WHERE sid=$3) WHERE cid=$2;
  UPDATE moderator SET last_moderate=now() WHERE uid=$1;
  DELETE FROM user_edit WHERE sid=$3;
END;
$_$;


ALTER FUNCTION public.edit_entry(uid_ integer, cid_ integer, sid_ integer) OWNER TO piperka;

--
-- Name: edit_entry(integer, integer, integer, inet); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.edit_entry(integer, integer, integer, inet) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE
new_sid int;
BEGIN
  -- Store old values
  new_sid := nextval('submit_sid_seq');
  INSERT INTO submit_tag (sid, tagid) SELECT new_sid, tagid FROM comic_tag WHERE cid=$2;
  INSERT INTO external_entry_submit (sid, epid, entry) SELECT new_sid, epid, entry FROM external_entry WHERE cid=$2;
  INSERT INTO edit_history (sid, cid, uid, from_ip, description) SELECT new_sid, cid, $1, $4, COALESCE(description, '') FROM comics WHERE cid=$2;
  -- Upsert new
  DELETE FROM comic_tag WHERE cid=$2;
  INSERT INTO comic_tag (cid, tagid) SELECT cid, tagid FROM user_edit JOIN submit_tag USING (sid) WHERE sid=$3;
  DELETE FROM external_entry WHERE cid=$2;
  INSERT INTO external_entry (cid, epid, entry) SELECT cid, epid, entry FROM user_edit JOIN external_entry_submit USING (sid) WHERE sid=$3;
  UPDATE comics SET description=(SELECT description FROM user_edit WHERE sid=$3) WHERE cid=$2;
  UPDATE moderator SET last_moderate=now() WHERE uid=$1;
  RETURN new_sid;
END;
$_$;


ALTER FUNCTION public.edit_entry(integer, integer, integer, inet) OWNER TO kaol;

--
-- Name: follow_permission_permitted_interest(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.follow_permission_permitted_interest() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  privacy int;
BEGIN
IF (TG_OP = 'DELETE') THEN
  privacy := (SELECT users.privacy FROM users WHERE uid=OLD.uid);
  IF (privacy = 2) THEN
    DELETE FROM permitted_interest WHERE uid=OLD.followee AND interest = OLD.uid;
  END IF;
ELSIF (TG_OP = 'INSERT') THEN
  privacy := (SELECT users.privacy FROM users WHERE uid=NEW.uid);
  IF (privacy = 2) THEN
    INSERT INTO permitted_interest (uid, interest, cid) SELECT follower.uid, interest, cid FROM follower JOIN subscriptions ON interest=subscriptions.uid WHERE interest = NEW.uid AND follower.uid = NEW.followee;
  END IF;
END IF;
RETURN NULL;
END
$$;


ALTER FUNCTION public.follow_permission_permitted_interest() OWNER TO kaol;

--
-- Name: follower_permitted_interest(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.follower_permitted_interest() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  privacy int;
BEGIN
IF (TG_OP = 'DELETE') THEN
  DELETE FROM permitted_interest WHERE uid=OLD.uid AND interest=OLD.interest;
ELSIF (TG_OP = 'INSERT') THEN
  privacy := (SELECT users.privacy FROM users WHERE uid=NEW.interest);
  IF (privacy = 3 OR (privacy = 2 AND (NEW.uid, NEW.interest) IN (SELECT followee, uid FROM follow_permission))) THEN
    INSERT INTO permitted_interest (uid, interest, cid) SELECT NEW.uid, NEW.interest, cid FROM subscriptions WHERE uid=NEW.interest;
  END IF;
END IF;
RETURN NULL;
END
$$;


ALTER FUNCTION public.follower_permitted_interest() OWNER TO kaol;

--
-- Name: fragment_era_update(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.fragment_era_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF TG_OP='UPDATE' AND ((NEW.ord, NEW.subord, NEW.fragment) <> (OLD.ord, OLD.subord, OLD.fragment)) THEN
    INSERT INTO page_era (cid, ord, subord) VALUES (NEW.cid, NEW.ord, NEW.subord) ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
  END IF;
  IF TG_OP='DELETE' OR TG_OP='UPDATE' AND (NEW.ord <> OLD.ord OR NEW.subord <> OLD.subord) THEN
    INSERT INTO page_era (cid, ord, subord) VALUES (OLD.cid, OLD.ord, OLD.subord) ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.fragment_era_update() OWNER TO piperka;

--
-- Name: generate_oauth2_token(integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.generate_oauth2_token(in_opid integer, OUT token uuid) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
BEGIN
  LOOP
    SELECT uuid_generate_v4() INTO token;
    BEGIN
      INSERT INTO oauth2_state (opid, state) VALUES (in_opid, token);
      RETURN;
    EXCEPTION WHEN unique_violation THEN
      -- Fall through, loop again
    END;
  END LOOP;
END;
$$;


ALTER FUNCTION public.generate_oauth2_token(in_opid integer, OUT token uuid) OWNER TO kaol;

--
-- Name: generate_session(integer, uuid); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.generate_session(in_uid integer, in_token_for uuid, OUT token uuid) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
BEGIN
  LOOP
    SELECT uuid_generate_v4() INTO token;
    -- Clashes should be nigh impossible, but check anyway
    BEGIN
      INSERT INTO p_session (ses, uid, token_for) VALUES (token, in_uid, in_token_for);
      RETURN;
    EXCEPTION WHEN unique_violation THEN
      -- Fall through, loop again
    END;
  END LOOP;
END;
$$;


ALTER FUNCTION public.generate_session(in_uid integer, in_token_for uuid, OUT token uuid) OWNER TO piperka;

--
-- Name: get_and_update_stats(integer, boolean); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.get_and_update_stats(uid integer, is_login boolean, OUT new_comics integer, OUT total_new integer, OUT new_in integer, OUT total_new_hide integer, OUT new_in_hide integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  -- On user login or when user has been inactive for over 10 minutes, reset before stamp to previous last stamp
  IF is_login OR (select seen_comics_last < NOW() - time '0:10' FROM user_site_activity WHERE user_site_activity.uid=get_and_update_stats.uid) THEN
    UPDATE user_site_activity SET seen_comics_before = seen_comics_last, seen_comics_last = NOW() WHERE user_site_activity.uid=get_and_update_stats.uid;
  ELSE
    -- In any case, update the last stamp
    UPDATE user_site_activity SET seen_comics_last = NOW() WHERE user_site_activity.uid=get_and_update_stats.uid;
  END IF;
  -- Show number of new comics since seen_comics_before stamp
  SELECT COUNT(*) INTO new_comics FROM comics, user_site_activity WHERE user_site_activity.uid=get_and_update_stats.uid AND comics.added_on > user_site_activity.seen_comics_before;
  INSERT INTO comic_remain_frag_cache SELECT uid, cid, num FROM comic_remain_frag(uid) JOIN comics USING (cid) WHERE cid NOT IN (SELECT cid FROM comic_remain_frag_cache WHERE comic_remain_frag_cache.uid=get_and_update_stats.uid);
  SELECT * FROM user_unread_stats(uid) INTO total_new, new_in, total_new_hide, new_in_hide;
END;
$$;


ALTER FUNCTION public.get_and_update_stats(uid integer, is_login boolean, OUT new_comics integer, OUT total_new integer, OUT new_in integer, OUT total_new_hide integer, OUT new_in_hide integer) OWNER TO kaol;

--
-- Name: get_max_frag(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.get_max_frag(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT COALESCE(array_upper((SELECT fragments FROM page_fragments WHERE cid=$1 and ord=$2), 1), 1)$_$;


ALTER FUNCTION public.get_max_frag(integer, integer) OWNER TO kaol;

--
-- Name: hiatus_other_inactive(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.hiatus_other_inactive() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  UPDATE hiatus_status SET active=false WHERE cid=NEW.cid AND active;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.hiatus_other_inactive() OWNER TO kaol;

--
-- Name: initial_user_login(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.initial_user_login() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF NEW.initial_lmid IS NULL THEN
    RAISE 'initial_lmid undefined for new user';
  END IF;
  UPDATE login_method SET uid=NEW.uid WHERE lmid=NEW.initial_lmid;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.initial_user_login() OWNER TO piperka;

--
-- Name: limit_notification_endpoints(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.limit_notification_endpoints() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (SELECT count(*) FROM user_push_notification WHERE uid=NEW.uid) > 5 THEN
    DELETE FROM user_push_notification WHERE unid=(SELECT unid FROM user_push_notification WHERE uid=NEW.uid ORDER BY unid LIMIT 1);
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.limit_notification_endpoints() OWNER TO kaol;

--
-- Name: match_comic(text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.match_comic(text) RETURNS SETOF integer
    LANGUAGE plpgsql IMMUTABLE
    AS $_$
DECLARE
minlen integer;
maxlen integer;
mycount integer;
stem text;
stem_escaped text;
cid integer;
BEGIN
maxlen := char_length($1);
minlen := position('/' in $1);
IF minlen = 0 THEN minlen := maxlen; END IF;
IF maxlen > 200 THEN maxlen := 200; END IF;
LOOP
IF minlen > maxlen OR maxlen = 0 THEN
  EXIT;
END IF;
stem := SUBSTRING($1 FOR minlen);
stem_escaped := replace(replace(replace(stem, E'\\', E'\\\\'), '_', E'\\_'), '%', E'\\%');
-- Plan caching wouldn't know that "stem" doesn't start with a %.
execute 'SELECT COUNT(*) FROM comics WHERE canonical_bookmark_form(url_base) like $1||''%''' into mycount using stem_escaped;
IF mycount = 1 OR EXISTS (SELECT * FROM comics WHERE canonical_bookmark_form(url_base)=stem) THEN
  EXIT;
END IF;
minlen := minlen+1;
END LOOP;
IF minlen <= maxlen THEN
  RETURN QUERY EXECUTE 'SELECT cid FROM comics WHERE canonical_bookmark_form(url_base) like $1||''%''' using stem_escaped;
END IF;
END;
$_$;


ALTER FUNCTION public.match_comic(text) OWNER TO piperka;

--
-- Name: match_home(text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.match_home(text) RETURNS TABLE(v_cid integer, v_ord integer, v_subord integer)
    LANGUAGE sql IMMUTABLE
    AS $_$
SELECT cid, max_ord_of(cid), max_subord_for(cid, max_ord_of(cid))
  FROM comics
  WHERE $1 IN (canonical_bookmark_form(homepage), canonical_bookmark_form(fixed_head));
$_$;


ALTER FUNCTION public.match_home(text) OWNER TO piperka;

--
-- Name: match_page(integer, text, boolean); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.match_page(cid integer, canonical text, here boolean, OUT ord integer, OUT subord integer, OUT at_max boolean) RETURNS record
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE
regexp text;
BEGIN
SELECT bookmark_regexp INTO regexp FROM comics WHERE comics.cid=$1;
IF regexp IS NOT NULL THEN
  SELECT updates.ord, CASE WHEN $3 THEN 0 ELSE 1+max_subord_for_0($1, updates.ord) END INTO ord, subord FROM updates WHERE name=substring($2 from regexp) AND updates.cid=$1;
  at_max := NOT here AND ord = (SELECT MAX(updates.ord) FROM updates WHERE updates.cid=$1);
END IF;
IF ord IS NULL THEN
  SELECT * INTO ord, subord, at_max FROM match_page_2($1, $2, '', $3);
  -- Allow for updates table to have '#' in it
  IF ord IS NULL AND position('#' in $2) <> 0 THEN
    SELECT * INTO ord, subord, at_max FROM match_page_2($1, substring($2 for position('#' in $2)-1), substring($2 from position('#' in $2)+1), $3);
  END IF;
END IF;
END;
$_$;


ALTER FUNCTION public.match_page(cid integer, canonical text, here boolean, OUT ord integer, OUT subord integer, OUT at_max boolean) OWNER TO piperka;

--
-- Name: match_page_2(integer, text, text, boolean); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.match_page_2(v_cid integer, body text, frag text, here boolean, OUT v_ord integer, OUT v_subord integer, OUT at_max boolean) RETURNS record
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE
base text;
tail text;
lbase int;
ltail int;
lbody int;
BEGIN
SELECT canonical_bookmark_form(url_base), url_tail INTO base, tail FROM comics WHERE cid=$1;
lbase := length(base);
lbody := length(body);
ltail := length(tail);
IF lbody <= lbase+ltail THEN
  RETURN;
END IF;
IF position(base in body) = 1 THEN
-- First try to match with tail
  IF substring(body from lbody-ltail+1)=tail THEN
    SELECT ord INTO v_ord FROM updates WHERE cid=$1 AND name=substring(body from lbase+1 for lbody-lbase-ltail) LIMIT 1;
  ELSE
-- Try with empty tail
    SELECT ord INTO v_ord FROM updates WHERE cid=$1 AND name=substring(body FROM lbase+1) LIMIT 1;
  END IF;
-- Special case: If tail is empty and body is with a slash, try without it
  IF v_ord IS NULL AND tail='' AND substring(body from lbody)='/' THEN
    SELECT ord INTO v_ord FROM updates WHERE cid=$1 AND name=substring(body FROM lbase+1 FOR lbody-lbase-1) LIMIT 1;
  END IF;
  IF v_ord IS NOT NULL THEN
    SELECT COALESCE(subord,0), v_ord = max_ord AND COALESCE(subord = max_subord_for(v_cid, v_ord), true)
      INTO v_subord, at_max FROM updates
      JOIN (SELECT cid, MAX(ord) AS max_ord FROM updates GROUP BY cid) AS mo USING (cid)
      LEFT JOIN (SELECT cid, ord, subord FROM page_fragments WHERE fragment=frag) AS f USING (cid, ord)
      WHERE cid=$1 and ord=v_ord;
    IF NOT here THEN
      v_subord := 1+CASE WHEN v_subord=0 THEN max_subord_for_0(v_cid, v_ord) ELSE v_subord END;
    ELSE
      at_max := false;
    END IF;
  END IF;
END IF;
END;
$_$;


ALTER FUNCTION public.match_page_2(v_cid integer, body text, frag text, here boolean, OUT v_ord integer, OUT v_subord integer, OUT at_max boolean) OWNER TO piperka;

--
-- Name: max_ord_of(integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.max_ord_of(integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT ord FROM max_update_ord WHERE cid=$1 ORDER BY ord DESC LIMIT 1$_$;


ALTER FUNCTION public.max_ord_of(integer) OWNER TO kaol;

--
-- Name: max_subord_for(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.max_subord_for(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT COALESCE(MAX(subord), 1) FROM page_fragments WHERE cid=$1 AND ord=$2;$_$;


ALTER FUNCTION public.max_subord_for(integer, integer) OWNER TO kaol;

--
-- Name: max_subord_for_0(integer, integer); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.max_subord_for_0(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$
SELECT COALESCE(MAX(subord), 0) FROM updates LEFT JOIN page_fragments USING (cid, ord) WHERE cid=$1 AND ord=$2;
$_$;


ALTER FUNCTION public.max_subord_for_0(integer, integer) OWNER TO piperka;

--
-- Name: must_have_login2(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.must_have_login2() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  n text;
BEGIN
  IF OLD.uid IN (SELECT uid FROM users WHERE deleted_at IS NULL) AND NOT EXISTS (SELECT 1 FROM login_method AS lm WHERE OLD.uid = lm.uid AND OLD.lmid <> lm.lmid) AND OLD.uid NOT IN (SELECT uid FROM users WHERE passwd IS NOT NULL) THEN
    n := (SELECT name FROM users WHERE uid=OLD.uid);
    RAISE EXCEPTION 'Account % with no login credentials %', n, old.uid USING ERRCODE = 'PI001';
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.must_have_login2() OWNER TO piperka;

--
-- Name: ordering_form(text); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.ordering_form(text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$SELECT SUBSTRING(LOWER($1) FROM '^(?:the )?(.*)')$_$;


ALTER FUNCTION public.ordering_form(text) OWNER TO kaol;

--
-- Name: page_era_update(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.page_era_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF TG_OP='UPDATE' AND (NEW.name <> OLD.name OR NEW.ord <> OLD.ord) THEN
    INSERT INTO page_era (cid, ord, subord) VALUES (NEW.cid, NEW.ord, 0) ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
    INSERT INTO page_era (cid, ord, subord) SELECT cid, ord, subord FROM page_fragments WHERE cid=NEW.cid ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
  END IF;
  IF TG_OP='DELETE' OR TG_OP='UPDATE' AND NEW.ord <> OLD.ord THEN
    INSERT INTO page_era (cid, ord, subord) VALUES (OLD.cid, OLD.ord, 0) ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
    INSERT INTO page_era (cid, ord, subord) SELECT cid, ord, subord FROM page_fragments WHERE cid=OLD.cid AND ord=OLD.ord ON CONFLICT (cid, ord, subord) DO UPDATE SET era=page_era.era+1;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.page_era_update() OWNER TO piperka;

--
-- Name: privacy_permitted_interest(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.privacy_permitted_interest() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'DELETE') THEN
  DELETE FROM permitted_interest WHERE uid=OLD.uid OR interest=OLD.uid;
ELSIF (TG_OP = 'UPDATE' AND NEW.privacy <> OLD.privacy) THEN
  DELETE FROM permitted_interest WHERE interest=NEW.uid;
  IF (NEW.privacy = 2 OR NEW.privacy = 3) THEN
    INSERT INTO permitted_interest (uid, interest, cid) SELECT follower.uid, interest, cid FROM follower JOIN subscriptions ON interest=subscriptions.uid WHERE interest = NEW.uid;
    IF (NEW.privacy = 2) THEN
      DELETE FROM permitted_interest WHERE interest=NEW.uid AND (uid, interest) NOT IN (SELECT followee, uid FROM follow_permission);
    END IF;
  END IF;
  IF (NEW.privacy = 1) THEN
    DELETE FROM permitted_interest WHERE uid=NEW.uid AND interest IN (SELECT uid FROM follow_permission JOIN users USING (uid) WHERE privacy=2);
  END IF;
END IF;
RETURN NULL;
END
$$;


ALTER FUNCTION public.privacy_permitted_interest() OWNER TO kaol;

--
-- Name: prune_null_oauth2_login(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.prune_null_oauth2_login() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM login_method_oauth2 WHERE uid IS NULL AND opid = NEW.opid AND identification = NEW.identification;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.prune_null_oauth2_login() OWNER TO piperka;

--
-- Name: random_string(integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.random_string(len integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  res text := '';
  i int := 0;
  ch_len int := array_length(chars, 1)-1;
BEGIN
  FOR i IN 1..len LOOP
    res := res || chars[ceil(ch_len * random())];
  END LOOP;
  RETURN res;
END;
$$;


ALTER FUNCTION public.random_string(len integer) OWNER TO kaol;

--
-- Name: recent_update(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.recent_update(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT ord FROM recent WHERE uid=$1 AND cid=$2$_$;


ALTER FUNCTION public.recent_update(integer, integer) OWNER TO kaol;

--
-- Name: recent_update_name(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.recent_update_name(integer, integer) RETURNS text
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT name FROM updates WHERE cid=$2 AND ord=recent_update($1, cid)$_$;


ALTER FUNCTION public.recent_update_name(integer, integer) OWNER TO kaol;

--
-- Name: recover_session(uuid); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.recover_session(session uuid) RETURNS TABLE(uid integer, csrf_ham uuid)
    LANGUAGE plpgsql
    AS $$
BEGIN
  SELECT users.uid FROM p_session JOIN users USING (uid) INTO recover_session.uid WHERE ses = session AND token_for IS NULL;
  IF uid IS NOT NULL THEN
    UPDATE p_session SET last_active = NOW() WHERE ses=session;
    UPDATE users SET last_active = NOW() WHERE users.uid=recover_session.uid;
    IF (SELECT EXISTS (SELECT FROM pg_tables WHERE schemaname=current_schema AND tablename='user_site_activity')) THEN
      UPDATE user_site_activity SET last_active = users.last_active FROM users WHERE users.uid = user_site_activity.uid AND users.uid = recover_session.uid;
    END IF;
    SELECT ses FROM p_session WHERE token_for=session INTO csrf_ham;
    IF csrf_ham IS NULL THEN
      SELECT token INTO csrf_ham FROM generate_session(uid, session);
    END IF;
    RETURN QUERY SELECT uid, csrf_ham;
  END IF;
END;
$$;


ALTER FUNCTION public.recover_session(session uuid) OWNER TO piperka;

--
-- Name: redir_url_and_last(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.redir_url_and_last(integer, integer) RETURNS SETOF public.redir_url_and_last
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT url_base || name || url_tail || COALESCE('#' || (SELECT fragment FROM page_fragments WHERE cid = $2 AND page_fragments.ord = updates.ord AND subord = CASE WHEN unread_fragments_correction = 1 THEN subscr_or_recent_subord_for ELSE 0 END), '') as url, subscr_or_recent_ord_for, subscr_or_recent_subord_for FROM comics JOIN updates USING (cid), subscr_or_recent_ord_for($1, $2), subscr_or_recent_subord_for($1, $2), unread_fragments_correction($1, $2) WHERE cid=$2 AND ord=subscr_or_recent_ord_for+1-unread_fragments_correction$_$;


ALTER FUNCTION public.redir_url_and_last(integer, integer) OWNER TO kaol;

--
-- Name: redir_url_and_last(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.redir_url_and_last(integer, integer, integer) RETURNS SETOF public.redir_url_and_last
    LANGUAGE sql
    AS $_$
  WITH subscr_or_recent AS
    (SELECT subscr_or_recent_ord_for($1, $2) AS ord, subscr_or_recent_subord_for($1, $2) AS subord, unread_fragments_correction($1, $2) AS correction),
    upd AS
    (SELECT cid, name FROM updates, subscr_or_recent WHERE updates.ord=CASE WHEN subscr_or_recent.ord+1-correction+$3 < 0 THEN 0 ELSE subscr_or_recent.ord+1-correction+$3 END)
  SELECT url_base || name || url_tail ||
    COALESCE('#' || (SELECT fragment FROM page_fragments WHERE cid = $2 AND page_fragments.ord = subscr_or_recent.ord
      AND subord = CASE WHEN correction = 1 THEN subscr_or_recent.subord ELSE 0 END), '') AS url,
    subscr_or_recent.ord, subscr_or_recent.subord
  FROM subscr_or_recent, comics LEFT JOIN upd USING (cid)
  WHERE cid=$2 AND CASE WHEN name IS NULL THEN (cid, subscr_or_recent.ord) IN (SELECT cid, ord-1 FROM max_update_ord JOIN fixed_head USING (cid)) ELSE true END
$_$;


ALTER FUNCTION public.redir_url_and_last(integer, integer, integer) OWNER TO kaol;

--
-- Name: refresh_user_remain_frag(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.refresh_user_remain_frag() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  INSERT INTO comic_remain_frag_cache SELECT NEW.uid, cid, num FROM comic_remain_frag(NEW.uid) JOIN comics USING (cid) WHERE cid NOT IN (SELECT cid FROM comic_remain_frag_cache WHERE uid=NEW.uid);
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.refresh_user_remain_frag() OWNER TO kaol;

--
-- Name: reset_user_password(text, character, text); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.reset_user_password(name text, hash character, password text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
<<func>>
DECLARE
  uid integer;
BEGIN
  uid := (SELECT users.uid FROM users JOIN pwdgen_hash USING (uid)
       WHERE lower(users.name)=lower($1) AND pwdgen_hash.hash = $2
       AND pwdgen_hash.stamp > now() - ('1 day'::interval));
  IF uid IS NULL THEN
    RETURN FALSE;
  END IF;
  PERFORM auth_create_password($3, uid);
  DELETE FROM pwdgen_hash WHERE pwdgen_hash.uid=func.uid;
  RETURN TRUE;
END;
$_$;


ALTER FUNCTION public.reset_user_password(name text, hash character, password text) OWNER TO piperka;

--
-- Name: set_bookmark(integer, integer, boolean); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.set_bookmark(in_uid integer, in_cid integer, start_at_first boolean, OUT total_new integer, OUT new_in integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
  was_subscribed boolean;
  new_ord integer;
  new_subord integer;
BEGIN
  SELECT (in_uid, in_cid) IN (SELECT uid, cid FROM subscriptions) INTO was_subscribed;
  IF start_at_first THEN
    new_ord := 0;
    new_subord := 0;
  ELSE
    SELECT ord, COALESCE((SELECT MAX(subord)+1 FROM page_fragments
 WHERE page_fragments.cid=in_cid AND ord=updates.ord), 1)
      FROM updates WHERE cid=in_cid ORDER BY ord DESC LIMIT 1 INTO new_ord, new_subord;
  END IF;
  IF was_subscribed THEN
    UPDATE subscriptions set ord=new_ord, subord=new_subord WHERE cid=in_cid AND uid=in_uid;
  ELSE
    INSERT INTO subscriptions (uid, cid, ord, subord) VALUES (in_uid, in_cid, new_ord, new_subord);
  END IF;
  INSERT INTO comic_remain_frag_cache SELECT in_uid, cid, num FROM comic_remain_frag(in_uid) WHERE cid=in_cid;
  SELECT COALESCE(SUM(num), 0), COUNT(*) FROM comic_remain_frag_cache JOIN comics USING (cid) WHERE num > 0 AND uid=in_uid INTO total_new, new_in;
END;
$$;


ALTER FUNCTION public.set_bookmark(in_uid integer, in_cid integer, start_at_first boolean, OUT total_new integer, OUT new_in integer) OWNER TO kaol;

--
-- Name: set_bookmark(integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.set_bookmark(in_uid integer, in_cid integer, in_ord integer, in_subord integer, OUT total_new integer, OUT new_in integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  IF (SELECT (in_uid, in_cid) IN (SELECT uid, cid FROM subscriptions)) THEN
    UPDATE subscriptions SET ord=in_ord, subord=in_subord WHERE uid=in_uid AND cid=in_cid;
  ELSE
    INSERT INTO subscriptions (uid, cid, ord, subord) values (in_uid, in_cid, in_ord, in_subord);
  END IF;
  INSERT INTO comic_remain_frag_cache SELECT in_uid, cid, num FROM comic_remain_frag(in_uid) WHERE cid=in_cid;
  SELECT COALESCE(SUM(num), 0), COUNT(*) FROM comic_remain_frag_cache JOIN comics USING (cid) WHERE num > 0 AND uid=in_uid INTO total_new, new_in;
END;
$$;


ALTER FUNCTION public.set_bookmark(in_uid integer, in_cid integer, in_ord integer, in_subord integer, OUT total_new integer, OUT new_in integer) OWNER TO kaol;

--
-- Name: subscr_or_recent_ord_for(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.subscr_or_recent_ord_for(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT CASE WHEN subscriptions.ord+1-unread_fragments_correction <= max_ord_of($2) THEN subscriptions.ord ELSE recent_update($1, $2) END FROM subscriptions, unread_fragments_correction($1, $2) WHERE uid=$1 AND cid=$2$_$;


ALTER FUNCTION public.subscr_or_recent_ord_for(integer, integer) OWNER TO kaol;

--
-- Name: subscr_or_recent_subord_for(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.subscr_or_recent_subord_for(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT CASE WHEN ord+1-unread_fragments_correction <= max_ord_of($2) THEN subord ELSE (SELECT subord FROM recent WHERE uid=$1 AND cid=$2 LIMIT 1) END FROM subscriptions, unread_fragments_correction($1, $2) WHERE uid=$1 AND cid=$2$_$;


ALTER FUNCTION public.subscr_or_recent_subord_for(integer, integer) OWNER TO kaol;

--
-- Name: subscription_new_user_update(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.subscription_new_user_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  maxord max_update_ord%ROWTYPE;
BEGIN
  IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
    SELECT * INTO maxord FROM max_update_ord WHERE cid=NEW.cid;
    IF NEW.ord=maxord.ord AND NEW.subord>=maxord.subord THEN
      INSERT INTO new_user_update (uid, cid, ord, subord) SELECT NEW.uid, NEW.cid, NEW.ord, NEW.subord ON CONFLICT (uid, cid) DO UPDATE SET updated=false, notified=false, ord=NEW.ord, subord=NEW.subord;
    ELSE
      DELETE FROM new_user_update WHERE uid=NEW.uid AND cid=NEW.cid;
    END IF;
  ELSIF TG_OP = 'DELETE' THEN
    DELETE FROM new_user_update WHERE uid=OLD.uid AND cid=OLD.cid;
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.subscription_new_user_update() OWNER TO kaol;

--
-- Name: subscriptions_permitted_interest(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.subscriptions_permitted_interest() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  privacy int;
BEGIN
IF (TG_OP = 'DELETE') THEN
  DELETE FROM permitted_interest WHERE interest=OLD.uid AND cid=OLD.cid;
ELSIF (TG_OP = 'INSERT') THEN
  -- safeguard
  DELETE FROM permitted_interest WHERE interest=NEW.uid AND cid=NEW.cid;
  privacy := (SELECT users.privacy FROM users WHERE uid=NEW.uid);
  IF (privacy = 2) THEN
    INSERT INTO permitted_interest (uid, interest, cid) SELECT followee, uid, NEW.cid FROM follow_permission WHERE uid=NEW.uid AND (uid, followee) IN (SELECT interest, uid FROM follower);
  ELSIF (privacy = 3) THEN
    INSERT INTO permitted_interest (uid, interest, cid) SELECT uid, interest, NEW.cid FROM follower WHERE interest=NEW.uid;
  END IF;
END IF;
RETURN NULL;
END
$$;


ALTER FUNCTION public.subscriptions_permitted_interest() OWNER TO kaol;

--
-- Name: token_login(uuid); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.token_login(token uuid, OUT uid integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE
  old_ses uuid;
BEGIN
  SELECT p_session.uid FROM appuser_token JOIN p_session ON appuser_token.token = p_session.ses WHERE appuser_token.token=token_login.token INTO token_login.uid;
  IF uid IS NULL THEN
    SELECT p_session.uid, ses FROM p_session WHERE ses=$1 AND token_for IS NOT NULL AND begun < '2018-02-25' INTO token_login.uid, old_ses;
    IF old_ses IS NOT NULL THEN
      INSERT INTO appuser_token (token) VALUES (old_ses);
    END IF;
  END IF;
END;
$_$;


ALTER FUNCTION public.token_login(token uuid, OUT uid integer) OWNER TO kaol;

--
-- Name: unique_user_names(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.unique_user_names() RETURNS trigger
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
  IF lower(NEW.name) IN (SELECT name FROM users WHERE users.uid<>NEW.uid) THEN
    RAISE unique_violation USING MESSAGE = 'Duplicate user name: ' || NEW.name;
  END IF;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.unique_user_names() OWNER TO kaol;

--
-- Name: unread_fragments(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.unread_fragments(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT CASE WHEN subord <= COALESCE(array_upper(fragments, 1), 1) THEN 1 ELSE 0 END FROM subscriptions LEFT JOIN page_fragments USING (cid, ord) WHERE uid=$1 and cid=$2$_$;


ALTER FUNCTION public.unread_fragments(integer, integer) OWNER TO kaol;

--
-- Name: unread_fragments_correction(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.unread_fragments_correction(integer, integer) RETURNS integer
    LANGUAGE sql STABLE PARALLEL SAFE
    AS $_$SELECT CASE WHEN subscriptions.subord <= coalesce(max(page_fragments.subord), 0) THEN 1 ELSE 0 END FROM subscriptions LEFT JOIN page_fragments USING (cid, ord) WHERE uid=$1 AND cid=$2 GROUP BY subscriptions.subord$_$;


ALTER FUNCTION public.unread_fragments_correction(integer, integer) OWNER TO kaol;

--
-- Name: unset_bookmark(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.unset_bookmark(in_uid integer, in_cid integer, OUT total_new integer, OUT new_in integer) RETURNS record
    LANGUAGE plpgsql
    AS $$
BEGIN
  DELETE FROM subscriptions WHERE cid=in_cid AND uid=in_uid;
  INSERT INTO comic_remain_frag_cache SELECT in_uid, cid, num FROM comic_remain_frag(in_uid) WHERE cid=in_cid;
  SELECT COALESCE(SUM(num), 0), COUNT(*) FROM comic_remain_frag_cache JOIN comics USING (cid) WHERE num > 0 AND uid=in_uid INTO total_new, new_in;
END;
$$;


ALTER FUNCTION public.unset_bookmark(in_uid integer, in_cid integer, OUT total_new integer, OUT new_in integer) OWNER TO kaol;

--
-- Name: update_last_set(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.update_last_set() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF current_setting('piperka.mass_update', true) IS NULL THEN
  NEW.last_set := timezone('UTC'::text, now());
END IF;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.update_last_set() OWNER TO kaol;

--
-- Name: update_max_update_ord(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.update_max_update_ord() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _cid integer;
BEGIN
  IF current_setting('piperka.mass_update', true) IS NOT NULL THEN
    RETURN NULL;
  END IF;
  IF (SELECT EXISTS (SELECT 1 FROM pg_tables WHERE tablename = 'piperka_temp_mass_update')) THEN
    IF (SELECT EXISTS (SELECT * FROM piperka_temp_mass_update)) THEN
      RETURN NULL;
    END IF;
  END IF;
  -- Writing this without the loop causes a seq scan on updates
  FOR _cid IN SELECT DISTINCT cid FROM mod_updates LOOP
    INSERT INTO max_update_ord (cid, ord, subord)
      SELECT _cid, ord, coalesce(max(subord), 0)
      FROM (SELECT cid, max(ord)+CASE WHEN cid IN (SELECT cid FROM fixed_head) THEN 1 ELSE 0 END
            AS ord FROM updates
            WHERE updates.cid=_cid GROUP BY cid) AS max_upd
      LEFT JOIN page_fragments USING (cid, ord) GROUP BY ord
      ON CONFLICT (cid) DO UPDATE SET ord=EXCLUDED.ord, subord=EXCLUDED.subord;
  END LOOP;
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.update_max_update_ord() OWNER TO kaol;

--
-- Name: update_name(integer, integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.update_name(integer, integer) RETURNS text
    LANGUAGE sql
    AS $_$SELECT name FROM updates WHERE cid=$1 AND ord=$2$_$;


ALTER FUNCTION public.update_name(integer, integer) OWNER TO kaol;

--
-- Name: update_readers(); Type: FUNCTION; Schema: public; Owner: piperka
--

CREATE FUNCTION public.update_readers() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'DELETE') THEN
  IF (SELECT countme FROM users WHERE uid=OLD.uid) THEN
    UPDATE comics SET readers=CASE WHEN readers<=0 THEN 0 ELSE readers-1 END WHERE cid=OLD.cid;
  END IF;
ELSIF (TG_OP = 'INSERT') THEN
  IF (SELECT countme FROM users WHERE uid=NEW.uid) THEN
    UPDATE comics SET readers=readers+1 WHERE cid=NEW.cid;
  END IF;
END IF;
RETURN NULL;
END
$$;


ALTER FUNCTION public.update_readers() OWNER TO piperka;

--
-- Name: update_recent_change(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.update_recent_change() RETURNS void
    LANGUAGE sql
    AS $$ update comics set recent_change =
(select ratio from
(select stamp, (av+1)/(lead(av,2) over (partition by cid order by stamp desc)+1) as ratio from
(select stamp, avg(readers) over (partition by cid order by stamp rows between 2 preceding and current row) as av from readers_history where cid=comics.cid order by stamp desc) as x where stamp >=(now() - interval '4 days')::date) as y where stamp=(now() - interval '1 day')::date)
$$;


ALTER FUNCTION public.update_recent_change() OWNER TO kaol;

--
-- Name: updates_new_user_update(); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.updates_new_user_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _cid integer;
BEGIN
  UPDATE new_user_update AS u SET updated=true FROM mod_updates JOIN max_update_ord USING (cid) JOIN new_user_update USING (cid) WHERE u.uid=new_user_update.uid AND u.cid=new_user_update.cid AND (new_user_update.ord < max_update_ord.ord OR (new_user_update.ord=max_update_ord.ord AND new_user_update.subord<max_update_ord.subord));
  RETURN NULL;
END;
$$;


ALTER FUNCTION public.updates_new_user_update() OWNER TO kaol;

--
-- Name: user_unread_stats(integer); Type: FUNCTION; Schema: public; Owner: kaol
--

CREATE FUNCTION public.user_unread_stats(uid integer, OUT total_new integer, OUT new_in integer, OUT total_new_hide integer, OUT new_in_hide integer) RETURNS record
    LANGUAGE sql
    AS $$SELECT COALESCE(SUM(num), 0), COUNT(*), COALESCE(SUM(CASE WHEN s.cid IS NULL THEN null ELSE num END), 0), COUNT(s.cid)  FROM comics JOIN comic_remain_frag_cache AS c USING (cid) LEFT JOIN subscriptions AS s ON c.cid=s.cid AND c.uid = s.uid AND (num <= 50 OR coalesce(restored_on > now() - '14 days' :: interval, false) OR coalesce(last_set > now() - '14 days' :: interval, false)) WHERE c.uid=user_unread_stats.uid AND num > 0$$;


ALTER FUNCTION public.user_unread_stats(uid integer, OUT total_new integer, OUT new_in integer, OUT total_new_hide integer, OUT new_in_hide integer) OWNER TO kaol;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: comics; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.comics (
    cid integer NOT NULL,
    title text,
    url_base text,
    url_tail text,
    fixed_head text,
    homepage text NOT NULL,
    added_on timestamp without time zone DEFAULT now(),
    description text,
    tags integer DEFAULT 0 NOT NULL,
    bookmark_regexp text,
    category integer,
    readers integer DEFAULT 0 NOT NULL,
    mapped boolean DEFAULT false NOT NULL,
    recent_change real,
    restored_on timestamp without time zone
);


ALTER TABLE piperka.comics OWNER TO piperka;

--
-- Name: graveyard; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.graveyard (
    cid integer NOT NULL,
    title text,
    url_base text,
    url_tail text,
    fixed_head text,
    homepage text NOT NULL,
    added_on timestamp without time zone,
    description text,
    tags integer NOT NULL,
    bookmark_regexp text,
    category integer,
    reason text NOT NULL,
    removed_on timestamp without time zone DEFAULT now()
);


ALTER TABLE piperka.graveyard OWNER TO piperka;

--
-- Name: all_comics; Type: VIEW; Schema: piperka; Owner: piperka
--

CREATE VIEW piperka.all_comics AS
 SELECT comics.cid,
    comics.title,
    comics.url_base,
    comics.url_tail,
    comics.fixed_head,
    comics.homepage,
    comics.added_on,
    comics.description,
    comics.tags,
    comics.bookmark_regexp
   FROM piperka.comics
UNION
 SELECT graveyard.cid,
    graveyard.title,
    graveyard.url_base,
    graveyard.url_tail,
    graveyard.fixed_head,
    graveyard.homepage,
    graveyard.added_on,
    graveyard.description,
    graveyard.tags,
    graveyard.bookmark_regexp
   FROM piperka.graveyard;


ALTER TABLE piperka.all_comics OWNER TO piperka;

--
-- Name: alphabets; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.alphabets (
    letter character(1) NOT NULL
);


ALTER TABLE public.alphabets OWNER TO piperka;

--
-- Name: alphabet_index; Type: MATERIALIZED VIEW; Schema: piperka; Owner: piperka
--

CREATE MATERIALIZED VIEW piperka.alphabet_index AS
 SELECT alphabets.letter,
    count(*) AS ord
   FROM (public.alphabets
     JOIN ( SELECT public.ordering_form(comics.title) AS canon
           FROM piperka.comics) s ON ((lower((alphabets.letter)::text) > s.canon)))
  GROUP BY alphabets.letter
  ORDER BY alphabets.letter
  WITH NO DATA;


ALTER TABLE piperka.alphabet_index OWNER TO piperka;

--
-- Name: audit_log; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.audit_log (
    audit_id integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    cid integer NOT NULL,
    audit jsonb
);


ALTER TABLE piperka.audit_log OWNER TO piperka;

--
-- Name: audit_log_audit_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.audit_log_audit_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.audit_log_audit_id_seq OWNER TO piperka;

--
-- Name: audit_log_audit_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.audit_log_audit_id_seq OWNED BY piperka.audit_log.audit_id;


--
-- Name: author; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.author (
    auid integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE piperka.author OWNER TO piperka;

--
-- Name: author_auid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.author_auid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.author_auid_seq OWNER TO piperka;

--
-- Name: author_auid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.author_auid_seq OWNED BY piperka.author.auid;


--
-- Name: author_comics; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.author_comics (
    auid integer NOT NULL,
    cid integer NOT NULL
);


ALTER TABLE piperka.author_comics OWNER TO piperka;

--
-- Name: banner_data; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.banner_data (
    bdid integer NOT NULL,
    content bytea NOT NULL,
    mime text NOT NULL
);


ALTER TABLE piperka.banner_data OWNER TO piperka;

--
-- Name: banner_data_bdid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.banner_data_bdid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.banner_data_bdid_seq OWNER TO piperka;

--
-- Name: banner_data_bdid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.banner_data_bdid_seq OWNED BY piperka.banner_data.bdid;


--
-- Name: banner_submits; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.banner_submits (
    id integer NOT NULL,
    cid integer,
    uid integer,
    url text
);


ALTER TABLE piperka.banner_submits OWNER TO piperka;

--
-- Name: banner_submits_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.banner_submits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.banner_submits_id_seq OWNER TO piperka;

--
-- Name: banner_submits_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.banner_submits_id_seq OWNED BY piperka.banner_submits.id;


--
-- Name: banners; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.banners (
    cid integer NOT NULL,
    file text,
    bdid integer
);


ALTER TABLE piperka.banners OWNER TO piperka;

--
-- Name: bookmarking_log; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.bookmarking_log (
    id integer NOT NULL,
    at_date timestamp without time zone,
    uid integer,
    host inet,
    want_here boolean,
    url text,
    cid integer,
    ord integer,
    subord integer,
    is_at_max boolean
);


ALTER TABLE piperka.bookmarking_log OWNER TO piperka;

--
-- Name: bookmarking_log_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.bookmarking_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.bookmarking_log_id_seq OWNER TO piperka;

--
-- Name: bookmarking_log_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.bookmarking_log_id_seq OWNED BY piperka.bookmarking_log.id;


--
-- Name: claim_token; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.claim_token (
    uid integer NOT NULL,
    cid integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    token text DEFAULT public.random_string(10) NOT NULL
);


ALTER TABLE piperka.claim_token OWNER TO piperka;

--
-- Name: comic_alias; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.comic_alias (
    caid integer NOT NULL,
    cid integer NOT NULL,
    alias text NOT NULL
);


ALTER TABLE piperka.comic_alias OWNER TO piperka;

--
-- Name: comic_alias_caid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.comic_alias_caid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.comic_alias_caid_seq OWNER TO piperka;

--
-- Name: comic_alias_caid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.comic_alias_caid_seq OWNED BY piperka.comic_alias.caid;


--
-- Name: comic_remain_frag_cache; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.comic_remain_frag_cache (
    uid integer NOT NULL,
    cid integer NOT NULL,
    num integer NOT NULL
);


ALTER TABLE piperka.comic_remain_frag_cache OWNER TO piperka;

--
-- Name: comic_tag; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.comic_tag (
    cid integer NOT NULL,
    tagid smallint NOT NULL
);


ALTER TABLE piperka.comic_tag OWNER TO piperka;

--
-- Name: comic_title_history; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.comic_title_history (
    ctitid integer NOT NULL,
    uid integer NOT NULL,
    cid integer NOT NULL,
    old_title text NOT NULL,
    stamp timestamp without time zone DEFAULT now(),
    from_ip inet NOT NULL
);


ALTER TABLE piperka.comic_title_history OWNER TO piperka;

--
-- Name: comic_title_history_ctitid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.comic_title_history_ctitid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.comic_title_history_ctitid_seq OWNER TO piperka;

--
-- Name: comic_title_history_ctitid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.comic_title_history_ctitid_seq OWNED BY piperka.comic_title_history.ctitid;


--
-- Name: comics_cid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.comics_cid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.comics_cid_seq OWNER TO piperka;

--
-- Name: comics_cid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.comics_cid_seq OWNED BY piperka.comics.cid;


--
-- Name: crawler_config; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.crawler_config (
    cid integer NOT NULL,
    parser_type smallint,
    update_score real DEFAULT 0,
    update_value real DEFAULT 0 NOT NULL,
    last_updated timestamp without time zone DEFAULT '-infinity'::timestamp without time zone NOT NULL,
    head_crop_count smallint DEFAULT 0 NOT NULL,
    extra_url text,
    extra_data text,
    old_head_crop_count smallint DEFAULT 0 NOT NULL
);


ALTER TABLE piperka.crawler_config OWNER TO piperka;

--
-- Name: crawler_log; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.crawler_log (
    plog_id integer NOT NULL,
    run_id integer NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    event_type public.crawl_event_type NOT NULL,
    event jsonb NOT NULL
);


ALTER TABLE piperka.crawler_log OWNER TO piperka;

--
-- Name: crawl_base_redirects; Type: MATERIALIZED VIEW; Schema: piperka; Owner: piperka
--

CREATE MATERIALIZED VIEW piperka.crawl_base_redirects AS
 SELECT DISTINCT ON (b.cid) b.cid,
    b.stamp,
    b.fail_url,
    b.origin_url
   FROM ( SELECT a.run_id,
            a.cid,
            a.fail_url,
            a.origin_url,
            a.stamp
           FROM ( SELECT p.run_id,
                    comics.cid,
                    (array_agg(((p.event -> 'target'::text) ->> 'next_url'::text) ORDER BY p.plog_id) FILTER (WHERE ((p.event_type = 'parse'::public.crawl_event_type) AND ("substring"(((p.event -> 'target'::text) ->> 'next_url'::text), 1, length(comics.url_base)) <> comics.url_base) AND ((((p.event -> 'target'::text) ->> 'next_url'::text) <> crawler_config.extra_url) OR (crawler_config.extra_url IS NULL)))))[1] AS fail_url,
                    (array_agg(((p.event -> 'target'::text) ->> 'next_url'::text) ORDER BY p.plog_id DESC) FILTER (WHERE (p.event_type = 'target'::public.crawl_event_type)))[1] AS origin_url,
                    max(p.stamp) AS stamp
                   FROM (((piperka.comics
                     JOIN piperka.crawler_config USING (cid))
                     JOIN piperka.crawler_log p USING (cid))
                     LEFT JOIN ( SELECT DISTINCT ON (x.cid) x.cid,
                            x.run_id
                           FROM ( SELECT count(*) FILTER (WHERE (crawler_log.event_type = 'target'::public.crawl_event_type)) AS tg,
                                    count(*) FILTER (WHERE (crawler_log.event_type = 'parse'::public.crawl_event_type)) AS pr,
                                    COALESCE((max((crawler_log.event ->> 'head_crop'::text)))::integer, 0) AS hc,
                                    crawler_log.cid,
                                    crawler_log.run_id
                                   FROM piperka.crawler_log
                                  GROUP BY crawler_log.cid, crawler_log.run_id
                                  ORDER BY crawler_log.run_id DESC) x
                          WHERE ((x.tg = 1) AND (x.pr = x.hc))
                          ORDER BY x.cid, x.run_id DESC) succ USING (cid))
                  WHERE ((succ.run_id IS NULL) OR (succ.run_id < p.run_id))
                  GROUP BY p.run_id, comics.cid) a
          WHERE (a.fail_url IS NOT NULL)) b
  ORDER BY b.cid, b.run_id DESC
  WITH NO DATA;


ALTER TABLE piperka.crawl_base_redirects OWNER TO piperka;

--
-- Name: crawl_error; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.crawl_error (
    errid integer NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    url character varying(500) NOT NULL,
    http_code integer,
    http_message character varying(200)
);


ALTER TABLE piperka.crawl_error OWNER TO piperka;

--
-- Name: crawl_error_errid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.crawl_error_errid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.crawl_error_errid_seq OWNER TO piperka;

--
-- Name: crawl_error_errid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.crawl_error_errid_seq OWNED BY piperka.crawl_error.errid;


--
-- Name: crawl_head_redirects; Type: MATERIALIZED VIEW; Schema: piperka; Owner: piperka
--

CREATE MATERIALIZED VIEW piperka.crawl_head_redirects AS
 SELECT tg.cid,
    ((tg.event -> 'target'::text) ->> 'next_url'::text) AS origin_url,
    hr.fail_url,
    (hr.fail_code)::integer AS fail_code
   FROM ((piperka.crawler_log tg
     JOIN ( SELECT DISTINCT ON (crawler_log.cid) crawler_log.cid,
            crawler_log.run_id,
            ((crawler_log.event -> 'parse'::text) ->> 'next_url'::text) AS fail_url,
            ((crawler_log.event -> 'parse'::text) ->> 'code'::text) AS fail_code
           FROM piperka.crawler_log
          WHERE ((crawler_log.stamp > (now() + '-25:00:00'::interval)) AND (crawler_log.event_type = 'parse'::public.crawl_event_type) AND (((crawler_log.event -> 'target'::text) ->> 'head_redir'::text) IS NOT NULL))
          ORDER BY crawler_log.cid, crawler_log.run_id DESC) hr USING (cid, run_id))
     JOIN piperka.comics USING (cid))
  WHERE (tg.event_type = 'target'::public.crawl_event_type)
  WITH NO DATA;


ALTER TABLE piperka.crawl_head_redirects OWNER TO piperka;

--
-- Name: crawler_health; Type: MATERIALIZED VIEW; Schema: piperka; Owner: piperka
--

CREATE MATERIALIZED VIEW piperka.crawler_health AS
 SELECT x.cid,
    x.origin,
    x.event,
    x.last_success
   FROM ( SELECT c1.cid,
            c2.event,
            c1.origin,
            max(c3.stamp) AS last_success
           FROM ((((( SELECT DISTINCT ON (crawler_log.cid) crawler_log.run_id,
                    crawler_log.cid,
                    ((crawler_log.event -> 'target'::text) ->> 'next_url'::text) AS origin
                   FROM piperka.crawler_log
                  WHERE ((crawler_log.event_type = 'target'::public.crawl_event_type) AND (crawler_log.stamp > (now() - '7 days'::interval)))
                  ORDER BY crawler_log.cid, crawler_log.run_id DESC) c1
             JOIN piperka.crawler_log c2 USING (run_id, cid))
             JOIN piperka.comics USING (cid))
             LEFT JOIN piperka.crawler_log c3 ON (((c1.cid = c3.cid) AND (c3.event_type = 'terminal'::public.crawl_event_type) AND (((c3.event -> 'end'::text) ->> 'type'::text) <> ALL (ARRAY['faulty'::text, 'http_error'::text, 'crawl'::text])) AND (c3.stamp > (now() - '6 mons'::interval)))))
             LEFT JOIN piperka.crawler_log c4 ON (((c1.cid = c4.cid) AND (c4.run_id = c1.run_id) AND (c4.event_type = 'parse'::public.crawl_event_type) AND (((c4.event -> 'target'::text) ->> 'head_redir'::text) IS NULL))))
          WHERE ((c2.event_type = 'terminal'::public.crawl_event_type) AND (c4.cid IS NULL) AND (((c2.event -> 'end'::text) ->> 'type'::text) = ANY (ARRAY['faulty'::text, 'http_error'::text, 'crawl'::text])))
          GROUP BY c1.cid, c1.origin, c2.event) x
  WHERE COALESCE((x.last_success < (now() - '7 days'::interval)), true)
  WITH NO DATA;


ALTER TABLE piperka.crawler_health OWNER TO piperka;

--
-- Name: crawler_log_plog_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.crawler_log_plog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.crawler_log_plog_id_seq OWNER TO piperka;

--
-- Name: crawler_log_plog_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.crawler_log_plog_id_seq OWNED BY piperka.crawler_log.plog_id;


--
-- Name: crawler_log_run_id; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.crawler_log_run_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.crawler_log_run_id OWNER TO piperka;

--
-- Name: crawler_log_run_id; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.crawler_log_run_id OWNED BY piperka.crawler_log.run_id;


--
-- Name: disinterest; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.disinterest (
    uid integer NOT NULL,
    cid integer NOT NULL
);


ALTER TABLE piperka.disinterest OWNER TO piperka;

--
-- Name: submit_sid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.submit_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.submit_sid_seq OWNER TO piperka;

--
-- Name: user_edit; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.user_edit (
    sid integer DEFAULT nextval('piperka.submit_sid_seq'::regclass) NOT NULL,
    cid integer NOT NULL,
    added_on timestamp without time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    uid integer,
    from_ip inet NOT NULL,
    description character varying(20000) NOT NULL,
    banner_url character varying(500)
);


ALTER TABLE piperka.user_edit OWNER TO piperka;

--
-- Name: edit_history; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.edit_history (
)
INHERITS (piperka.user_edit);


ALTER TABLE piperka.edit_history OWNER TO piperka;

--
-- Name: external_entries; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.external_entries (
    cid integer NOT NULL,
    epid integer NOT NULL,
    entry text NOT NULL
);


ALTER TABLE piperka.external_entries OWNER TO piperka;

--
-- Name: external_entry; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.external_entry (
    cid integer NOT NULL,
    epid smallint NOT NULL,
    entry text NOT NULL
);


ALTER TABLE piperka.external_entry OWNER TO piperka;

--
-- Name: external_pedias; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.external_pedias (
    epid integer NOT NULL,
    name text NOT NULL,
    homepage text,
    url_base text NOT NULL
);


ALTER TABLE piperka.external_pedias OWNER TO piperka;

--
-- Name: fixed_head; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.fixed_head (
    cid integer NOT NULL
);


ALTER TABLE piperka.fixed_head OWNER TO piperka;

--
-- Name: hiatus_comics; Type: MATERIALIZED VIEW; Schema: piperka; Owner: piperka
--

CREATE MATERIALIZED VIEW piperka.hiatus_comics AS
 SELECT comics.cid,
    max(b.stmp) AS stamp
   FROM (piperka.comics
     LEFT JOIN ( SELECT a.cid,
            a.stmp
           FROM ( SELECT crawler_log.cid,
                    crawler_log.run_id,
                    min(crawler_log.stamp) AS stmp,
                    (count(
                        CASE
                            WHEN ((crawler_log.event_type = 'parse'::public.crawl_event_type) AND (((crawler_log.event -> 'parse'::text) ->> 'type'::text) = 'result'::text)) THEN 1
                            ELSE NULL::integer
                        END) - max(
                        CASE
                            WHEN (crawler_log.event_type = 'terminal'::public.crawl_event_type) THEN COALESCE(((crawler_log.event ->> 'head_crop'::text))::smallint, crawler_config.old_head_crop_count)
                            ELSE NULL::smallint
                        END)) AS new_pages
                   FROM ((piperka.crawler_log
                     JOIN piperka.crawler_config USING (cid))
                     JOIN piperka.comics comics_1 USING (cid))
                  WHERE (crawler_log.stamp > (now() - '1 year'::interval))
                  GROUP BY crawler_log.cid, crawler_log.run_id) a
          WHERE (a.new_pages > 0)) b USING (cid))
  WHERE ((comics.added_on IS NULL) OR (comics.added_on < (now() - '6 mons'::interval)))
  GROUP BY comics.cid
 HAVING ((max(b.stmp) IS NULL) OR (max(b.stmp) < (now() - '6 mons'::interval)))
  WITH NO DATA;


ALTER TABLE piperka.hiatus_comics OWNER TO piperka;

--
-- Name: hiatus_defer; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.hiatus_defer (
    cid integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE piperka.hiatus_defer OWNER TO piperka;

--
-- Name: hiatus_status; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.hiatus_status (
    hsid integer NOT NULL,
    cid integer NOT NULL,
    active boolean DEFAULT true NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    code smallint,
    uid integer NOT NULL,
    CONSTRAINT hiatus_status_check CHECK (((code IS NOT NULL) OR (NOT active)))
);

ALTER TABLE ONLY piperka.hiatus_status REPLICA IDENTITY FULL;


ALTER TABLE piperka.hiatus_status OWNER TO piperka;

--
-- Name: hiatus_status_hsid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.hiatus_status_hsid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.hiatus_status_hsid_seq OWNER TO piperka;

--
-- Name: hiatus_status_hsid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.hiatus_status_hsid_seq OWNED BY piperka.hiatus_status.hsid;


--
-- Name: hiatus_user_edit; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.hiatus_user_edit (
    uhid integer NOT NULL,
    cid integer NOT NULL,
    code smallint NOT NULL,
    reason text NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    uid integer,
    processed_by integer,
    hsid integer
);


ALTER TABLE piperka.hiatus_user_edit OWNER TO piperka;

--
-- Name: hiatus_user_edit_uhid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.hiatus_user_edit_uhid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.hiatus_user_edit_uhid_seq OWNER TO piperka;

--
-- Name: hiatus_user_edit_uhid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.hiatus_user_edit_uhid_seq OWNED BY piperka.hiatus_user_edit.uhid;


--
-- Name: inferred_schedule; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.inferred_schedule (
    cid integer NOT NULL,
    schedule smallint[],
    weekly_updates smallint,
    monthly_updates smallint
);


ALTER TABLE piperka.inferred_schedule OWNER TO piperka;

--
-- Name: max_update_ord; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.max_update_ord (
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer NOT NULL
);


ALTER TABLE piperka.max_update_ord OWNER TO piperka;

--
-- Name: merged; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.merged (
    cid integer NOT NULL,
    merged_from integer NOT NULL,
    stamp timestamp without time zone DEFAULT now()
);


ALTER TABLE piperka.merged OWNER TO piperka;

--
-- Name: nearest_infer; Type: MATERIALIZED VIEW; Schema: piperka; Owner: piperka
--

CREATE MATERIALIZED VIEW piperka.nearest_infer AS
 WITH hour_of_week AS (
         SELECT (((24 * (((date_part('dow'::text, CURRENT_DATE))::smallint + 6) % 7)))::double precision + date_part('hour'::text, CURRENT_TIMESTAMP)) AS h
        )
 SELECT comics.cid,
    (min((((((((a.s + 168))::double precision - hour_of_week.h) + (6)::double precision))::smallint)::integer % 168)) - 6) AS till_scheduled
   FROM hour_of_week,
    ((piperka.comics
     JOIN ( SELECT x.cid,
            x.s
           FROM hour_of_week hour_of_week_1,
            ( SELECT inferred_schedule.cid,
                    s.s
                   FROM piperka.inferred_schedule,
                    LATERAL unnest(inferred_schedule.schedule) s(s)
                UNION
                 SELECT inferred_schedule.cid,
                    (s.s + 168)
                   FROM piperka.inferred_schedule,
                    LATERAL unnest(inferred_schedule.schedule) s(s)
                  WHERE (s.s < 24)) x
          WHERE ((((x.s)::double precision - hour_of_week_1.h) > ('-6'::integer)::double precision) AND (((x.s)::double precision - hour_of_week_1.h) < (24)::double precision))) a USING (cid))
     JOIN piperka.crawler_config USING (cid))
  WHERE ((crawler_config.last_updated < (CURRENT_TIMESTAMP - '12:00:00'::interval)) AND (NOT (comics.cid IN ( SELECT hiatus_status.cid
           FROM piperka.hiatus_status
          WHERE hiatus_status.active))))
  GROUP BY comics.cid
  WITH NO DATA;


ALTER TABLE piperka.nearest_infer OWNER TO piperka;

--
-- Name: new_user_update; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.new_user_update (
    uid integer NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer NOT NULL,
    updated boolean DEFAULT false NOT NULL,
    notified boolean DEFAULT false NOT NULL
);


ALTER TABLE piperka.new_user_update OWNER TO piperka;

--
-- Name: ordering_form_titles; Type: VIEW; Schema: piperka; Owner: piperka
--

CREATE VIEW piperka.ordering_form_titles AS
 SELECT comics.cid,
    public.ordering_form(comics.title) AS canon
   FROM piperka.comics;


ALTER TABLE piperka.ordering_form_titles OWNER TO piperka;

--
-- Name: page_era; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.page_era (
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer NOT NULL,
    era integer DEFAULT 1 NOT NULL
);


ALTER TABLE piperka.page_era OWNER TO piperka;

--
-- Name: page_fragments; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.page_fragments (
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer NOT NULL,
    fragment text
);


ALTER TABLE piperka.page_fragments OWNER TO piperka;

--
-- Name: permitted_interest; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.permitted_interest (
    uid integer NOT NULL,
    interest integer NOT NULL,
    cid integer NOT NULL
);


ALTER TABLE piperka.permitted_interest OWNER TO piperka;

--
-- Name: pick_history; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.pick_history (
    pickid integer NOT NULL,
    stamp date NOT NULL,
    uid integer,
    cid integer[] NOT NULL
);


ALTER TABLE piperka.pick_history OWNER TO piperka;

--
-- Name: pick_history_pickid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.pick_history_pickid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.pick_history_pickid_seq OWNER TO piperka;

--
-- Name: pick_history_pickid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.pick_history_pickid_seq OWNED BY piperka.pick_history.pickid;


--
-- Name: piperka_map; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.piperka_map (
    cid integer NOT NULL
);


ALTER TABLE piperka.piperka_map OWNER TO piperka;

--
-- Name: readers_history; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.readers_history (
    cid integer NOT NULL,
    readers integer NOT NULL,
    stamp date DEFAULT now() NOT NULL
);


ALTER TABLE piperka.readers_history OWNER TO piperka;

--
-- Name: recent; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.recent (
    uid integer NOT NULL,
    cid integer NOT NULL,
    ord integer,
    used_on timestamp without time zone,
    subord integer
);

ALTER TABLE ONLY piperka.recent REPLICA IDENTITY FULL;


ALTER TABLE piperka.recent OWNER TO piperka;

--
-- Name: redirect_log; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.redirect_log (
    rhid integer NOT NULL,
    at_date timestamp without time zone DEFAULT now(),
    uid integer,
    host inet NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer NOT NULL,
    max_ord integer NOT NULL,
    offset_back boolean NOT NULL,
    url text NOT NULL
);


ALTER TABLE piperka.redirect_log OWNER TO piperka;

--
-- Name: redirect_log_rhid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.redirect_log_rhid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.redirect_log_rhid_seq OWNER TO piperka;

--
-- Name: redirect_log_rhid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.redirect_log_rhid_seq OWNED BY piperka.redirect_log.rhid;


--
-- Name: related_comics; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.related_comics (
    cid integer NOT NULL,
    rel smallint NOT NULL,
    tgt integer NOT NULL,
    score real NOT NULL
);


ALTER TABLE piperka.related_comics OWNER TO piperka;

--
-- Name: screenshot_raw; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.screenshot_raw (
    scr_id integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer DEFAULT 0 NOT NULL,
    url text NOT NULL,
    title text,
    file text NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    thumb boolean DEFAULT false NOT NULL
);


ALTER TABLE piperka.screenshot_raw OWNER TO piperka;

--
-- Name: screenshot_raw_scr_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.screenshot_raw_scr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.screenshot_raw_scr_id_seq OWNER TO piperka;

--
-- Name: screenshot_raw_scr_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.screenshot_raw_scr_id_seq OWNED BY piperka.screenshot_raw.scr_id;


--
-- Name: size_history; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.size_history (
    at_date date NOT NULL,
    comics integer,
    pages integer
);


ALTER TABLE piperka.size_history OWNER TO piperka;

--
-- Name: submit; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.submit (
    sid integer DEFAULT nextval('piperka.submit_sid_seq'::regclass) NOT NULL,
    title text NOT NULL,
    homepage text NOT NULL,
    first_page text,
    description text,
    submitted_on timestamp without time zone DEFAULT now() NOT NULL,
    uid integer,
    want_email boolean DEFAULT false NOT NULL,
    email text,
    banner_url character varying(500),
    from_ip inet NOT NULL
);


ALTER TABLE piperka.submit OWNER TO piperka;

--
-- Name: submit_banner; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.submit_banner (
    sid integer NOT NULL,
    banner bytea,
    mime text,
    bdid integer
);


ALTER TABLE piperka.submit_banner OWNER TO piperka;

--
-- Name: submit_tag; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.submit_tag (
    sid integer NOT NULL,
    tagid smallint NOT NULL
);


ALTER TABLE piperka.submit_tag OWNER TO piperka;

--
-- Name: submits; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.submits (
    id integer NOT NULL,
    name text NOT NULL,
    homepage text NOT NULL,
    tags integer NOT NULL,
    description text,
    submitted_on timestamp without time zone NOT NULL,
    from_user text NOT NULL,
    want_notify boolean DEFAULT false NOT NULL,
    email text,
    banner text
);


ALTER TABLE piperka.submits OWNER TO piperka;

--
-- Name: submits_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.submits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.submits_id_seq OWNER TO piperka;

--
-- Name: submits_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.submits_id_seq OWNED BY piperka.submits.id;


--
-- Name: subscriptions; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.subscriptions (
    uid integer NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    subord integer NOT NULL,
    last_set timestamp without time zone
);


ALTER TABLE piperka.subscriptions OWNER TO piperka;

--
-- Name: ticket; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.ticket (
    tid integer NOT NULL,
    cid integer NOT NULL,
    uid integer,
    message jsonb NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE piperka.ticket OWNER TO piperka;

--
-- Name: ticket_resolve; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.ticket_resolve (
    tid integer NOT NULL,
    msg text NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE piperka.ticket_resolve OWNER TO piperka;

--
-- Name: ticket_tid_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.ticket_tid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.ticket_tid_seq OWNER TO piperka;

--
-- Name: ticket_tid_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.ticket_tid_seq OWNED BY piperka.ticket.tid;


--
-- Name: update_alerts; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.update_alerts (
    id integer NOT NULL,
    cid integer,
    at_date timestamp without time zone,
    kind smallint NOT NULL,
    received_next text,
    resp_length integer,
    content text
);


ALTER TABLE piperka.update_alerts OWNER TO piperka;

--
-- Name: update_alerts_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.update_alerts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.update_alerts_id_seq OWNER TO piperka;

--
-- Name: update_alerts_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.update_alerts_id_seq OWNED BY piperka.update_alerts.id;


--
-- Name: updates; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.updates (
    cid integer NOT NULL,
    ord integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE piperka.updates OWNER TO piperka;

--
-- Name: updates_weekly; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.updates_weekly (
    hour smallint NOT NULL,
    update_count integer DEFAULT 0 NOT NULL
);


ALTER TABLE piperka.updates_weekly OWNER TO piperka;

--
-- Name: user_edits; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.user_edits (
    id integer NOT NULL,
    comic integer,
    added_on timestamp without time zone,
    from_user text,
    tags integer,
    description text
);


ALTER TABLE piperka.user_edits OWNER TO piperka;

--
-- Name: user_edits_id_seq; Type: SEQUENCE; Schema: piperka; Owner: piperka
--

CREATE SEQUENCE piperka.user_edits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE piperka.user_edits_id_seq OWNER TO piperka;

--
-- Name: user_edits_id_seq; Type: SEQUENCE OWNED BY; Schema: piperka; Owner: piperka
--

ALTER SEQUENCE piperka.user_edits_id_seq OWNED BY piperka.user_edits.id;


--
-- Name: user_site_activity; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.user_site_activity (
    uid integer NOT NULL,
    last_login timestamp without time zone,
    last_active timestamp without time zone,
    seen_comics_before timestamp without time zone DEFAULT now() NOT NULL,
    seen_comics_last timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE piperka.user_site_activity OWNER TO piperka;

--
-- Name: world_map; Type: TABLE; Schema: piperka; Owner: piperka
--

CREATE TABLE piperka.world_map (
    stamp date NOT NULL,
    cid integer NOT NULL,
    radius real NOT NULL,
    weight smallint NOT NULL,
    x double precision NOT NULL,
    y double precision NOT NULL
);


ALTER TABLE piperka.world_map OWNER TO piperka;

--
-- Name: world_map_json; Type: VIEW; Schema: piperka; Owner: piperka
--

CREATE VIEW piperka.world_map_json AS
 SELECT world_map.stamp,
    ( SELECT jsonb_object_agg(world_map.cid, jsonb_build_object('radius', world_map.radius, 'weight', world_map.weight, 'x', world_map.x, 'y', world_map.y)) AS jsonb_object_agg) AS json
   FROM piperka.world_map
  GROUP BY world_map.stamp
  ORDER BY world_map.stamp DESC
 LIMIT 5;


ALTER TABLE piperka.world_map_json OWNER TO piperka;

--
-- Name: appuser_token; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.appuser_token (
    token uuid NOT NULL
);


ALTER TABLE public.appuser_token OWNER TO piperka;

--
-- Name: cookie_jar; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.cookie_jar (
    cid integer NOT NULL,
    cookies text NOT NULL
);


ALTER TABLE public.cookie_jar OWNER TO piperka;

--
-- Name: crawl_error_type; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.crawl_error_type (
    errtype integer NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.crawl_error_type OWNER TO piperka;

--
-- Name: external_entry_submit; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.external_entry_submit (
    sid integer NOT NULL,
    epid integer NOT NULL,
    entry text NOT NULL
);


ALTER TABLE public.external_entry_submit OWNER TO piperka;

--
-- Name: extra_whitelist; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.extra_whitelist (
    extra text NOT NULL
);


ALTER TABLE public.extra_whitelist OWNER TO piperka;

--
-- Name: follow_permission; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.follow_permission (
    uid integer NOT NULL,
    followee integer NOT NULL
);


ALTER TABLE public.follow_permission OWNER TO piperka;

--
-- Name: follower; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.follower (
    uid integer NOT NULL,
    interest integer NOT NULL
);


ALTER TABLE public.follower OWNER TO piperka;

--
-- Name: lmid_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.lmid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lmid_seq OWNER TO piperka;

--
-- Name: login_method; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.login_method (
    lmid integer DEFAULT nextval('public.lmid_seq'::regclass) NOT NULL,
    uid integer,
    stamp timestamp without time zone DEFAULT now()
);


ALTER TABLE public.login_method OWNER TO piperka;

--
-- Name: login_method_oauth2; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.login_method_oauth2 (
    opid integer NOT NULL,
    identification text NOT NULL
)
INHERITS (public.login_method);


ALTER TABLE public.login_method_oauth2 OWNER TO piperka;

--
-- Name: login_method_passwd; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.login_method_passwd (
    hash character(60) NOT NULL
)
INHERITS (public.login_method);

ALTER TABLE ONLY public.login_method_passwd REPLICA IDENTITY FULL;


ALTER TABLE public.login_method_passwd OWNER TO piperka;

--
-- Name: moderator; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.moderator (
    uid integer NOT NULL,
    last_moderate timestamp without time zone,
    level integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.moderator OWNER TO piperka;

--
-- Name: oauth2_account; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.oauth2_account (
    accountid integer NOT NULL,
    opid integer NOT NULL,
    identification text NOT NULL,
    uid integer NOT NULL
);


ALTER TABLE public.oauth2_account OWNER TO piperka;

--
-- Name: oauth2_account_accountid_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.oauth2_account_accountid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_account_accountid_seq OWNER TO piperka;

--
-- Name: oauth2_account_accountid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piperka
--

ALTER SEQUENCE public.oauth2_account_accountid_seq OWNED BY public.oauth2_account.accountid;


--
-- Name: oauth2_provider; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.oauth2_provider (
    opid integer NOT NULL,
    client_id text NOT NULL,
    client_secret text NOT NULL,
    discovery text,
    name text NOT NULL,
    display_name text NOT NULL
);


ALTER TABLE public.oauth2_provider OWNER TO piperka;

--
-- Name: oauth2_provider_opid_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.oauth2_provider_opid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_opid_seq OWNER TO piperka;

--
-- Name: oauth2_provider_opid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piperka
--

ALTER SEQUENCE public.oauth2_provider_opid_seq OWNED BY public.oauth2_provider.opid;


--
-- Name: oauth2_state; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.oauth2_state (
    osessid integer NOT NULL,
    opid integer,
    state uuid NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    action bytea,
    error text,
    identification text
);


ALTER TABLE public.oauth2_state OWNER TO piperka;

--
-- Name: oauth2_state_osessid_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.oauth2_state_osessid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_state_osessid_seq OWNER TO piperka;

--
-- Name: oauth2_state_osessid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piperka
--

ALTER SEQUENCE public.oauth2_state_osessid_seq OWNED BY public.oauth2_state.osessid;


--
-- Name: p_session; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.p_session (
    ses uuid NOT NULL,
    uid integer NOT NULL,
    begun timestamp without time zone DEFAULT now(),
    last_active timestamp without time zone DEFAULT now(),
    token_for uuid
);


ALTER TABLE public.p_session OWNER TO piperka;

--
-- Name: parsers; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.parsers (
    id integer NOT NULL,
    text_hook text,
    start_hook text,
    auto_try boolean DEFAULT true
);


ALTER TABLE public.parsers OWNER TO piperka;

--
-- Name: parsers_id_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.parsers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parsers_id_seq OWNER TO piperka;

--
-- Name: parsers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piperka
--

ALTER SEQUENCE public.parsers_id_seq OWNED BY public.parsers.id;


--
-- Name: pwdgen_hash; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.pwdgen_hash (
    uid integer NOT NULL,
    hash character(32),
    stamp timestamp without time zone DEFAULT now()
);


ALTER TABLE public.pwdgen_hash OWNER TO piperka;

--
-- Name: user_delete_request; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.user_delete_request (
    uid integer NOT NULL,
    requested_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.user_delete_request OWNER TO piperka;

--
-- Name: user_push_notification; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.user_push_notification (
    unid integer NOT NULL,
    uid integer NOT NULL,
    endpoint text NOT NULL,
    auth text NOT NULL,
    p256dh text NOT NULL,
    agent text,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_success timestamp without time zone
);


ALTER TABLE public.user_push_notification OWNER TO piperka;

--
-- Name: user_push_notification_unid_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.user_push_notification_unid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_push_notification_unid_seq OWNER TO piperka;

--
-- Name: user_push_notification_unid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piperka
--

ALTER SEQUENCE public.user_push_notification_unid_seq OWNED BY public.user_push_notification.unid;


--
-- Name: users; Type: TABLE; Schema: public; Owner: piperka
--

CREATE TABLE public.users (
    uid integer NOT NULL,
    name text NOT NULL,
    email text,
    passwd character(32),
    new_windows boolean DEFAULT false NOT NULL,
    last_login timestamp without time zone,
    last_active timestamp without time zone,
    seen_comics_before timestamp without time zone DEFAULT now() NOT NULL,
    display_rows smallint DEFAULT 40,
    display_columns smallint DEFAULT 3,
    public_profile boolean DEFAULT false,
    created_on timestamp without time zone DEFAULT now(),
    hold_bookmark boolean DEFAULT false NOT NULL,
    writeup text,
    bookmark_sort smallint DEFAULT 0,
    offset_bookmark_by_one boolean DEFAULT false NOT NULL,
    countme boolean DEFAULT true NOT NULL,
    privacy integer DEFAULT 2 NOT NULL,
    initial_lmid integer,
    seen_comics_last timestamp without time zone DEFAULT now() NOT NULL,
    bookmarklet_token character(10) DEFAULT public.random_string(10) NOT NULL,
    auid integer,
    last_update_disclaimer boolean DEFAULT false NOT NULL,
    countme_override boolean,
    sticky_show_hidden boolean DEFAULT false NOT NULL,
    deleted_at timestamp without time zone
);


ALTER TABLE public.users OWNER TO piperka;

--
-- Name: users_uid_seq; Type: SEQUENCE; Schema: public; Owner: piperka
--

CREATE SEQUENCE public.users_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_uid_seq OWNER TO piperka;

--
-- Name: users_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: piperka
--

ALTER SEQUENCE public.users_uid_seq OWNED BY public.users.uid;


--
-- Name: audit_log audit_id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.audit_log ALTER COLUMN audit_id SET DEFAULT nextval('piperka.audit_log_audit_id_seq'::regclass);


--
-- Name: author auid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.author ALTER COLUMN auid SET DEFAULT nextval('piperka.author_auid_seq'::regclass);


--
-- Name: banner_data bdid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.banner_data ALTER COLUMN bdid SET DEFAULT nextval('piperka.banner_data_bdid_seq'::regclass);


--
-- Name: banner_submits id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.banner_submits ALTER COLUMN id SET DEFAULT nextval('piperka.banner_submits_id_seq'::regclass);


--
-- Name: bookmarking_log id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.bookmarking_log ALTER COLUMN id SET DEFAULT nextval('piperka.bookmarking_log_id_seq'::regclass);


--
-- Name: comic_alias caid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comic_alias ALTER COLUMN caid SET DEFAULT nextval('piperka.comic_alias_caid_seq'::regclass);


--
-- Name: comic_title_history ctitid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comic_title_history ALTER COLUMN ctitid SET DEFAULT nextval('piperka.comic_title_history_ctitid_seq'::regclass);


--
-- Name: comics cid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comics ALTER COLUMN cid SET DEFAULT nextval('piperka.comics_cid_seq'::regclass);


--
-- Name: crawl_error errid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.crawl_error ALTER COLUMN errid SET DEFAULT nextval('piperka.crawl_error_errid_seq'::regclass);


--
-- Name: crawler_log plog_id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.crawler_log ALTER COLUMN plog_id SET DEFAULT nextval('piperka.crawler_log_plog_id_seq'::regclass);


--
-- Name: edit_history sid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.edit_history ALTER COLUMN sid SET DEFAULT nextval('piperka.submit_sid_seq'::regclass);


--
-- Name: edit_history added_on; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.edit_history ALTER COLUMN added_on SET DEFAULT timezone('utc'::text, now());


--
-- Name: hiatus_status hsid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.hiatus_status ALTER COLUMN hsid SET DEFAULT nextval('piperka.hiatus_status_hsid_seq'::regclass);


--
-- Name: hiatus_user_edit uhid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.hiatus_user_edit ALTER COLUMN uhid SET DEFAULT nextval('piperka.hiatus_user_edit_uhid_seq'::regclass);


--
-- Name: pick_history pickid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.pick_history ALTER COLUMN pickid SET DEFAULT nextval('piperka.pick_history_pickid_seq'::regclass);


--
-- Name: redirect_log rhid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.redirect_log ALTER COLUMN rhid SET DEFAULT nextval('piperka.redirect_log_rhid_seq'::regclass);


--
-- Name: screenshot_raw scr_id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.screenshot_raw ALTER COLUMN scr_id SET DEFAULT nextval('piperka.screenshot_raw_scr_id_seq'::regclass);


--
-- Name: submits id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.submits ALTER COLUMN id SET DEFAULT nextval('piperka.submits_id_seq'::regclass);


--
-- Name: ticket tid; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.ticket ALTER COLUMN tid SET DEFAULT nextval('piperka.ticket_tid_seq'::regclass);


--
-- Name: update_alerts id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.update_alerts ALTER COLUMN id SET DEFAULT nextval('piperka.update_alerts_id_seq'::regclass);


--
-- Name: user_edits id; Type: DEFAULT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.user_edits ALTER COLUMN id SET DEFAULT nextval('piperka.user_edits_id_seq'::regclass);


--
-- Name: login_method_oauth2 lmid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_oauth2 ALTER COLUMN lmid SET DEFAULT nextval('public.lmid_seq'::regclass);


--
-- Name: login_method_oauth2 stamp; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_oauth2 ALTER COLUMN stamp SET DEFAULT now();


--
-- Name: login_method_passwd lmid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_passwd ALTER COLUMN lmid SET DEFAULT nextval('public.lmid_seq'::regclass);


--
-- Name: login_method_passwd stamp; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_passwd ALTER COLUMN stamp SET DEFAULT now();


--
-- Name: oauth2_account accountid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_account ALTER COLUMN accountid SET DEFAULT nextval('public.oauth2_account_accountid_seq'::regclass);


--
-- Name: oauth2_provider opid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_provider ALTER COLUMN opid SET DEFAULT nextval('public.oauth2_provider_opid_seq'::regclass);


--
-- Name: oauth2_state osessid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_state ALTER COLUMN osessid SET DEFAULT nextval('public.oauth2_state_osessid_seq'::regclass);


--
-- Name: parsers id; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.parsers ALTER COLUMN id SET DEFAULT nextval('public.parsers_id_seq'::regclass);


--
-- Name: user_push_notification unid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.user_push_notification ALTER COLUMN unid SET DEFAULT nextval('public.user_push_notification_unid_seq'::regclass);


--
-- Name: users uid; Type: DEFAULT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.users ALTER COLUMN uid SET DEFAULT nextval('public.users_uid_seq'::regclass);


--
-- Name: audit_log audit_log_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.audit_log
    ADD CONSTRAINT audit_log_pkey PRIMARY KEY (audit_id);


--
-- Name: author_comics author_comics_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.author_comics
    ADD CONSTRAINT author_comics_pkey PRIMARY KEY (auid, cid);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (auid);


--
-- Name: banner_data banner_data_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.banner_data
    ADD CONSTRAINT banner_data_pkey PRIMARY KEY (bdid);


--
-- Name: banner_submits banner_submits_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.banner_submits
    ADD CONSTRAINT banner_submits_pkey PRIMARY KEY (id);


--
-- Name: banners banners_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.banners
    ADD CONSTRAINT banners_pkey PRIMARY KEY (cid);


--
-- Name: bookmarking_log bookmarking_log_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.bookmarking_log
    ADD CONSTRAINT bookmarking_log_pkey PRIMARY KEY (id);


--
-- Name: claim_token claim_token_uid_cid_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.claim_token
    ADD CONSTRAINT claim_token_uid_cid_key UNIQUE (uid, cid);

ALTER TABLE ONLY piperka.claim_token REPLICA IDENTITY USING INDEX claim_token_uid_cid_key;


--
-- Name: comic_alias comic_alias_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comic_alias
    ADD CONSTRAINT comic_alias_pkey PRIMARY KEY (caid);


--
-- Name: comic_remain_frag_cache comic_remain_frag_cache_uid_cid_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comic_remain_frag_cache
    ADD CONSTRAINT comic_remain_frag_cache_uid_cid_key UNIQUE (uid, cid);

ALTER TABLE ONLY piperka.comic_remain_frag_cache REPLICA IDENTITY USING INDEX comic_remain_frag_cache_uid_cid_key;


--
-- Name: comic_title_history comic_title_history_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comic_title_history
    ADD CONSTRAINT comic_title_history_pkey PRIMARY KEY (ctitid);


--
-- Name: comics comics_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comics
    ADD CONSTRAINT comics_pkey PRIMARY KEY (cid);


--
-- Name: crawl_error crawl_error_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.crawl_error
    ADD CONSTRAINT crawl_error_pkey PRIMARY KEY (errid);


--
-- Name: crawler_config crawler_config_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.crawler_config
    ADD CONSTRAINT crawler_config_pkey PRIMARY KEY (cid);


--
-- Name: crawler_log crawler_log_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.crawler_log
    ADD CONSTRAINT crawler_log_pkey PRIMARY KEY (plog_id);


--
-- Name: disinterest disinterest_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.disinterest
    ADD CONSTRAINT disinterest_pkey PRIMARY KEY (uid, cid);


--
-- Name: edit_history edit_history_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.edit_history
    ADD CONSTRAINT edit_history_pkey PRIMARY KEY (sid);


--
-- Name: external_entries external_entries_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.external_entries
    ADD CONSTRAINT external_entries_pkey PRIMARY KEY (cid, epid);


--
-- Name: external_entry external_entry_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.external_entry
    ADD CONSTRAINT external_entry_pkey PRIMARY KEY (cid, epid);


--
-- Name: external_pedias external_pedias_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.external_pedias
    ADD CONSTRAINT external_pedias_pkey PRIMARY KEY (epid);


--
-- Name: fixed_head fixed_head_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.fixed_head
    ADD CONSTRAINT fixed_head_pkey PRIMARY KEY (cid);


--
-- Name: graveyard graveyard_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.graveyard
    ADD CONSTRAINT graveyard_pkey PRIMARY KEY (cid);


--
-- Name: hiatus_defer hiatus_defer_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.hiatus_defer
    ADD CONSTRAINT hiatus_defer_pkey PRIMARY KEY (cid);


--
-- Name: hiatus_status hiatus_status_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.hiatus_status
    ADD CONSTRAINT hiatus_status_pkey PRIMARY KEY (hsid);


--
-- Name: hiatus_user_edit hiatus_user_edit_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.hiatus_user_edit
    ADD CONSTRAINT hiatus_user_edit_pkey PRIMARY KEY (uhid);


--
-- Name: inferred_schedule inferred_schedule_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.inferred_schedule
    ADD CONSTRAINT inferred_schedule_pkey PRIMARY KEY (cid);


--
-- Name: max_update_ord max_update_ord_cid_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.max_update_ord
    ADD CONSTRAINT max_update_ord_cid_key UNIQUE (cid);


--
-- Name: max_update_ord max_update_ord_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.max_update_ord
    ADD CONSTRAINT max_update_ord_pkey PRIMARY KEY (cid);


--
-- Name: merged merged_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.merged
    ADD CONSTRAINT merged_pkey PRIMARY KEY (merged_from);


--
-- Name: page_era page_era_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.page_era
    ADD CONSTRAINT page_era_pkey PRIMARY KEY (cid, ord, subord);


--
-- Name: page_fragments page_fragments_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.page_fragments
    ADD CONSTRAINT page_fragments_pkey PRIMARY KEY (cid, ord, subord);


--
-- Name: permitted_interest permitted_interest_uid_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.permitted_interest
    ADD CONSTRAINT permitted_interest_uid_key UNIQUE (uid, interest, cid);

ALTER TABLE ONLY piperka.permitted_interest REPLICA IDENTITY USING INDEX permitted_interest_uid_key;


--
-- Name: pick_history pick_history_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.pick_history
    ADD CONSTRAINT pick_history_pkey PRIMARY KEY (pickid);


--
-- Name: piperka_map piperka_map_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.piperka_map
    ADD CONSTRAINT piperka_map_pkey PRIMARY KEY (cid);


--
-- Name: redirect_log redirect_log_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.redirect_log
    ADD CONSTRAINT redirect_log_pkey PRIMARY KEY (rhid);


--
-- Name: related_comics related_comics_cid_rel_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.related_comics
    ADD CONSTRAINT related_comics_cid_rel_key UNIQUE (cid, rel);


--
-- Name: related_comics related_comics_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.related_comics
    ADD CONSTRAINT related_comics_pkey PRIMARY KEY (cid, rel);


--
-- Name: screenshot_raw screenshot_raw_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.screenshot_raw
    ADD CONSTRAINT screenshot_raw_pkey PRIMARY KEY (scr_id);


--
-- Name: size_history size_history_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.size_history
    ADD CONSTRAINT size_history_pkey PRIMARY KEY (at_date);


--
-- Name: submit_banner submit_banner_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.submit_banner
    ADD CONSTRAINT submit_banner_pkey PRIMARY KEY (sid);


--
-- Name: submit submit_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.submit
    ADD CONSTRAINT submit_pkey PRIMARY KEY (sid);


--
-- Name: submit_tag submit_tag_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.submit_tag
    ADD CONSTRAINT submit_tag_pkey PRIMARY KEY (sid, tagid);


--
-- Name: submits submits_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.submits
    ADD CONSTRAINT submits_pkey PRIMARY KEY (id);


--
-- Name: subscriptions subscriptions_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (uid, cid);


--
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (tid);


--
-- Name: ticket_resolve ticket_resolve_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.ticket_resolve
    ADD CONSTRAINT ticket_resolve_pkey PRIMARY KEY (tid);


--
-- Name: update_alerts update_alerts_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.update_alerts
    ADD CONSTRAINT update_alerts_pkey PRIMARY KEY (id);


--
-- Name: updates updates_cid_ord_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.updates
    ADD CONSTRAINT updates_cid_ord_key UNIQUE (cid, ord);


--
-- Name: updates updates_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.updates
    ADD CONSTRAINT updates_pkey PRIMARY KEY (cid, ord);


--
-- Name: updates_weekly updates_weekly_hour_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.updates_weekly
    ADD CONSTRAINT updates_weekly_hour_key UNIQUE (hour);

ALTER TABLE ONLY piperka.updates_weekly REPLICA IDENTITY USING INDEX updates_weekly_hour_key;


--
-- Name: user_edit user_edit_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.user_edit
    ADD CONSTRAINT user_edit_pkey PRIMARY KEY (sid);


--
-- Name: user_site_activity user_site_activity_pkey; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.user_site_activity
    ADD CONSTRAINT user_site_activity_pkey PRIMARY KEY (uid);


--
-- Name: world_map world_map_stamp_cid_key; Type: CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.world_map
    ADD CONSTRAINT world_map_stamp_cid_key UNIQUE (stamp, cid);


--
-- Name: alphabets alphabets_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.alphabets
    ADD CONSTRAINT alphabets_pkey PRIMARY KEY (letter);


--
-- Name: appuser_token appuser_token_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.appuser_token
    ADD CONSTRAINT appuser_token_pkey PRIMARY KEY (token);


--
-- Name: cookie_jar cookie_jar_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.cookie_jar
    ADD CONSTRAINT cookie_jar_pkey PRIMARY KEY (cid);


--
-- Name: crawl_error_type crawl_error_type_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.crawl_error_type
    ADD CONSTRAINT crawl_error_type_pkey PRIMARY KEY (errtype);


--
-- Name: external_entry_submit external_entry_submit_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.external_entry_submit
    ADD CONSTRAINT external_entry_submit_pkey PRIMARY KEY (sid, epid);


--
-- Name: follow_permission follow_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.follow_permission
    ADD CONSTRAINT follow_permission_pkey PRIMARY KEY (uid, followee);


--
-- Name: follow_permission follow_permission_uid_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.follow_permission
    ADD CONSTRAINT follow_permission_uid_key UNIQUE (uid, followee);


--
-- Name: follower follower_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.follower
    ADD CONSTRAINT follower_pkey PRIMARY KEY (uid, interest);


--
-- Name: follower follower_uid_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.follower
    ADD CONSTRAINT follower_uid_key UNIQUE (uid, interest);


--
-- Name: login_method_oauth2 login_method_oauth2_opid_identification_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_oauth2
    ADD CONSTRAINT login_method_oauth2_opid_identification_key UNIQUE (opid, identification);

ALTER TABLE ONLY public.login_method_oauth2 REPLICA IDENTITY USING INDEX login_method_oauth2_opid_identification_key;


--
-- Name: login_method_oauth2 login_method_oauth2_uid_opid_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_oauth2
    ADD CONSTRAINT login_method_oauth2_uid_opid_key UNIQUE (uid, opid);


--
-- Name: login_method_passwd login_method_passwd_uid_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method_passwd
    ADD CONSTRAINT login_method_passwd_uid_key UNIQUE (uid);


--
-- Name: login_method login_method_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.login_method
    ADD CONSTRAINT login_method_pkey PRIMARY KEY (lmid);


--
-- Name: moderator moderator_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.moderator
    ADD CONSTRAINT moderator_pkey PRIMARY KEY (uid);


--
-- Name: oauth2_account oauth2_account_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_account
    ADD CONSTRAINT oauth2_account_pkey PRIMARY KEY (accountid);


--
-- Name: oauth2_provider oauth2_provider_name_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_provider
    ADD CONSTRAINT oauth2_provider_name_key UNIQUE (name);


--
-- Name: oauth2_provider oauth2_provider_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_provider
    ADD CONSTRAINT oauth2_provider_pkey PRIMARY KEY (opid);


--
-- Name: oauth2_state oauth2_state_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_state
    ADD CONSTRAINT oauth2_state_pkey PRIMARY KEY (osessid);


--
-- Name: oauth2_state oauth2_state_state_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_state
    ADD CONSTRAINT oauth2_state_state_key UNIQUE (state);


--
-- Name: p_session p_session_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.p_session
    ADD CONSTRAINT p_session_pkey PRIMARY KEY (ses);


--
-- Name: parsers parsers_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.parsers
    ADD CONSTRAINT parsers_pkey PRIMARY KEY (id);


--
-- Name: pwdgen_hash pwdgen_hash_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.pwdgen_hash
    ADD CONSTRAINT pwdgen_hash_pkey PRIMARY KEY (uid);


--
-- Name: pwdgen_hash pwdgen_hash_uid_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.pwdgen_hash
    ADD CONSTRAINT pwdgen_hash_uid_key UNIQUE (uid);


--
-- Name: user_delete_request user_delete_request_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.user_delete_request
    ADD CONSTRAINT user_delete_request_pkey PRIMARY KEY (uid);


--
-- Name: user_push_notification user_push_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.user_push_notification
    ADD CONSTRAINT user_push_notification_pkey PRIMARY KEY (unid);


--
-- Name: users users_name_key; Type: CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_name_key UNIQUE (name);


--
-- Name: added_on_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX added_on_idx ON piperka.comics USING btree (added_on);


--
-- Name: audit_log_cid_stamp_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX audit_log_cid_stamp_idx ON piperka.audit_log USING btree (cid, stamp);


--
-- Name: bookmark_form_comics_index; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX bookmark_form_comics_index ON piperka.comics USING btree (public.canonical_bookmark_form(url_base) text_pattern_ops);


--
-- Name: bookmark_form_fixed_head_index; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX bookmark_form_fixed_head_index ON piperka.comics USING btree (public.canonical_bookmark_form(fixed_head) text_pattern_ops);


--
-- Name: bookmark_form_homepage_index; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX bookmark_form_homepage_index ON piperka.comics USING btree (public.canonical_bookmark_form(homepage) text_pattern_ops);


--
-- Name: comic_remain_frag_cache_cid_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX comic_remain_frag_cache_cid_idx ON piperka.comic_remain_frag_cache USING btree (cid);


--
-- Name: comic_tag_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX comic_tag_idx ON piperka.comic_tag USING btree (cid, tagid);

ALTER TABLE ONLY piperka.comic_tag REPLICA IDENTITY USING INDEX comic_tag_idx;


--
-- Name: crawer_log_event_type_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX crawer_log_event_type_idx ON piperka.crawler_log USING btree (event_type, (((event -> 'end'::text) ->> 'type'::text)));


--
-- Name: crawl_error_by_cid; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX crawl_error_by_cid ON piperka.crawl_error USING btree (cid, stamp);


--
-- Name: crawl_error_stamp_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX crawl_error_stamp_idx ON piperka.crawl_error USING btree (stamp DESC, cid) WHERE (stamp > ('2013-06-01 07:54:43.638984'::timestamp without time zone - '30 days'::interval));


--
-- Name: crawler_log_cid_stamp; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX crawler_log_cid_stamp ON piperka.crawler_log USING btree (cid, stamp);


--
-- Name: crawler_log_run_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX crawler_log_run_idx ON piperka.crawler_log USING btree (run_id);


--
-- Name: crawler_log_stamp_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX crawler_log_stamp_idx ON piperka.crawler_log USING btree (stamp);


--
-- Name: disinterest_uid_cid_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX disinterest_uid_cid_idx ON piperka.disinterest USING btree (uid, cid);


--
-- Name: external_entries_cid; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX external_entries_cid ON piperka.external_entries USING btree (cid);


--
-- Name: graveyard_added_on_key; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX graveyard_added_on_key ON piperka.graveyard USING btree (added_on);


--
-- Name: graveyard_cid_key; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX graveyard_cid_key ON piperka.graveyard USING btree (cid);


--
-- Name: hiatus_status_active_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX hiatus_status_active_idx ON piperka.hiatus_status USING btree (cid) WHERE (active = true);


--
-- Name: hiatus_user_edit_processed_by_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX hiatus_user_edit_processed_by_idx ON piperka.hiatus_user_edit USING btree (processed_by) WHERE (processed_by IS NULL);


--
-- Name: nearest_infer_cid_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX nearest_infer_cid_idx ON piperka.nearest_infer USING btree (cid);


--
-- Name: new_user_update_uid_cid_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX new_user_update_uid_cid_idx ON piperka.new_user_update USING btree (uid, cid);

ALTER TABLE ONLY piperka.new_user_update REPLICA IDENTITY USING INDEX new_user_update_uid_cid_idx;


--
-- Name: nsfw_comics_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX nsfw_comics_idx ON piperka.comic_tag USING btree (cid) WHERE (tagid = ANY (ARRAY[59, 60, 12, 13, 1]));


--
-- Name: nsfw_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX nsfw_idx ON piperka.comic_tag USING btree (cid) WHERE (tagid = 13);


--
-- Name: page_fragments_cidord; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX page_fragments_cidord ON piperka.page_fragments USING btree (cid, ord);


--
-- Name: readers_history_idx; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX readers_history_idx ON piperka.readers_history USING btree (cid, stamp);

ALTER TABLE ONLY piperka.readers_history REPLICA IDENTITY USING INDEX readers_history_idx;


--
-- Name: recent_usercomic; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX recent_usercomic ON piperka.recent USING btree (uid, cid);


--
-- Name: screenshot_thumb_constraint; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE UNIQUE INDEX screenshot_thumb_constraint ON piperka.screenshot_raw USING btree (cid, ord, subord) WHERE thumb;


--
-- Name: subs_cid; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX subs_cid ON piperka.subscriptions USING btree (cid);


--
-- Name: updates_comicname; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX updates_comicname ON piperka.updates USING btree (cid, name);


--
-- Name: updates_comicord; Type: INDEX; Schema: piperka; Owner: piperka
--

CREATE INDEX updates_comicord ON piperka.updates USING btree (cid, ord);


--
-- Name: oauth2_id_idx; Type: INDEX; Schema: public; Owner: piperka
--

CREATE INDEX oauth2_id_idx ON public.oauth2_account USING btree (opid, identification);


--
-- Name: p_session_token_for_idx; Type: INDEX; Schema: public; Owner: piperka
--

CREATE INDEX p_session_token_for_idx ON public.p_session USING btree (token_for) WHERE (token_for IS NOT NULL);


--
-- Name: parsers_id_key; Type: INDEX; Schema: public; Owner: piperka
--

CREATE INDEX parsers_id_key ON public.parsers USING btree (id);


--
-- Name: unique_uid; Type: INDEX; Schema: public; Owner: piperka
--

CREATE UNIQUE INDEX unique_uid ON public.users USING btree (uid);


--
-- Name: user_push_notification_uid_endpoint_idx; Type: INDEX; Schema: public; Owner: piperka
--

CREATE UNIQUE INDEX user_push_notification_uid_endpoint_idx ON public.user_push_notification USING btree (uid, endpoint);


--
-- Name: users_auid_idx; Type: INDEX; Schema: public; Owner: piperka
--

CREATE UNIQUE INDEX users_auid_idx ON public.users USING btree (auid);


--
-- Name: users_name_key_lower; Type: INDEX; Schema: public; Owner: piperka
--

CREATE INDEX users_name_key_lower ON public.users USING btree (lower(name));


--
-- Name: users_uid_key; Type: INDEX; Schema: public; Owner: piperka
--

CREATE UNIQUE INDEX users_uid_key ON public.users USING btree (uid);

ALTER TABLE ONLY public.users REPLICA IDENTITY USING INDEX users_uid_key;


--
-- Name: fixed_head a1_fixed_head_max_update_ord; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER a1_fixed_head_max_update_ord AFTER INSERT ON piperka.fixed_head REFERENCING NEW TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.update_max_update_ord();


--
-- Name: page_fragments a1_fragment_max_update_ord; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER a1_fragment_max_update_ord AFTER INSERT ON piperka.page_fragments REFERENCING NEW TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.update_max_update_ord();


--
-- Name: updates a1_update_max_update_ord; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER a1_update_max_update_ord AFTER INSERT ON piperka.updates REFERENCING NEW TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.update_max_update_ord();


--
-- Name: fixed_head a2_fixed_head_max_update_ord; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER a2_fixed_head_max_update_ord AFTER DELETE ON piperka.fixed_head REFERENCING OLD TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.update_max_update_ord();


--
-- Name: page_fragments a2_fragment_max_update_ord; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER a2_fragment_max_update_ord AFTER DELETE ON piperka.page_fragments REFERENCING OLD TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.update_max_update_ord();


--
-- Name: updates a2_update_max_update_ord; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER a2_update_max_update_ord AFTER DELETE ON piperka.updates REFERENCING OLD TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.update_max_update_ord();


--
-- Name: comics comic_era; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER comic_era AFTER DELETE OR UPDATE ON piperka.comics FOR EACH ROW EXECUTE FUNCTION public.comic_era_update();


--
-- Name: submit drop_sid_friends; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER drop_sid_friends AFTER DELETE ON piperka.submit FOR EACH ROW EXECUTE FUNCTION public.drop_sid_friends();


--
-- Name: user_edit drop_sid_friends; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER drop_sid_friends AFTER DELETE ON piperka.user_edit FOR EACH ROW EXECUTE FUNCTION public.drop_sid_friends();


--
-- Name: page_fragments fragment_era; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER fragment_era AFTER DELETE OR UPDATE ON piperka.page_fragments FOR EACH ROW EXECUTE FUNCTION public.fragment_era_update();


--
-- Name: hiatus_status hiatus_other_inactive; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER hiatus_other_inactive BEFORE INSERT ON piperka.hiatus_status FOR EACH ROW EXECUTE FUNCTION public.hiatus_other_inactive();


--
-- Name: updates page_era; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER page_era AFTER DELETE OR UPDATE ON piperka.updates FOR EACH ROW EXECUTE FUNCTION public.page_era_update();


--
-- Name: page_fragments page_fragment_remain_frag_cache_clean; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER page_fragment_remain_frag_cache_clean AFTER INSERT OR DELETE ON piperka.page_fragments FOR EACH ROW EXECUTE FUNCTION public.clear_user_remain_frag_cache();


--
-- Name: comics piperka_site_tags; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER piperka_site_tags AFTER INSERT OR UPDATE ON piperka.comics FOR EACH ROW EXECUTE FUNCTION piperka.site_tag();


--
-- Name: subscriptions subscription_new_user_update; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER subscription_new_user_update AFTER INSERT OR DELETE OR UPDATE ON piperka.subscriptions FOR EACH ROW EXECUTE FUNCTION public.subscription_new_user_update();


--
-- Name: subscriptions subscription_remain_frag_cache_clean; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER subscription_remain_frag_cache_clean AFTER INSERT OR DELETE OR UPDATE ON piperka.subscriptions FOR EACH ROW EXECUTE FUNCTION public.clear_user_remain_frag_cache();


--
-- Name: subscriptions subscription_update_last_set; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER subscription_update_last_set BEFORE INSERT OR UPDATE ON piperka.subscriptions FOR EACH ROW EXECUTE FUNCTION public.update_last_set();


--
-- Name: subscriptions subscriptions_permitted_interest; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER subscriptions_permitted_interest AFTER INSERT OR UPDATE ON piperka.subscriptions FOR EACH ROW EXECUTE FUNCTION public.subscriptions_permitted_interest();


--
-- Name: subscriptions update_readers; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER update_readers AFTER INSERT OR DELETE ON piperka.subscriptions FOR EACH ROW EXECUTE FUNCTION public.update_readers();


--
-- Name: updates updates_new_user_update; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER updates_new_user_update AFTER INSERT ON piperka.updates REFERENCING NEW TABLE AS mod_updates FOR EACH STATEMENT EXECUTE FUNCTION public.updates_new_user_update();


--
-- Name: updates updates_remain_frag_cache_clean; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER updates_remain_frag_cache_clean AFTER INSERT OR DELETE ON piperka.updates FOR EACH ROW EXECUTE FUNCTION public.clear_user_remain_frag_cache();


--
-- Name: user_site_activity user_active_remain_frag_cache; Type: TRIGGER; Schema: piperka; Owner: piperka
--

CREATE TRIGGER user_active_remain_frag_cache AFTER UPDATE ON piperka.user_site_activity FOR EACH ROW WHEN (((new.last_active <> old.last_active) OR (new.last_login <> old.last_login))) EXECUTE FUNCTION public.refresh_user_remain_frag();


--
-- Name: p_session delete_csrf_tokens; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER delete_csrf_tokens AFTER DELETE ON public.p_session FOR EACH ROW EXECUTE FUNCTION public.delete_csrf_tokens();


--
-- Name: follow_permission follow_permission_permitted_interest; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER follow_permission_permitted_interest AFTER INSERT OR DELETE ON public.follow_permission FOR EACH ROW EXECUTE FUNCTION public.follow_permission_permitted_interest();


--
-- Name: follower follower_permitted_interest; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER follower_permitted_interest AFTER INSERT OR DELETE ON public.follower FOR EACH ROW EXECUTE FUNCTION public.follower_permitted_interest();


--
-- Name: users initial_user_login; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER initial_user_login AFTER INSERT ON public.users FOR EACH ROW EXECUTE FUNCTION public.initial_user_login();


--
-- Name: user_push_notification limit_notification_endpoints; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER limit_notification_endpoints AFTER INSERT ON public.user_push_notification FOR EACH ROW EXECUTE FUNCTION public.limit_notification_endpoints();


--
-- Name: login_method_oauth2 must_have_login2; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER must_have_login2 AFTER DELETE ON public.login_method_oauth2 FOR EACH ROW EXECUTE FUNCTION public.must_have_login2();


--
-- Name: login_method_passwd must_have_login2; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER must_have_login2 AFTER DELETE ON public.login_method_passwd FOR EACH ROW EXECUTE FUNCTION public.must_have_login2();


--
-- Name: users privacy_permitted_interest; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER privacy_permitted_interest AFTER DELETE OR UPDATE ON public.users FOR EACH ROW EXECUTE FUNCTION public.privacy_permitted_interest();


--
-- Name: login_method_oauth2 prune_null_oauth2_login; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE TRIGGER prune_null_oauth2_login BEFORE INSERT ON public.login_method_oauth2 FOR EACH ROW EXECUTE FUNCTION public.prune_null_oauth2_login();


--
-- Name: users unique_user_names; Type: TRIGGER; Schema: public; Owner: piperka
--

CREATE CONSTRAINT TRIGGER unique_user_names AFTER INSERT OR UPDATE OF name ON public.users NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE FUNCTION public.unique_user_names();


--
-- Name: claim_token claim_token_uid_fkey; Type: FK CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.claim_token
    ADD CONSTRAINT claim_token_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid);


--
-- Name: comic_title_history comic_title_history_uid_fkey; Type: FK CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.comic_title_history
    ADD CONSTRAINT comic_title_history_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid);


--
-- Name: redirect_log redirect_log_uid_fkey; Type: FK CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.redirect_log
    ADD CONSTRAINT redirect_log_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid) ON DELETE SET NULL;


--
-- Name: ticket_resolve ticket_resolve_tid_fkey; Type: FK CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.ticket_resolve
    ADD CONSTRAINT ticket_resolve_tid_fkey FOREIGN KEY (tid) REFERENCES piperka.ticket(tid) ON DELETE CASCADE;


--
-- Name: user_site_activity user_site_activity_uid_fkey; Type: FK CONSTRAINT; Schema: piperka; Owner: piperka
--

ALTER TABLE ONLY piperka.user_site_activity
    ADD CONSTRAINT user_site_activity_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid);


--
-- Name: appuser_token appuser_token_token_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.appuser_token
    ADD CONSTRAINT appuser_token_token_fkey FOREIGN KEY (token) REFERENCES public.p_session(ses) ON DELETE CASCADE;


--
-- Name: moderator moderator_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.moderator
    ADD CONSTRAINT moderator_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid) ON DELETE CASCADE;


--
-- Name: oauth2_account oauth2_account_opid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_account
    ADD CONSTRAINT oauth2_account_opid_fkey FOREIGN KEY (opid) REFERENCES public.oauth2_provider(opid);


--
-- Name: oauth2_account oauth2_account_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_account
    ADD CONSTRAINT oauth2_account_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid) ON DELETE CASCADE;


--
-- Name: oauth2_state oauth2_state_opid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.oauth2_state
    ADD CONSTRAINT oauth2_state_opid_fkey FOREIGN KEY (opid) REFERENCES public.oauth2_provider(opid) ON DELETE CASCADE;


--
-- Name: user_delete_request user_delete_request_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.user_delete_request
    ADD CONSTRAINT user_delete_request_uid_fkey FOREIGN KEY (uid) REFERENCES public.users(uid) ON DELETE CASCADE;


--
-- Name: users users_auid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: piperka
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_auid_fkey FOREIGN KEY (auid) REFERENCES piperka.author(auid);


--
-- PostgreSQL database dump complete
--

