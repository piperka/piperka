{-# LANGUAGE TupleSections #-}

module Crawler.AutoDiscover where

import Control.Exception (bracket)
import Control.Lens
import qualified Control.Monad.Parallel as P
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy as L
import Data.Default.Class
import Data.Text (Text)
import qualified Data.Vector as V
import GHC.Conc (getNumProcessors)
import System.Process

import Crawler.AutoDiscover.Query
import Crawler.Curl.Download
import Crawler.DB
import Crawler.Types
import Crawler.Parser.Perl.Comm

autoDiscover :: ProgramConfig -> Text -> Text -> Text -> Text -> IO (Either String [Int])
autoDiscover config ub ut src tgt = do
  -- This is used to create external processes, therefore don't use
  -- getNumCapabilities.
  n <- getNumProcessors
  parsers <- P.sequence $ replicate n (startParser' NoStream)
  res <- runExceptT $ do
    cfgs <- map (\f -> f Nothing ub ut Nothing Nothing) <$>
            withExceptT show (loadAllConfigs $ config ^. dbConfig)
    content <- withExceptT show $ ExceptT $ downloadPage def $
               def & nextURL .~ ub <> src <> ut
    lift $ autoDiscover' parsers cfgs src content tgt
  mapM_ closeParser parsers
  return res

loadAllConfigs
  :: DBConfig
  -> ExceptT ByteString IO [MakeCrawlerConfig]
loadAllConfigs  = flip withConn $ fetchAll

-- TODO: Use a pool, don't just round robin
autoDiscover' :: [ParserHandle] -> [CrawlerConfig] -> Text -> L.ByteString -> Text
            -> IO [Int]
autoDiscover' ps cfgs src content tgt = do
  let check parser cfg = bracket (startInstance parser cfg) closeInstance $ \inst -> do
        parsed <- feedPage inst src content
        return $ (parserType cfg,) $ case parsed of
          Right (Result x _) -> x == tgt
          Right (Multi xs) | V.null xs -> False
                           | V.head xs == tgt -> True
                           | otherwise -> (src,tgt) `elem`
                             (snd $ V.foldr
                              (\a (b,ys) -> (Just a,) $ maybe id ((:) . (a,)) b $ ys)
                              (Nothing, []) xs)
          _ -> False
  map fst . filter snd <$> P.sequence (zipWith check (cycle ps) cfgs)
