{-# LANGUAGE OverloadedStrings #-}

module Crawler.Compensations where

import Contravariant.Extras.Contrazip
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Cont
import Data.Int
import qualified Data.IntMap as IntMap
import Data.Maybe
import qualified Data.Vector as V
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement

import Crawler.Crawl
import Crawler.Types

compensation
  :: Int
  -> CrawlEvent
  -> (Maybe (ArchiveState -> IO Compensate))
compensation ord (TerminalEvent end _ _) =
  case end of
    EndLoop pos -> if pos > 1 && pos < ord - 1 then Just $ \archive ->
      flip runContT return $ callCC $ \exit -> do
      let archive' = ((pageCount .~ (ord - pos - 2)) .
                      (archivePages %~ V.drop (pos+1))) <$> archive
          loopStart = ord-pos-1
      next <- do
        res <- head . IntMap.elems <$> liftIO (crawl archive' (Just 1))
        case res ^. endCondition of
          Just (EndLoop n) -> exit (ReversedLoop loopStart n)
          _ -> return ()
        maybe (exit NoNextLoop >> undefined) return $ listToMaybe $ res ^. newPages
      gap <- maybe (exit (NoMatchForNext next) >> undefined)
        (return . (pos -)) $
        (V.findIndex (== next) $ (head $ IntMap.elems archive) ^. archivePages)
      when (gap <= 0) $ exit $ ReversedLoop loopStart (-gap)
      return $ RemoveLoop loopStart gap
      else Nothing
    _ -> Nothing

compensation _ _ = Nothing

compensate
  :: Int32
  -> Compensate
  -> Session ()
compensate cid (RemoveLoop ord gap) = do
  let encoder = contrazip3 (E.param E.int4) (E.param E.int4) (E.param E.int4)
      stmt x = statement (cid, fromIntegral ord, fromIntegral gap) $
        Statement x encoder D.unit True
  S.sql "CREATE TEMPORARY TABLE temp_updates_copy (LIKE updates) ON COMMIT DROP"
  stmt "INSERT INTO temp_updates_copy SELECT * FROM updates \
       \WHERE cid = $1 AND ord>=$2+$3"
  stmt "UPDATE temp_updates_copy SET ord=ord-$3"
  stmt "DELETE FROM updates WHERE cid=$1 AND ord>=$2"
  S.sql "INSERT INTO updates SELECT * FROM temp_updates_copy"
  stmt "UPDATE subscriptions SET ord=CASE WHEN ord<=$2+$3 THEN $2 \
       \ELSE ord-$3 END, subord = CASE WHEN ord<$2+$3 THEN 0 ELSE subord END \
       \WHERE cid=$1 AND (subord=0 AND ord>$2 OR subord>0 AND ord>=$2)"
  stmt "UPDATE recent SET ord=CASE WHEN ord<=$2+$3 THEN $2 \
       \ELSE ord-$3 END, subord = CASE WHEN ord<$2+$3 THEN 0 ELSE subord END \
       \WHERE cid=$1 AND (subord=0 AND ord>$2 OR subord>0 AND ord>=$2)"
  stmt "UPDATE page_fragments SET ord=ord-$3 WHERE cid=$1 AND ord>=$2"

compensate _ _ = return ()
