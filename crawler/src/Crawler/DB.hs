{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}

module Crawler.DB (DBConfig(..), withConn, withConn', withConnTransactionally) where

import Control.Exception
import Control.Monad.Trans.Except
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.ByteString.UTF8 (fromString)
import Hasql.Connection
import Hasql.Session

import Crawler.Types

withConn
  :: DBConfig
  -> Session a
  -> ExceptT ByteString IO a
withConn cfg = withConn' cfg . run

withConn'
  :: Show e
  => DBConfig
  -> (Connection -> IO (Either e a))
  -> ExceptT ByteString IO a
withConn' (DBConfig cfg schema) f = ExceptT $
  bracket prepare
  (either (const $ return ()) release) eitherRun
  where
    eitherFail
      :: (Show e, Monad m)
      => (a -> m (Either ByteString b))
      -> Either e a
      -> m (Either ByteString b)
    eitherFail = either (return . Left . fromString . show)
    prepare = acquire cfg >>= eitherFail
      (\c -> run (sql $ "SET search_path to " <> schema <> ",public;") c >>=
             eitherFail (const . return $ Right c))
    eitherRun = eitherFail (\x -> first (fromString . show) <$> f x)

withConnTransactionally
  :: DBConfig
  -> Session a
  -> ExceptT ByteString IO a
withConnTransactionally cfg session = withConn cfg
  (sql "BEGIN" >> session >>= \a -> sql "END" >> return a)
