{-# LANGUAGE OverloadedStrings #-}

module Crawler.AutoDiscover.Query where

import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

import Crawler.Types

queryAll :: Statement () [MakeCrawlerConfig]
queryAll = Statement sql E.unit (D.rowList decoder) True
  where
    decoder =
      makeCrawlerConfig
      <$> D.nullableColumn D.text
      <*> D.column D.text
      <*> (D.column $ fromIntegral <$> D.int2)
    sql = "SELECT text_hook, start_hook, id FROM parsers ORDER BY id"

fetchAll :: Session [MakeCrawlerConfig]
fetchAll = statement () queryAll
