{-# LANGUAGE OverloadedStrings #-}

module Crawler.DB.Push where

import Data.Functor.Contravariant
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Text (Text)
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement
import Web.WebPush

updatedComics
  :: Session (IntMap [(Int, Text)])
updatedComics = IntMap.fromAscListWith (<>) . (fmap . fmap) pure <$>
  (statement () $ Statement sql E.unit (D.rowList decoder) True)
  where
    decoder =
      (,)
      <$> D.column (fromIntegral <$> D.int4)
      <*> ((,)
           <$> D.column (fromIntegral <$> D.int4)
           <*> D.column D.text
          )
    sql = "SELECT uid, cid, title FROM comics JOIN new_user_update USING (cid) \
          \WHERE updated \
          \AND uid IN (SELECT uid FROM user_push_notification) \
          \AND uid IN (SELECT uid FROM new_user_update WHERE updated AND NOT notified) \
          \ORDER BY uid, ordering_form(title)"

pushEndpoints
  :: Session (IntMap [(Int, PushNotification ())])
pushEndpoints = IntMap.fromAscListWith (<>) <$>
  (statement () $ Statement sql E.unit (D.rowList decoder) True)
  where
    decoder =
      (,)
      <$> D.column (fromIntegral <$> D.int4)
      <*> (fmap pure $ (,)
           <$> D.column (fromIntegral <$> D.int4)
           <*> (mkPushNotification
                <$> D.column D.text
                <*> D.column D.text
                <*> D.column D.text
               )
          )
    sql = "SELECT uid, unid, endpoint, p256dh, auth FROM user_push_notification \
          \WHERE uid IN (SELECT uid FROM new_user_update WHERE updated AND NOT notified) \
          \ORDER BY uid"

deleteEndpoints
  :: [Int]
  -> Session ()
deleteEndpoints = flip statement $
  Statement sql encoder D.unit True
  where
    encoder = E.param . E.array . E.dimension foldl . E.element $
              fromIntegral >$< E.int4
    sql = "DELETE FROM user_push_notification USING UNNEST($1) WHERE unid=unnest"

successEndpoints
  :: [Int]
  -> Session ()
successEndpoints = flip statement $
  Statement sql encoder D.unit True
  where
    encoder = E.param . E.array . E.dimension foldl . E.element $
              fromIntegral >$< E.int4
    sql = "UPDATE user_push_notification SET last_success=transaction_timestamp() \
          \FROM UNNEST($1) WHERE unid=unnest"

updateNotified
  :: [Int]
  -> Session ()
updateNotified = flip statement $
  Statement sql encoder D.unit True
  where
    encoder = E.param . E.array . E.dimension foldl . E.element $
              fromIntegral >$< E.int4
    sql = "UPDATE new_user_update SET notified=true \
          \FROM UNNEST($1) JOIN user_push_notification ON user_push_notification.unid=unnest \
          \WHERE new_user_update.uid = user_push_notification.uid"
