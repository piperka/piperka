{-# LANGUAGE OverloadedStrings #-}

module Crawler.DB.Audit where

import Contravariant.Extras.Contrazip
import Data.Aeson (toJSON)
import Data.Functor.Contravariant
import Data.Int
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

import Crawler.Types

getAuditCandidate
  :: Session (Maybe Int32)
getAuditCandidate = statement () stmt
  where
    stmt = Statement sql E.unit (D.rowMaybe $ D.column D.int4) True
    sql = "SELECT cid FROM crawler_config JOIN comics USING (cid) \
          \LEFT JOIN audit_log USING (cid) \
          \WHERE stamp IS NULL OR stamp < now() - interval '1 week' \
          \AND update_score > 30 AND update_value < 0.000001 \
          \AND last_updated < now() - interval '1 month' \
          \AND (SELECT count(*) FROM updates WHERE cid=comics.cid) > 2 \
          \ORDER BY stamp DESC LIMIT 1"

bumpAuditComic
  :: Int32
  -> Session ()
bumpAuditComic = flip statement stmt
  where
    stmt = Statement sql (E.param E.int4) D.unit True
    sql = "UPDATE crawler_config SET update_score=update_score-60 WHERE cid=$1"

saveAuditResult
  :: Int32
  -> Maybe AuditError
  -> Session ()
saveAuditResult cid err = statement (cid, err) stmt
  where
    stmt = Statement sql encoder D.unit True
    encoder = contrazip2 (E.param E.int4) (fmap toJSON >$< E.nullableParam E.jsonb)
    sql = "INSERT INTO audit_log (cid, audit) VALUES ($1, $2)"
