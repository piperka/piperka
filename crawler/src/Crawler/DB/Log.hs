{-# LANGUAGE OverloadedStrings #-}

module Crawler.DB.Log where

import Contravariant.Extras.Contrazip
import Control.Monad.Trans.Except
import Data.Aeson (fromJSON, toJSON, Result(Error, Success))
import Data.ByteString (ByteString)
import Data.ByteString.UTF8 (fromString)
import Data.Functor.Contravariant
import Data.Int
import Data.Text (Text)
import Hasql.Connection
import Hasql.Decoders as D
import Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

import Crawler.Types

getRunId :: Connection -> ExceptT ByteString IO Int32
getRunId = withExceptT (fromString . show) . ExceptT . run
  (statement () $
   Statement "SELECT nextval('crawler_log_run_id')"
   E.unit (D.singleRow $ D.column D.int4) True)

logEvent
  :: Int32
  -> (Int, Int, CrawlEvent)
  -> Connection
  -> ExceptT String IO ()
logEvent runId (cid, ord, ev) =
  withExceptT show . ExceptT . run (statement (runId, cid, ord, ev) stmt)
  where
    eventEnum (TargetEvent _) = "target"
    eventEnum (ParseEvent _ _) = "parse"
    eventEnum (TerminalEvent _ _ _) = "terminal"
    eventEnum (CompensateEvent _) = "compensate"
    stmt = Statement sql encoder D.unit True
    encoder = contrazip4
      (E.param E.int4)
      (fromIntegral >$< E.param E.int4)
      (fromIntegral >$< E.param E.int4)
      ((toJSON >$< E.param E.jsonb) <> (E.param $ E.enum eventEnum))
    sql = "INSERT INTO crawler_log (run_id, cid, ord, event, event_type) \
          \VALUES ($1, $2, $3, $4, $5 :: crawl_event_type)"

-- Report errors when the first page download leads to an error
logToErrors
  :: Int32
  -> Connection
  -> ExceptT ByteString IO ()
logToErrors runId =
  withExceptT (fromString . show) . ExceptT . run (statement runId stmt)
  where
    stmt = Statement sql (E.param E.int4) D.unit True
    sql = "INSERT INTO crawl_error (cid, ord, stamp, url, http_code, http_message) \
          \SELECT cid, x.ord, stamp, substring(tgt FOR 500), \
          \(event->'end'->>'code') :: int, substring(event->'end'->>'message' FOR 200) \
          \FROM crawler_log AS c \
          \JOIN (SELECT cid, ord, run_id, event->'target'->>'next_url' AS tgt, \
          \ event_type, \
          \ row_number() OVER (PARTITION BY cid ORDER BY plog_id DESC) AS r \
          \ FROM crawler_log WHERE event_type IN ('target', 'parse')) AS x \
          \USING (cid, run_id) \
          \WHERE run_id=$1 AND r=1 \
          \AND x.event_type = 'target' AND c.event_type = 'terminal' \
          \AND event->'end'->>'type' in ('http_error', 'parser', 'crawl')"

-- Get all latest events for all comics
getLogEvents
  :: Session (Either String [(Int32, Int32, CrawlEvent)])
getLogEvents = mapM (\(a,b,c) -> case c of
                        Success c' -> Right (a,b,c')
                        Error err -> Left err) <$>
  (statement () $ Statement sql E.unit (D.rowList decoder) True)
  where
    sql = "SELECT cid, ord, CASE WHEN event->>'head_crop' IS NULL \
          \THEN event || jsonb ('{\"head_crop\":'||old_head_crop_count||'}') \
          \ELSE event END FROM (\
          \SELECT cid, ord, event FROM crawler_log JOIN comics USING (cid) \
          \WHERE plog_id IN (SELECT max(plog_id) FROM crawler_log \
          \WHERE event_type='terminal' GROUP BY cid)) AS a \
          \JOIN crawler_config USING (cid)"
    decoder = (,,)
      <$> D.column D.int4
      <*> D.column D.int4
      <*> (fromJSON <$> D.column D.jsonb)

-- Used by test suite
getLogErrors :: Session [(Int, Int, Int, Int, Text)]
getLogErrors = statement () stmt
  where
    stmt = Statement sql E.unit (D.rowList decoder) True
    decoder = (,,,,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> (fromIntegral <$> D.column D.int4)
      <*> (fromIntegral <$> D.column D.int4)
      <*> (fromIntegral <$> D.column D.int4)
      <*> D.column D.text
    sql = "SELECT errid, cid, ord, http_code, url FROM crawl_error"
