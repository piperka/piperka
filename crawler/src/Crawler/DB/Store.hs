{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Crawler.DB.Store where

import Contravariant.Extras.Contrazip
import Control.Lens
import Control.Monad
import Data.Bool
import Data.Functor.Contravariant
import Data.Int
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.List ((\\))
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement

import Crawler.DB.Score
import Crawler.Types

updateCounts :: ArchiveState -> CrawlState -> IntMap Int
updateCounts = (IntMap.filter ((>0)) .) . IntMap.intersectionWith
  (\a c -> (V.length (a ^. archivePages) + length (c ^. newPages) - 1 +
            if c ^. addFixedHead then 1 else 0) - a ^. pageCount -
           if c ^. replacingHead then 1 else 0
  )

saveCrawlScores :: [Int32] -> Session ()
saveCrawlScores updated = do
  increaseScores
  decreaseTriggered
  decreaseFound updated

saveCrawl :: CrawlState -> Session ()
saveCrawl crawl = do
  let inp f = map (\(a,b,c) -> (fromIntegral a, fromIntegral b, c)) $
        concatMap
        (uncurry (zipWith (\a (b,c) -> (a,b,c)) . repeat)) $
        IntMap.assocs $ IntMap.mapWithKey f crawl
      start i = let s = crawl IntMap.! i in
        V.length (view (originalArchive . archivePages) s) -
        bool 0 1 (view replacingHead s)
      pages :: [(Int32, Int32, Text)] = inp $
        \i -> zip [start i..] . reverse . view newPages
      maxOrdsPlus = map (\(a,b) -> (fromIntegral a, succ b)) . IntMap.assocs .
        IntMap.fromListWith max $ map (\(a,b,_) -> (fromIntegral a, b)) pages
      frags :: [(Int32, Int32, Vector Text)] = inp $
        \i -> zip [start i - 1..] . reverse . (^. fragments)
      cids = map fromIntegral $ IntMap.keys crawl
      haveFixedHead = map fromIntegral $ IntMap.keys $ IntMap.filter (view addFixedHead) crawl
  statement (cids \\ haveFixedHead) deleteFixedHead
  statement haveFixedHead insertFixedHead
  mapM_ (flip statement deleteExcessUpdates) maxOrdsPlus
  mapM_ (flip statement deleteExcessFragments) maxOrdsPlus
  statement pages savePages
  forM_ frags $ \(a,b,c) -> do
    statement (a, b, fromIntegral $ V.length c) deleteOldFragments
    statement (a, b, c) saveFragments
  mapM_ (flip statement updateMax) cids

savePages :: Statement [(Int32, Int32, Text)] ()
savePages = Statement sql encoder D.unit True
  where
    encArray = E.param . E.array . E.dimension foldl
    enc = encArray . E.element
    encoder = unzip3 >$< contrazip3 (enc E.int4) (enc E.int4) (enc E.text)
    sql = "INSERT INTO updates (cid, ord, name) \
          \SELECT * FROM UNNEST($1, $2, $3) \
          \ON CONFLICT (cid, ord) DO UPDATE SET name=EXCLUDED.name"

deleteOldFragments :: Statement (Int32, Int32, Int32) ()
deleteOldFragments = Statement sql encoder D.unit True
  where
    encoder = contrazip3 (E.param E.int4) (E.param E.int4) (E.param E.int4)
    sql = "DELETE FROM page_fragments \
          \WHERE cid=$1 AND ord=$2 AND subord>$3"

saveFragments :: Statement (Int32, Int32, Vector Text) ()
saveFragments = Statement sql encoder D.unit True
  where
    encoder = contrazip3 (E.param E.int4) (E.param E.int4)
              (E.param . E.array . E.dimension V.foldl' $ E.element E.text)
    sql = "INSERT INTO page_fragments (cid, ord, subord, fragment) \
          \SELECT $1, $2, ordinality, unnest FROM UNNEST($3) WITH ORDINALITY \
          \ON CONFLICT (cid, ord, subord) \
          \DO UPDATE SET fragment = excluded.fragment"

deleteExcessUpdates :: Statement (Int32, Int32) ()
deleteExcessUpdates = Statement sql encoder D.unit True
  where
    encoder = (fst >$< E.param E.int4) <> (snd >$< E.param E.int4)
    sql = "DELETE FROM updates WHERE cid=$1 AND ord>=$2"

deleteExcessFragments :: Statement (Int32, Int32) ()
deleteExcessFragments = Statement sql encoder D.unit True
  where
    encoder = (fst >$< E.param E.int4) <> (snd >$< E.param E.int4)
    sql = "DELETE FROM page_fragments WHERE cid=$1 AND ord>=$2"

prepareMassUpdate :: Session ()
prepareMassUpdate = do
  S.sql "SET LOCAL piperka.mass_update TO true"

updateMax :: Statement Int32 ()
updateMax = Statement sql (E.param E.int4) D.unit True
  where
    sql = "INSERT INTO max_update_ord (cid, ord, subord) \
          \SELECT $1, ord, coalesce(max(subord), 0) FROM \
          \(SELECT cid, max(ord)+ \
          \ CASE WHEN cid IN (SELECT cid FROM fixed_head) THEN 1 ELSE 0 END AS ord \
          \ FROM updates WHERE updates.cid=$1 \
          \ GROUP BY cid) AS max_upd \
          \LEFT JOIN page_fragments USING (cid, ord) GROUP BY ord \
          \ON CONFLICT (cid) DO UPDATE SET ord=EXCLUDED.ord, subord=EXCLUDED.subord"

-- TODO: Adding only fragments should update last updated as well
updateLastUpdated :: [Int32] -> Session ()
updateLastUpdated cids = do
  statement cids $ Statement sql encoder D.unit True
  S.sql "REFRESH MATERIALIZED VIEW nearest_infer"
  where
    encoder = E.param $ E.array $ E.dimension foldl $ E.element E.int4
    sql = "UPDATE crawler_config SET last_updated=now() \
          \WHERE cid = ANY($1 :: int[])"

deleteFixedHead :: Statement [Int32] ()
deleteFixedHead = Statement sql encoder D.unit True
  where
    encoder = E.param . E.array . E.dimension foldl $ E.element E.int4
    sql = "DELETE FROM fixed_head WHERE cid=ANY($1 :: int[])"

insertFixedHead :: Statement [Int32] ()
insertFixedHead = Statement sql encoder D.unit True
  where
    encoder = E.param . E.array . E.dimension foldl $ E.element E.int4
    sql = "INSERT INTO fixed_head (cid) SELECT unnest($1) \
          \ON CONFLICT DO NOTHING"
