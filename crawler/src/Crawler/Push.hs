{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Crawler.Push where

import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson (ToJSON)
import Data.ByteString.UTF8 (toString)
import qualified Data.IntMap as IntMap
import Data.List (intercalate)
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Foreign.C.String (withCStringLen)
import Network.HTTP.Client.TLS
import System.IO
import qualified System.Posix.Syslog as Posix
import System.Posix.Syslog (Priority(..))
import Web.WebPush

import Crawler.DB
import Crawler.DB.Push
import Crawler.Types

data PushNotificationMessage = PushNotificationMessage
  { title :: T.Text
  , body :: T.Text
  , icon :: T.Text
  , url :: T.Text
  }
  deriving (Generic, ToJSON)

data PushResult = Success | Failure PushNotificationError Text | Shutdown Text

-- Use Web Push Notifications to tell of updates.
pushMessages :: ProgramConfig -> VAPIDKeys -> IO ()
pushMessages config vapidKeys = Posix.withSyslog "crawler" [] Posix.User $ do
  let db = config ^. dbConfig
      (faviconURL, updatesURL) = case dbSchema db  of
        "piperka" -> ("https://piperka.net/favicon.png", "https://piperka.net/")
        "teksti" -> ("https://teksti.eu/favicon.png", "https://teksti.eu/")
        _ -> ("", "")
  mgr <- newTlsManager
  res <- runExceptT $ do
    (updated, endpoints) <- withConn db $ ((,) <$> updatedComics <*> pushEndpoints)
    let msgs = updated <&> \xs ->
          let len = length xs
              lst = T.intercalate ", " $ map snd xs
              msg = if T.length lst > 80 then T.take 80 lst <> " ..." else lst
              prefix = if len == 1 then "One update: " else T.pack (show len) <> " updates: "
          in prefix <> msg
        send (uid, xs) = let msg = maybe "You have updates" id $ IntMap.lookup uid msgs
          in mapM (\(unid, pn) ->
                     (sendPushNotification vapidKeys mgr $
                      (pn & pushMessage .~
                       (PushNotificationMessage "New unread comics" msg
                        faviconURL updatesURL)
                      )
                     ) <&> ((unid,uid),) . either
                     (($ (pn ^. pushEndpoint)) .
                      \case
                        RecepientEndpointNotFound -> Shutdown
                        e -> Failure e
                     ) (const Success)) xs
        reportSend ((unid,uid), Success) =
          (Info, "push succeeded uid " <> show @Int uid <> " unid " <> show @Int unid)
        reportSend ((unid,uid), Failure err endp) =
          (Warning, "push send failure uid " <> show @Int uid <> " unid " <> show @Int unid <>
                    " " <> T.unpack endp <> " " <> show err)
        reportSend ((unid,uid), Shutdown endp) =
          (Notice, "push shutdown uid " <> show @Int uid <> " unid " <> show @Int unid <>
                   " " <> T.unpack endp)
    -- Log sends
    liftIO $ flip IntMap.foldMapWithKey updated $ \uid xs -> do
      syslog Notice $
        "pushing updates user " <> show @Int uid <>
        " comics " <> intercalate " " (map (show @Int . fst) xs)
      syslog Debug $
        "update content " <> show @Int uid <> " " <>
        maybe "NULL" T.unpack (IntMap.lookup uid msgs)
    sendRes <- lift $ fmap join $ mapM send $ IntMap.toList endpoints
    -- Warn about transient errors
    liftIO $ mapM_ (uncurry syslog . reportSend) sendRes
    let del = map (fst . fst) $ filter ((\case Shutdown _ -> True; _ -> False) . snd) sendRes
        suc = map (fst . fst) $ filter ((\case Success -> True; _ -> False) . snd) sendRes
    withConnTransactionally db $ do
      deleteEndpoints del
      successEndpoints suc
      updateNotified suc
  case res of
    Left err -> do
      let msg = "DB error in push notifications: " <> toString err
      syslog Error msg
      hPutStrLn stderr msg
    Right _ -> return ()
  where
    syslog pri msg = withCStringLen msg $ Posix.syslog Nothing pri
