{-# LANGUAGE OverloadedStrings #-}

module Crawler.Parser.Perl.Actions (parseAction) where

import Control.Applicative ((<|>))
import Control.Lens
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Data.Char
import Data.Default.Class
import qualified Data.IntMap as IntMap
import Data.List (elemIndex, nub)
import Data.Maybe
import Data.Text (pack, unpack)
import qualified Data.Text.ICU as ICU
import qualified Data.Vector as V
import Network.URI
import Network.URI.Encode

import Crawler.Types

parseAction
  :: Monad m
  => ArchiveState
  -> (Int, ParseResult)
  -> StateT CrawlState m (Either CrawlEndCondition (Maybe CrawlTarget))
parseAction world (cid, res) = do
  st <- gets (IntMap.! cid)
  let (ArchiveEntry _ pages _ cfg) = world IntMap.! cid
      processFragments frag = modify $ IntMap.adjust (over fragments (frag:)) cid
      staticRegex = ICU.regex [ICU.Multiline] "^#\\s+(?:rss|url):\\s*(\\S+)"
      singleshot = isJust $ ICU.find staticRegex $ startHook cfg
      deleteLast = modify $ IntMap.adjust
        ((newPages %~ (drop 1)) .
         (\s -> set newestPagePurge (not $ null $ s ^. newPages) s)) cid
      lastPage = urlBase cfg <>
                 (if null $ st ^. newPages
                   then V.head pages
                   else head $ st ^. newPages) <> urlTail cfg
      loopIdx page =
        (page `elemIndex` (st ^. newPages)) <|>
        ((length (st ^. newPages) +) <$> (page `V.elemIndex` pages))
      newNext page = do
        modify $ IntMap.adjust (over newPages (page:)) cid
        return $ Right $
          if singleshot then Nothing
          else Just $ def
               & extraHeaders .~ [("Referer", lastPage)]
               & nextURL .~ (urlBase cfg <> page <> urlTail cfg)
               & nextPage .~ Just page
  case res of
    Result page frag -> do
      processFragments frag
      modify $ IntMap.adjust (redirects .~ 0) cid
      maybe (newNext page) (return . Left . EndLoop) $ do
        i <- loopIdx page
        guard $ i /= 0 || not (null $ st ^. newPages) || (not $ st ^. replacingHead)
        return i
    Alternatives xs -> do
      modify $ IntMap.adjust (redirects .~ 0) cid
      case filter (isNothing . loopIdx) . nub $ V.toList xs of
        [page] -> newNext page
        [] -> return $ Right Nothing
        ys -> return . Left $ EndAlternatives ys
    Multi ps -> if V.null ps then return $ Right Nothing else
      (fmap . fmap) (join . listToMaybe . take 1 . reverse) $
      runExceptT $ sequence $ map
      (\r -> ExceptT $ parseAction world (cid, Result r V.empty)) $
      reverse $ takeWhile (/= V.head pages) $ reverse $ V.toList ps
    AtFixedHead frag -> do
      processFragments frag
      modify $ IntMap.adjust (addFixedHead .~ True) cid
      return $ Right Nothing
    None frag -> do
      processFragments frag
      return $ Right Nothing
    Stop -> return $ Right Nothing
    Fetch auto tgt -> maybe id (\x -> if x then maybeFaultyStored st else id) auto $ do
      let doneRedirects = st ^. redirects
          -- The parser may return just a path+query so make it into a
          -- full URL.
          tgt' = do
            uri <- parseURIReference $ unpack $ tgt ^. nextURL
            lastUri <- parseURI $ encodeWith enc $ unpack lastPage
            return $ tgt & if uriIsAbsolute uri
              then id
              else nextURL .~
                   (pack $ (uriToString (const "") $ relativeTo uri lastUri) $ "")
      modify $ IntMap.adjust (over redirects (+1)) cid
      if doneRedirects < 9
        then return . maybe (Left EndInvalidRedirect) (Right . Just) $ tgt'
        else deleteLast >> (return $ Left EndRedirects)
    Faulty -> do
      deleteLast
      return $ Left EndFaulty
    ResponseError code msg -> maybeFaultyStored st $ do
      deleteLast
      return $ Left $ EndError code msg
  where
    enc x = (isAlphaNum x && isAscii x) || x `elem` (":/?=._" :: String)
    maybeFaultyStored st normal =
      let a = st ^. originalArchive
          c = a ^. cConfig in
        if null (st ^. newPages)
           && not (st ^. addFixedHead)
           && not (st ^. replacingHead)
           && V.length (a ^. archivePages) > 1
        then do
          modify $ IntMap.adjust (replacingHead .~ True) cid
          return $ Right $ Just $ def
            & headRedir .~ True
            & nextURL .~ (urlBase c <> (a ^. archivePages) V.! 1 <> urlTail c)
        else normal
