{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Crawler.Helpers.GoComics where

import Control.Concurrent
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson
import qualified Data.ByteString as B
import Data.ByteString.Lazy (fromStrict, toStrict)
import Data.Default.Class
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time.Calendar
import Data.Time.Clock
import Data.Vector (Vector)
import qualified Data.Vector as V
import Text.Printf

import Crawler.Curl.Download
import Crawler.Types

goComicsHelper
  :: Text
  -> IO (Either String (Text, Vector Text))
goComicsHelper firstUrl = do
  today <- utctDay <$> getCurrentTime
  let (maxYear, maxMonth) = (\(y, m, _) -> (fromIntegral y, m)) $ toGregorian today
      lim = maxYear*12+maxMonth-1
      (domain, ps) = T.split (=='/') <$> T.splitAt 25 firstUrl
      comicName = head ps
      startYear = (read . T.unpack $ ps !! 1) :: Int
      startMonth = (read . T.unpack $ ps !! 2) :: Int
      start = startYear*12+startMonth-1
      ts = map (\x -> let (y, m) = divMod x 12
                          y' = T.pack (printf "%d" y :: String)
                          m' = T.pack (printf "%02d" (m+1) :: String)
                      in def & nextURL .~
                         ("https://www.gocomics.com/calendar/" <>
                          comicName <> "/" <> y' <> "/" <> m')) [start .. lim]
      retry 0 f = f
      retry n f = f >>=
        either (const $ threadDelay 5000000 >> retry (n-1) f) (return . Right)
  runExceptT $ do
    when (domain /= "https://www.gocomics.com/") $ throwE "Not a GoComics url?"
    when (length ps /= 4) $ throwE "Path split failed"
    when (start > lim) $ throwE "Start after today"
    -- I wish Network.HTTP could build a Response from a ByteString
    (comicName,) . V.concat <$> mapM
      (\tgt -> let withLine f = withExceptT
                                (\x -> (show $ tgt ^. nextURL) <> ": " <> f x)
        in liftIO (threadDelay 1000000) >>
           (ExceptT . retry (3 :: Int) . runExceptT $
            (withLine show . ExceptT $ downloadPage def tgt) >>=
            withLine id . ExceptT . return . eitherDecode .
            fromStrict . snd . B.breakSubstring "\r\n\r\n" . toStrict)
      ) ts
