module Crawler.Audit where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import qualified Data.IntMap as IntMap
import Data.IORef
import qualified Data.Vector as V
import Hasql.Session
import System.Random

import Crawler.Crawl
import Crawler.DB
import Crawler.DB.Fetch
import Crawler.Parser.Perl.Comm
import Crawler.Types

auditComic
  :: ProgramConfig
  -> Int
  -> IO (Maybe AuditError)
auditComic cfg cid =
  fmap (either (Just . DBError) id) . runExceptT $
  withConn' (cfg ^. dbConfig) $ \conn ->
  bracket startParser closeParser $
  \pHandle -> runExceptT $ do
    archive <- fmap (fmap (IntMap.! cid)) .
      ExceptT $ run (fetchSingleArchive False $ fromIntegral cid) conn
    lift $ runMaybeT $ do
      a <- MaybeT . return $ archive
      if 2 >= a ^. pageCount then MaybeT . return $ Just TooFew
        else MaybeT $ auditComic' cfg cid pHandle a

auditComic'
  :: ProgramConfig
  -> Int
  -> ParserHandle
  -> ArchiveEntry
  -> IO (Maybe AuditError)
auditComic' cfg cid pHandle archive = do
  cutoff <- newIORef (0 :: Int)
  -- Sample archive
  n <- getStdRandom $ randomR (0, archive ^. pageCount - 1)
  let archive' = (pageCount .~ n) . (archivePages %~ V.drop n) $ archive
      cc = archive ^. cConfig
      targetPage = urlBase cc <> V.head (archive' ^. archivePages) <> urlTail cc
  -- Run crawler just enough to get the next page
  end <- fmap (IntMap.! cid) . readMVar =<<
    (crawl' cfg (IntMap.singleton cid archive') pHandle $ \_ _ _ -> do
        isDone <- gets (not . null . view newPages . (IntMap.! cid))
        reqs <- liftIO $ atomicModifyIORef cutoff (\x -> let x' = succ x in (x', x'))
        when (isDone || reqs > 10) $ modify $
          IntMap.adjust (endCondition .~ Just EndRequest) cid
      )
  if null $ end ^. newPages
    then return $ Just $ NoNext n targetPage $ end ^. endCondition
    -- Compare crawl result to archive
    else return $ let len' = length $ end ^. newPages
                      (len, n') = if end ^. replacingHead
                        then (succ len', pred n)
                        else (len', n)
                      orig = V.take len . V.drop (n' - 1) $ archive ^. archivePages
                      endPages = V.fromList $ end ^. newPages
    in guard (endPages /= orig) >> (Just $ MismatchPage n orig endPages)
