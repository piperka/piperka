module Crawler.Crawl.Interactive where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.State
import qualified Data.IntMap as IntMap
import Data.IORef
import System.IO

import Crawler.Crawl
import Crawler.Parser.Perl.Comm
import Crawler.Types

crawlInteractive :: ProgramConfig -> ArchiveState -> IO (Maybe CrawlState)
crawlInteractive config archive = do
  hSetBuffering stdin NoBuffering
  hSetEcho stdin False
  stopFlag <- newMVar False
  discardResults <- newIORef False
  showInstructions <- newIORef False
  messages <- newChan
  let outputter = do
        help <- liftIO $ readIORef showInstructions
        when help $ liftIO $ do
          putStrLn "press s)top, c)continue, q)uit"
          writeIORef showInstructions False
        readChan messages >>= maybe (return ()) (\x -> putStrLn x >> outputter)
      listener = forkIO $
        let listenPause = do
              a <- getChar
              case a of
                ' ' -> do
                  void $ takeMVar stopFlag
                  writeIORef showInstructions True
                  listenCommand
                _ -> listenPause
            listenCommand = do
              a <- getChar
              case a of
                's' -> putMVar stopFlag True
                'c' -> putMVar stopFlag False >> listenPause
                'q' -> putMVar stopFlag True >> writeIORef discardResults True
                _ -> listenCommand
        in listenPause
      control cid ord ev = do
        liftIO $ writeChan messages $ Just $
          "cid " <> show cid <> " ord " <> show ord <> " " <> show ev
        stop <- liftIO $ readMVar stopFlag
        when stop $
          modify $ IntMap.adjust (endCondition .~ Just EndRequest) cid
  void $ forkIO outputter
  bracket ((,) <$> listener <*> startParser)
    (\(a,b) -> killThread a >> closeParser b) $ \(_, pHandle) -> do
    resultVar <- crawl' config archive pHandle control
    res <- takeMVar resultVar
    discard <- readIORef discardResults
    writeChan messages Nothing
    return $ if discard then Nothing else Just res
