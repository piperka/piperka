{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Crawler.Crawl.Logging where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.UTF8 as UTF8
import qualified Data.IntMap as IntMap
import Data.IORef
import qualified Network.HTTP.Client as HTTP.Client
import System.Directory (doesDirectoryExist, removeFile)
import System.IO.Error (isDoesNotExistError)
import qualified Text.Show.ByteString as B

import Crawler.Crawl
import Crawler.DB
import Crawler.DB.Log
import Crawler.Parser.Perl.Comm
import Crawler.Types

writeProgress :: ProgramConfig -> ProgressDisplay -> IO ()
writeProgress cfg CleanUp =
  removeFile (progressFile cfg) `catch` handleExists
  where handleExists e
          | isDoesNotExistError e = return ()
          | otherwise = throwIO e

writeProgress cfg (Crawling runSize point) =
  -- Just to make testing a bit easier
  doesDirectoryExist "/var/local/piperka/run/shared/" >>=
  flip when
  (B.writeFile (progressFile cfg) $
    "The update crawler is at work (" <> B.show @Int point <>
    "/" <> B.show @Int runSize <> ")")

writeProgress cfg (Finishing point) =
  doesDirectoryExist "/var/local/piperka/run/shared/" >>=
  flip when
  (B.writeFile (progressFile cfg) $
    "Finalizing hourly crawler run (" <> B.show @Int point <> ")")

progressFile :: ProgramConfig -> FilePath
progressFile cfg =
  let DBConfig _ schema = cfg ^. dbConfig
  in "/var/local/piperka/run/shared/" <> B8.unpack schema <> "/update_status"

notifyAfterUpdate :: ProgramConfig -> IO ()
notifyAfterUpdate cfg = do
  mgr <- HTTP.Client.newManager HTTP.Client.defaultManagerSettings
  let DBConfig _ schema = cfg ^. dbConfig
      req = (HTTP.Client.parseRequest_ $ "http://localhost/crawlDone/" <> UTF8.toString schema)
            { HTTP.Client.method = "HEAD" }
      notify p =
        (() <$ HTTP.Client.httpNoBody (req { HTTP.Client.port = p }) mgr) `catch`
        \(_ :: HTTP.Client.HttpException) -> return ()
  mapM_ notify [8000,8001]

-- | Non-interactive logging crawl with cutoff
crawlLog :: ProgramConfig -> ArchiveState -> Int -> Bool
         -> IO (Either ByteString CrawlState)
crawlLog cfg archive cutoff verbose = runExceptT $
  withConn' (cfg ^. dbConfig) $ \conn ->
  bracket startParser closeParser $
  \pHandle -> runExceptT $ do
  counter <- liftIO $ newIORef (0 :: Int)
  let setUpdate :: Int -> IO ()
      setUpdate = (cfg ^. displayProgress) cfg . Crawling (IntMap.size archive)
  liftIO $ setUpdate 0
  runId <- getRunId conn
  let control cid ord ev = do
        liftIO $ do
          case ev of
            ParseEvent _ _ -> modifyIORef counter (+1)
            _ -> return ()
          setUpdate =<< readIORef counter
        let cut = ord - archive IntMap.! cid ^. pageCount >= cutoff
            hc = archive IntMap.! cid ^. headCropCount
        when cut $
          modify $ IntMap.adjust (endCondition .~ Just EndCutoff) cid
        let ev' = if not cut then ev else
              TerminalEvent EndCutoff hc $
              case ev of
                ParseEvent p' _ -> Just p'
                TerminalEvent _ _ p' -> p'
                _ -> Nothing
        void $ liftIO $ runExceptT (logEvent runId (cid, ord, ev') conn)
        when verbose $ liftIO $ print (cid, ord, ev)
  res <- liftIO $ takeMVar =<< crawl' cfg archive pHandle control
  logToErrors runId conn
  return res
