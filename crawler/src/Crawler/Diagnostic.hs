{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Crawler.Diagnostic where

import Contravariant.Extras.Contrazip
import Control.Monad
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString, hPut)
import Data.ByteString.Char8 (pack)
import Data.Functor.Contravariant
import qualified Data.IntMap.Strict as IntMap
import Data.IntMap.Strict (IntMap)
import Data.Time
import Data.Vector (Vector)
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import qualified Hasql.Session as S
import Hasql.Session hiding (sql)
import Hasql.Statement
import GHC.Generics
import System.IO (stderr)

import Crawler.DB

data Diagnostic = Diagnostic
  { updateScore :: Maybe Float
  , updateValue :: Float
  , lastUpdated :: Maybe String
  , schedule :: Maybe [Int]
  } deriving (Generic)

instance ToJSON Diagnostic where
  toEncoding = genericToEncoding defaultOptions

instance FromJSON Diagnostic

epoch :: UTCTime
epoch = UTCTime (fromGregorian 1970 1 1) $ fromInteger 0

diagnosticDecoder :: D.Row Diagnostic
diagnosticDecoder = Diagnostic
  <$> D.nullableColumn D.float4
  <*> D.column D.float4
  <*> D.nullableColumn (formatTime defaultTimeLocale "%F %T" <$> D.timestamp)
  <*> D.nullableColumn (D.array $ D.dimension replicateM $ D.element (fromIntegral <$> D.int2))

queryDiagnostic :: Statement () (IntMap Diagnostic)
queryDiagnostic = Statement sql E.unit
                  (IntMap.fromAscList <$> D.rowList decoder) True
  where
    decoder = (,)
      <$> D.column (fromIntegral <$> D.int4)
      <*> diagnosticDecoder
    sql =
      "SELECT cid, update_score, update_value, last_updated, schedule \
      \FROM comics JOIN crawler_config USING (cid) \
      \LEFT JOIN inferred_schedule USING (cid) \
      \ORDER BY cid"

importCrawlerConfig :: Statement (Int, Diagnostic) ()
importCrawlerConfig = Statement sql encoder D.unit True
  where
    encoder =
      contrazip2
      (E.param $ fromIntegral >$< E.int4)
      (mconcat
       [ (updateScore >$< E.nullableParam E.float4)
       , (updateValue >$< E.param E.float4)
       , ((parseTimeM True defaultTimeLocale "%F %T" <=< lastUpdated) >$< E.nullableParam E.timestamp)
       ]
      )
    sql =
      "UPDATE crawler_config SET update_score=$2, update_value=$3, \
      \last_updated=COALESCE($4,'-infinity'::timestamp) \
      \WHERE cid=$1"

importSchedule :: Statement (Int, Diagnostic) ()
importSchedule = Statement sql encoder D.unit True
  where
    encoder =
      contrazip2
      (E.param $ fromIntegral >$< E.int4)
      (schedule >$< (E.nullableParam $ E.array $ E.dimension foldl $ E.element (fromIntegral >$< E.int2)))
    sql =
      "INSERT INTO inferred_schedule (cid, schedule) VALUES ($1, $2) \
      \ON CONFLICT (cid) DO UPDATE SET schedule=EXCLUDED.schedule"

dumpScheduleDiagnostic
  :: DBConfig
  -> String
  -> IO ()
dumpScheduleDiagnostic cfg filename =
  runExceptT (withConn cfg (statement () queryDiagnostic)) >>=
  either (hPut stderr) (encodeFile filename)

restoreScheduleDiagnostic
  :: DBConfig
  -> String
  -> IO (Either ByteString ())
restoreScheduleDiagnostic cfg filename = runExceptT $ do
  sched :: Vector (Int,Diagnostic) <- ExceptT $ either (Left . pack) Right <$> eitherDecodeFileStrict filename
  withConnTransactionally cfg $ do
    mapM_ (flip statement importCrawlerConfig) sched
    S.sql "UPDATE inferred_schedule SET schedule=NULL"
    mapM_ (flip statement importSchedule) sched
