{-# LANGUAGE OverloadedStrings #-}

module Crawler.Test.DB.Fetch where

import Data.Int
import Hasql.Decoders as D
import Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

fetchFixedHead :: Session [Int32]
fetchFixedHead = statement () $ Statement sql E.unit (D.rowList decoder) True
  where
    sql = "SELECT cid FROM fixed_head"
    decoder = D.column D.int4
