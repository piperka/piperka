module Crawler.Test where

import Test.Hspec

import Crawler.Snap
import Crawler.Test.Command
import Crawler.Test.Compensations
import Crawler.Test.Crawler
import Crawler.Test.DB
import Crawler.Test.ParseResult
import Crawler.Test.SimpleParse
import System.Directory

main :: IO ()
main = do
  setCurrentDirectory "crawler"
  testServe $ hspec spec

spec :: Spec
spec = do
  describe "Parser process" $ perlProcessTests
  describe "processResponse" $ parallel parseResultTests
  describe "Crawler" $ parallel crawlerTests
  describe "DB" $ dbTests
  describe "Command" $ commandTests
  describe "Compensations" $ compensationsTests
