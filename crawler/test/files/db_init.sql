SET client_min_messages TO warning;

create table if not exists updates (
    cid int NOT NULL,
    ord int NOT NULL,
    name text NOT NULL,
    unique (cid, ord)
);

truncate updates;

create table if not exists page_fragments (
    cid int not null,
    ord int not null,
    subord int not null,
    fragment text not null,
    unique (cid, ord, subord)
);

truncate page_fragments;

CREATE TABLE if not exists crawler_config (
    cid integer NOT NULL,
    parser_type smallint,
    update_score real DEFAULT 0,
    update_value real DEFAULT 0 NOT NULL,
    last_updated timestamp without time zone DEFAULT '-infinity'::timestamp without time zone NOT NULL,
    head_crop_count smallint DEFAULT 0 NOT NULL,
    extra_url text,
    extra_data text,
    old_head_crop_count smallint DEFAULT 0 NOT NULL
);

truncate crawler_config;

CREATE TABLE if not exists hiatus_status (
    hsid serial PRIMARY KEY,
    cid integer NOT NULL,
    active boolean DEFAULT true NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    code smallint,
    uid integer NOT NULL,
    CONSTRAINT hiatus_status_check CHECK (((code IS NOT NULL) OR (NOT active)))
);

CREATE TABLE if not exists inferred_schedule (
    cid integer PRIMARY KEY,
    schedule smallint[]
);

CREATE MATERIALIZED VIEW IF NOT EXISTS nearest_infer AS WITH hour_of_week AS (
  select 24*((cast(extract(dow from current_date) as int2)+6)%7)+
  extract(hour from current_timestamp) as h)
  select cid, min(cast(a.s+168-hour_of_week.h+6 as int2)%168)-6 as till_scheduled from hour_of_week, comics join
  (select cid, s from hour_of_week, (select cid, s from inferred_schedule, unnest(schedule) as s union
  select cid, s+168 from inferred_schedule, unnest(schedule) as s where s < 24) as x
  where s-hour_of_week.h > -6 and s-hour_of_week.h < 24) as a using (cid) join crawler_config using (cid)
  where last_updated < current_timestamp - '12 hours'::interval
  and cid not in (select cid from hiatus_status where active) group by cid;

CREATE OR REPLACE FUNCTION scheduled_updates_near(stamp timestamp default current_timestamp, hours_before int default 1, hours_after int default 1) RETURNS TABLE(cid int, schedule timestamp) AS $$
  WITH monday AS (
  SELECT date_trunc('day', stamp)+(((extract(isodow FROM stamp)-1) || ' days ago')::interval) AS monday)
  SELECT cid, decoded FROM
  (SELECT cid, monday+(n || ' hours') :: interval AS decoded FROM monday,
  (SELECT cid, n FROM inferred_schedule, UNNEST(schedule) AS n
  UNION
  SELECT cid, n+168 FROM inferred_schedule, UNNEST(schedule) AS n where n<hours_after
  UNION
  SELECT cid, n-168 FROM inferred_schedule, UNNEST(schedule) AS n where n>168-hours_before) AS a ORDER BY decoded) AS b
  WHERE decoded >= date_trunc('hour', stamp) - (hours_before||' hours')::interval
  AND decoded <= stamp + (hours_after||' hours')::interval
$$ PARALLEL SAFE STABLE LANGUAGE SQL;

CREATE TABLE if not exists comics (
    cid integer NOT NULL,
    url_base text,
    url_tail text,
    fixed_head text,
    homepage text NOT NULL
);

truncate comics;

CREATE TABLE if not exists parsers (
    id integer NOT NULL,
    text_hook text,
    start_hook text,
    auto_try boolean DEFAULT true
);

truncate parsers;

CREATE TABLE if not exists max_update_ord (
    cid int NOT NULL UNIQUE,
    ord int NOT NULL,
    subord int NOT NULL
);

truncate max_update_ord;

CREATE TABLE if not exists fixed_head (
    cid int PRIMARY KEY
);

truncate fixed_head;

-- Check upserts with this
insert into max_update_ord (cid, ord, subord) values (1, 10, 100);

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'crawl_event_type') THEN
    CREATE TYPE crawl_event_type AS ENUM ('target', 'parse', 'terminal');
  END IF;
END$$;

CREATE TABLE IF NOT EXISTS crawler_log (
    plog_id serial PRIMARY KEY,
    run_id integer NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    stamp timestamp without time zone NOT NULL DEFAULT now(),
    event_type crawl_event_type NOT NULL,
    event jsonb NOT NULL
);

CREATE INDEX IF NOT EXISTS crawler_log_cid_stamp ON crawler_log (cid, stamp);

truncate crawler_log;

create sequence if not exists crawler_log_run_id owned by crawler_log.run_id;

select setval('crawler_log_run_id', 1);

-- This is the old error table used by Piperka.
CREATE TABLE if not exists crawl_error (
    errid serial NOT NULL,
    cid integer NOT NULL,
    ord integer NOT NULL,
    stamp timestamp without time zone DEFAULT now() NOT NULL,
    url character varying(500) NOT NULL,
    http_code integer,
    http_message character varying(200)
);

truncate crawl_error;

select setval('crawl_error_errid_seq', 1);

CREATE TABLE if not exists subscriptions (
    uid integer,
    cid integer,
    ord integer NOT NULL,
    subord integer NOT NULL
);

truncate subscriptions;

CREATE TABLE IF NOT EXISTS recent (
    uid integer,
    cid integer,
    ord integer,
    used_on timestamp without time zone,
    subord integer
);

truncate recent;

CREATE OR REPLACE FUNCTION public.reduce_dim(anyarray)
RETURNS SETOF anyarray AS
$function$
DECLARE
    s $1%TYPE;
BEGIN
  IF array_length($1, 1) > 0 THEN
    FOREACH s SLICE 1  IN ARRAY $1 LOOP
      RETURN NEXT s;
    END LOOP;
  END IF;
  RETURN;
END;
$function$
LANGUAGE plpgsql IMMUTABLE;

copy updates (cid, ord, name) from stdin;
1	0	1
2	0	page0
2	1	page1
3	0	page1
3	1	garbage
4	0	page1
4	1	page2
5	0	simple_page
6	0	aaa
7	0	page1
7	1	page2
7	2	page3
8	0	page1
8	1	page2
9	0	invalid
10	0	1
10	1	5
10	2	6
10	3	2
10	4	3
10	5	4
11	0	page1
11	1	redirecting_page
12	0	page1
12	1	404page
13	0	page1
13	1	page2
13	2	404page
\.

copy fixed_head (cid) from stdin;
4
8
\.

copy page_fragments (cid, ord, subord, fragment) from stdin;
2	0	1	frag0-1
2	1	1	frag1-1
2	1	2	frag1-2
2	1	3	frag1-3
\.

copy parsers (id, text_hook, start_hook) from stdin;
1	\N	if ($tag eq 'a' && exists $attr->{rel} && $attr->{rel} eq 'next') {if ($attr->{href} =~ m<id=(\\d+)>) {$next = $1;} $self->eof;}
2	\N	if ($tag eq 'a') {if ($attr->{href} =~ m<\\#(.+)>) {push @fragments, $1;} if (exists $attr->{rel} && $attr->{rel} eq 'next') {if ($attr->{href} =~ m<([^/]+)\\.html>) {$next = $1;} $self->eof;}}
3	\N	if ($tag eq 'a' && exists $attr->{rel} && $attr->{rel} eq 'next') {if ($attr->{href} =~ m<([^/]+)\\.html>) {$next = $1;} $self->eof;}
4	\N	if ($tag eq 'a' && exists $attr->{rel} && $attr->{rel} eq 'next') {if ($attr->{href} =~ m<([^/]+)\\.html>) {if ($1 eq 'index') {$at_fixed_head=1;} else {$next = $1}} $self->eof;}
5	\N	# rss: http://localhost:8000/simple_rss.xml\n; my $seen = [];foreach my $item(@{$feed->{rss}->{items}}) {if ($item->{link} =~ m<8000/([^.]+)\\.html>) {unshift @$seen, $1;}} if(scalar @$seen > 0) {$next = $seen; $param->{stop} = 1; $self->eof;}
6	\N	if ($tag eq 'a' && exists $attr->{class} && $attr->{class} eq 'misleading-next') {if ($attr->{href} =~ m<([^/]+)\\.html>) {$next = $1;} $self->eof;}
\.

copy crawler_config (cid, parser_type, head_crop_count, update_score, update_value, extra_url, extra_data, old_head_crop_count) from stdin;
1	1	0	58	0	\N	\N	0
2	2	0	0	20	\N	\N	0
3	3	1	58	0	\N	\N	1
4	4	0	58	0	\N	\N	0
5	5	0	0	0	\N	\N	0
6	5	0	58	0	http://localhost:8000/alternate.xml	\N	0
7	3	0	58	0	\N	\N	0
8	3	0	58	0	\N	\N	0
9	3	0	58	0	\N	\N	0
10	1	0	0	0	\N	\N	0
11	3	0	0	0	\N	\N	0
12	3	0	0	0	\N	\N	0
13	3	0	58	0	\N	\N	0
\.

copy comics (cid, url_base, url_tail, fixed_head, homepage) from stdin;
1	http://localhost:8000/simple_page.html?id=			http://localhost:8000/simple_page.html
2	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
3	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
4	http://localhost:8000/fixed_head/	.html		http://localhost:8000/fixed_head/
5	http://localhost:8000/	.html		http://localhost:8000/
6	http://localhost:8000/	.html		http://localhost:8000/
7	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
8	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
9	http://localhost:8000/invalid/	.html		http://localhost:8000/invalid/
10	http://localhost:8000/increasing?max=7&id=			http://localhost:8000/increasing?max=7
11	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
12	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
13	http://localhost:8000/fragments/	.html		http://localhost:8000/fragments/page1.html
\.

copy subscriptions (uid, cid, ord, subord) from stdin;
1	10	0	1
2	10	1	0
3	10	1	1
4	10	2	0
5	10	2	1
6	10	3	0
7	10	3	1
8	10	4	1
\.
