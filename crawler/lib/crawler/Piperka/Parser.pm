package Piperka::Parser;

use HTTP::Request;
use HTTP::Request::Common;
use HTML::Parser;
use Text::Iconv;
use URI;
use Encode;
use URI::Escape;

# used by parsers
use XML::Feed;

sub new {
    my $class = shift;
    my $crawler = shift;
    my $config = shift;
    $config->{url_tail} = '' unless defined $config->{url_tail};
    my $self = {crawler => $crawler, is_new => 0, retrycount => 0};
    foreach my $k (keys %$config) {
	$self->{$k} = $config->{$k};
    }
    my ($start_hook, $extra_url) = ($config->{start_hook}, $config->{extra_url});
    if ($start_hook =~ m<^#.*?\breplace:\s*(\S+)\s+(\S+)>m) {
	($self->{orig}, $self->{replace}) = ($1, $2);
    }
    if ($start_hook =~ m<^#.*?\bextra_tags:\s*(.+?)\s*$>m) {
	@{$self->{extra_tags}} = split ' ', $1;
    }
    if ($start_hook =~ m<^#.*\brss:\s*>m) {
	$self->{rss} = 1;
	$self->{static_url} = 1;
    } elsif ($start_hook =~ m<^#.*?\burl:\s*(\S+)>m) {
	$self->{url} = defined $extra_url ? $extra_url : $1;
	$self->{allow_file} = 1;
	$self->{static_url} = 1;
    }
    $self->{nocontent} = $start_hook =~ m<^\#\s*nocontent\b>m ? 1 : 0;
    $self->{ignoreerrorstatus} = $start_hook =~ m<^\#\s*\bignoreerrorstatus\b>m ? 1 : 0;
    $self->{noredir} = 0;
    bless $self, $class;
    return $self;
}

sub set_url {
    my ($self, $main) = @_;
    $self->{url_main} = $main;
    if (exists $self->{static_url} && $self->{static_url}) {
	return;
    }
    $self->{url} = $self->{url_base}.$main.$self->{url_tail};
    if (exists $self->{replace}) {
	$self->{url} =~ s/$self->{orig}/$self->{replace}/;
    }
}

sub extract_next {
    local ($param, $response) = @_;
    local ($url_base, $url, $text_hook, $start_hook, $extra_data) = ($param->{url_base}, $param->{url}, $param->{text_hook}, $param->{start_hook}, $param->{extra_data});
    local $content;

    # curl already does decompression, remove the header to prevent
    # HTTP::Message from trying it in decoded_content call.
    $response->remove_header('content-encoding');

    $content = $response->decoded_content;
    "" =~ //;

    local $next = undef;
    local $at_fixed_head = 0;
    local $faulty = 0;
    local @fragments;
    local $retrywith;
    my @report_tags = qw(a h2 img area map body link select option input image li param div span meta frame td button);
    if (defined $param->{extra_tags}) {
	push @report_tags, @{$param->{extra_tags}};
    }
    my %options = (api_version => 3, report_tags => \@report_tags);

    if (defined $param->{response_handler}) {
	my $handler = $param->{response_handler};
	delete $param->{response_handler};
	$content = undef;
	# We use "local" to get dynamically scoped variables for this function call.
	&$handler($response->header('Location'));
    } elsif (defined $param->{rss}) {
	my $feed = XML::Feed->parse(\$content);
	eval $start_hook;
	$content = undef;
    } elsif (defined $param->{force_plain} || ($param->{retrycount} == 0 && $start_hook =~ m<^\#\s*plain\b>)) {
	eval $start_hook;
	$content = undef;
    } else {
	if ($param->{nocontent}) {
	    $content = undef;
	    $param->{nocontent} = 0;
	    eval $start_hook;
	}
	$options{start_h} =
	    [ sub { my ($self, $tag, $attr) = @_;
		    $self->{last_tag} = $tag;
		    eval $start_hook;
		    if ($tag eq 'a' && exists $attr->{href}) {
			$self->{last_href}=$attr->{href};
		    }
	      },
	      'self,tagname,attr' ];
    }
    if (defined $text_hook) {
	$options{text_h} =
	    [ sub {
		my ($self, $text) = @_;
		eval $text_hook;
	      },
	      'self,text' ];
    }
    if (defined $content) {
	if ($content eq '') {
	    $faulty = 1;
	} else {
	    my $p = HTML::Parser->new(%options);
	    $p->parse($content);
	}
    }
    if (defined $retrywith) {
	$param->{crawler}->add($param, $retrywith);
    }

    undef $next if defined $next && $next eq '';
    if (defined $next) {
	# Unfortunately this amounts to a guess
	my $charset = 'utf8';
	if (ref $next eq 'ARRAY') {
	    foreach my $n (@$next) {
		$n = Encode::decode($charset, uri_unescape($n));
	    }
	} else {
	    $next = Encode::decode($charset, uri_unescape($next));
	}
    }
    if (defined $next) {
	$param->{retrycount} = 0;
	delete $param->{force_plain};
    }
    $param->{retrycount} = 0 if defined $next;
    $param->{at_fixed_head} = $at_fixed_head;

    return ($next, $faulty, \@fragments);
}

sub redirects_to {
    my $self = shift;
    my ($redir, $func, $from) = @_;
    $from = $self->{url} unless defined $from;
    $param->{response_handler} = $func;
    $redir = HEAD($redir);
    $redir->referer($from);
    $self->{noredir} = 1;
    $self->{manual} = 1;
    $self->{crawler}->add($self, $redir, 1);
}

sub retrywith {
    my $self = shift;
    $self->{manual} = 1;
    $self->{crawler}->add(@_);
}

1;
