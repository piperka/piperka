package Piperka::Control;

use HTTP::Request;
use JSON;

use Piperka::Parser;

# Receive requests from the caller process and wrap responses from the
# parser as instructions.

# This class is passed to crawler to stand in for crawler.

sub new {
    my $class = shift;
    my $self = {};
    bless $self, $class;
    return $self;
}

# Parser wants to fetch another page.
sub add {
    my ($self, $param, $request) = @_;
    ++$param->{retrycount};
    my $payload = {type => 'fetch'};
    $payload->{auto} = defined $param->{manual} ? \0 : \1;
    delete $param->{manual};
    if (ref $request eq '') {
	$payload->{next_url} = $request;
    } else {
	my @headers;
	if (defined $request->referer) {
	    push @headers, ['Referer', $request->referer];
	}
	for my $hdr ($request->header_field_names) {
	    push @headers, [$hdr, $request->header($hdr).''];
	}
	$payload->{next_url} = $request->uri->as_string;
	$payload->{extra_headers} = \@headers;
	$payload->{no_content} = $request->method eq 'HEAD' ? \1 : \0;
	if ($request->method eq 'POST') {
	    $payload->{post_params} = $request->content;
	}
    }

    my $buf = encode_json($payload);
    print STDOUT pack('l', length($buf));
    print STDOUT $buf;
    $self->{have_add} = 1;
}

# Read config from stdin
sub receive_config {
    my $buf;
    read STDIN, $buf, 4;
    my $len = unpack("l", $buf);
    my $i = 0;
    while ($i < $len) {
	$i += read STDIN, $buf, $len-$i, $i;
    }
    my $cfg = decode_json $buf;
    my $control = Piperka::Control->new;
    $control->{parser} = Piperka::Parser->new($control, $cfg);
    return $control;
}

sub receive_page {
    my $self = shift;
    my $buf;
    my $read_len = read STDIN, $buf, 4;
    if (!defined $read_len || $read_len < 4) {
	die "bad read: $!\n";
    }
    my $len = unpack("l", $buf);
    my $i = 0;
    while ($i < $len) {
	$i += read STDIN, $buf, $len-$i, $i;
    }
    return $buf;
}

# Receive response from stdin
sub receive_response {
    my $self = shift;
    my $buf;
    my $read_len = read STDIN, $buf, 4;
    if (!defined $read_len || $read_len < 4) {
	die "bad read: $!\n";
    }
    my $len = unpack("l", $buf);
    my $i = 0;
    while ($i < $len) {
	$i += read STDIN, $buf, $len-$i, $i;
    }
    return HTTP::Response->parse($buf);
}

sub use_parser {
    my ($self, $url_main, $resp) = @_;
    my $param = $self->{parser};
    $param->set_url($url_main);
    # Is the HTTP response ok for parse
    if ($resp->is_error() && !$param->{nocontent} && !$param->{ignoreerrorstatus}) {
	return $self->result_response_error($resp->code, $resp->message)
    }
    # Is it a redirect and the parser isn't requesting handling it on
    # its own
    if ($resp->is_redirect() && !$param->{noredir} && defined $resp->header('Location')) {
	my $request = new HTTP::Request($param->{nocontent} ? 'HEAD' : 'GET',
					$resp->header('Location'));
	$self->add($param, $request);
	return 0;
    }

    $self->{have_add} = 0;
    my ($next, $faulty, $fragments) =
	$param->extract_next($resp);
    if ($self->{have_add}) {
	return 1;
    }

    if ($faulty) {
	return $self->result_faulty;
    }

    $param->{noredir} = 0;
    return $self->result_ok($next, $fragments);
}

sub result_faulty {
    my $self = shift;
    my $buf = encode_json({type => 'faulty'});
    print STDOUT pack('l', length($buf));
    print STDOUT $buf;
    return 0;
}

sub result_response_error {
    my $self = shift;
    my ($code, $message) = @_;
    my $buf = encode_json({type => 'error', code => (0+$code), message => $message});
    print STDOUT pack('l', length($buf));
    print STDOUT $buf;
    return 0;
}

sub result_ok {
    my $self = shift;
    my ($next, $fragments) = @_;
    my $buf;
    my $next_page = 1;
    if (exists $self->{parser}->{at_fixed_head} && $self->{parser}->{at_fixed_head}) {
	$fragments = [] unless defined $fragments;
	$buf = encode_json({type => 'at_fixed_head', fragments => $fragments});
	$next_page = 0;
    } elsif (exists $self->{parser}->{alternatives}) {
	$buf = encode_json({type => 'alternatives', pages => $self->{parser}->{alternatives}});
	delete $self->{parser}->{alternatives};
    } elsif (!defined $next) {
	$fragments = [] unless defined $fragments;
	$buf = encode_json({type => 'none', fragments => $fragments});
	$next_page = 0;
    } elsif (ref $next eq 'ARRAY') {
	$buf = encode_json({type => 'multi', pages => $next});
    } else {
	$fragments = [] unless defined $fragments;
	$buf = encode_json({type => 'result', 'next' => $next, fragments => $fragments});
    }
    print STDOUT pack('l', length($buf));
    print STDOUT $buf;
    return $next_page;
}

sub receive_id {
    my $buf;
    my $len = read STDIN, $buf, 4;
    if (!defined $len || $len < 4) {
	die "error reading id: $!\n";
    }
    return unpack("l", $buf);
}

sub run_loop {
    $| = 1;
    my %ctrl = ();
    my $cmd;
    while (1) {
	my $len = read STDIN, $cmd, 1;
	if (!defined $len) {
	    die "error reading handle: $!\n";
	} elsif ($len == 0) {
	    exit 0;
	}
	my $id = receive_id;
	if ($cmd eq 'i') {
	    $ctrl{$id} = receive_config;
	} elsif ($cmd eq 'p') {
	    my $page = $ctrl{$id}->receive_page;
	    my $response = $ctrl{$id}->receive_response;
	    $ctrl{$id}->use_parser($page, $response);
	} elsif ($cmd eq 'c') {
	    delete $ctrl{$id};
	}
    }
}

1;
