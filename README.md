Installation instructions for Piperka backend
=============================================

Update 2024-10-06
-----------------

The instructions below are (at least partially) outdated and I'll need
to update them later, but this is the short of the current state:

Set up a chroot environment with `debootstrap bullseye`.  Piperka's
backend is still on GHC 8.8 but the rest of the system is on Bookworm.
Bookworm has GHC 9.0 and I'll be skipping on that one and will jump
straight to what Trixie has, after it releases.  Use `systemd-nspawn`
to run a container in it and set up a regular user in it
(recommendation for convenience: match your main system's UID for it).
Do `apt install sudo && adduser YOURUSER sudo` in it and check out the
repository.

```
touch lib/pg_*
make init
sudo -u postgres pg_restore schema.sql piperka
cabal repl piperka-snap
````

It's a rather custom setup and if someone feels like writing a docker
option or something I'm not against including it but I'm unlikely to
look into it myself.  All of this requires running Linux.

Old instructions
----------------

The backend has a dependency on the crawler, please follow its
installation instructions first.

The default development configuration uses dev.piperka.net as its
hostname. The login cookies are set as secure and you will need to set
up a reverse HTTPS proxy. Piperka expects forwarded requests to have
`X-Forwarded-For` (IP address) and `X-Forwarded-Proto` headers set.
Add the hostname to `/etc/hosts` and set it to point to localhost. The
backend server runs on port 8000.

Here's an example nginx configuration:
```
server {
	listen 80;
	listen [::]:80;
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	# SSL needs a cert, at least Debian makes this available
	include snippets/snakeoil.conf;

	server_name dev.piperka.net;

	location / {
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_pass http://127.0.0.1:8000/piperka/;
	}
}
```

The server uses a bit of XSLT for its setup.

```
sudo apt install xsltproc
```


Database
--------

```
sudo apt install postgresql-plperl postgresql-contrib
```

Piperka uses "piperka" as its user. To enable logins as that user for
yourself, add something like this to `pg_ident.conf`: `piperka
youruser piperka` and edit `pg_hba.conf` as follows: `local piperka
all peer map=piperka` and restart postgresql.

Download the schema from https://gitlab.com/piperka/schema and run the
following commands as postgres superuser.

```
CREATE ROLE kaol WITH LOGIN;
CREATE ROLE piperka WITH LOGIN;
GRANT piperka TO kaol;
CREATE DATABASE piperka WITH OWNER piperka;
GRANT ALL ON DATABASE piperka TO kaol;
\c piperka
CREATE LANGUAGE plperl;
CREATE EXTENSION plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION pgcrypto WITH SCHEMA public;
CREATE EXTENSION "uuid-ossp" WITH SCHEMA public;
```

Install the schema with (as postgres superuser)

```
psql piperka postgres < piperka-schema.sql
```

Configuration files
-------------------

The server requires some configuration files. The source tree includes
example configuration, copy them as follows.

```
cp devel.cfg.example devel.cfg
cp snaplets/auth/devel.cfg.example snaplets/auth/devel.cfg
cp snaplets/apiAuth/devel.cfg.example snaplets/apiAuth/devel.cfg
```

Congratulations, this should be enough to start the site. The site
should be fine to run with `cabal repl` and then `main`. The crawler
page may experience hiccups when run that way due to websockets use.

Moderator role
--------------

To start with, create a user account.

Some functionality may be restricted to user with uid 3 (which is what
I use on the real site). You may want to adjust your admin uid to be
the same. Run an update to the uid in the `login_method` table if you
change the uid after account creation.

You'll also need to run `insert into moderator (uid) values (3)`.

Crawler
-------

You'll need a parser configuration in the `parsers` table to use
it. Use `insert into parsers (start_hook) values ('')` to insert this
example.

```perl
if ($tag eq "a" && exists $attr->{rel} && $attr->{rel} eq "next") {
    my $url_tail = quotemeta($param->{url_tail});
    my $base = quotemeta($url_base);
    my $full_uri = URI->new_abs($attr->{href}, $url);
    if ($full_uri =~ m<^$base(.+)$url_tail$>) {
        $next = $1;
    }
    $self->eof;
}
```

Install perl modules in `../crawler/lib/crawler` to site_perl or set
`PERL5LIB`.

Then head to mod_crawler.html. To see that it works, set `parser_type`
as 1 and use `https://piperka.net/blog/` as the `url_base`, `/` as
`url_tail` and insert `2011/hello-world` as the first page. Click
start and it should crawl all the blog pages (I don't mind).

Moderator interface
-------------------

Here's a list of pages relevant to maintaining the site:

* moderate.html
* submissions.html
* mod_ticket.html

The usual way to navigate to the crawler page is from the submissions
or the mod_ticket pages. The easiest way to add comics is to submit
them from the usual submit page first and by opening the crawler from
the submissions page. After saving the initial crawl, click on the
genentry link.

Recommendations
---------------

```
sudo apt install libblas-dev liblapack-dev
```
